<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Trans_bmn_model extends CI_Model
{
    public $table = 'tbl_trans_bmn';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // datatables
    public function json($nip)
    {
        $this->datatables->select('id,id_user,id_bmn,nama_bmn,tgl_pinjam,tgl_kembali,keperluan,lokasi,tbl_trans_bmn.keterangan,tgl_penggunaan,status,nama');
        $this->datatables->from('tbl_trans_bmn');
        //add this line for join
        $this->datatables->join('master_biodata', 'tbl_trans_bmn.id_user = master_biodata.nip');
        $this->datatables->where("tbl_trans_bmn.id_user = $nip"); //menampilkan milik sendiri
        $this->datatables->add_column('action', anchor(site_url('trans_bmn/read/$1'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail', 'style' => 'margin:1px')) . ' 
            ' . anchor(site_url('trans_bmn/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Edit', 'style' => 'margin:1px')) . ' 
            ' . anchor(site_url('trans_bmn/pembatalan/$1'), '<i class="fa fa-stop" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Batal', 'style' => 'margin:1px')) . '
                ' . anchor(site_url('trans_bmn/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" style = "margin:1px" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');

        return $this->datatables->generate();
    }

    public function json_persetujuan($nip)
    {
        $this->datatables->select('id,id_user,id_bmn,nama_bmn,tgl_pinjam,tgl_kembali,keperluan,lokasi,tbl_trans_bmn.keterangan,tgl_penggunaan,status,nama, id_atasan, id_krt');
        $this->datatables->from('tbl_trans_bmn');
        //add this line for join
        $this->datatables->join('master_biodata', 'tbl_trans_bmn.id_user = master_biodata.nip');
        $this->datatables->where("id_atasan = $nip AND (status != 'DIBUAT' AND status != 'DIBATALKAN')");
        $this->datatables->or_where("id_krt = $nip AND (status != 'DIBUAT' AND status != 'AJUKAN_1' AND status != 'DIBATALKAN')");

        $this->datatables->add_column('action', anchor(site_url('trans_bmn/read/$1/persetujuan'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail', 'style' => 'margin:1px')) . ' '
            //.anchor(site_url('trans_bmn/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm','title' => 'Detail')).' '
            //.anchor(site_url('trans_bmn/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" onclick="javasciprt: return confirm(\'Are You Sure ?\')"')
            , 'id');

        return $this->datatables->generate();
    }

    // TODO : menampilkan riwayat seluruhnya
    // TODO : menampilkan riwayat approval yang telah diproses

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    // get total rows
    public function total_rows($q = null)
    {
        $this->db->like('id', $q);
        $this->db->or_like('id_user', $q);
        $this->db->or_like('id_bmn', $q);
        $this->db->or_like('nama_bmn', $q);
        $this->db->or_like('tgl_pinjam', $q);
        $this->db->or_like('tgl_kembali', $q);
        $this->db->or_like('keperluan', $q);
        $this->db->or_like('lokasi', $q);
        $this->db->or_like('keterangan', $q);
        $this->db->or_like('tgl_penggunaan', $q);
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = null)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('id_user', $q);
        $this->db->or_like('id_bmn', $q);
        $this->db->or_like('nama_bmn', $q);
        $this->db->or_like('tgl_pinjam', $q);
        $this->db->or_like('tgl_kembali', $q);
        $this->db->or_like('keperluan', $q);
        $this->db->or_like('lokasi', $q);
        $this->db->or_like('keterangan', $q);
        $this->db->or_like('tgl_penggunaan', $q);
        $this->db->limit($limit, $start);

        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    public function checkKetersediaan($id_rr, $tgl_mulai, $tgl_akhir)
    {
        $sql = "SELECT * FROM tbl_trans_bmn 
                WHERE (`status` = 'DISETUJUI') AND (`id_bmn` = $id_rr) AND 
                ((tgl_pinjam > '$tgl_mulai' AND tgl_pinjam < '$tgl_akhir') OR
                (tgl_kembali > '$tgl_mulai' AND tgl_kembali < '$tgl_akhir'))";
        $query = $this->db->query($sql);
        // ada jam yang sama
        if ($this->db->count_all_results()) {
            //balikin datanya
            return $query->result();
        }
        // tidak ada jam yang sama
        return false;
    }

    public function getDataAroundToday($botd, $eotd)
    {
        $sql = "SELECT tbl_trans_bmn.*, master_biodata.nama FROM tbl_trans_bmn JOIN master_biodata ON tbl_trans_bmn.id_user = master_biodata.nip WHERE tgl_pinjam <= '$botd' AND tgl_kembali >= '$eotd' AND status = 'DISETUJUI' ORDER BY tgl_pinjam ASC LIMIT 15";
        $query = $this->db->query($sql);
        if ($this->db->count_all_results()) {
            //balikin datanya
            return $query->result();
        }
        // tidak datanya
        return array();
    }
}

/* End of file Trans_bmn_model.php */
/* Location: ./application/models/Trans_bmn_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-10 20:24:40 */
/* http://harviacode.com */
