<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Perjanjian_bmn_model extends CI_Model
{

    public $table = 'tbl_perjanjian_bmn';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // datatables
    public function json($nip)
    {
        $this->datatables->select('a.id, a.is_approved, b.nama_bmn , c.nama as pejabat, d.nama as pemohon, b.id_user, b.id_atasan, b.id_krt');
        $this->datatables->from('tbl_perjanjian_bmn a');
        $this->datatables->join('tbl_trans_bmn b', 'a.id_trans_bmn = b.id');
        $this->datatables->join('master_biodata c', 'a.id_pejabat = c.nip');
        $this->datatables->join('master_biodata d', 'b.id_user = d.nip');
        $this->datatables->where("id_pejabat = $nip");
        $this->datatables->or_where("id_krt = $nip");
        $this->datatables->or_where("id_atasan = $nip");
        $this->datatables->or_where("id_user = $nip");
        // $this->datatables->add_column('action', anchor(site_url('perjanjian_bmn/read/$1'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail')) . "
        // //     " . anchor(site_url('perjanjian_bmn/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail')) . "
        // //         " . anchor(site_url('perjanjian_bmn/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');

        $this->datatables->add_column('action', anchor(site_url('perjanjian_bmn/read/$1'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail')), 'id');
        return $this->datatables->generate();
    }

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    public function get_by_id_trans($id)
    {
        $this->db->where('id_trans_bmn', $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    public function total_rows($q = null)
    {
        $this->db->like('id', $q);
        $this->db->or_like('id_trans_bmn', $q);
        $this->db->or_like('id_pejabat', $q);
        $this->db->or_like('is_approved', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = null)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('id_trans_bmn', $q);
        $this->db->or_like('id_pejabat', $q);
        $this->db->or_like('is_approved', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }


    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Perjanjian_bmn_model.php */
/* Location: ./application/models/Perjanjian_bmn_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-17 16:33:38 */
/* http://harviacode.com */
