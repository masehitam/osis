<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bmn_model extends CI_Model
{

    public $table = 'tbl_bmn';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json()
    {
        $this->datatables->select('id,nama,kode,tahun,stok');
        $this->datatables->from('tbl_bmn');
        //add this line for join
        //$this->datatables->join('table2', 'tbl_bmn.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('bmn/read/$1'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail')) . " 
            " . anchor(site_url('bmn/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Edit')) . " 
                " . anchor(site_url('bmn/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('id', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('kode', $q);
        $this->db->or_like('tahun', $q);
        $this->db->or_like('stok', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('kode', $q);
        $this->db->or_like('tahun', $q);
        $this->db->or_like('stok', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    public function search_pegawai($nama)
    {
        $this->db->like('nama', $nama, 'both');
        $this->db->order_by('nama', 'ASC');
        $this->db->limit(10);

        return $this->db->get('tbl_bmn')->result();
    }

}

/* End of file Bmn_model.php */
/* Location: ./application/models/Bmn_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-10 15:08:57 */
/* http://harviacode.com */