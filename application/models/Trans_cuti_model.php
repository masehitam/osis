<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Trans_cuti_model extends CI_Model
{
    public $table = 'tbl_trans_cuti';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // datatables
    public function json($nip)
    {
        $this->datatables->select('id,tbl_trans_cuti.nip,jenis_cuti,tgl_mulai,tgl_selesai,alamat,nip_atasan,nip_pejabat,alasan,status,wkt_cuti,date_created,date_diajukan,date_reaksi_satu,date_reaksi_dua, nama');
        $this->datatables->from('tbl_trans_cuti');
        //add this line for join
        $this->datatables->join('master_biodata', 'tbl_trans_cuti.nip = master_biodata.nip');
        $this->datatables->where("tbl_trans_cuti.nip = $nip"); //menampilkan milik sendiri
        $this->datatables->add_column('action', anchor(site_url('trans_cuti/read/$1'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail', 'style' => 'margin:1px')) . '
            ' . anchor(site_url('trans_cuti/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Edit', 'style' => 'margin:1px')) . '
            ' . anchor(site_url('trans_cuti/pembatalan/$1'), '<i class="fa fa-stop" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Batal', 'style' => 'margin:1px')) . '
                ' . anchor(site_url('trans_cuti/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" style = "margin:1px" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');

        return $this->datatables->generate();
    }

    public function json_persetujuan($nip)
    {
        $this->datatables->select('id,tbl_trans_cuti.nip,jenis_cuti,tgl_mulai,tgl_selesai,alamat,nip_atasan,nip_pejabat,alasan,status,wkt_cuti,date_created,date_diajukan,date_reaksi_satu,date_reaksi_dua, nama');
        $this->datatables->from('tbl_trans_cuti');
        //add this line for join
        $this->datatables->join('master_biodata', 'tbl_trans_cuti.nip = master_biodata.nip');
        $this->datatables->where("nip_atasan = $nip AND (status != 'DIBUAT' AND status != 'DIBATALKAN')");
        $this->datatables->or_where("nip_pejabat = $nip AND (status != 'DIBUAT' AND status != 'DIBATALKAN' AND status != 'PERLU_VALIDASI')");
        $this->datatables->add_column('action', anchor(site_url('trans_cuti/read/$1/persetujuan'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail', 'style' => 'margin:1px')) . ' '
            //.anchor(site_url('trans_cuti/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm','title' => 'Detail')).' '
            //.anchor(site_url('trans_cuti/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" onclick="javasciprt: return confirm(\'Are You Sure ?\')"')
            , 'id');

        return $this->datatables->generate();
    }

    public function json_validasi()
    {
        $this->datatables->select('id,tbl_trans_cuti.nip,jenis_cuti,tgl_mulai,tgl_selesai,alamat,nip_atasan,nip_pejabat,alasan,status,wkt_cuti,date_created,date_diajukan,date_reaksi_satu,date_reaksi_dua, nama');
        $this->datatables->from('tbl_trans_cuti');
        $this->datatables->join('master_biodata', 'tbl_trans_cuti.nip = master_biodata.nip');
        $this->datatables->where("status != 'DIBUAT' AND status != 'DIBATALKAN'");
        //$this->db->order_by("status DESC, date_created DESC;");
        $this->datatables->add_column('action', anchor(site_url('trans_cuti/read/$1/validasi'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail', 'style' => 'margin:1px')) . ' '
            //.anchor(site_url('trans_cuti/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm','title' => 'Detail')).' '
            //.anchor(site_url('trans_cuti/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" onclick="javasciprt: return confirm(\'Are You Sure ?\')"')
            , 'id');

        return $this->datatables->generate();
    }

    // TODO : menampilkan riwayat seluruhnya
    // TODO : menampilkan riwayat approval yang telah diproses

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    // get total rows
    public function total_rows($q = null)
    {
        $this->db->like('id', $q);
        $this->db->or_like('nip', $q);
        $this->db->or_like('jenis_cuti', $q);
        $this->db->or_like('tgl_mulai', $q);
        $this->db->or_like('tgl_selesai', $q);
        $this->db->or_like('alamat', $q);
        $this->db->or_like('nip_atasan', $q);
        $this->db->or_like('nip_pejabat', $q);
        $this->db->or_like('alasan', $q);
        $this->db->or_like('status', $q);
        $this->db->or_like('wkt_cuti', $q);
        $this->db->or_like('date_created', $q);
        $this->db->or_like('date_diajukan', $q);
        $this->db->or_like('date_reaksi_satu', $q);
        $this->db->or_like('date_reaksi_dua', $q);
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = null)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('nip', $q);
        $this->db->or_like('jenis_cuti', $q);
        $this->db->or_like('tgl_mulai', $q);
        $this->db->or_like('tgl_selesai', $q);
        $this->db->or_like('alamat', $q);
        $this->db->or_like('nip_atasan', $q);
        $this->db->or_like('nip_pejabat', $q);
        $this->db->or_like('alasan', $q);
        $this->db->or_like('status', $q);
        $this->db->or_like('wkt_cuti', $q);
        $this->db->or_like('date_created', $q);
        $this->db->or_like('date_diajukan', $q);
        $this->db->or_like('date_reaksi_satu', $q);
        $this->db->or_like('date_reaksi_dua', $q);
        $this->db->limit($limit, $start);

        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    public function hitung_cuti($nip, $tahun)
    {
        $sql = "SELECT SUM(jumlah_hari) AS jumlah_hari FROM tbl_trans_cuti WHERE nip = '$nip' AND STATUS = 'DISETUJUI' AND YEAR(date_created) = '$tahun'";
        //print_r($sql);
        $result = $this->db->query($sql);
        if ($result->row()->jumlah_hari != null) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function getDataAroundToday($botd, $eotd)
    {
        $sql = "SELECT tbl_trans_cuti.*, master_biodata.nama FROM tbl_trans_cuti JOIN master_biodata ON tbl_trans_cuti.nip = master_biodata.nip WHERE tgl_mulai <= '$botd' AND tgl_selesai >= '$eotd' AND status = 'DISETUJUI' ORDER BY tgl_mulai ASC LIMIT 15";
        $query = $this->db->query($sql);
        if ($this->db->count_all_results()) {
            //balikin datanya
            return $query->result();
        }
        // tidak datanya
        return array();
    }
}

/* End of file Trans_cuti_model.php */
/* Location: ./application/models/Trans_cuti_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-04-09 11:39:56 */
/* http://harviacode.com */
