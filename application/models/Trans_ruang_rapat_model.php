<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Trans_ruang_rapat_model extends CI_Model
{
    public $table = 'tbl_trans_ruang_rapat';
    public $id = 'id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // datatables
    public function json($nip)
    {
        $this->datatables->select('id,kegiatan,id_pegguna,tgl_mulai,tgl_akhir,status,wkt_kegiatan,nama');
        $this->datatables->from('tbl_trans_ruang_rapat');
        //add this line for join
        $this->datatables->join('master_biodata', 'tbl_trans_ruang_rapat.id_pegguna = master_biodata.nip');
        $this->datatables->where("tbl_trans_ruang_rapat.id_pegguna = $nip"); // menampilkan milik sendiri
        $this->datatables->add_column('action', anchor(site_url('trans_ruang_rapat/read/$1'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail', 'style' => 'margin:1px')) . ' 
            ' . anchor(site_url('trans_ruang_rapat/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Edit', 'style' => 'margin:1px')) . ' 
            ' . anchor(site_url('trans_ruang_rapat/pembatalan/$1'), '<i class="fa fa-stop" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Batal', 'style' => 'margin:1px')) . ' 
                ' . anchor(site_url('trans_ruang_rapat/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" style = "margin:1px" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');

        return $this->datatables->generate();
    }

    public function json_persetujuan($nip)
    {
        $this->datatables->select('id,kegiatan,id_pegguna,tgl_mulai,tgl_akhir,status,wkt_kegiatan,nama');
        $this->datatables->from('tbl_trans_ruang_rapat');
        //add this line for join
        $this->datatables->join('master_biodata', 'tbl_trans_ruang_rapat.id_pegguna = master_biodata.nip');
        $this->datatables->where("id_atasan = $nip AND (status != 'DIBUAT' AND status != 'DIBATALKAN')");
        $this->datatables->or_where("id_krt = $nip AND (status != 'DIBUAT' AND status != 'AJUKAN_1' AND status != 'DIBATALKAN')");
        $this->datatables->add_column('action',
            anchor(site_url('trans_ruang_rapat/read/$1/persetujuan'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail')) . ' '
            //.anchor(site_url('trans_ruang_rapat/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm','title' => 'Detail')).' '
            //.anchor(site_url('trans_ruang_rapat/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" onclick="javasciprt: return confirm(\'Are You Sure ?\')"')
            , 'id');

        return $this->datatables->generate();
    }

    // TODO : menampilkan riwayat seluruhnya
    // TODO : menampilkan riwayat approval yang telah diproses

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    // get total rows
    public function total_rows($q = null)
    {
        $this->db->like('id', $q);
        $this->db->or_like('kegiatan', $q);
        $this->db->or_like('id_pegguna', $q);
        $this->db->or_like('tgl_mulai', $q);
        $this->db->or_like('tgl_akhir', $q);
        $this->db->or_like('status', $q);
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = null)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('kegiatan', $q);
        $this->db->or_like('id_pegguna', $q);
        $this->db->or_like('tgl_mulai', $q);
        $this->db->or_like('tgl_akhir', $q);
        $this->db->or_like('status', $q);
        $this->db->limit($limit, $start);

        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    public function checkKetersediaan($id_rr, $tgl_mulai, $tgl_akhir)
    {
        // tgl_mulai < '8' && tgl_akhir > '8' OR
        // tgl_mulai < '11' && tgl_akhir > '11';
        $sql = "SELECT * FROM tbl_trans_ruang_rapat 
                WHERE (`status` = 'DISETUJUI') AND (`id_ruang_rapat` = $id_rr) AND 
                ((tgl_mulai > '$tgl_mulai' AND tgl_mulai < '$tgl_akhir') OR
                (tgl_akhir > '$tgl_mulai' AND tgl_akhir < '$tgl_akhir'))";
        $query = $this->db->query($sql);
        // ada jam yang sama
        if ($this->db->count_all_results()) {
            //balikin datanya
            return $query->result();
        }
        // tidak ada jam yang sama
        return false;
    }

    public function getDataAroundToday($botd, $eotd)
    {
        $sql = "SELECT tbl_trans_ruang_rapat.*, tbl_ruang_rapat.ruangan as nama FROM tbl_trans_ruang_rapat JOIN tbl_ruang_rapat ON tbl_trans_ruang_rapat.id_ruang_rapat = tbl_ruang_rapat.id WHERE tgl_mulai >= '$botd' AND tgl_mulai <= '$eotd' AND status = 'DISETUJUI' ORDER BY tgl_mulai ASC LIMIT 15";
        $query = $this->db->query($sql);
        if ($this->db->count_all_results()) {
            //balikin datanya
            return $query->result();
        }
        // tidak datanya
        return array();
    }
}

/* End of file Trans_ruang_rapat_model.php */
/* Location: ./application/models/Trans_ruang_rapat_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-10 15:09:35 */
/* http://harviacode.com */
