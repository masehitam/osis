<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master_biodata_model extends CI_Model
{
    public $table = 'master_biodata';
    public $id = 'master_biodata_id';
    public $order = 'DESC';

    public function __construct()
    {
        parent::__construct();
    }

    // datatables
    public function json()
    {
        $this->datatables->select('master_biodata_id,bkn_id,nip,nama_cetak,nama,jenis_jab_id,struk_id,fung_id,unit_id_es1,unit_id_es2,unit_id_es3,unit_id_es4,unit_staf_id,statpeg_id,aktif_id,jenis_peg_id,jenjang_id,gol_id,no_karpeg,absensi_id,jeniskel_id,tmp_lahir,tgl_lahir,agama_id,kawin_id,alamat_ktp,kode_pos_ktp,alamat_tinggal,kode_pos_tinggal,telp_rumah,telp_cellular,no_ext_kantor,email_kantor,email_pribadi,gol_darah,no_bpjs,ktp_id,npwp,bank_id,cabang_bank,no_rek,atas_nama,photo,keterangan,dihapus,ktp_fileup,npwp_fileup,kartukeluarga_fileup,karpeg_fileup,editdate,userlog');
        $this->datatables->from('master_biodata');
        //add this line for join
        //$this->datatables->join('table2', 'master_biodata.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('master_biodata/read/$1'), '<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Detail')) . ' 
            ' . anchor(site_url('master_biodata/update/$1'), '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm', 'title' => 'Edit')) . ' 
                ' . anchor(site_url('master_biodata/delete/$1'), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'class="btn btn-danger btn-sm" title="Hapus" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'master_biodata_id');

        return $this->datatables->generate();
    }

    // get all
    public function get_all()
    {
        $this->db->order_by($this->id, $this->order);

        return $this->db->get($this->table)->result();
    }

    // get data by id
    public function get_by_id($id)
    {
        $this->db->where($this->id, $id);

        return $this->db->get($this->table)->row();
    }

    public function get_by_nip($id)
    {
        $this->db->where('nip', $id);

        return $this->db->get($this->table)->row();
    }

    // get total rows
    public function total_rows($q = null)
    {
        $this->db->like('master_biodata_id', $q);
        $this->db->or_like('bkn_id', $q);
        $this->db->or_like('nip', $q);
        $this->db->or_like('nama_cetak', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('jenis_jab_id', $q);
        $this->db->or_like('struk_id', $q);
        $this->db->or_like('fung_id', $q);
        $this->db->or_like('unit_id_es1', $q);
        $this->db->or_like('unit_id_es2', $q);
        $this->db->or_like('unit_id_es3', $q);
        $this->db->or_like('unit_id_es4', $q);
        $this->db->or_like('unit_staf_id', $q);
        $this->db->or_like('statpeg_id', $q);
        $this->db->or_like('aktif_id', $q);
        $this->db->or_like('jenis_peg_id', $q);
        $this->db->or_like('jenjang_id', $q);
        $this->db->or_like('gol_id', $q);
        $this->db->or_like('no_karpeg', $q);
        $this->db->or_like('absensi_id', $q);
        $this->db->or_like('jeniskel_id', $q);
        $this->db->or_like('tmp_lahir', $q);
        $this->db->or_like('tgl_lahir', $q);
        $this->db->or_like('agama_id', $q);
        $this->db->or_like('kawin_id', $q);
        $this->db->or_like('alamat_ktp', $q);
        $this->db->or_like('kode_pos_ktp', $q);
        $this->db->or_like('alamat_tinggal', $q);
        $this->db->or_like('kode_pos_tinggal', $q);
        $this->db->or_like('telp_rumah', $q);
        $this->db->or_like('telp_cellular', $q);
        $this->db->or_like('no_ext_kantor', $q);
        $this->db->or_like('email_kantor', $q);
        $this->db->or_like('email_pribadi', $q);
        $this->db->or_like('gol_darah', $q);
        $this->db->or_like('no_bpjs', $q);
        $this->db->or_like('ktp_id', $q);
        $this->db->or_like('npwp', $q);
        $this->db->or_like('bank_id', $q);
        $this->db->or_like('cabang_bank', $q);
        $this->db->or_like('no_rek', $q);
        $this->db->or_like('atas_nama', $q);
        $this->db->or_like('photo', $q);
        $this->db->or_like('keterangan', $q);
        $this->db->or_like('dihapus', $q);
        $this->db->or_like('ktp_fileup', $q);
        $this->db->or_like('npwp_fileup', $q);
        $this->db->or_like('kartukeluarga_fileup', $q);
        $this->db->or_like('karpeg_fileup', $q);
        $this->db->or_like('editdate', $q);
        $this->db->or_like('userlog', $q);
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    // get data with limit and search
    public function get_limit_data($limit, $start = 0, $q = null)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('master_biodata_id', $q);
        $this->db->or_like('bkn_id', $q);
        $this->db->or_like('nip', $q);
        $this->db->or_like('nama_cetak', $q);
        $this->db->or_like('nama', $q);
        $this->db->or_like('jenis_jab_id', $q);
        $this->db->or_like('struk_id', $q);
        $this->db->or_like('fung_id', $q);
        $this->db->or_like('unit_id_es1', $q);
        $this->db->or_like('unit_id_es2', $q);
        $this->db->or_like('unit_id_es3', $q);
        $this->db->or_like('unit_id_es4', $q);
        $this->db->or_like('unit_staf_id', $q);
        $this->db->or_like('statpeg_id', $q);
        $this->db->or_like('aktif_id', $q);
        $this->db->or_like('jenis_peg_id', $q);
        $this->db->or_like('jenjang_id', $q);
        $this->db->or_like('gol_id', $q);
        $this->db->or_like('no_karpeg', $q);
        $this->db->or_like('absensi_id', $q);
        $this->db->or_like('jeniskel_id', $q);
        $this->db->or_like('tmp_lahir', $q);
        $this->db->or_like('tgl_lahir', $q);
        $this->db->or_like('agama_id', $q);
        $this->db->or_like('kawin_id', $q);
        $this->db->or_like('alamat_ktp', $q);
        $this->db->or_like('kode_pos_ktp', $q);
        $this->db->or_like('alamat_tinggal', $q);
        $this->db->or_like('kode_pos_tinggal', $q);
        $this->db->or_like('telp_rumah', $q);
        $this->db->or_like('telp_cellular', $q);
        $this->db->or_like('no_ext_kantor', $q);
        $this->db->or_like('email_kantor', $q);
        $this->db->or_like('email_pribadi', $q);
        $this->db->or_like('gol_darah', $q);
        $this->db->or_like('no_bpjs', $q);
        $this->db->or_like('ktp_id', $q);
        $this->db->or_like('npwp', $q);
        $this->db->or_like('bank_id', $q);
        $this->db->or_like('cabang_bank', $q);
        $this->db->or_like('no_rek', $q);
        $this->db->or_like('atas_nama', $q);
        $this->db->or_like('photo', $q);
        $this->db->or_like('keterangan', $q);
        $this->db->or_like('dihapus', $q);
        $this->db->or_like('ktp_fileup', $q);
        $this->db->or_like('npwp_fileup', $q);
        $this->db->or_like('kartukeluarga_fileup', $q);
        $this->db->or_like('karpeg_fileup', $q);
        $this->db->or_like('editdate', $q);
        $this->db->or_like('userlog', $q);
        $this->db->limit($limit, $start);

        return $this->db->get($this->table)->result();
    }

    // insert data
    public function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    public function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    public function update_by_nip($nip, $data)
    {
        $this->db->where('nip', $nip);
        $this->db->update($this->table, $data);
    }

    // delete data
    public function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    public function search_pegawai($nama)
    {
        $this->db->like('nama', $nama, 'both');
        $this->db->order_by('nama', 'ASC');
        $this->db->limit(10);

        return $this->db->get('master_biodata')->result();
    }

    public function get_nip_nama_all()
    {
        $sql = 'SELECT nip, nama FROM master_biodata;';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pegawai_belum_masuk()
    {
        $sql = "SELECT * FROM master_biodata a WHERE a.nip NOT IN (SELECT nip FROM tbl_user);";
        $result = $this->db->query($sql);
        return $result->result();
    }
}

/* End of file Master_biodata_model.php */
/* Location: ./application/models/Master_biodata_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-12 20:43:46 */
/* http://harviacode.com */
