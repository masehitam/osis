<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA SISA CUTI</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">
                <table class='table table-bordered'>
                    <tr>
                        <td width='200'>Nama <?php echo form_error('nama') ?></td>
                        <td><input type="text" class="form-control" name="nama" id="nama" placeholder="Nama"
                                   value="<?php echo $nip; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Nip <?php echo form_error('nip') ?></td>
                        <td><input type="text" class="form-control" name="nip" id="nip" placeholder="Nip"
                                   value="<?php echo $nip; ?>" readonly/></td>
                    </tr>
                    <tr>
                        <td width='200'>Tahun <?php echo form_error('tahun') ?></td>
                        <td><input type="number" class="form-control" name="tahun" id="tahun" placeholder="Tahun"
                                   value="<?php
                                   if (strlen($tahun) < 1) {
                                       echo date("Y");
                                   } else {
                                       echo $tahun;
                                   } ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Sisa <?php echo form_error('sisa') ?></td>
                        <td><input type="number" class="form-control" name="sisa" id="sisa" placeholder="Sisa"
                                   value="<?php
                                   if (strlen($sisa) < 1) {
                                       echo "14";
                                   } else {
                                       echo $sisa;
                                   } ?>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <button type="submit" class="btn btn-danger"><i
                                        class="fa fa-floppy-o"></i> <?php echo $button ?></button>
                            <a href="<?php echo site_url('sisa_cuti_baru') ?>" class="btn btn-info"><i
                                        class="fa fa-sign-out"></i> Kembali</a></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.js"></script>
<script>
    $(function () {
        //autocomplete
        $("#nama").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama").val("" + suggestion.value);
                $("#nip").val("" + suggestion.id);
            }
        });
    });
</script>