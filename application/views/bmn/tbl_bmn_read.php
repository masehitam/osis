<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Pengelolaan Ruang Rapat</h3>
            </div>
            <table class="table">
                <tr>
                    <td style="width: 200px">Nama</td>
                    <td><?php echo $nama; ?></td>
                </tr>
                <tr>
                    <td>Kode</td>
                    <td><?php echo $kode; ?></td>
                </tr>
                <tr>
                    <td>Tahun</td>
                    <td><?php echo $tahun; ?></td>
                </tr>
                <!-- <tr><td>Stok</td><td><?php //echo $stok; ?></td></tr> -->
                <tr>
                    <td></td>
                    <td>
                        <a href="<?php echo site_url('bmn'); ?>" class="btn btn-default" onclick="goBack()"><i
                                    class="fa fa-sign-out"></i> Kembali</a>
                    </td>
                </tr>
            </table>
        </div>
    </section>
</div>