<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA TBL_BMN</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">

                <table class='table table-bordered'>

                    <tr>
                        <td width='200'>Nama <?php echo form_error('nama') ?></td>
                        <td><input type="text" class="form-control" name="nama" id="nama" placeholder="Nama"
                                   value="<?php echo $nama; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Kode <?php echo form_error('kode') ?></td>
                        <td><input type="text" class="form-control" name="kode" id="kode" placeholder="Kode"
                                   value="<?php echo $kode; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Tahun <?php echo form_error('tahun') ?></td>
                        <td><input type="text" class="form-control" name="tahun" id="tahun" placeholder="Tahun"
                                   value="<?php echo $tahun; ?>"/></td>
                    </tr>
                    <!-- <tr><td width='200'>Stok <?php //echo form_error('stok') ?></td><td><input type="text" class="form-control" name="stok" id="stok" placeholder="Stok" value="<?php //echo $stok; ?>" /></td></tr> -->
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <button type="submit" class="btn btn-danger"><i
                                        class="fa fa-floppy-o"></i> <?php echo $button ?></button>
                            <a href="<?php echo site_url('bmn') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i>
                                Kembali</a></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
</div>