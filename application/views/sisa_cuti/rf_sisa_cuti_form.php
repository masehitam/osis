<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA RF_SISA_CUTI</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">

                <table class='table table-bordered'>

                    <tr>
                        <td width='200'>Bkn Id <?php echo form_error('bkn_id') ?></td>
                        <td><input type="text" class="form-control" name="bkn_id" id="bkn_id" placeholder="Bkn Id"
                                   value="<?php echo $bkn_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Nip <?php echo form_error('nip') ?></td>
                        <td><input type="text" class="form-control" name="nip" id="nip" placeholder="Nip"
                                   value="<?php echo $nip; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Nama Cetak <?php echo form_error('nama_cetak') ?></td>
                        <td><input type="text" class="form-control" name="nama_cetak" id="nama_cetak"
                                   placeholder="Nama Cetak" value="<?php echo $nama_cetak; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Th2018 <?php echo form_error('th2018') ?></td>
                        <td><input type="text" class="form-control" name="th2018" id="th2018" placeholder="Th2018"
                                   value="<?php echo $th2018; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Th2019 <?php echo form_error('th2019') ?></td>
                        <td><input type="text" class="form-control" name="th2019" id="th2019" placeholder="Th2019"
                                   value="<?php echo $th2019; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Th2020 <?php echo form_error('th2020') ?></td>
                        <td><input type="text" class="form-control" name="th2020" id="th2020" placeholder="Th2020"
                                   value="<?php echo $th2020; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Th2021 <?php echo form_error('th2021') ?></td>
                        <td><input type="text" class="form-control" name="th2021" id="th2021" placeholder="Th2021"
                                   value="<?php echo $th2021; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Th2022 <?php echo form_error('th2022') ?></td>
                        <td><input type="text" class="form-control" name="th2022" id="th2022" placeholder="Th2022"
                                   value="<?php echo $th2022; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Th2023 <?php echo form_error('th2023') ?></td>
                        <td><input type="text" class="form-control" name="th2023" id="th2023" placeholder="Th2023"
                                   value="<?php echo $th2023; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Th2024 <?php echo form_error('th2024') ?></td>
                        <td><input type="text" class="form-control" name="th2024" id="th2024" placeholder="Th2024"
                                   value="<?php echo $th2024; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Th2025 <?php echo form_error('th2025') ?></td>
                        <td><input type="text" class="form-control" name="th2025" id="th2025" placeholder="Th2025"
                                   value="<?php echo $th2025; ?>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="master_biodata_id" value="<?php echo $master_biodata_id; ?>"/>
                            <button type="submit" class="btn btn-danger"><i
                                        class="fa fa-floppy-o"></i> <?php echo $button ?></button>
                            <a href="<?php echo site_url('sisa_cuti') ?>" class="btn btn-info"><i
                                        class="fa fa-sign-out"></i> Kembali</a></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
</div>