<!doctype html>
<html>
<head>
    <link rel="shortcut icon" href="https://lpsk.go.id/assets/resources/css/img/lpsk.png" type="image/x-icon"/>
    <title>Ref Sisa Cuti</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    <style>
        body {
            padding: 15px;
        }
    </style>
</head>
<body>
<h2 style="margin-top:0px">Rf_sisa_cuti Read</h2>
<table class="table">
    <tr>
        <td>Bkn Id</td>
        <td><?php echo $bkn_id; ?></td>
    </tr>
    <tr>
        <td>Nip</td>
        <td><?php echo $nip; ?></td>
    </tr>
    <tr>
        <td>Nama Cetak</td>
        <td><?php echo $nama_cetak; ?></td>
    </tr>
    <tr>
        <td>Th2018</td>
        <td><?php echo $th2018; ?></td>
    </tr>
    <tr>
        <td>Th2019</td>
        <td><?php echo $th2019; ?></td>
    </tr>
    <tr>
        <td>Th2020</td>
        <td><?php echo $th2020; ?></td>
    </tr>
    <tr>
        <td>Th2021</td>
        <td><?php echo $th2021; ?></td>
    </tr>
    <tr>
        <td>Th2022</td>
        <td><?php echo $th2022; ?></td>
    </tr>
    <tr>
        <td>Th2023</td>
        <td><?php echo $th2023; ?></td>
    </tr>
    <tr>
        <td>Th2024</td>
        <td><?php echo $th2024; ?></td>
    </tr>
    <tr>
        <td>Th2025</td>
        <td><?php echo $th2025; ?></td>
    </tr>
    <tr>
        <td></td>
        <td><a href="<?php echo site_url('sisa_cuti') ?>" class="btn btn-default">Cancel</a></td>
    </tr>
</table>
</body>
</html>