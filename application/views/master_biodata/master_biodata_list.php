<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning box-solid">
                    <div class="box-header">
                        <h3 class="box-title">KELOLA DATA MASTER_BIODATA</h3>
                    </div>
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <?php echo $this->session->userdata('message') != '' ? $this->session->userdata('message') : ''; ?>
                        </div>
                        <div style="padding-bottom: 10px;">
                            <?php echo anchor(site_url('master_biodata/create'), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-danger btn-sm"'); ?>
                        </div>
                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                            <tr>
                                <th width="30px">No</th>
                                <th>Bkn Id</th>
                                <th>Nip</th>
                                <th>Nama Cetak</th>
                                <th>Nama</th>
                                <th>Jenis Jab Id</th>
                                <th>Struk Id</th>
                                <th>Fung Id</th>
                                <th>Unit Id Es1</th>
                                <th>Unit Id Es2</th>
                                <th>Unit Id Es3</th>
                                <th>Unit Id Es4</th>
                                <th>Unit Staf Id</th>
                                <th>Statpeg Id</th>
                                <th>Aktif Id</th>
                                <th>Jenis Peg Id</th>
                                <th>Jenjang Id</th>
                                <th>Gol Id</th>
                                <th>No Karpeg</th>
                                <th>Absensi Id</th>
                                <th>Jeniskel Id</th>
                                <th>Tmp Lahir</th>
                                <th>Tgl Lahir</th>
                                <th>Agama Id</th>
                                <th>Kawin Id</th>
                                <th>Alamat Ktp</th>
                                <th>Kode Pos Ktp</th>
                                <th>Alamat Tinggal</th>
                                <th>Kode Pos Tinggal</th>
                                <th>Telp Rumah</th>
                                <th>Telp Cellular</th>
                                <th>No Ext Kantor</th>
                                <th>Email Kantor</th>
                                <th>Email Pribadi</th>
                                <th>Gol Darah</th>
                                <th>No Bpjs</th>
                                <th>Ktp Id</th>
                                <th>Npwp</th>
                                <th>Bank Id</th>
                                <th>Cabang Bank</th>
                                <th>No Rek</th>
                                <th>Atas Nama</th>
                                <th>Photo</th>
                                <th>Keterangan</th>
                                <th>Dihapus</th>
                                <th>Ktp Fileup</th>
                                <th>Npwp Fileup</th>
                                <th>Kartukeluarga Fileup</th>
                                <th>Karpeg Fileup</th>
                                <th>Editdate</th>
                                <th>Userlog</th>
                                <th width="200px">Action</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function (e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "master_biodata/json", "type": "POST"},
            columns: [
                {
                    "data": "master_biodata_id",
                    "orderable": false
                }, {"data": "bkn_id"}, {"data": "nip"}, {"data": "nama_cetak"}, {"data": "nama"}, {"data": "jenis_jab_id"}, {"data": "struk_id"}, {"data": "fung_id"}, {"data": "unit_id_es1"}, {"data": "unit_id_es2"}, {"data": "unit_id_es3"}, {"data": "unit_id_es4"}, {"data": "unit_staf_id"}, {"data": "statpeg_id"}, {"data": "aktif_id"}, {"data": "jenis_peg_id"}, {"data": "jenjang_id"}, {"data": "gol_id"}, {"data": "no_karpeg"}, {"data": "absensi_id"}, {"data": "jeniskel_id"}, {"data": "tmp_lahir"}, {"data": "tgl_lahir"}, {"data": "agama_id"}, {"data": "kawin_id"}, {"data": "alamat_ktp"}, {"data": "kode_pos_ktp"}, {"data": "alamat_tinggal"}, {"data": "kode_pos_tinggal"}, {"data": "telp_rumah"}, {"data": "telp_cellular"}, {"data": "no_ext_kantor"}, {"data": "email_kantor"}, {"data": "email_pribadi"}, {"data": "gol_darah"}, {"data": "no_bpjs"}, {"data": "ktp_id"}, {"data": "npwp"}, {"data": "bank_id"}, {"data": "cabang_bank"}, {"data": "no_rek"}, {"data": "atas_nama"}, {"data": "photo"}, {"data": "keterangan"}, {"data": "dihapus"}, {"data": "ktp_fileup"}, {"data": "npwp_fileup"}, {"data": "kartukeluarga_fileup"}, {"data": "karpeg_fileup"}, {"data": "editdate"}, {"data": "userlog"},
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>