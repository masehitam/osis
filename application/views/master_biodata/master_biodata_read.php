<!doctype html>
<html>
<head>
    <link rel="shortcut icon" href="https://lpsk.go.id/assets/resources/css/img/lpsk.png" type="image/x-icon"/>
    <title>Detail Biodata</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    <style>
        body {
            padding: 15px;
        }
    </style>
</head>
<body>
<h2 style="margin-top:0px">Master_biodata Read</h2>
<table class="table">
    <tr>
        <td>Bkn Id</td>
        <td><?php echo $bkn_id; ?></td>
    </tr>
    <tr>
        <td>Nip</td>
        <td><?php echo $nip; ?></td>
    </tr>
    <tr>
        <td>Nama Cetak</td>
        <td><?php echo $nama_cetak; ?></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td><?php echo $nama; ?></td>
    </tr>
    <tr>
        <td>Jenis Jab Id</td>
        <td><?php echo $jenis_jab_id; ?></td>
    </tr>
    <tr>
        <td>Struk Id</td>
        <td><?php echo $struk_id; ?></td>
    </tr>
    <tr>
        <td>Fung Id</td>
        <td><?php echo $fung_id; ?></td>
    </tr>
    <tr>
        <td>Unit Id Es1</td>
        <td><?php echo $unit_id_es1; ?></td>
    </tr>
    <tr>
        <td>Unit Id Es2</td>
        <td><?php echo $unit_id_es2; ?></td>
    </tr>
    <tr>
        <td>Unit Id Es3</td>
        <td><?php echo $unit_id_es3; ?></td>
    </tr>
    <tr>
        <td>Unit Id Es4</td>
        <td><?php echo $unit_id_es4; ?></td>
    </tr>
    <tr>
        <td>Unit Staf Id</td>
        <td><?php echo $unit_staf_id; ?></td>
    </tr>
    <tr>
        <td>Statpeg Id</td>
        <td><?php echo $statpeg_id; ?></td>
    </tr>
    <tr>
        <td>Aktif Id</td>
        <td><?php echo $aktif_id; ?></td>
    </tr>
    <tr>
        <td>Jenis Peg Id</td>
        <td><?php echo $jenis_peg_id; ?></td>
    </tr>
    <tr>
        <td>Jenjang Id</td>
        <td><?php echo $jenjang_id; ?></td>
    </tr>
    <tr>
        <td>Gol Id</td>
        <td><?php echo $gol_id; ?></td>
    </tr>
    <tr>
        <td>No Karpeg</td>
        <td><?php echo $no_karpeg; ?></td>
    </tr>
    <tr>
        <td>Absensi Id</td>
        <td><?php echo $absensi_id; ?></td>
    </tr>
    <tr>
        <td>Jeniskel Id</td>
        <td><?php echo $jeniskel_id; ?></td>
    </tr>
    <tr>
        <td>Tmp Lahir</td>
        <td><?php echo $tmp_lahir; ?></td>
    </tr>
    <tr>
        <td>Tgl Lahir</td>
        <td><?php echo $tgl_lahir; ?></td>
    </tr>
    <tr>
        <td>Agama Id</td>
        <td><?php echo $agama_id; ?></td>
    </tr>
    <tr>
        <td>Kawin Id</td>
        <td><?php echo $kawin_id; ?></td>
    </tr>
    <tr>
        <td>Alamat Ktp</td>
        <td><?php echo $alamat_ktp; ?></td>
    </tr>
    <tr>
        <td>Kode Pos Ktp</td>
        <td><?php echo $kode_pos_ktp; ?></td>
    </tr>
    <tr>
        <td>Alamat Tinggal</td>
        <td><?php echo $alamat_tinggal; ?></td>
    </tr>
    <tr>
        <td>Kode Pos Tinggal</td>
        <td><?php echo $kode_pos_tinggal; ?></td>
    </tr>
    <tr>
        <td>Telp Rumah</td>
        <td><?php echo $telp_rumah; ?></td>
    </tr>
    <tr>
        <td>Telp Cellular</td>
        <td><?php echo $telp_cellular; ?></td>
    </tr>
    <tr>
        <td>No Ext Kantor</td>
        <td><?php echo $no_ext_kantor; ?></td>
    </tr>
    <tr>
        <td>Email Kantor</td>
        <td><?php echo $email_kantor; ?></td>
    </tr>
    <tr>
        <td>Email Pribadi</td>
        <td><?php echo $email_pribadi; ?></td>
    </tr>
    <tr>
        <td>Gol Darah</td>
        <td><?php echo $gol_darah; ?></td>
    </tr>
    <tr>
        <td>No Bpjs</td>
        <td><?php echo $no_bpjs; ?></td>
    </tr>
    <tr>
        <td>Ktp Id</td>
        <td><?php echo $ktp_id; ?></td>
    </tr>
    <tr>
        <td>Npwp</td>
        <td><?php echo $npwp; ?></td>
    </tr>
    <tr>
        <td>Bank Id</td>
        <td><?php echo $bank_id; ?></td>
    </tr>
    <tr>
        <td>Cabang Bank</td>
        <td><?php echo $cabang_bank; ?></td>
    </tr>
    <tr>
        <td>No Rek</td>
        <td><?php echo $no_rek; ?></td>
    </tr>
    <tr>
        <td>Atas Nama</td>
        <td><?php echo $atas_nama; ?></td>
    </tr>
    <tr>
        <td>Photo</td>
        <td><?php echo $photo; ?></td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td><?php echo $keterangan; ?></td>
    </tr>
    <tr>
        <td>Dihapus</td>
        <td><?php echo $dihapus; ?></td>
    </tr>
    <tr>
        <td>Ktp Fileup</td>
        <td><?php echo $ktp_fileup; ?></td>
    </tr>
    <tr>
        <td>Npwp Fileup</td>
        <td><?php echo $npwp_fileup; ?></td>
    </tr>
    <tr>
        <td>Kartukeluarga Fileup</td>
        <td><?php echo $kartukeluarga_fileup; ?></td>
    </tr>
    <tr>
        <td>Karpeg Fileup</td>
        <td><?php echo $karpeg_fileup; ?></td>
    </tr>
    <tr>
        <td>Editdate</td>
        <td><?php echo $editdate; ?></td>
    </tr>
    <tr>
        <td>Userlog</td>
        <td><?php echo $userlog; ?></td>
    </tr>
    <tr>
        <td></td>
        <td><a href="<?php echo site_url('master_biodata') ?>" class="btn btn-default">Cancel</a></td>
    </tr>
</table>
</body>
</html>