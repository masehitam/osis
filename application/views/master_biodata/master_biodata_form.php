<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA MASTER_BIODATA</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">

                <table class='table table-bordered'>
                    <tr>
                        <td width='200'>Bkn Id <?php echo form_error('bkn_id'); ?></td>
                        <td><input type="text" class="form-control" name="bkn_id" id="bkn_id" placeholder="Bkn Id"
                                   value="<?php echo $bkn_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Nip <?php echo form_error('nip'); ?></td>
                        <td><input type="text" class="form-control" name="nip" id="nip" placeholder="Nip"
                                   value="<?php echo $nip; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Nama Cetak <?php echo form_error('nama_cetak'); ?></td>
                        <td><input type="text" class="form-control" name="nama_cetak" id="nama_cetak"
                                   placeholder="Nama Cetak" value="<?php echo $nama_cetak; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Nama <?php echo form_error('nama'); ?></td>
                        <td><input type="text" class="form-control" name="nama" id="nama" placeholder="Nama"
                                   value="<?php echo $nama; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Jenis Jab Id <?php echo form_error('jenis_jab_id'); ?></td>
                        <td><input type="text" class="form-control" name="jenis_jab_id" id="jenis_jab_id"
                                   placeholder="Jenis Jab Id" value="<?php echo $jenis_jab_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Struk Id <?php echo form_error('struk_id'); ?></td>
                        <td><input type="text" class="form-control" name="struk_id" id="struk_id" placeholder="Struk Id"
                                   value="<?php echo $struk_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Fung Id <?php echo form_error('fung_id'); ?></td>
                        <td><input type="text" class="form-control" name="fung_id" id="fung_id" placeholder="Fung Id"
                                   value="<?php echo $fung_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Unit Id Es1 <?php echo form_error('unit_id_es1'); ?></td>
                        <td><input type="text" class="form-control" name="unit_id_es1" id="unit_id_es1"
                                   placeholder="Unit Id Es1" value="<?php echo $unit_id_es1; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Unit Id Es2 <?php echo form_error('unit_id_es2'); ?></td>
                        <td><input type="text" class="form-control" name="unit_id_es2" id="unit_id_es2"
                                   placeholder="Unit Id Es2" value="<?php echo $unit_id_es2; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Unit Id Es3 <?php echo form_error('unit_id_es3'); ?></td>
                        <td><input type="text" class="form-control" name="unit_id_es3" id="unit_id_es3"
                                   placeholder="Unit Id Es3" value="<?php echo $unit_id_es3; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Unit Id Es4 <?php echo form_error('unit_id_es4'); ?></td>
                        <td><input type="text" class="form-control" name="unit_id_es4" id="unit_id_es4"
                                   placeholder="Unit Id Es4" value="<?php echo $unit_id_es4; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Unit Staf Id <?php echo form_error('unit_staf_id'); ?></td>
                        <td><input type="text" class="form-control" name="unit_staf_id" id="unit_staf_id"
                                   placeholder="Unit Staf Id" value="<?php echo $unit_staf_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Statpeg Id <?php echo form_error('statpeg_id'); ?></td>
                        <td><input type="text" class="form-control" name="statpeg_id" id="statpeg_id"
                                   placeholder="Statpeg Id" value="<?php echo $statpeg_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Aktif Id <?php echo form_error('aktif_id'); ?></td>
                        <td><input type="text" class="form-control" name="aktif_id" id="aktif_id" placeholder="Aktif Id"
                                   value="<?php echo $aktif_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Jenis Peg Id <?php echo form_error('jenis_peg_id'); ?></td>
                        <td><input type="text" class="form-control" name="jenis_peg_id" id="jenis_peg_id"
                                   placeholder="Jenis Peg Id" value="<?php echo $jenis_peg_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Jenjang Id <?php echo form_error('jenjang_id'); ?></td>
                        <td><input type="text" class="form-control" name="jenjang_id" id="jenjang_id"
                                   placeholder="Jenjang Id" value="<?php echo $jenjang_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Gol Id <?php echo form_error('gol_id'); ?></td>
                        <td><input type="text" class="form-control" name="gol_id" id="gol_id" placeholder="Gol Id"
                                   value="<?php echo $gol_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>No Karpeg <?php echo form_error('no_karpeg'); ?></td>
                        <td><input type="text" class="form-control" name="no_karpeg" id="no_karpeg"
                                   placeholder="No Karpeg" value="<?php echo $no_karpeg; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Absensi Id <?php echo form_error('absensi_id'); ?></td>
                        <td><input type="text" class="form-control" name="absensi_id" id="absensi_id"
                                   placeholder="Absensi Id" value="<?php echo $absensi_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Jeniskel Id <?php echo form_error('jeniskel_id'); ?></td>
                        <td><input type="text" class="form-control" name="jeniskel_id" id="jeniskel_id"
                                   placeholder="Jeniskel Id" value="<?php echo $jeniskel_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Tmp Lahir <?php echo form_error('tmp_lahir'); ?></td>
                        <td><input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir"
                                   placeholder="Tmp Lahir" value="<?php echo $tmp_lahir; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Tgl Lahir <?php echo form_error('tgl_lahir'); ?></td>
                        <td><input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir"
                                   placeholder="Tgl Lahir" value="<?php echo $tgl_lahir; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Agama Id <?php echo form_error('agama_id'); ?></td>
                        <td><input type="text" class="form-control" name="agama_id" id="agama_id" placeholder="Agama Id"
                                   value="<?php echo $agama_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Kawin Id <?php echo form_error('kawin_id'); ?></td>
                        <td><input type="text" class="form-control" name="kawin_id" id="kawin_id" placeholder="Kawin Id"
                                   value="<?php echo $kawin_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Alamat Ktp <?php echo form_error('alamat_ktp'); ?></td>
                        <td><input type="text" class="form-control" name="alamat_ktp" id="alamat_ktp"
                                   placeholder="Alamat Ktp" value="<?php echo $alamat_ktp; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Kode Pos Ktp <?php echo form_error('kode_pos_ktp'); ?></td>
                        <td><input type="text" class="form-control" name="kode_pos_ktp" id="kode_pos_ktp"
                                   placeholder="Kode Pos Ktp" value="<?php echo $kode_pos_ktp; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Alamat Tinggal <?php echo form_error('alamat_tinggal'); ?></td>
                        <td><input type="text" class="form-control" name="alamat_tinggal" id="alamat_tinggal"
                                   placeholder="Alamat Tinggal" value="<?php echo $alamat_tinggal; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Kode Pos Tinggal <?php echo form_error('kode_pos_tinggal'); ?></td>
                        <td><input type="text" class="form-control" name="kode_pos_tinggal" id="kode_pos_tinggal"
                                   placeholder="Kode Pos Tinggal" value="<?php echo $kode_pos_tinggal; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Telp Rumah <?php echo form_error('telp_rumah'); ?></td>
                        <td><input type="text" class="form-control" name="telp_rumah" id="telp_rumah"
                                   placeholder="Telp Rumah" value="<?php echo $telp_rumah; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Telp Cellular <?php echo form_error('telp_cellular'); ?></td>
                        <td><input type="text" class="form-control" name="telp_cellular" id="telp_cellular"
                                   placeholder="Telp Cellular" value="<?php echo $telp_cellular; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>No Ext Kantor <?php echo form_error('no_ext_kantor'); ?></td>
                        <td><input type="text" class="form-control" name="no_ext_kantor" id="no_ext_kantor"
                                   placeholder="No Ext Kantor" value="<?php echo $no_ext_kantor; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Email Kantor <?php echo form_error('email_kantor'); ?></td>
                        <td><input type="text" class="form-control" name="email_kantor" id="email_kantor"
                                   placeholder="Email Kantor" value="<?php echo $email_kantor; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Email Pribadi <?php echo form_error('email_pribadi'); ?></td>
                        <td><input type="text" class="form-control" name="email_pribadi" id="email_pribadi"
                                   placeholder="Email Pribadi" value="<?php echo $email_pribadi; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Gol Darah <?php echo form_error('gol_darah'); ?></td>
                        <td><input type="text" class="form-control" name="gol_darah" id="gol_darah"
                                   placeholder="Gol Darah" value="<?php echo $gol_darah; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>No Bpjs <?php echo form_error('no_bpjs'); ?></td>
                        <td><input type="text" class="form-control" name="no_bpjs" id="no_bpjs" placeholder="No Bpjs"
                                   value="<?php echo $no_bpjs; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Ktp Id <?php echo form_error('ktp_id'); ?></td>
                        <td><input type="text" class="form-control" name="ktp_id" id="ktp_id" placeholder="Ktp Id"
                                   value="<?php echo $ktp_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Npwp <?php echo form_error('npwp'); ?></td>
                        <td><input type="text" class="form-control" name="npwp" id="npwp" placeholder="Npwp"
                                   value="<?php echo $npwp; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Bank Id <?php echo form_error('bank_id'); ?></td>
                        <td><input type="text" class="form-control" name="bank_id" id="bank_id" placeholder="Bank Id"
                                   value="<?php echo $bank_id; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Cabang Bank <?php echo form_error('cabang_bank'); ?></td>
                        <td><input type="text" class="form-control" name="cabang_bank" id="cabang_bank"
                                   placeholder="Cabang Bank" value="<?php echo $cabang_bank; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>No Rek <?php echo form_error('no_rek'); ?></td>
                        <td><input type="text" class="form-control" name="no_rek" id="no_rek" placeholder="No Rek"
                                   value="<?php echo $no_rek; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Atas Nama <?php echo form_error('atas_nama'); ?></td>
                        <td><input type="text" class="form-control" name="atas_nama" id="atas_nama"
                                   placeholder="Atas Nama" value="<?php echo $atas_nama; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Photo <?php echo form_error('photo'); ?></td>
                        <td><input type="text" class="form-control" name="photo" id="photo" placeholder="Photo"
                                   value="<?php echo $photo; ?>"/></td>
                    </tr>

                    <tr>
                        <td width='200'>Keterangan <?php echo form_error('keterangan'); ?></td>
                        <td> <textarea class="form-control" rows="3" name="keterangan" id="keterangan"
                                       placeholder="Keterangan"><?php echo $keterangan; ?></textarea></td>
                    </tr>
                    <tr>
                        <td width='200'>Dihapus <?php echo form_error('dihapus'); ?></td>
                        <td><input type="text" class="form-control" name="dihapus" id="dihapus" placeholder="Dihapus"
                                   value="<?php echo $dihapus; ?>"/></td>
                    </tr>

                    <tr>
                        <td width='200'>Ktp Fileup <?php echo form_error('ktp_fileup'); ?></td>
                        <td> <textarea class="form-control" rows="3" name="ktp_fileup" id="ktp_fileup"
                                       placeholder="Ktp Fileup"><?php echo $ktp_fileup; ?></textarea></td>
                    </tr>

                    <tr>
                        <td width='200'>Npwp Fileup <?php echo form_error('npwp_fileup'); ?></td>
                        <td> <textarea class="form-control" rows="3" name="npwp_fileup" id="npwp_fileup"
                                       placeholder="Npwp Fileup"><?php echo $npwp_fileup; ?></textarea></td>
                    </tr>

                    <tr>
                        <td width='200'>Kartukeluarga Fileup <?php echo form_error('kartukeluarga_fileup'); ?></td>
                        <td> <textarea class="form-control" rows="3" name="kartukeluarga_fileup"
                                       id="kartukeluarga_fileup"
                                       placeholder="Kartukeluarga Fileup"><?php echo $kartukeluarga_fileup; ?></textarea>
                        </td>
                    </tr>

                    <tr>
                        <td width='200'>Karpeg Fileup <?php echo form_error('karpeg_fileup'); ?></td>
                        <td> <textarea class="form-control" rows="3" name="karpeg_fileup" id="karpeg_fileup"
                                       placeholder="Karpeg Fileup"><?php echo $karpeg_fileup; ?></textarea></td>
                    </tr>
                    <tr>
                        <td width='200'>Editdate <?php echo form_error('editdate'); ?></td>
                        <td><input type="text" class="form-control" name="editdate" id="editdate" placeholder="Editdate"
                                   value="<?php echo $editdate; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Userlog <?php echo form_error('userlog'); ?></td>
                        <td><input type="text" class="form-control" name="userlog" id="userlog" placeholder="Userlog"
                                   value="<?php echo $userlog; ?>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="master_biodata_id" value="<?php echo $master_biodata_id; ?>"/>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                <?php echo $button; ?></button>
                            <a href="<?php echo site_url('master_biodata'); ?>" class="btn btn-info"><i
                                        class="fa fa-sign-out"></i> Kembali</a></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
</div>