<html>
<head>
    <link rel="shortcut icon" href="https://lpsk.go.id/assets/resources/css/img/lpsk.png" type="image/x-icon"/>
    <title>Blokir Akses</title>
    <style type="text/css">
        .box {
            width: 600px;
            border: 1px black dashed;
            padding: 20px;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="box">
    <p>Anda Tidak Boleh Mengakses Halaman Ini</p>
    <p>Silahkan Hubungi Administrator</p>
</div>
</body>
</html>