<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA TBL_DISPO_RR</h3>
                <?php echo validation_errors(); ?>
            </div>
            <form action="<?php echo $action; ?>" method="post">
                <table class="table table-bordered">
                    <tr>
                        <td width="200">
                            ID Peminjaman
                            <?php echo form_error('id_trans_rr') ?>
                        </td>
                        <td><input type="text" class="form-control" name="id_trans_rr" id="id_trans_rr"
                                   placeholder="Id Trans Rr" value="<?php echo $id_trans_rr; ?>" readonly="readonly"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="200">
                            Penerima (Konsumsi)
                            <?php echo form_error('nama_penerima') ?>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="nama_penerima" id="nama_penerima"
                                   placeholder="Nama Penerima" value="<?php echo $nama_penerima; ?>"/>
                            <input type="hidden" name="id_penerima" id="id_penerima"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="200">
                            Penerima (Perlengkapan)
                            <?php echo form_error('nama_penerima') ?>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="nama_penerima2" id="nama_penerima2"
                                   placeholder="Nama Penerima" value="<?php echo $nama_penerima2; ?>"/>
                            <input type="hidden" name="id_penerima2" id="id_penerima2"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="200">
                            Komentar
                            <?php echo form_error('komentar') ?>
                        </td>
                        <td><textarea class="form-control" rows="3" name="komentar" id="komentar"
                                      placeholder="Komentar"><?php echo $komentar; ?></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-floppy-o"></i>
                                <?php echo $button ?>
                            </button>
                            <a href="<?php echo site_url('dispo_ruang_rapat') ?>" class="btn btn-info"><i
                                        class="fa fa-sign-out"></i> Kembali</a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/daterangepicker.js"></script>
<script>
    $(function () {
        //autocomplete
        $("#nama_penerima").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_penerima").val("" + suggestion.value);
                $("#id_penerima").val("" + suggestion.id);
            },
        });
        $("#nama_penerima2").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_penerima2").val("" + suggestion.value);
                $("#id_penerima2").val("" + suggestion.id);
            },
        });
    });
</script>
</section>
</div>
