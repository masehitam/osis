<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Pengelolaan Ruang Rapat</h3>
            </div>
            <table class="table">
                <tr>
                    <td>
                        <h5 class="box-title"><b>Detail Disposisi</b></h3>
                    </td>
                </tr>
                <!-- <tr><td>ID Peminjaman Ruang Rapat</td><td><?php //echo $id_trans_rr; ?></td></tr> -->
                <tr>
                    <td width="200px">Penerima</td>
                    <td><?php echo $id_penerima; ?></td>
                </tr>
                <tr>
                    <td>Bagian</td>
                    <td><b><?php echo $bagian; ?></b></td>
                </tr>
                <tr>
                    <td>Komentar</td>
                    <td><?php echo $komentar; ?></td>
                </tr>
                <tr>
                    <td>Dibuat</td>
                    <td><?php echo $date_created; ?></td>
                </tr>
                <tr>
                    <td>
                        <h5 class="box-title"><b>Detail Peminjaman</b></h3>
                    </td>
                </tr>
                <tr>
                    <td>Ruang Rapat</td>
                    <td><?php echo $ruang_rapat; ?></td>
                </tr>
                <tr>
                    <td style="width: 200px">Peminjam</td>
                    <td><?php echo $nama; ?></td>
                </tr>
                <tr>
                    <td>Kegiatan</td>
                    <td><?php echo $kegiatan; ?></td>
                </tr>
                <tr>
                    <td>Waktu Penggunaan</td>
                    <td><?php echo $wkt_kegiatan; ?></td>
                </tr>
                <tr>
                    <td>Jumlah Peserta</td>
                    <td><?php echo $jml_peserta; ?></td>
                </tr>
                <tr>
                    <td>Konsumsi</td>
                    <td><?php echo $konsumsi; ?></td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td><?php echo $keterangan_konsumsi; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><a href="<?php echo site_url('dispo_ruang_rapat') ?>" class="btn btn-default">Kembali</a></td>
                </tr>
            </table>
        </div>
    </section>
</div>