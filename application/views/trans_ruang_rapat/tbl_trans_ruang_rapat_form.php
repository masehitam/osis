<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Formulir Peminjaman Ruang Rapat</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">
                <?php //echo validation_errors();?>
                <table class='table table-bordered'>
                    <tr>
                        <td width='200'>Nama Ruangan
                            <?php echo form_error('ruang_rapat'); ?>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="ruang_rapat" id="ruang_rapat"
                                   placeholder="Ruang Rapat" value="<?php echo $ruang_rapat; ?>"/>
                            <input type="hidden" class="type" id="id_ruang_rapat" name="id_ruang_rapat"
                                   value="<?php echo $id_ruang_rapat; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td width='200'>Kegiatan
                            <?php echo form_error('kegiatan'); ?>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="kegiatan" id="kegiatan"
                                   placeholder="Nama Kegiatan" value="<?php echo $kegiatan; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td width='200'>Tanggal Kegiatan
                            <?php echo form_error('wkt_kegiatan'); ?>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="wkt_kegiatan" id="wkt_kegiatan"
                                   placeholder="Waktu Kegiatan" value="<?php echo $wkt_kegiatan; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td width='200'>Jumlah Peserta
                            <?php echo form_error('jml_peserta'); ?>
                        </td>
                        <td>
                            <input type="number" min="1" max="100" class="form-control" name="jml_peserta"
                                   id="jml_peserta" placeholder="Jumlah Peserta" value="<?php echo $jml_peserta; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td width='200'>Konsumsi
                            <?php echo form_error('konsumsi'); ?>
                        </td>
                        <td>
                            <?php $array_konsumsi = explode(',', $konsumsi); ?>
                            <input class="form-check-input" type="checkbox" value="makanan" id="makanan"
                                   name="konsumsi[]" <?php echo in_array('makanan', $array_konsumsi) ? "checked" : "" ?>>
                            <label class="form-check-label" for="invalidCheck2">
                                Makanan
                            </label>
                            <br/>
                            <input class="form-check-input" type="checkbox" value="snack" id="snack"
                                   name="konsumsi[]" <?php echo in_array('snack', $array_konsumsi) ? "checked" : "" ?>>
                            <label class="form-check-label" for="invalidCheck2">
                                Snack
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td width='200'>Keterangan<?php echo form_error('keterangan_konsumsi'); ?></td>
                        <td><textarea class="form-control" rows="3" name="keterangan_konsumsi" id="keterangan_konsumsi"
                                      placeholder="Keterangan"><?php echo $keterangan_konsumsi; ?></textarea></td>
                    </tr>
                    <tr>
                        <td width='200'>Nama Atasan
                            <?php echo form_error('nama_atasan'); ?>
                        </td>
                        <td>
                            <input type="text" rows="3" class="form-control" name="nama_atasan" id="nama_atasan"
                                   placeholder="Nama Atasan" value="<?php echo $nama_atasan; ?>"/>
                        </td>
                        <input type="hidden" class="type" id="id_atasan" name="id_atasan"
                               value="<?php echo $id_atasan; ?>">
                    </tr>
                    <tr>
                        <td width='200'>Nama Pejabat Berwenang
                            <?php echo form_error('nama_krt'); ?>
                        </td>
                        <td>
                            <input type="text" readonly="readonly" class="form-control" name="nama_krt" id="nama_krt"
                                   placeholder="Nama Pejabat yang Berwenang" value="<?php echo $nama_krt; ?>"/>
                        </td>
                        <input type="hidden" class="type" id="id_krt" name="id_krt" value="<?php echo $id_krt; ?>">
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                <?php echo $button; ?>
                            </button>
                            <a href="<?php echo site_url('trans_ruang_rapat'); ?>" class="btn btn-info"><i
                                        class="fa fa-sign-out"></i> Kembali</a></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/daterangepicker.js"></script>
<script>
    $(function () {
        //autocomplete
        $("#nama_krt").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_krt").val("" + suggestion.value);
                $("#id_krt").val("" + suggestion.id);
            }
        });

        $("#nama_atasan").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_atasan").val("" + suggestion.value);
                $("#id_atasan").val("" + suggestion.id);
            }
        });

        $("#ruang_rapat").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/ruang_rapat/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#ruang_rapat").val("" + suggestion.value);
                $("#id_ruang_rapat").val("" + suggestion.id);
            }
        });

        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        var maxLimitDate = new Date(nowDate.getFullYear() + 1, nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        $('input[name="wkt_kegiatan"]').daterangepicker({
            timePicker: true,
            opens: 'left',
            minDate: today,
            maxDate: maxLimitDate,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(2, 'hour'),
            locale: {
                format: 'DD-MM-YYYY HH:mm'
            },
        }, function (start, end, label) {
            //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });
</script>