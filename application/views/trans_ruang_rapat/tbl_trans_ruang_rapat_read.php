<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Form Pengajuan Penggunaan Ruang Rapat</h3>
            </div>
            <table class="table">
                <tr>
                    <td>Ruang Rapat</td>
                    <td><?php echo $ruang_rapat; ?></td>
                </tr>
                <tr>
                    <td style="width: 200px">Peminjam</td>
                    <td><?php echo $nama; ?></td>
                </tr>
                <tr>
                    <td>Kegiatan</td>
                    <td><?php echo $kegiatan; ?></td>
                </tr>
                <tr>
                    <td>Waktu Penggunaan</td>
                    <td><?php echo $wkt_kegiatan; ?></td>
                </tr>
                <tr>
                    <td>Jumlah Peserta</td>
                    <td><?php echo $jml_peserta; ?></td>
                </tr>
                <tr>
                    <td>Konsumsi</td>
                    <td><?php echo $konsumsi; ?></td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td><?php echo $keterangan_konsumsi; ?></td>
                </tr>
                <?php
                if (isset($alasan_pembatalan) && ($status == 'DIBATALKAN')) :
                    ?>
                    <tr>
                        <td>Alasan Pembatalan</td>
                        <td><?php echo $alasan_pembatalan; ?></td>
                    </tr>
                <?php
                endif;
                if (isset($alasan_penolakan) && (($status == 'TOLAK_1') ||
                        ($status == 'TOLAK_2') ||
                        ($status == 'GAGAL_VALIDASI')
                    )
                ):
                    ?>
                    <tr>
                        <td>Alasan Penolakan</td>
                        <td>
                            <?php
                            echo $alasan_penolakan;
                            if ($status == "TOLAK_1") {
                                if (isset($nama_atasan)) {
                                    echo " ($nama_atasan)";
                                }
                            } elseif ($status == "TOLAK_2") {
                                if (isset($nama_krt)) {
                                    echo " ($nama_krt)";
                                }
                            } else {
                                echo " (Kepegawaian)";
                            }
                            ?>
                        </td>
                    </tr>
                <?php
                elseif (($this->session->userdata()['nip'] != $id_pegguna) && ($status != 'DIBUAT')) :
                    ?>
                    <tr>
                        <td width='200'>Alasan</td>
                        <td>
                            <form method="POST" action="<?php echo site_url('trans_ruang_rapat/down/' . $id); ?>">
                                <textarea class="form-control" rows="3" name="alasan_penolakan" id="alasan_penolakan"
                                          placeholder="Alasan"></textarea>
                        </td>
                    </tr>
                <?php
                endif;
                ?>
                <tr>
                    <td></td>
                    <td>
                        <!-- Yang mengajukan -->
                        <?php if (($this->session->userdata()['nip'] == $id_pegguna) && ($status == 'DIBUAT')) : ?>
                            <a style="width: 200px; margin: 8px"
                               href="<?php echo site_url('trans_ruang_rapat/up/' . $id); ?>" class="btn btn-success"><i
                                        class="fa fa-send-o"></i>
                                Ajukan</a>
                            <!-- Atasan -->
                        <?php elseif (($this->session->userdata()['nip'] == $id_atasan) && ($status == 'AJUKAN_1')) : ?>
                            <a style="width: 200px; margin: 8px"
                               href="<?php echo site_url('trans_ruang_rapat/up/' . $id); ?>" class="btn btn-primary"><i
                                        class="fa fa-check-square-o"></i>
                                Setujui</a>
                            <!--                            <a style="width: 200px; margin: 8px" href="--><?php //echo site_url('trans_ruang_rapat/down/'.$id); ?><!--" class="btn btn-danger"><i class="fa fa-ban"></i>-->
                            <!--                            Tolak</a>-->
                            <button type="submit" style="width: 200px; margin: 8px" class="btn btn-danger"><i
                                        class="fa fa-ban"></i> Tolak
                            </button>
                            </form>
                        <?php elseif (($this->session->userdata()['nip'] == $id_krt) && ($status == 'AJUKAN_2')) : ?>
                            <a style="width: 200px; margin: 8px"
                               href="<?php echo site_url('trans_ruang_rapat/up/' . $id); ?>" class="btn btn-primary"><i
                                        class="fa fa-check-square-o"></i>
                                Setujui</a>
                            <!--                            <a style="width: 200px; margin: 8px" href="--><?php //echo site_url('trans_ruang_rapat/down/'.$id); ?><!--" class="btn btn-danger"><i class="fa fa-ban"></i>-->
                            <!--                            Tolak</a>-->
                            <button type="submit" style="width: 200px; margin: 8px" class="btn btn-danger"><i
                                        class="fa fa-ban"></i> Tolak
                            </button>
                            </form>
                        <?php endif; ?>
                        <?php if (($this->session->userdata()['nip'] == $id_krt) && ($status == 'DISETUJUI')) : ?>
                            <a style="width: 200px; margin: 8px"
                               href="<?php echo site_url('dispo_ruang_rapat/create/' . $id); ?>"
                               class="btn btn-primary"><i class="fa fa-check-square-o"></i>
                                Disposisi</a>
                        <?php endif; ?>
                        <a style="width: 200px; margin: 8px"
                           href="<?php echo site_url('cetak/form_ruang_rapat/' . $id); ?>" target="_blank"
                           class="btn btn-info"><i class="fa fa-print"></i>
                            Cetak</a>
                        <?php if ($page == 'index'): ?>
                            <a style="width: 200px; margin: 8px" href="<?php echo site_url('trans_ruang_rapat'); ?>"
                               class="btn btn-default" onclick="goBack()"><i
                                        class="fa fa-sign-out"></i> Kembali</a>
                        <?php else: ?>
                            <a style="width: 200px; margin: 8px"
                               href="<?php echo site_url('trans_ruang_rapat/index_persetujuan'); ?>"
                               class="btn btn-default" onclick="goBack()"><i
                                        class="fa fa-sign-out"></i> Kembali</a>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>
    </section>
</div>