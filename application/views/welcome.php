<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Sistem Informasi Pelayanan Kantor
            <small>DASHBOARD</small>
            <?php
            // echo "<pre>";
            // print_r($rapat);
            // print_r($cuti);
            // print_r($bmn);
            // echo "</pre>";
            // die();
            ?>
        </h1>
    </section>
    <section class="content">
        <?php echo alert('alert-info', 'Pesan', 'Selamat Datang di Halaman Utama Sistem Informasi Pelayanan Kantor'); ?>
        <?php
        $user = $this->session->userdata();
        ?>
        <div class="row">
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Agenda Rapat Hari Ini</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th style="width: 200px">Ruang Rapat</th>
                                <th>Kegiatan</th>
                                <th style="width: 40px">Jam</th>
                            </tr>
                            <?php
                            $counter = 1;
                            foreach ($rapat as $key => $value) :
                                ?>
                                <tr>
                                    <td><?php echo $counter++; ?></td>
                                    <td><?php echo $value->nama ?></td>
                                    <td><?php echo $value->kegiatan ?></td>
                                    <td>
                                        <span class="badge bg-blue"><?php echo '  ' . substr($value->wkt_kegiatan, 11, 5) . ' - ' . substr($value->wkt_kegiatan, 30, 5) . '  ' ?></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Peminjaman BMN</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Nama Barang</th>
                                <th style="width: 200px">Peminjam</th>
                                <th style="width: 40px">Tanggal Penggunaan</th>
                            </tr>
                            <?php
                            $counter = 1;
                            foreach ($bmn as $key => $value) :
                                ?>
                                <tr>
                                    <td><?php echo $counter++; ?></td>
                                    <td><?php echo $value->nama_bmn ?></td>
                                    <td><?php echo $value->nama ?></td>
                                    <td><span class="badge bg-yellow"><?php echo $value->tgl_penggunaan ?></span></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">«</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Sedang Cuti</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Nama</th>
                            </tr>
                            <?php
                            $counter = 1;
                            foreach ($cuti as $key => $value) :
                                ?>
                                <tr>
                                    <td><?php echo $counter++; ?></td>
                                    <td><?php echo $value->nama ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
</div>