<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Diri</h3>
                    </div>
                    <table class="table">
                        <tr>
                            <td style="width : 150px">Nama Lengkap</td>
                            <td><?php echo $full_name; ?></td>
                        </tr>
                        <tr>
                            <td>Nama Pengguna</td>
                            <td><?php echo $email; ?></td>
                        </tr>
                        <tr>
                            <td>Kata Sandi</td>
                            <td><?php echo '{ENKRIPSI}'; ?></td>
                        </tr>
                        <tr>
                            <td colspan='2'>
                                <a style="width: 200px; margin: 8px" href="<?php echo site_url('') ?>"
                                   class="btn btn-default">Kembali</a>
                                <a style="width: 200px; margin: 8px"
                                   href="<?php echo site_url('profile/update/' . $id_users); ?>"
                                   class="btn btn-warning">Ganti Kata Sandi</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

        
        