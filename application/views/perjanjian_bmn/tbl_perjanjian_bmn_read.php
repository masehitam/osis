<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Perjanjian Penggunaan BMN</h3>
            </div>
            <table class="table">
                <tr>
                    <td style="width: 200px">Nama Peminjam</td>
                    <td><?php echo $nama; ?></td>
                </tr>
                <tr>
                    <td>Nama Bmn</td>
                    <td><?php echo $nama_bmn; ?></td>
                </tr>
                <tr>
                    <td>Tanggal Penggunaan</td>
                    <td><?php echo $tgl_penggunaan; ?></td>
                </tr>
                <tr>
                    <td>Keperluan</td>
                    <td><?php echo $keperluan; ?></td>
                </tr>
                <tr>
                    <td>Lokasi</td>
                    <td><?php echo $lokasi; ?></td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td><?php echo $keterangan; ?></td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td>
                        <?php if (($this->session->userdata()['nip'] == $id_pejabat) && ($is_approved == '0')): ?>
                            <a href="<?php echo site_url('perjanjian_bmn/up/' . $id); ?>" style="width : 200px; margin-left:8px;margin-right:8px;" class="btn btn-success"><i
                                        class="fa fa-send-o"></i>
                                Setujui</a>
                                
                        <?php endif; ?>
                        <?php if (($status == "DISETUJUI") && ($this->session->userdata()['nip'] == $id_krt)) : ?>
                            <a href="<?php echo site_url('trans_bmn/up/' . $id_trans_bmn); ?>" class="btn btn-success" style="width : 200px; margin-left:8px;margin-right:8px;"><i class="fa fa-send-o"></i>
                            Kembalikan</a>
                        <?php endif; ?>
                        <a href="<?php echo site_url('cetak/form_perjanjian_bmn/' . $id); ?>" target="_blank"
                           class="btn btn-info" style="width : 200px; margin-left:8px;margin-right:8px;"><i class="fa fa-print"></i>
                            Cetak</a>
                        <a href="<?php echo site_url('perjanjian_bmn'); ?>" class="btn btn-default"
                           onclick="goBack()" style="width : 200px; margin-left:8px;margin-right:8px;"><i
                                    class="fa fa-sign-out"></i> Kembali</a>
                    </td>
                </tr>
            </table>
        </div>
    </section>
</div>