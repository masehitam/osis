<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA TBL_PERJANJIAN_BMN</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">

                <table class='table table-bordered'>

                    <tr>
                        <td width='200'>Id Trans Bmn <?php echo form_error('id_trans_bmn') ?></td>
                        <td><input type="text" class="form-control" name="id_trans_bmn" id="id_trans_bmn"
                                   placeholder="Id Trans Bmn" value="<?php echo $id_trans_bmn; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Id Pejabat <?php echo form_error('id_pejabat') ?></td>
                        <td><input type="text" class="form-control" name="id_pejabat" id="id_pejabat"
                                   placeholder="Id Pejabat" value="<?php echo $id_pejabat; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Is Approved <?php echo form_error('is_approved') ?></td>
                        <td><input type="text" class="form-control" name="is_approved" id="is_approved"
                                   placeholder="Is Approved" value="<?php echo $is_approved; ?>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <button type="submit" class="btn btn-danger"><i
                                        class="fa fa-floppy-o"></i> <?php echo $button ?></button>
                            <a href="<?php echo site_url('perjanjian_bmn') ?>" class="btn btn-info"><i
                                        class="fa fa-sign-out"></i> Kembali</a></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
</div>