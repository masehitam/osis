<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Cuti</h3>
            </div>
            <div class="box-body">
                <div style="padding-bottom: 10px;">
                    <?php echo $this->session->userdata('message') != '' ? $this->session->userdata('message') : ''; ?>
                </div>
                <table class="table">
                    <tr>
                        <td style="width: 200px">Nip</td>
                        <td><?php echo $nip; ?></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td><?php echo $nama; ?></td>
                    </tr>
                    <tr>
                        <td>Jenis Cuti</td>
                        <td><?php echo $jenis_cuti; ?></td>
                    </tr>
                    <tr>
                        <td>Waktu Cuti</td>
                        <td><?php echo $wkt_cuti . ' - (' . $jumlah_hari . ' Hari)'; ?></td>
                    </tr>
                    <tr>
                        <td>Alasan Pengajuan Cuti</td>
                        <td><?php echo $alasan; ?></td>
                    </tr>
                    <tr>
                        <td>Alamat Selama Cuti</td>
                        <td><?php echo $alamat; ?></td>
                    </tr>
                    <tr>
                        <td>Alamat Email</td>
                        <td><?php echo $email; ?></td>
                    </tr>
                    <tr>
                        <td>Sisa Cuti Tahun Ini</td>
                        <td><?php echo $sisa_tahun_ini; ?></td>
                    </tr>
                    <tr>
                        <td>Sisa Cuti Tahun Kemarin</td>
                        <td><?php echo $sisa_tahun_kemarin; ?></td>
                    </tr>
                    <?php 
                        if (isset($nama_atasan)) :
                    ?>    
                        <tr>
                            <td>Nama Atasan Langsung</td>
                            <td><?php echo $nama_atasan; ?></td>
                        </tr>
                    <?php   
                        endif;
                    ?>
                    <?php 
                        if (isset($nama_pejabat)) :
                    ?>    
                        <tr>
                            <td>Nama Pejabat</td>
                            <td><?php echo $nama_pejabat; ?></td>
                        </tr>
                    <?php   
                        endif;
                    ?>
                    <?php
                    if (isset($alasan_pembatalan) && ($status == 'DIBATALKAN')) :
                        ?>
                        <tr>
                            <td>Alasan Pembatalan</td>
                            <td><?php echo $alasan_pembatalan; ?></td>
                        </tr>
                    <?php
                    endif;
                    if (isset($alasan_penolakan) && (($status == 'TOLAK_1') ||
                            ($status == 'TOLAK_2') ||
                            ($status == 'GAGAL_VALIDASI')
                        )
                    ):
                        ?>
                        <tr>
                            <td>Alasan Penolakan</td>
                            <td>
                                <?php
                                echo $alasan_penolakan;
                                if ($status == "TOLAK_1") {
                                    if (isset($nama_atasan)) {
                                        echo " ($nama_atasan)";
                                    }
                                } elseif ($status == "TOLAK_2") {
                                    if (isset($nama_pejabat)) {
                                        echo " ($nama_pejabat)";
                                    }
                                } else {
                                    echo " (Kepegawaian)";
                                }
                                ?>
                            </td>
                        </tr>
                    <?php
                    elseif (($this->session->userdata()['nip'] != $nip) && ($status != 'DIBUAT')) :
                        ?>
                        <tr>
                            <td width='200'>Alasan (diisi apabila ingin menjelaskan alasan)</td>
                            <td>
                                <form method="POST" action="<?php echo site_url('trans_cuti/down/' . $id); ?>">
                                    <textarea class="form-control" rows="3" name="alasan_penolakan"
                                              id="alasan_penolakan"
                                              placeholder="Alasan"></textarea>
                            </td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    <tr>
                        <td></td>
                        <td>
                            <!-- Yang mengajukan -->
                            <?php if (($this->session->userdata()['nip'] == $nip) && ($status == 'DIBUAT')): ?>
                                <a style="width: 200px; margin: 8px"
                                   href="<?php echo site_url('trans_cuti/up/' . $id); ?>"
                                   class="btn btn-success"><i class="fa fa-send-o"></i>
                                    Ajukan</a>
                                <!-- Atasan -->
                            <?php elseif (($this->session->userdata()['nip'] == $nip_atasan) && ($status == 'AJUKAN_1')): ?>
                                <a style="width: 200px; margin: 8px"
                                   href="<?php echo site_url('trans_cuti/up/' . $id); ?>"
                                   class="btn btn-primary"><i class="fa fa-check-square-o"></i>
                                    Setujui</a>
                                <!--                            <a style="width: 200px; margin: 8px" href="--><?php //echo site_url('trans_cuti/down/' . $id); ?><!--" class="btn btn-danger"><i class="fa fa-ban"></i>-->
                                <!--                            Tolak</a>-->
                                <button type="submit" style="width: 200px; margin: 8px" class="btn btn-danger"><i
                                            class="fa fa-ban"></i> Tolak
                                </button>
                                </form>
                            <?php elseif (($this->session->userdata()['nip'] == $nip_pejabat) && ($status == 'AJUKAN_2')): ?>
                                <a style="width: 200px; margin: 8px"
                                   href="<?php echo site_url('trans_cuti/up/' . $id . '/' . $es); ?>"
                                   class="btn btn-primary"><i class="fa fa-check-square-o"></i>
                                    Setujui</a>
                                <!--                            <a style="width: 200px; margin: 8px" href="--><?php //echo site_url('trans_cuti/down/' . $id); ?><!--" class="btn btn-danger"><i class="fa fa-ban"></i>-->
                                <!--                            Tolak</a>-->
                                <button type="submit" style="width: 200px; margin: 8px" class="btn btn-danger"><i
                                            class="fa fa-ban"></i> Tolak
                                </button>
                                </form>
                            <?php endif; ?>
                            <?php if ($this->session->userdata()['id_user_level'] == 4 && $status == 'PERLU_VALIDASI'): ?>
                                <a style="width: 200px; margin: 8px"
                                   href="<?php echo site_url('trans_cuti/up/' . $id); ?>"
                                   class="btn btn-primary"><i class="fa fa-check-square-o"></i>
                                    Setujui</a>
                                <!--                            <a style="width: 200px; margin: 8px" href="--><?php //echo site_url('trans_cuti/down/' . $id); ?><!--" class="btn btn-danger"><i class="fa fa-ban"></i>-->
                                <!--                            Tolak</a>-->
                                <button type="submit" style="width: 200px; margin: 8px" class="btn btn-danger"><i
                                            class="fa fa-ban"></i> Tolak
                                </button>
                                </form>
                            <?php endif; ?>
                            <a style="width: 200px; margin: 8px"
                               href="<?php echo site_url('cetak/form_cuti/' . $id . '/' . $es); ?>" target="_blank"
                               class="btn btn-info"><i class="fa fa-print"></i>
                                Cetak</a>
                            <?php if ($page == 'index'): ?>
                                <a style="width: 200px; margin: 8px" href="<?php echo site_url('trans_cuti'); ?>"
                                   class="btn btn-default" onclick="goBack()"><i
                                            class="fa fa-sign-out"></i> Kembali</a>
                            <?php elseif ($page == 'persetujuan'): ?>
                                <a style="width: 200px; margin: 8px"
                                   href="<?php echo site_url('trans_cuti/index_persetujuan'); ?>"
                                   class="btn btn-default"
                                   onclick="goBack()"><i
                                            class="fa fa-sign-out"></i> Kembali</a>
                            <?php else: ?>
                                <a style="width: 200px; margin: 8px"
                                   href="<?php echo site_url('trans_cuti/index_validasi'); ?>" class="btn btn-default"
                                   onclick="goBack()"><i
                                            class="fa fa-sign-out"></i> Kembali</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
</div>