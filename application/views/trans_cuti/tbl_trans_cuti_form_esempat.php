<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Formulir Pengajuan Cuti Staf dan Eselon 4</h3>
            </div>
            <?php //echo validation_errors();?>
            <form action="<?php echo $action; ?>" method="post">
                <table class='table table-bordered'>
                    <tr>
                        <td width='200'>Jenis Cuti <?php echo form_error('jenis_cuti'); ?></td>
                        <td><?php echo cmb_dinamis('jenis_cuti', 'tbl_cuti', 'jenis_cuti', 'jenis_cuti'); ?></td>
                    </tr>
                    <tr>
                        <td width='200'>Wkt Cuti <?php echo form_error('wkt_cuti'); ?></td>
                        <td><input type="text" class="form-control" name="wkt_cuti" id="wkt_cuti" placeholder="Wkt Cuti"
                                   value="<?php echo $wkt_cuti; ?>"/></td>
                    </tr>
                    <tr>
                        <td width='200'>Alasan <?php echo form_error('alasan'); ?></td>
                        <td><textarea class="form-control" rows="3" name="alasan" id="alasan"
                                      placeholder="Alasan"><?php echo $alasan; ?></textarea></td>
                    </tr>
                    <tr>
                        <td width='200'>Alamat Selama Menjalani Cuti<?php echo form_error('alamat'); ?></td>
                        <td><textarea class="form-control" rows="3" name="alamat" id="alamat"
                                      placeholder="Alamat"><?php echo $alamat; ?></textarea></td>
                    </tr>
                    <tr>
                        <td width='200'>Mengetahui<?php echo form_error('nama_atasan'); ?></td>
                        <td><input type="text" class="form-control" name="nama_atasan" id="nama_atasan"
                                   placeholder="Nama Atasan Langsung" value="<?php echo $nama_atasan; ?>"/></td>
                        <input type="hidden" class="type" id="nip_atasan" name="nip_atasan"
                               value="<?php echo $nip_atasan; ?>">
                    </tr>
                    <tr>
                        <td width='200'>Menyetujui<?php echo form_error('nama_pejabat'); ?></td>
                        <td><input type="text" class="form-control" name="nama_pejabat" id="nama_pejabat"
                                   placeholder="Kepala Biro Administrasi / Sekretaris Jendral LPSK"
                                   value="<?php echo $nama_pejabat; ?>"/></td>
                        <input type="hidden" class="type" id="nip_pejabat" name="nip_pejabat"
                               value="<?php echo $nip_pejabat; ?>">
                    </tr>
                    <tr>
                        <td width='200'>Alamat Email <?php echo form_error('email'); ?></td>
                        <td><input type="text" class="form-control" name="email" id="email"
                                   placeholder="Alamat Email Kantor / Pribadi" value="<?php echo $email; ?>"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <button type="submit" class="btn btn-danger"><i
                                        class="fa fa-floppy-o"></i> <?php echo $button; ?></button>
                            <a href="<?php echo site_url('trans_cuti'); ?>" class="btn btn-info"><i
                                        class="fa fa-sign-out"></i> Kembali</a></td>
                    </tr>
                </table>
            </form>
        </div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/daterangepicker.js"></script>
<script>
    $(function () {
        //autocomplete
        $("#nama_pejabat").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_pejabat").val("" + suggestion.value);
                $("#nip_pejabat").val("" + suggestion.id);
            }
        });

        $("#nama_atasan").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_atasan").val("" + suggestion.value);
                $("#nip_atasan").val("" + suggestion.id);
            }
        });

        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        var maxLimitDate = new Date(nowDate.getFullYear() + 1, nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        $('input[name="wkt_cuti"]').daterangepicker({
            opens: 'left',
            minDate: today,
            maxDate: maxLimitDate,
            locale: {
                format: 'DD/MM/YYYY'
            },
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                .format('YYYY-MM-DD'));
        });
    });
</script>