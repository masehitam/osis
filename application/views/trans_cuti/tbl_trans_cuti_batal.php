<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Pembatalan Pengjuan Cuti</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">
                <table class='table table-bordered'>
                    <tr>
                        <td width='150'>Alasan Pembatalan <?php echo form_error('alasan') ?></td>
                        <td>
                            <textarea class="form-control" rows="3" name="alasan" id="alasan"
                                      placeholder="Alasan Pembatalan"><?php echo $alasan; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                            <button type="submit" class="btn btn-danger"><i
                                        class="fa fa-floppy-o"></i> <?php echo $button ?></button>
                            <a href="<?php echo site_url('trans_cuti') ?>" class="btn btn-info"><i
                                        class="fa fa-sign-out"></i> Kembali</a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
</div>
</div>