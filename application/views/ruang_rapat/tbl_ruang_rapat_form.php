<div class="content-wrapper">
    <section class="content">
        <?php //echo alert('alert-info', 'Selamat', 'Data Berhasil Diperbaharui');?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Form Kelola Ruang Rapat</h3>
                    </div>
                    <?php echo validation_errors(); ?>
                    <div class="box-body">
                        <form action="<?php echo $action; ?>" method="post">
                            <table class='table table-bordered'>
                                <tr>
                                    <td width='200'>Ruangan
                                        <?php echo form_error('ruangan') ?>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="ruangan" id="ruangan"
                                               placeholder="Ruangan" value="<?php echo $ruangan; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td width='200'>Pengelola
                                        <?php echo form_error('ruangan') ?>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="pengelola" id="pengelola"
                                               placeholder="Pengelola Ruangan" value="<?php echo $pengelola; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                <tr>
                                    <td></td>
                                    <td>
                                        <input type="hidden" id="id_pengelola" name="id_pengelola"
                                               value="<?php echo $id_pengelola; ?>"/>
                                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                            <?php echo $button ?>
                                        </button>
                                        <a href="<?php echo site_url('ruang_rapat') ?>" class="btn btn-info"><i
                                                    class="fa fa-sign-out"></i> Kembali</a>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- ./wrapper -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.js"></script>
<script>
    $(function () {
        //autocomplete
        $("#pengelola").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#pengelola").val("" + suggestion.value);
                $("#id_pengelola").val("" + suggestion.id);
            }
        });
    });
</script>