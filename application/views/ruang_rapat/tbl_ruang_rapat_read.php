<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Pengelolaan Ruang Rapat</h3>
            </div>
            <table class="table">
                <tr>
                    <td style="width: 200px">Nama Ruang Rapat</td>
                    <td><?php echo $ruangan; ?></td>
                </tr>
                <tr>
                    <td style="width: 200px">Nama Pengelola</td>
                    <td><?php echo $pengelola; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="<?php echo site_url('ruang_rapat'); ?>" class="btn btn-default" onclick="goBack()"><i
                                    class="fa fa-sign-out"></i> Kembali</a>
                    </td>
                </tr>
            </table>
        </div>
    </section>
</div>