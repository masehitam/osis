<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Riwayat Pengajuan Peminjaman BMN</h3>
                    </div>
                    <div class="box-body">
                        <div style="padding-bottom: 10px;">
                            <?php echo $this->session->userdata('message') != '' ? $this->session->userdata('message') : ''; ?>
                        </div>
                        <!-- <div style="padding-bottom: 10px;">
                            <?php //echo anchor(site_url('trans_bmn/create'), '<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-danger btn-sm"'); ?>
                        </div> -->
                        <table class="table table-bordered table-striped" id="mytable">
                            <thead>
                            <tr>
                                <th width="30px">No</th>
                                <th width="70px">Peminjam</th>
                                <th>Nama Barang</th>
                                <th width="100px">Tanggal Penggunaan</th>
                                <th width="70px">Status</th>
                                <th width="75px">Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $(' #mytable_filter input').off('.DT').on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {
                "url": "<?php echo base_url('trans_bmn/json_persetujuan'); ?>",
                "type": "POST"
            },
            columns: [{
                "data": "id",
                "orderable": false
            },
                {
                    "data": "nama"
                },
                {
                    "data": "nama_bmn"
                },
                {
                    "data": "tgl_penggunaan"
                },
                {
                    "data": "status",
                    "render": function (data, type, row) {
                        if (data == 'DIBUAT') {
                            return 'Dibuat';
                        } else if (data == 'DIBATALKAN') {
                            return 'Dibatalkan';
                        } else if (data == 'AJUKAN_1') {
                            return 'Diajukan ke Atasan';
                        } else if (data == 'AJUKAN_2') {
                            return 'Diajukan ke Pejabat';
                        } else if (data == 'PERLU_VALIDASI') {
                            return 'Diajukan ke Kepegawaian';
                        } else if (data == 'GAGAL_VALIDASI') {
                            return 'Ditolak Kepegawaian';
                        } else if (data == 'TOLAK_1') {
                            return 'Ditolak Atasan';
                        } else if (data == 'TOLAK_2') {
                            return 'Ditolak Pejbat';
                        } else if (data == 'DISETUJUI') {
                            return 'Disetujui';
                        } else if (data == 'DIKEMBALIKAN') {
                            return 'Dikembalikan';
                        } else {
                            return data;
                        }
                    },
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page *
                    length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>