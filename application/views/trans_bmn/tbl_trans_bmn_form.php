<div class="content-wrapper">
    <section class="content">
        <?php //echo alert('alert-info', 'Selamat', 'Data Berhasil Diperbaharui');?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Form Pengajuan Penggunaan BMN</h3>
                    </div>
                    <?php //echo validation_errors();?>
                    <div class="box-body">
                        <form action="<?php echo $action; ?>" method="post">
                            <table class='table table-bordered'>
                                <tr>
                                    <td width='200'>Nama Barang <?php echo form_error('nama_bmn'); ?></td>
                                    <td>
                                        <input type="text" class="form-control" name="nama_bmn" id="nama_bmn"
                                               placeholder="Nama BMN" value="<?php echo $nama_bmn; ?>"/>
                                        <input type="hidden" class="type" id="id_bmn" name="id_bmn"
                                               value="<?php echo $id_bmn; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td width='200'>Keperluan Kegiatan<?php echo form_error('keperluan'); ?></td>
                                    <td><textarea class="form-control" rows="3" name="keperluan" id="keperluan"
                                                  placeholder="Keperluan"><?php echo $keperluan; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td width='200'>Tanggal Penggunaan <?php echo form_error('tgl_penggunaan'); ?></td>
                                    <td><input type="text" class="form-control" name="tgl_penggunaan"
                                               id="tgl_penggunaan" placeholder="Tgl Penggunaan"
                                               value="<?php echo $tgl_penggunaan; ?>"/></td>
                                </tr>
                                <tr>
                                    <td width='200'>Lokasi Kegiatan<?php echo form_error('lokasi'); ?></td>
                                    <td><textarea class="form-control" rows="3" name="lokasi" id="lokasi"
                                                  placeholder="Lokasi"><?php echo $lokasi; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td width='200'>Keterangan <?php echo form_error('keterangan'); ?></td>
                                    <td><textarea class="form-control" rows="3" name="keterangan" id="keterangan"
                                                  placeholder="Keterangan"><?php echo $keterangan; ?></textarea></td>
                                </tr>
                                <tr>
                                    <td width='200'>Nama Atasan<?php echo form_error('nama_atasan'); ?></td>
                                    <td><input type="text" class="form-control" name="nama_atasan" id="nama_atasan"
                                               placeholder="Nama Atasan" value="<?php echo $nama_atasan; ?>"/></td>
                                    <input type="hidden" class="type" id="id_atasan" name="id_atasan"
                                           value="<?php echo $id_atasan; ?>">
                                </tr>
                                <tr>
                                    <td width='200'>Nama Pejabat Berwenang<?php echo form_error('nama_krt'); ?></td>
                                    <td><input readonly="readonly" type="text" class="form-control" name="nama_krt"
                                               id="nama_krt"
                                               placeholder="Nama Pejabat Berwenang"
                                               value="<?php echo $nama_krt; ?>"/></td>
                                    <input type="hidden" class="type" id="id_krt" name="id_krt"
                                           value="<?php echo $id_krt; ?>">
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="hidden" name="id" value="<?php echo $id; ?>"/>
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i>
                                            <?php echo $button; ?></button>
                                        <a href="<?php echo site_url('trans_bmn'); ?>" class="btn btn-info"><i
                                                    class="fa fa-sign-out"></i> Kembali</a></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- ./wrapper -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/daterangepicker/daterangepicker.js"></script>
<script>
    $(function () {
        //autocomplete
        $("#nama_krt").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_krt").val("" + suggestion.value);
                $("#id_krt").val("" + suggestion.id);
            }
        });

        $("#nama_atasan").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/master_biodata/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_atasan").val("" + suggestion.value);
                $("#id_atasan").val("" + suggestion.id);
            }
        });

        $("#nama_bmn").devbridgeAutocomplete({
            serviceUrl: "<?php echo site_url('api/bmn/get_nama_autocomplete'); ?>",
            dataType: "JSON", // Tipe data JSON.
            onSelect: function (suggestion) {
                $("#nama_bmn").val("" + suggestion.value);
                $("#id_bmn").val("" + suggestion.id);
            }
        });

        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        var maxLimitDate = new Date(nowDate.getFullYear() + 1, nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        $('input[name="tgl_penggunaan"]').daterangepicker({
            opens: 'left',
            minDate: today,
            maxDate: maxLimitDate,
            locale: {
                format: 'DD/MM/YYYY'
            },
        }, function (start, end, label) {
            // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
            //     .format('YYYY-MM-DD'));
        });
    });
</script>