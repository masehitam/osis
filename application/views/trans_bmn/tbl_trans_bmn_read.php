<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Pengajuan Penggunaan BMN</h3>
            </div>
            <table class="table">
                <tr>
                    <td style="width: 200px">Nama Peminjam</td>
                    <td><?php echo $nama; ?></td>
                </tr>
                <tr>
                    <td>Nama Barang</td>
                    <td><?php echo $nama_bmn; ?></td>
                </tr>
                <tr>
                    <td>Tanggal Penggunaan</td>
                    <td><?php echo $tgl_penggunaan; ?></td>
                </tr>
                <tr>
                    <td>Keperluan</td>
                    <td><?php echo $keperluan; ?></td>
                </tr>
                <tr>
                    <td>Lokasi</td>
                    <td><?php echo $lokasi; ?></td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td><?php echo $keterangan; ?></td>
                </tr>
                <?php
                if (isset($alasan_pembatalan) && ($status == 'DIBATALKAN')) :
                    ?>
                    <tr>
                        <td>Alasan Pembatalan</td>
                        <td>
                            <?php
                            echo $alasan_pembatalan;
                            ?>
                        </td>
                    </tr>
                <?php
                endif;
                if (isset($alasan_penolakan) && (($status == 'TOLAK_1') ||
                        ($status == 'TOLAK_2') ||
                        ($status == 'GAGAL_VALIDASI')
                    )
                ):
                    ?>
                    <tr>
                        <td>Alasan Penolakan</td>
                        <td>
                            <?php
                            echo $alasan_penolakan;
                            if ($status == "TOLAK_1") {
                                if (isset($nama_atasan)) {
                                    echo " ($nama_atasan)";
                                }
                            } elseif ($status == "TOLAK_2") {
                                if (isset($nama_krt)) {
                                    echo " ($nama_krt)";
                                }
                            } else {
                                echo " (Kepegawaian)";
                            }
                            ?>
                        </td>
                    </tr>
                <?php
                elseif (($this->session->userdata()['nip'] != $id_user) &&
                        (
                            ($status != 'AJUKAN_1') 
                            || ($status != 'AJUKAN_2') 
                        )
                ) :
                    ?>
                    <tr>
                        <td width='200'>Alasan</td>
                        <td>
                            <form method="POST" action="<?php echo site_url('trans_bmn/down/' . $id); ?>">
                                <textarea class="form-control" rows="3" name="alasan_penolakan" id="alasan_penolakan"
                                          placeholder="Alasan"></textarea>
                        </td>
                    </tr>
                <?php
                endif;
                ?>
                <tr>
                    <td>Tindakan</td>
                    <td>
                        <!-- Yang mengajukan -->
                        <?php if (($this->session->userdata()['nip'] == $id_user) && ($status == 'DIBUAT')) : ?>
                            <a style="width: 200px; margin: 8px" href="<?php echo site_url('trans_bmn/up/' . $id); ?>"
                               class="btn btn-success"><i class="fa fa-send-o"></i>
                                Ajukan</a>
                            <!-- Atasan -->
                        <?php elseif (($this->session->userdata()['nip'] == $id_atasan) && ($status == 'AJUKAN_1')) : ?>
                            <a style="width: 200px; margin: 8px" href="<?php echo site_url('trans_bmn/up/' . $id); ?>"
                               class="btn btn-primary"><i class="fa fa-check-square-o"></i>
                                Setujui</a>
                            <!--                            <a style="width: 200px; margin: 8px" href="--><?php //echo site_url('trans_bmn/down/'.$id); ?><!--" class="btn btn-danger"><i class="fa fa-ban"></i>-->
                            <!--                            Tolak</a>-->
                            <button type="submit" style="width: 200px; margin: 8px" class="btn btn-danger"><i
                                        class="fa fa-ban"></i> Tolak
                            </button>
                            </form>
                        <?php elseif (($this->session->userdata()['nip'] == $id_krt) && ($status == 'AJUKAN_2')) : ?>
                            <a style="width: 200px; margin: 8px" href="<?php echo site_url('trans_bmn/up/' . $id); ?>"
                               class="btn btn-primary"><i class="fa fa-check-square-o"></i>
                                Setujui</a>
                            <!--                            <a style="width: 200px; margin: 8px" href="--><?php //echo site_url('trans_bmn/down/'.$id); ?><!--" class="btn btn-danger"><i class="fa fa-ban"></i>-->
                            <!--                            Tolak</a>-->
                            <button type="submit" style="width: 200px; margin: 8px" class="btn btn-danger"><i
                                        class="fa fa-ban"></i> Tolak
                            </button>
                            </form>
                        <?php endif; ?>
                        <?php if (($this->session->userdata()['nip'] == $id_krt) && ($status == 'DISETUJUI')) : ?>
                            <a style="width: 200px; margin: 8px"
                               href="<?php echo site_url('perjanjian_bmn/buat_baru/' . $id); ?>" class="btn btn-info"><i
                                        class="fa fa-eye"></i>
                                Lihat / Ajukan Perjanjian</a>
                        <?php endif; ?>
                        <a style="width: 200px; margin: 8px" href="<?php echo site_url('cetak/form_bmn/' . $id); ?>"
                           target="_blank" class="btn btn-info"><i class="fa fa-print"></i>
                            Cetak</a>
                        <?php if ($page == 'index'): ?>
                            <a style="width: 200px; margin: 8px" href="<?php echo site_url('trans_bmn'); ?>"
                               class="btn btn-default" onclick="goBack()"><i
                                        class="fa fa-sign-out"></i> Kembali</a>
                        <?php else: ?>
                            <a style="width: 200px; margin: 8px"
                               href="<?php echo site_url('trans_bmn/index_persetujuan'); ?>" class="btn btn-default"
                               onclick="goBack()"><i
                                        class="fa fa-sign-out"></i> Kembali</a>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>
    </section>
</div>