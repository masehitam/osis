<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sisa_cuti_baru extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Sisa_cuti_baru_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'sisa_cuti_baru/tbl_sisa_cuti_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Sisa_cuti_baru_model->json();
    }

    public function read($id)
    {
        $row = $this->Sisa_cuti_baru_model->get_by_id($id);
        if ($row) {
            $data = array(
                'nip' => $row->nip,
                'tahun' => $row->tahun,
                'sisa' => $row->sisa,
                'id' => $row->id,
            );
            $this->template->load('template', 'sisa_cuti_baru/tbl_sisa_cuti_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sisa_cuti_baru'));
        }
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nip' => $this->input->post('nip', TRUE),
                'tahun' => $this->input->post('tahun', TRUE),
                'sisa' => $this->input->post('sisa', TRUE),
            );

            $this->Sisa_cuti_baru_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('sisa_cuti_baru'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('tahun', 'tahun', 'trim|required');
        $this->form_validation->set_rules('sisa', 'sisa', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function create()
    {
        $data = array(
            'button' => 'Simpan',
            'action' => site_url('sisa_cuti_baru/create_action'),
            'nip' => set_value('nip'),
            'tahun' => set_value('tahun'),
            'sisa' => set_value('sisa'),
            'id' => set_value('id'),
        );
        $this->template->load('template', 'sisa_cuti_baru/tbl_sisa_cuti_form', $data);
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'nip' => $this->input->post('nip', TRUE),
                'tahun' => $this->input->post('tahun', TRUE),
                'sisa' => $this->input->post('sisa', TRUE),
            );

            $this->Sisa_cuti_baru_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('sisa_cuti_baru'));
        }
    }

    public function update($id)
    {
        $row = $this->Sisa_cuti_baru_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('sisa_cuti_baru/update_action'),
                'nip' => set_value('nip', $row->nip),
                'tahun' => set_value('tahun', $row->tahun),
                'sisa' => set_value('sisa', $row->sisa),
                'id' => set_value('id', $row->id),
            );
            $this->template->load('template', 'sisa_cuti_baru/tbl_sisa_cuti_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sisa_cuti_baru'));
        }
    }

    public function delete($id)
    {
        $row = $this->Sisa_cuti_baru_model->get_by_id($id);

        if ($row) {
            $this->Sisa_cuti_baru_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('sisa_cuti_baru'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sisa_cuti_baru'));
        }
    }

}

/* End of file Sisa_cuti_baru.php */
/* Location: ./application/controllers/Sisa_cuti_baru.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-07-01 08:57:15 */
/* http://harviacode.com */