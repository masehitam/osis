<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Trans_cuti extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Trans_cuti_model');
        $this->load->model('Master_biodata_model');
        $this->load->model('Detail_pegawai_model');
        $this->load->model('Sisa_cuti_baru_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'trans_cuti/tbl_trans_cuti_list');
    }

    public function index_persetujuan()
    {
        $this->template->load('template', 'trans_cuti/tbl_trans_cuti_persetujuan_list');
    }

    public function index_validasi()
    {
        $this->template->load('template', 'trans_cuti/tbl_trans_cuti_validasi_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        $nip = $this->session->userdata()['nip'];
        echo $this->Trans_cuti_model->json($nip);
    }

    public function json_persetujuan()
    {
        header('Content-Type: application/json');
        $nip = $this->session->userdata()['nip'];
        echo $this->Trans_cuti_model->json_persetujuan($nip);
    }

    public function json_validasi()
    {
        header('Content-Type: application/json');
        echo $this->Trans_cuti_model->json_validasi();
    }

    public function read($id, $page = 'index')
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        $bio = $this->Master_biodata_model->get_by_nip($row->nip);
        $data_pegawai = $this->Master_biodata_model->get_by_nip($row->nip);
        $data_atasan = $this->Master_biodata_model->get_by_nip($row->nip_atasan);
        $data_pejabat = $this->Master_biodata_model->get_by_nip($row->nip_pejabat); // pembuat cuti
        $sisa_cuti_tahun_ini = $this->Sisa_cuti_baru_model->get_by_nip_tahun($row->nip, date("Y"));
        $sisa_cuti_tahun_kemarin = $this->Sisa_cuti_baru_model->get_by_nip_tahun($row->nip, date("Y", strtotime("-1 year")));

        if ($data_pegawai->unit_id_es1 != 0
            && $data_pegawai->unit_id_es2 != 0
            && $data_pegawai->unit_id_es3 == 0
            && $data_pegawai->unit_id_es4 == 0) {
            $es = '2';
        } elseif ($data_pegawai->unit_id_es1 != 0
            && $data_pegawai->unit_id_es2 != 0
            && $data_pegawai->unit_id_es3 != 0
            && $data_pegawai->unit_id_es4 == 0) {
            $es = '3';
        } else {
            $es = '4';
        }

        if ($row) {
            $data = array(
                'id' => $row->id,
                'nip' => $row->nip,
                'nama' => $bio->nama,
                'jenis_cuti' => $row->jenis_cuti,
                'tgl_mulai' => $row->tgl_mulai,
                'tgl_selesai' => $row->tgl_selesai,
                'alamat' => $row->alamat,
                'nip_atasan' => $row->nip_atasan,
                'nip_pejabat' => $row->nip_pejabat,
                'alasan' => $row->alasan,
                'status' => $row->status,
                'wkt_cuti' => $row->wkt_cuti,
                'date_created' => $row->date_created,
                'date_diajukan' => $row->date_diajukan,
                'date_reaksi_satu' => $row->date_reaksi_satu,
                'date_reaksi_dua' => $row->date_reaksi_dua,
                'email' => $row->email,
                'alasan_pembatalan' => $row->alasan_pembatalan,
                'alasan_penolakan' => $row->alasan_penolakan,
                'sisa_tahun_ini' => $sisa_cuti_tahun_ini->sisa,
                'sisa_tahun_kemarin' => $sisa_cuti_tahun_kemarin->sisa,
                'jumlah_hari' => $row->jumlah_hari,
                'page' => $page,
                'es' => $es,
            );
            if (isset($data_atasan)) {
                $data['nama_atasan'] = $data_atasan->nama;
            }
            if (isset($data_pejabat)) {
                $data['nama_pejabat'] = $data_pejabat->nama;
            }
            $this->template->load('template', 'trans_cuti/tbl_trans_cuti_read', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $tgl_penggunaan = $this->input->post('wkt_cuti', true);
            $tgl_penggunaan2 = explode(' - ', $tgl_penggunaan);

            $date1 = explode('/', $tgl_penggunaan2[0]);
            $date2 = explode('/', $tgl_penggunaan2[1]);

            $finalDate1 = $date1[2] . '-' . $date1[1] . '-' . $date1[0];
            $finalDate2 = $date2[2] . '-' . $date2[1] . '-' . $date2[0];

            $nip = $this->session->userdata()['nip'];

            $jabatan = $this->Detail_pegawai_model->get_by_nip($nip)->nmjabatan;
            if ($jabatan == 'Tenaga Pengamanan'){
                $jumlahHari = $this->menghitung_hari($finalDate1, $finalDate2, true);
            }else {
                $jumlahHari = $this->menghitung_hari($finalDate1, $finalDate2);
            }

            $sisa_cuti_tahun_ini = $this->Sisa_cuti_baru_model->get_by_nip_tahun($nip, date("Y"))->sisa;
            $sisa_cuti_tahun_kemarin = $this->Sisa_cuti_baru_model->get_by_nip_tahun($nip, date("Y", strtotime("-1 year")))->sisa;

            $sisa_cuti = $sisa_cuti_tahun_ini + $sisa_cuti_tahun_kemarin;

            $jenis_cuti = $this->input->post('jenis_cuti', true);
            if ($sisa_cuti >= $jumlahHari || strcmp($jenis_cuti, 'Cuti Tahunan') != 0) {
                $email = $this->input->post('email', true);
                if (stripos($email, 'lpsk.go.id') !== false) {
                    // tambah ke email kantor
                    $bio['email_kantor'] = $email;
                    $this->Master_biodata_model->update_by_nip($this->session->userdata()['nip'], $bio);
                } else {
                    //tambah ke email pribadi
                    $bio['email_pribadi'] = $email;
                    $this->Master_biodata_model->update_by_nip($this->session->userdata()['nip'], $bio);
                }

                $data = array(
                    'nip' => $nip,
                    'jenis_cuti' => $this->input->post('jenis_cuti', true),
                    'tgl_mulai' => strtotime($finalDate1),
                    'tgl_selesai' => strtotime($finalDate2),
                    'alamat' => $this->input->post('alamat', true),
                    'nip_atasan' => $this->input->post('nip_atasan', true),
                    'nip_pejabat' => $this->input->post('nip_pejabat', true),
                    'alasan' => $this->input->post('alasan', true),
                    'wkt_cuti' => $this->input->post('wkt_cuti', true),
                    'email' => $this->input->post('email', true),
                    'jumlah_hari' => $jumlahHari,
                );

                $this->Trans_cuti_model->insert($data);
                $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Ditambahkan</div>');
                redirect(site_url('trans_cuti'));
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-error" role="alert"> Sisa Cuti Tidak Mencukupi</div>');
                redirect(site_url('trans_cuti'));
            }
        }
    }

    public function _rules()
    {
        // $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('jenis_cuti', 'jenis cuti', 'trim|required');
        // $this->form_validation->set_rules('tgl_mulai', 'tgl mulai', 'trim|required');
        // $this->form_validation->set_rules('tgl_selesai', 'tgl selesai', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        // $this->form_validation->set_rules('nip_atasan', 'nip atasan', 'trim|required');
        // $this->form_validation->set_rules('nip_pejabat', 'nip pejabat', 'trim|required');
        $this->form_validation->set_rules('alasan', 'alasan', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        // $this->form_validation->set_rules('status', 'status', 'trim|required');
        $this->form_validation->set_rules('wkt_cuti', 'wkt cuti', 'trim|required');
        // $this->form_validation->set_rules('date_created', 'date created', 'trim|required');
        // $this->form_validation->set_rules('date_diajukan', 'date diajukan', 'trim|required');
        // $this->form_validation->set_rules('date_reaksi_satu', 'date reaksi satu', 'trim|required');
        // $this->form_validation->set_rules('date_reaksi_dua', 'date reaksi dua', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function create()
    {
        $nip = $this->session->userdata()['nip'];
        $data_pegawai = $this->Master_biodata_model->get_by_nip($nip);

        $email = $data_pegawai->email_kantor != null ? $data_pegawai->email_kantor : "";
        if (empty($email)) {
            $email = $data_pegawai->email_pribadi != null ? $data_pegawai->email_pribadi : "";
        }

        $data = array(
            'button' => 'Simpan',
            'action' => site_url('trans_cuti/create_action'),
            'id' => set_value('id'),
            // 'nip' => set_value('nip'),
            'jenis_cuti' => set_value('jenis_cuti'),
            // 'tgl_mulai' => set_value('tgl_mulai'),
            // 'tgl_selesai' => set_value('tgl_selesai'),
            'alamat' => set_value('alamat'),
            'nip_atasan' => set_value('nip_atasan'),
            'nama_atasan' => set_value('nama_atasan'),
            'nip_pejabat' => set_value('nip_pejabat'),
            'nama_pejabat' => set_value('nama_pejabat'),
            'alasan' => set_value('alasan'),
            'email' => set_value('email'),
            // 'status' => set_value('status'),
            'wkt_cuti' => set_value('wkt_cuti'),
            // 'date_created' => set_value('date_created'),
            // 'date_diajukan' => set_value('date_diajukan'),
            // 'date_reaksi_satu' => set_value('date_reaksi_satu'),
            // 'date_reaksi_dua' => set_value('date_reaksi_dua'),
        );

        $data['email'] = $email; //du

        if ($data_pegawai->unit_id_es1 != 0
            && $data_pegawai->unit_id_es2 != 0
            && $data_pegawai->unit_id_es3 == 0
            && $data_pegawai->unit_id_es4 == 0) {
            //$data['nama_atasan'] = 'Noor Sidharta';
            //$data['nip_atasan'] = '196409051990310004'; // Noor Sidharta
            //$data['nip_pejabat'] = '0'; //dummy

            $data['nip_atasan'] = '0'; //dummy
            $data['nama_pejabat'] = 'Noor Sidharta';
            $data['nip_pejabat'] = '196409051990310004'; // Noor Sidharta
            $this->template->load('template', 'trans_cuti/tbl_trans_cuti_form_esdua', $data);
        } elseif ($data_pegawai->unit_id_es1 != 0
            && $data_pegawai->unit_id_es2 != 0
            && $data_pegawai->unit_id_es3 != 0
            && $data_pegawai->unit_id_es4 == 0) {
            $data['nama_pejabat'] = 'Noor Sidharta';
            $data['nip_pejabat'] = '196409051990310004'; // Noor Sidharta
            $this->template->load('template', 'trans_cuti/tbl_trans_cuti_form_estiga', $data);
        } else {
            $data['nama_pejabat'] = 'Handari Restu Dewi';
            $data['nip_pejabat'] = '196404131991032001'; // Handari Restu Dewi
            $this->template->load('template', 'trans_cuti/tbl_trans_cuti_form_esempat', $data);
        }
    }

    public function menghitung_hari($mulai, $selesai, $workOnWeekEnd = false)
    {
        $start = new DateTime($mulai);
        $end = new DateTime($selesai);
        // otherwise the  end date is excluded (bug?)
        $end->modify('+1 day');
        $interval = $end->diff($start);
        // total days
        $days = $interval->days;
        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);
        // best stored as array, so you can add more than one
        //$holidays = ['2012-09-07'];
        if (!$workOnWeekEnd){
            foreach ($period as $dt) {
                $curr = $dt->format('D');
                // substract if Saturday or Sunday
                if ('Sat' == $curr || 'Sun' == $curr) {
                    --$days;
                }
                // (optional) for the updated question
                // elseif (in_array($dt->format('Y-m-d'), $holidays)) {
                //     --$days;
                // }
            }
        }

        return $days;
    }

    public function update_action()
    {
        $id = $this->input->post('id', true);
        $row = $this->Trans_cuti_model->get_by_id($id);
        if ($row->status != 'DIBUAT') {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data yang sudah diajukan tidak dapat diperbarui</div>');
            redirect(site_url('trans_cuti'));
        }

        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $tgl_penggunaan = $this->input->post('wkt_cuti', true);
            $tgl_penggunaan2 = explode(' - ', $tgl_penggunaan);

            $date1 = explode('/', $tgl_penggunaan2[0]);
            $date2 = explode('/', $tgl_penggunaan2[1]);

            $finalDate1 = $date1[2] . '-' . $date1[1] . '-' . $date1[0];
            $finalDate2 = $date2[2] . '-' . $date2[1] . '-' . $date2[0];

            $jabatan = $this->Detail_pegawai_model->get_by_nip($row->nip)->nmjabatan;
            if ($jabatan == 'Tenaga Pengamanan'){
                $jumlahHari = $this->menghitung_hari($finalDate1, $finalDate2, true);
            }else{
                $jumlahHari = $this->menghitung_hari($finalDate1, $finalDate2);
            }

            $data = array(
                //'nip' => $this->session->userdata()['nip'],
                'jenis_cuti' => $this->input->post('jenis_cuti', true),
                'tgl_mulai' => strtotime($finalDate1),
                'tgl_selesai' => ($finalDate2),
                'alamat' => $this->input->post('alamat', true),
                'nip_atasan' => $this->input->post('nip_atasan', true),
                'nip_pejabat' => $this->input->post('nip_pejabat', true),
                'alasan' => $this->input->post('alasan', true),
                'email' => $this->input->post('email', true),
                'wkt_cuti' => $this->input->post('wkt_cuti', true),
                'jumlah_hari' => $jumlahHari,
            );

            $this->Trans_cuti_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Diperbarui</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function update($id)
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        $this->_rule_update($row);
        $bio1 = $this->Master_biodata_model->get_by_nip($row->nip_atasan);
        if ($row->nip_pejabat != null) {
            $bio2 = $this->Master_biodata_model->get_by_nip($row->nip_pejabat);
        }

        if ($row) {

            $data_pegawai = $this->Master_biodata_model->get_by_nip($row->nip);

            if ($data_pegawai->unit_id_es1 != 0
                && $data_pegawai->unit_id_es2 != 0
                && $data_pegawai->unit_id_es3 == 0
                && $data_pegawai->unit_id_es4 == 0) {
                $data = array(
                    'button' => 'Update',
                    'action' => site_url('trans_cuti/update_action'),
                    'id' => set_value('id', $row->id),
                    'nip' => set_value('nip', $row->nip),
                    'jenis_cuti' => set_value('jenis_cuti', $row->jenis_cuti),
                    'tgl_mulai' => set_value('tgl_mulai', $row->tgl_mulai),
                    'tgl_selesai' => set_value('tgl_selesai', $row->tgl_selesai),
                    'alamat' => set_value('alamat', $row->alamat),
                    'nip_atasan' => set_value('nip_atasan', $row->nip_atasan),
                    'nama_atasan' => set_value('nama_atasan', $bio1->nama),
                    'nip_pejabat' => set_value('nip_pejabat', $row->nip_pejabat),
                    'nama_pejabat' => set_value('nama_pejabat', '0'),
                    'alasan' => set_value('alasan', $row->alasan),
                    'status' => set_value('status', $row->status),
                    'email' => set_value('email', $row->email),
                    'wkt_cuti' => set_value('wkt_cuti', $row->wkt_cuti),
                    'date_created' => set_value('date_created', $row->date_created),
                    'date_diajukan' => set_value('date_diajukan', $row->date_diajukan),
                    'date_reaksi_satu' => set_value('date_reaksi_satu', $row->date_reaksi_satu),
                    'date_reaksi_dua' => set_value('date_reaksi_dua', $row->date_reaksi_dua),
                ); //dummy
                $this->template->load('template', 'trans_cuti/tbl_trans_cuti_form_esdua', $data);
            } elseif ($data_pegawai->unit_id_es1 != 0
                && $data_pegawai->unit_id_es2 != 0
                && $data_pegawai->unit_id_es3 != 0
                && $data_pegawai->unit_id_es4 == 0) {
                $data = array(
                    'button' => 'Update',
                    'action' => site_url('trans_cuti/update_action'),
                    'id' => set_value('id', $row->id),
                    'nip' => set_value('nip', $row->nip),
                    'jenis_cuti' => set_value('jenis_cuti', $row->jenis_cuti),
                    'tgl_mulai' => set_value('tgl_mulai', $row->tgl_mulai),
                    'tgl_selesai' => set_value('tgl_selesai', $row->tgl_selesai),
                    'alamat' => set_value('alamat', $row->alamat),
                    'nip_atasan' => set_value('nip_atasan', $row->nip_atasan),
                    'nama_atasan' => set_value('nama_atasan', $bio1->nama),
                    'nip_pejabat' => set_value('nip_pejabat', $row->nip_pejabat),
                    'nama_pejabat' => set_value('nama_pejabat', $bio2->nama),
                    'alasan' => set_value('alasan', $row->alasan),
                    'status' => set_value('status', $row->status),
                    'email' => set_value('status', $row->email),
                    'wkt_cuti' => set_value('wkt_cuti', $row->wkt_cuti),
                    'date_created' => set_value('date_created', $row->date_created),
                    'date_diajukan' => set_value('date_diajukan', $row->date_diajukan),
                    'date_reaksi_satu' => set_value('date_reaksi_satu', $row->date_reaksi_satu),
                    'date_reaksi_dua' => set_value('date_reaksi_dua', $row->date_reaksi_dua),
                );
                $this->template->load('template', 'trans_cuti/tbl_trans_cuti_form_estiga', $data);
            } else {
                $data = array(
                    'button' => 'Update',
                    'action' => site_url('trans_cuti/update_action'),
                    'id' => set_value('id', $row->id),
                    'nip' => set_value('nip', $row->nip),
                    'jenis_cuti' => set_value('jenis_cuti', $row->jenis_cuti),
                    'tgl_mulai' => set_value('tgl_mulai', $row->tgl_mulai),
                    'tgl_selesai' => set_value('tgl_selesai', $row->tgl_selesai),
                    'alamat' => set_value('alamat', $row->alamat),
                    'nip_atasan' => set_value('nip_atasan', $row->nip_atasan),
                    'nama_atasan' => set_value('nama_atasan', $bio1->nama),
                    'nip_pejabat' => set_value('nip_pejabat', $row->nip_pejabat),
                    'nama_pejabat' => set_value('nama_pejabat', $bio2->nama),
                    'alasan' => set_value('alasan', $row->alasan),
                    'status' => set_value('status', $row->status),
                    'email' => set_value('status', $row->email),
                    'wkt_cuti' => set_value('wkt_cuti', $row->wkt_cuti),
                    'date_created' => set_value('date_created', $row->date_created),
                    'date_diajukan' => set_value('date_diajukan', $row->date_diajukan),
                    'date_reaksi_satu' => set_value('date_reaksi_satu', $row->date_reaksi_satu),
                    'date_reaksi_dua' => set_value('date_reaksi_dua', $row->date_reaksi_dua),
                );
                $this->template->load('template', 'trans_cuti/tbl_trans_cuti_form_esempat', $data);
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function _rule_update($row)
    {
        if ($row->status != "DIBUAT") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Detail Pengajuan Cuti yang Sudah Diajukan Tidak Dapat Diperbarui</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function delete($id)
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        $this->_rule_hapus($row);

        if ($row) {
            $this->Trans_cuti_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Dihapus</div>');
            redirect(site_url('trans_cuti'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function _rule_hapus($row)
    {
        if ($row->status == "DISETUJUI") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Status Pengajuan Cuti Sudah Disetujui Tidak Dapat Dihapus</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function pembatalan($id)
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        $this->_rule_pembatalan($row);
        $this->_rule_waktu($row);

        if ($row) {
            $data = array(
                'button' => 'Batalkan',
                'action' => site_url('trans_cuti/pembatalan_action'),
                'id' => set_value('id', $row->id),
                'alasan' => set_value('kegiatan'),
            );
            $this->template->load('template', 'trans_cuti/tbl_trans_cuti_batal', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function _rule_pembatalan($row)
    {
        if ($row->status == "DIBATALKAN") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Status Pengajuan Cuti Sudah Dibatalkan</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function _rule_waktu($row)
    {
        $date = new DateTime();
        if ($date->getTimestamp() >= $row->tgl_mulai) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Pengajuan Cuti tidak dapat dibatalkan apabila telah memasuki waktu Pengajuan Cuti</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function pembatalan_action()
    {
        $this->_pembatalan_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $data = array(
                'status' => 'DIBATALKAN',
                'alasan_pembatalan' => $this->input->post('alasan', true),
            );

            $this->Trans_cuti_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Diperbarui</div>');
            redirect(site_url('trans_cuti'));
        }
    }

    public function _pembatalan_rules()
    {
        $this->form_validation->set_rules('alasan', 'kegiatan', 'trim|required');
    }

    public function up($id, $es = '1')
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        $nip = $this->session->userdata()['nip'];

        $jenis_cuti = $row->jenis_cuti;
        if (strcmp($jenis_cuti, 'Cuti Tahunan') == 0) {
            $isCutiTahunan = true;
        } else {
            $isCutiTahunan = false;
        }

        switch ($row->status) {
            case 'DIBUAT':
                if ($this->check_sisa_cuti($id) || !$isCutiTahunan) {
                    $data['status'] = 'AJUKAN_1';
                    $data['date_diajukan'] = date('Y-m-d H:i:s');
                    // untuk eselon 2
                    // langsung validasi kepeg
                    if ($row->nip_atasan == '0') {
                        $data['status'] = 'PERLU_VALIDASI';
                        $data['date_reaksi_satu'] = date('Y-m-d H:i:s');
                    }
                    $this->Trans_cuti_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                    redirect(base_url('trans_cuti/read/' . $id));
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Sisa Cuti Tidak Mencukupi</div>');
                    redirect(base_url('trans_cuti/read/' . $id));
                }
                break;

            case 'AJUKAN_1':
                if ($this->check_sisa_cuti($id) || !$isCutiTahunan) {
                    $data['status'] = 'PERLU_VALIDASI';
                    $data['date_reaksi_satu'] = date('Y-m-d H:i:s');
                    $this->Trans_cuti_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                    redirect(base_url('trans_cuti/read/' . $id . '/persetujuan'));
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Sisa Cuti Tidak Mencukupi</div>');
                    redirect(base_url('trans_cuti/read/' . $id . '/persetujuan'));
                }
                break;

            case 'PERLU_VALIDASI':
                if ($this->check_sisa_cuti($id) || !$isCutiTahunan) {
                    $data['status'] = 'AJUKAN_2';
                    $data['date_validasi'] = date('Y-m-d H:i:s');
                    // tadinya untuk eselon 2
                    // langsung setujui
                    // tapi diganti
                    // if ($row->nip_pejabat == '0') {
                    //     $data['status'] = 'DISETUJUI';
                    //     $data['date_reaksi_dua'] = date('Y-m-d H:i:s');
                    // }
                    $this->Trans_cuti_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                    redirect(base_url('trans_cuti/read/' . $id . '/validasi'));
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Sisa Cuti Tidak Mencukupi</div>');
                    redirect(base_url('trans_cuti/read/' . $id . '/validasi'));
                }
                break;

            case 'AJUKAN_2':
                if ($this->check_sisa_cuti($id) || !$isCutiTahunan) {
                    if ($isCutiTahunan) {
                        $this->update_sisa_cuti($id);
                    }
                    $data['status'] = 'DISETUJUI';
                    $data['date_reaksi_dua'] = date('Y-m-d H:i:s');
                    $this->Trans_cuti_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                    $this->send_mail($id, $es);
                    //$this->form_cuti($id, $es);
                    redirect(base_url('trans_cuti/read/' . $id . '/persetujuan'));
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Sisa Cuti Tidak Mencukupi</div>');
                    redirect(base_url('trans_cuti/read/' . $id . '/persetujuan'));
                }
                break;
            default:
                echo 'gagal';
                die();
                break;
        }
    }

    public function check_sisa_cuti($id)
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        $lama_cuti = $row->jumlah_hari;

        $tahunIni = date("Y");
        $tahunKemarin = date("Y", strtotime("-1 year"));

        $tahun_ini = $this->Sisa_cuti_baru_model->get_by_nip_tahun($row->nip, $tahunIni)->sisa;
        $tahun_kemarin = $this->Sisa_cuti_baru_model->get_by_nip_tahun($row->nip, $tahunKemarin)->sisa;
        $sisa_cuti = $tahun_ini + $tahun_kemarin;

        if ($sisa_cuti >= $lama_cuti) {
            return true;
        } else {
            return false;
        }
    }

    public function generate_qr_code($id_cuti, $nip_actor)
    {
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE

        $config['cacheable'] = true; //boolean, the default is true
        $config['cachedir'] = './uploads/'; //string, the default is application/cache/
        $config['errorlog'] = './uploads/'; //string, the default is application/logs/
        $config['imagedir'] = './uploads/qrcode/'; //direktori penyimpanan qr code
        $config['quality'] = true; //boolean, the default is true
        $config['size'] = '1024'; //interger, the default is 1024
        $config['black'] = array(224, 255, 255); // array, default is array(255,255,255)
        $config['white'] = array(70, 130, 180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = 'CUTI-' . $id_cuti . '_' . $nip_actor . '.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = 'CUTI-' . $id_cuti . '_' . $nip_actor; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        //$this->mahasiswa_model->simpan_mahasiswa($nim,$nama,$prodi,$image_name); //simpan ke database
        //redirect('mahasiswa'); //redirect ke mahasiswa usai simpan data
    }

    public function update_sisa_cuti($id)
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        $lama_cuti = $row->jumlah_hari;

        $tahunIni = date("Y");
        $tahunKemarin = date("Y", strtotime("-1 year"));

        $tahun_ini = $this->Sisa_cuti_baru_model->get_by_nip_tahun($row->nip, $tahunIni)->sisa;
        $tahun_kemarin = $this->Sisa_cuti_baru_model->get_by_nip_tahun($row->nip, $tahunKemarin)->sisa;
        $sisa_cuti = $tahun_ini + $tahun_kemarin;

        // if ($this->check_sisa_cuti($id)) {
        //if ($sisa_cuti >= $lama_cuti) {
        if ($tahun_kemarin >= $lama_cuti) {
            $sisa = $tahun_kemarin - $lama_cuti;
            $data['sisa'] = $sisa;

            $this->Sisa_cuti_baru_model->update_nip_tahun($row->nip, $tahunKemarin, $data);
            return true;
        } else {
            //jika tahun kemarin sudah 0
            if ($tahun_kemarin >= 0) {
                $lama_cuti = $lama_cuti - $tahun_kemarin;

                $data['sisa'] = '0';
                $this->Sisa_cuti_baru_model->update_nip_tahun($row->nip, $tahunKemarin, $data);
            }
            $sisa = $tahun_ini - $lama_cuti;
            $data['sisa'] = $sisa;

            $this->Sisa_cuti_baru_model->update_nip_tahun($row->nip, $tahunIni, $data);
            return true;
        }
        // }else{
        //     return false;
        // }
    }

    public function send_mail($id, $es = '1')
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        $from_email = "no-reply@lpsk.go.id";

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.lpsk.go.id',
            'smtp_port' => 465,
            'smtp_user' => 'simpelkan@lpsk.go.id',
            'smtp_pass' => 'Cijantung@47',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->from($from_email, "Sistem Informasi Pelayanan Kantor");
        $this->email->to($row->email);//https://www.guerrillamail.com/
        $this->email->subject('Permohonan Cuti Disetujui');
        $this->email->message('Surat cuti anda telah disetujui, anda dapat mengakses surat tersebut <a href=' . base_url() . 'cetak/form_cuti/' . $id . '/' . $es . '>Disini</a>');
        //$this->email->attach(base_url().'uploads/berkas_cuti/CUTI-'.$id.'.pdf');
        //Send mail
        if ($this->email->send()) {
            //echo "berhasil kirim ke $row->email";die();
            $this->session->set_flashdata("message", "Email berhasil terkirim.");
        } else {
            echo "gagal";
            die();
            $this->session->set_flashdata("message", "Email gagal dikirim.");
        }
    }

    public function down($id)
    {
        $row = $this->Trans_cuti_model->get_by_id($id);
        switch ($row->status) {
            case 'AJUKAN_1':
                $data['status'] = 'TOLAK_1';
                $data['date_reaksi_satu'] = date('Y-m-d H:i:s');
                $data['alasan_penolakan'] = $this->input->post('alasan_penolakan', true);
                $this->Trans_cuti_model->update($id, $data);
                redirect(base_url('trans_cuti/read/' . $id . '/persetujuan'));
                break;

            case 'PERLU_VALIDASI':
                $data['status'] = 'GAGAL_VALIDASI';
                $data['date_validasi'] = date('Y-m-d H:i:s');
                $data['alasan_penolakan'] = $this->input->post('alasan_penolakan', true);
                $this->Trans_cuti_model->update($id, $data);
                redirect(base_url('trans_cuti/read/' . $id . '/validasi'));
                break;

            case 'AJUKAN_2':
                $data['status'] = 'TOLAK_2';
                $data['date_reaksi_dua'] = date('Y-m-d H:i:s');
                $data['alasan_penolakan'] = $this->input->post('alasan_penolakan', true);
                $this->Trans_cuti_model->update($id, $data);
                redirect(base_url('trans_cuti/read/' . $id . '/persetujuan'));
                break;

            default:
                echo 'gagal';
                die();
                break;
        }
    }
}

/* End of file Trans_cuti.php */
/* Location: ./application/controllers/Trans_cuti.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-04-09 11:39:56 */
/* http://harviacode.com */
