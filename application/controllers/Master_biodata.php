<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master_biodata extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Master_biodata_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'master_biodata/master_biodata_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Master_biodata_model->json();
    }

    public function read($id)
    {
        $row = $this->Master_biodata_model->get_by_id($id);
        if ($row) {
            $data = array(
                'master_biodata_id' => $row->master_biodata_id,
                'bkn_id' => $row->bkn_id,
                'nip' => $row->nip,
                'nama_cetak' => $row->nama_cetak,
                'nama' => $row->nama,
                'jenis_jab_id' => $row->jenis_jab_id,
                'struk_id' => $row->struk_id,
                'fung_id' => $row->fung_id,
                'unit_id_es1' => $row->unit_id_es1,
                'unit_id_es2' => $row->unit_id_es2,
                'unit_id_es3' => $row->unit_id_es3,
                'unit_id_es4' => $row->unit_id_es4,
                'unit_staf_id' => $row->unit_staf_id,
                'statpeg_id' => $row->statpeg_id,
                'aktif_id' => $row->aktif_id,
                'jenis_peg_id' => $row->jenis_peg_id,
                'jenjang_id' => $row->jenjang_id,
                'gol_id' => $row->gol_id,
                'no_karpeg' => $row->no_karpeg,
                'absensi_id' => $row->absensi_id,
                'jeniskel_id' => $row->jeniskel_id,
                'tmp_lahir' => $row->tmp_lahir,
                'tgl_lahir' => $row->tgl_lahir,
                'agama_id' => $row->agama_id,
                'kawin_id' => $row->kawin_id,
                'alamat_ktp' => $row->alamat_ktp,
                'kode_pos_ktp' => $row->kode_pos_ktp,
                'alamat_tinggal' => $row->alamat_tinggal,
                'kode_pos_tinggal' => $row->kode_pos_tinggal,
                'telp_rumah' => $row->telp_rumah,
                'telp_cellular' => $row->telp_cellular,
                'no_ext_kantor' => $row->no_ext_kantor,
                'email_kantor' => $row->email_kantor,
                'email_pribadi' => $row->email_pribadi,
                'gol_darah' => $row->gol_darah,
                'no_bpjs' => $row->no_bpjs,
                'ktp_id' => $row->ktp_id,
                'npwp' => $row->npwp,
                'bank_id' => $row->bank_id,
                'cabang_bank' => $row->cabang_bank,
                'no_rek' => $row->no_rek,
                'atas_nama' => $row->atas_nama,
                'photo' => $row->photo,
                'keterangan' => $row->keterangan,
                'dihapus' => $row->dihapus,
                'ktp_fileup' => $row->ktp_fileup,
                'npwp_fileup' => $row->npwp_fileup,
                'kartukeluarga_fileup' => $row->kartukeluarga_fileup,
                'karpeg_fileup' => $row->karpeg_fileup,
                'editdate' => $row->editdate,
                'userlog' => $row->userlog,
            );
            $this->template->load('template', 'master_biodata/master_biodata_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('master_biodata'));
        }
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = array(
                'bkn_id' => $this->input->post('bkn_id', true),
                'nip' => $this->input->post('nip', true),
                'nama_cetak' => $this->input->post('nama_cetak', true),
                'nama' => $this->input->post('nama', true),
                'jenis_jab_id' => $this->input->post('jenis_jab_id', true),
                'struk_id' => $this->input->post('struk_id', true),
                'fung_id' => $this->input->post('fung_id', true),
                'unit_id_es1' => $this->input->post('unit_id_es1', true),
                'unit_id_es2' => $this->input->post('unit_id_es2', true),
                'unit_id_es3' => $this->input->post('unit_id_es3', true),
                'unit_id_es4' => $this->input->post('unit_id_es4', true),
                'unit_staf_id' => $this->input->post('unit_staf_id', true),
                'statpeg_id' => $this->input->post('statpeg_id', true),
                'aktif_id' => $this->input->post('aktif_id', true),
                'jenis_peg_id' => $this->input->post('jenis_peg_id', true),
                'jenjang_id' => $this->input->post('jenjang_id', true),
                'gol_id' => $this->input->post('gol_id', true),
                'no_karpeg' => $this->input->post('no_karpeg', true),
                'absensi_id' => $this->input->post('absensi_id', true),
                'jeniskel_id' => $this->input->post('jeniskel_id', true),
                'tmp_lahir' => $this->input->post('tmp_lahir', true),
                'tgl_lahir' => $this->input->post('tgl_lahir', true),
                'agama_id' => $this->input->post('agama_id', true),
                'kawin_id' => $this->input->post('kawin_id', true),
                'alamat_ktp' => $this->input->post('alamat_ktp', true),
                'kode_pos_ktp' => $this->input->post('kode_pos_ktp', true),
                'alamat_tinggal' => $this->input->post('alamat_tinggal', true),
                'kode_pos_tinggal' => $this->input->post('kode_pos_tinggal', true),
                'telp_rumah' => $this->input->post('telp_rumah', true),
                'telp_cellular' => $this->input->post('telp_cellular', true),
                'no_ext_kantor' => $this->input->post('no_ext_kantor', true),
                'email_kantor' => $this->input->post('email_kantor', true),
                'email_pribadi' => $this->input->post('email_pribadi', true),
                'gol_darah' => $this->input->post('gol_darah', true),
                'no_bpjs' => $this->input->post('no_bpjs', true),
                'ktp_id' => $this->input->post('ktp_id', true),
                'npwp' => $this->input->post('npwp', true),
                'bank_id' => $this->input->post('bank_id', true),
                'cabang_bank' => $this->input->post('cabang_bank', true),
                'no_rek' => $this->input->post('no_rek', true),
                'atas_nama' => $this->input->post('atas_nama', true),
                'photo' => $this->input->post('photo', true),
                'keterangan' => $this->input->post('keterangan', true),
                'dihapus' => $this->input->post('dihapus', true),
                'ktp_fileup' => $this->input->post('ktp_fileup', true),
                'npwp_fileup' => $this->input->post('npwp_fileup', true),
                'kartukeluarga_fileup' => $this->input->post('kartukeluarga_fileup', true),
                'karpeg_fileup' => $this->input->post('karpeg_fileup', true),
                'editdate' => $this->input->post('editdate', true),
                'userlog' => $this->input->post('userlog', true),
            );

            $this->Master_biodata_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('master_biodata'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('bkn_id', 'bkn id', 'trim|required');
        $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('nama_cetak', 'nama cetak', 'trim|required');
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('jenis_jab_id', 'jenis jab id', 'trim|required');
        $this->form_validation->set_rules('struk_id', 'struk id', 'trim|required');
        $this->form_validation->set_rules('fung_id', 'fung id', 'trim|required');
        $this->form_validation->set_rules('unit_id_es1', 'unit id es1', 'trim|required');
        $this->form_validation->set_rules('unit_id_es2', 'unit id es2', 'trim|required');
        $this->form_validation->set_rules('unit_id_es3', 'unit id es3', 'trim|required');
        $this->form_validation->set_rules('unit_id_es4', 'unit id es4', 'trim|required');
        $this->form_validation->set_rules('unit_staf_id', 'unit staf id', 'trim|required');
        $this->form_validation->set_rules('statpeg_id', 'statpeg id', 'trim|required');
        $this->form_validation->set_rules('aktif_id', 'aktif id', 'trim|required');
        $this->form_validation->set_rules('jenis_peg_id', 'jenis peg id', 'trim|required');
        $this->form_validation->set_rules('jenjang_id', 'jenjang id', 'trim|required');
        $this->form_validation->set_rules('gol_id', 'gol id', 'trim|required');
        $this->form_validation->set_rules('no_karpeg', 'no karpeg', 'trim|required');
        $this->form_validation->set_rules('absensi_id', 'absensi id', 'trim|required');
        $this->form_validation->set_rules('jeniskel_id', 'jeniskel id', 'trim|required');
        $this->form_validation->set_rules('tmp_lahir', 'tmp lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required');
        $this->form_validation->set_rules('agama_id', 'agama id', 'trim|required');
        $this->form_validation->set_rules('kawin_id', 'kawin id', 'trim|required');
        $this->form_validation->set_rules('alamat_ktp', 'alamat ktp', 'trim|required');
        $this->form_validation->set_rules('kode_pos_ktp', 'kode pos ktp', 'trim|required');
        $this->form_validation->set_rules('alamat_tinggal', 'alamat tinggal', 'trim|required');
        $this->form_validation->set_rules('kode_pos_tinggal', 'kode pos tinggal', 'trim|required');
        $this->form_validation->set_rules('telp_rumah', 'telp rumah', 'trim|required');
        $this->form_validation->set_rules('telp_cellular', 'telp cellular', 'trim|required');
        $this->form_validation->set_rules('no_ext_kantor', 'no ext kantor', 'trim|required');
        $this->form_validation->set_rules('email_kantor', 'email kantor', 'trim|required');
        $this->form_validation->set_rules('email_pribadi', 'email pribadi', 'trim|required');
        $this->form_validation->set_rules('gol_darah', 'gol darah', 'trim|required');
        $this->form_validation->set_rules('no_bpjs', 'no bpjs', 'trim|required');
        $this->form_validation->set_rules('ktp_id', 'ktp id', 'trim|required');
        $this->form_validation->set_rules('npwp', 'npwp', 'trim|required');
        $this->form_validation->set_rules('bank_id', 'bank id', 'trim|required');
        $this->form_validation->set_rules('cabang_bank', 'cabang bank', 'trim|required');
        $this->form_validation->set_rules('no_rek', 'no rek', 'trim|required');
        $this->form_validation->set_rules('atas_nama', 'atas nama', 'trim|required');
        $this->form_validation->set_rules('photo', 'photo', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
        $this->form_validation->set_rules('dihapus', 'dihapus', 'trim|required');
        $this->form_validation->set_rules('ktp_fileup', 'ktp fileup', 'trim|required');
        $this->form_validation->set_rules('npwp_fileup', 'npwp fileup', 'trim|required');
        $this->form_validation->set_rules('kartukeluarga_fileup', 'kartukeluarga fileup', 'trim|required');
        $this->form_validation->set_rules('karpeg_fileup', 'karpeg fileup', 'trim|required');
        $this->form_validation->set_rules('editdate', 'editdate', 'trim|required');
        $this->form_validation->set_rules('userlog', 'userlog', 'trim|required');

        $this->form_validation->set_rules('master_biodata_id', 'master_biodata_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function create()
    {
        $data = array(
            'button' => 'Simpan',
            'action' => site_url('master_biodata/create_action'),
            'master_biodata_id' => set_value('master_biodata_id'),
            'bkn_id' => set_value('bkn_id'),
            'nip' => set_value('nip'),
            'nama_cetak' => set_value('nama_cetak'),
            'nama' => set_value('nama'),
            'jenis_jab_id' => set_value('jenis_jab_id'),
            'struk_id' => set_value('struk_id'),
            'fung_id' => set_value('fung_id'),
            'unit_id_es1' => set_value('unit_id_es1'),
            'unit_id_es2' => set_value('unit_id_es2'),
            'unit_id_es3' => set_value('unit_id_es3'),
            'unit_id_es4' => set_value('unit_id_es4'),
            'unit_staf_id' => set_value('unit_staf_id'),
            'statpeg_id' => set_value('statpeg_id'),
            'aktif_id' => set_value('aktif_id'),
            'jenis_peg_id' => set_value('jenis_peg_id'),
            'jenjang_id' => set_value('jenjang_id'),
            'gol_id' => set_value('gol_id'),
            'no_karpeg' => set_value('no_karpeg'),
            'absensi_id' => set_value('absensi_id'),
            'jeniskel_id' => set_value('jeniskel_id'),
            'tmp_lahir' => set_value('tmp_lahir'),
            'tgl_lahir' => set_value('tgl_lahir'),
            'agama_id' => set_value('agama_id'),
            'kawin_id' => set_value('kawin_id'),
            'alamat_ktp' => set_value('alamat_ktp'),
            'kode_pos_ktp' => set_value('kode_pos_ktp'),
            'alamat_tinggal' => set_value('alamat_tinggal'),
            'kode_pos_tinggal' => set_value('kode_pos_tinggal'),
            'telp_rumah' => set_value('telp_rumah'),
            'telp_cellular' => set_value('telp_cellular'),
            'no_ext_kantor' => set_value('no_ext_kantor'),
            'email_kantor' => set_value('email_kantor'),
            'email_pribadi' => set_value('email_pribadi'),
            'gol_darah' => set_value('gol_darah'),
            'no_bpjs' => set_value('no_bpjs'),
            'ktp_id' => set_value('ktp_id'),
            'npwp' => set_value('npwp'),
            'bank_id' => set_value('bank_id'),
            'cabang_bank' => set_value('cabang_bank'),
            'no_rek' => set_value('no_rek'),
            'atas_nama' => set_value('atas_nama'),
            'photo' => set_value('photo'),
            'keterangan' => set_value('keterangan'),
            'dihapus' => set_value('dihapus'),
            'ktp_fileup' => set_value('ktp_fileup'),
            'npwp_fileup' => set_value('npwp_fileup'),
            'kartukeluarga_fileup' => set_value('kartukeluarga_fileup'),
            'karpeg_fileup' => set_value('karpeg_fileup'),
            'editdate' => set_value('editdate'),
            'userlog' => set_value('userlog'),
        );
        $this->template->load('template', 'master_biodata/master_biodata_form', $data);
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('master_biodata_id', true));
        } else {
            $data = array(
                'bkn_id' => $this->input->post('bkn_id', true),
                'nip' => $this->input->post('nip', true),
                'nama_cetak' => $this->input->post('nama_cetak', true),
                'nama' => $this->input->post('nama', true),
                'jenis_jab_id' => $this->input->post('jenis_jab_id', true),
                'struk_id' => $this->input->post('struk_id', true),
                'fung_id' => $this->input->post('fung_id', true),
                'unit_id_es1' => $this->input->post('unit_id_es1', true),
                'unit_id_es2' => $this->input->post('unit_id_es2', true),
                'unit_id_es3' => $this->input->post('unit_id_es3', true),
                'unit_id_es4' => $this->input->post('unit_id_es4', true),
                'unit_staf_id' => $this->input->post('unit_staf_id', true),
                'statpeg_id' => $this->input->post('statpeg_id', true),
                'aktif_id' => $this->input->post('aktif_id', true),
                'jenis_peg_id' => $this->input->post('jenis_peg_id', true),
                'jenjang_id' => $this->input->post('jenjang_id', true),
                'gol_id' => $this->input->post('gol_id', true),
                'no_karpeg' => $this->input->post('no_karpeg', true),
                'absensi_id' => $this->input->post('absensi_id', true),
                'jeniskel_id' => $this->input->post('jeniskel_id', true),
                'tmp_lahir' => $this->input->post('tmp_lahir', true),
                'tgl_lahir' => $this->input->post('tgl_lahir', true),
                'agama_id' => $this->input->post('agama_id', true),
                'kawin_id' => $this->input->post('kawin_id', true),
                'alamat_ktp' => $this->input->post('alamat_ktp', true),
                'kode_pos_ktp' => $this->input->post('kode_pos_ktp', true),
                'alamat_tinggal' => $this->input->post('alamat_tinggal', true),
                'kode_pos_tinggal' => $this->input->post('kode_pos_tinggal', true),
                'telp_rumah' => $this->input->post('telp_rumah', true),
                'telp_cellular' => $this->input->post('telp_cellular', true),
                'no_ext_kantor' => $this->input->post('no_ext_kantor', true),
                'email_kantor' => $this->input->post('email_kantor', true),
                'email_pribadi' => $this->input->post('email_pribadi', true),
                'gol_darah' => $this->input->post('gol_darah', true),
                'no_bpjs' => $this->input->post('no_bpjs', true),
                'ktp_id' => $this->input->post('ktp_id', true),
                'npwp' => $this->input->post('npwp', true),
                'bank_id' => $this->input->post('bank_id', true),
                'cabang_bank' => $this->input->post('cabang_bank', true),
                'no_rek' => $this->input->post('no_rek', true),
                'atas_nama' => $this->input->post('atas_nama', true),
                'photo' => $this->input->post('photo', true),
                'keterangan' => $this->input->post('keterangan', true),
                'dihapus' => $this->input->post('dihapus', true),
                'ktp_fileup' => $this->input->post('ktp_fileup', true),
                'npwp_fileup' => $this->input->post('npwp_fileup', true),
                'kartukeluarga_fileup' => $this->input->post('kartukeluarga_fileup', true),
                'karpeg_fileup' => $this->input->post('karpeg_fileup', true),
                'editdate' => $this->input->post('editdate', true),
                'userlog' => $this->input->post('userlog', true),
            );

            $this->Master_biodata_model->update($this->input->post('master_biodata_id', true), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('master_biodata'));
        }
    }

    public function update($id)
    {
        $row = $this->Master_biodata_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('master_biodata/update_action'),
                'master_biodata_id' => set_value('master_biodata_id', $row->master_biodata_id),
                'bkn_id' => set_value('bkn_id', $row->bkn_id),
                'nip' => set_value('nip', $row->nip),
                'nama_cetak' => set_value('nama_cetak', $row->nama_cetak),
                'nama' => set_value('nama', $row->nama),
                'jenis_jab_id' => set_value('jenis_jab_id', $row->jenis_jab_id),
                'struk_id' => set_value('struk_id', $row->struk_id),
                'fung_id' => set_value('fung_id', $row->fung_id),
                'unit_id_es1' => set_value('unit_id_es1', $row->unit_id_es1),
                'unit_id_es2' => set_value('unit_id_es2', $row->unit_id_es2),
                'unit_id_es3' => set_value('unit_id_es3', $row->unit_id_es3),
                'unit_id_es4' => set_value('unit_id_es4', $row->unit_id_es4),
                'unit_staf_id' => set_value('unit_staf_id', $row->unit_staf_id),
                'statpeg_id' => set_value('statpeg_id', $row->statpeg_id),
                'aktif_id' => set_value('aktif_id', $row->aktif_id),
                'jenis_peg_id' => set_value('jenis_peg_id', $row->jenis_peg_id),
                'jenjang_id' => set_value('jenjang_id', $row->jenjang_id),
                'gol_id' => set_value('gol_id', $row->gol_id),
                'no_karpeg' => set_value('no_karpeg', $row->no_karpeg),
                'absensi_id' => set_value('absensi_id', $row->absensi_id),
                'jeniskel_id' => set_value('jeniskel_id', $row->jeniskel_id),
                'tmp_lahir' => set_value('tmp_lahir', $row->tmp_lahir),
                'tgl_lahir' => set_value('tgl_lahir', $row->tgl_lahir),
                'agama_id' => set_value('agama_id', $row->agama_id),
                'kawin_id' => set_value('kawin_id', $row->kawin_id),
                'alamat_ktp' => set_value('alamat_ktp', $row->alamat_ktp),
                'kode_pos_ktp' => set_value('kode_pos_ktp', $row->kode_pos_ktp),
                'alamat_tinggal' => set_value('alamat_tinggal', $row->alamat_tinggal),
                'kode_pos_tinggal' => set_value('kode_pos_tinggal', $row->kode_pos_tinggal),
                'telp_rumah' => set_value('telp_rumah', $row->telp_rumah),
                'telp_cellular' => set_value('telp_cellular', $row->telp_cellular),
                'no_ext_kantor' => set_value('no_ext_kantor', $row->no_ext_kantor),
                'email_kantor' => set_value('email_kantor', $row->email_kantor),
                'email_pribadi' => set_value('email_pribadi', $row->email_pribadi),
                'gol_darah' => set_value('gol_darah', $row->gol_darah),
                'no_bpjs' => set_value('no_bpjs', $row->no_bpjs),
                'ktp_id' => set_value('ktp_id', $row->ktp_id),
                'npwp' => set_value('npwp', $row->npwp),
                'bank_id' => set_value('bank_id', $row->bank_id),
                'cabang_bank' => set_value('cabang_bank', $row->cabang_bank),
                'no_rek' => set_value('no_rek', $row->no_rek),
                'atas_nama' => set_value('atas_nama', $row->atas_nama),
                'photo' => set_value('photo', $row->photo),
                'keterangan' => set_value('keterangan', $row->keterangan),
                'dihapus' => set_value('dihapus', $row->dihapus),
                'ktp_fileup' => set_value('ktp_fileup', $row->ktp_fileup),
                'npwp_fileup' => set_value('npwp_fileup', $row->npwp_fileup),
                'kartukeluarga_fileup' => set_value('kartukeluarga_fileup', $row->kartukeluarga_fileup),
                'karpeg_fileup' => set_value('karpeg_fileup', $row->karpeg_fileup),
                'editdate' => set_value('editdate', $row->editdate),
                'userlog' => set_value('userlog', $row->userlog),
            );
            $this->template->load('template', 'master_biodata/master_biodata_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('master_biodata'));
        }
    }

    public function delete($id)
    {
        $row = $this->Master_biodata_model->get_by_id($id);

        if ($row) {
            $this->Master_biodata_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('master_biodata'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('master_biodata'));
        }
    }

    public function get_nama_autocomplete()
    {
        if (isset($_GET['query'])) {
            $result = $this->Master_biodata_model->search_pegawai($_GET['query']);
            if (count($result) > 0) {
                $data = array();
                foreach ($result as $row) {
                    $output['suggestions'][] = [
                        'value' => $row->nama,
                        'id' => $row->nip,
                    ];
                }
                echo json_encode($output);
            }
        }
    }
}

/* End of file Master_biodata.php */
/* Location: ./application/controllers/Master_biodata.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-12 20:43:46 */
/* http://harviacode.com */
