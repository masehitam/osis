<?php

class Cetak extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('ciqrcode');
        $this->load->model('Trans_bmn_model');
        $this->load->model('Trans_ruang_rapat_model');
        $this->load->model('Ruang_rapat_model');
        $this->load->model('Trans_cuti_model');
        $this->load->model('Master_biodata_model');
        $this->load->model('Detail_pegawai_model');
        $this->load->model('Perjanjian_bmn_model');
        $this->load->model('Sisa_cuti_baru_model');
        $this->load->model('Bmn_model');
    }

    public function decide_pangkat($golongan){
        switch (strtolower($golongan)) {
            case 'iv/e':
                return 'Pembina Utama';
            case 'iv/d':
                return 'Pembina Utama Madya';
            case 'iv/c':
                return 'Pembina Utama Muda';
            case 'iv/b':
                return 'Pembina Tingkat I';
            case 'iv/a':
                return 'Pembina';
            case 'iii/d':
                return 'Penata Tingkat I';
            case 'iii/c':
                return 'Penata';
            case 'iii/b':
                return 'Penata Mudat Tingkat I';
            case 'iii/a':
                return 'Penata Muda';
            case 'ii/d':
                return 'Pengatur Tingkat I';
            case 'ii/c':
                return 'Pengatur';
            case 'ii/b':
                return 'Pengatur Muda Tingkat I';
            case 'ii/a':
                return 'Pengatur Muda';
            case 'i/d':
                return 'Juru Tingkat I';
            case 'i/c':
                return 'Juru';
            case 'i/b':
                return 'Juru Muda Tingkat I';
            case 'i/a':
                return 'Juru Muda';
            default:
                return "[Pangkat]";
        }
    }

   /* public function form_cuti($id, $status = '1')
    {
        $row = $this->Trans_cuti_model->get_by_id($id);

        $tahun_ini = date('Y');
        $result1 = $this->Trans_cuti_model->hitung_cuti($row->nip, $tahun_ini);
        $jumlah_cuti_tahun_ini = 0;
        if ($result1) {
            $jumlah_cuti_tahun_ini = $result1->jumlah_hari;
        }

        $tahun_kemarin = date('Y', strtotime('-1 year'));
        $result2 = $this->Trans_cuti_model->hitung_cuti($row->nip, $tahun_kemarin);

        $jumlah_cuti_tahun_kemaren = 0;
        if ($result2) {
            $jumlah_cuti_tahun_kemaren = $result2->jumlah_hari;
        }

        $bio1 = $this->Master_biodata_model->get_by_nip($row->nip);
        $bio1_detial = $this->Detail_pegawai_model->get_by_nip($row->nip);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->nip_atasan);
        $bio3 = $this->Master_biodata_model->get_by_nip($row->nip_pejabat);

        $pdf = new FPDF('p', 'mm', 'A4');
        $pdf->SetTitle('Formulir Pengajuan Cuti');
        $pdf->SetMargins(15, 15, 15);
        $pdf->AddPage();
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Image(base_url() . 'assets/images/logo.png', 20, 10, 23, 0);
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'LEMBAGA PERLINDUNGAN SAKSI DAN KORBAN', 0, 1, 'C');
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'SEKRETARIAT JENDRAL', 0, 1, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Jl. Raya Bogor KM. 24 No. 47-49', 0, 1, 'C');
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Kelurahan Susukan Kecamatan Ciracas Jakarta Timur', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(0, 6, '-------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Surat Permohonan Cuti', 0, 1, 'C');
        switch ($status) {
            case '2':
                $pdf->Cell(0, 6, '(Eselon II dan TA)', 0, 1, 'C');
                break;

            case '3':
                $pdf->Cell(0, 6, '(Eselon III)', 0, 1, 'C');
                break;

            default:
                $pdf->Cell(0, 6, '(Eselon IV dan Staf)', 0, 1, 'C');
                break;
        }
        $pdf->Ln(10);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(5, 6, 'Yang bertanda tangan di bawah ini,', 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Nama', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $bio1->nama_cetak, 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'NIP/NRP', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $bio1->nip, 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Pangkat/Golongan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pangkat = $this->decide_pangkat($bio1_detial->golongan);
        $pdf->Cell(0, 6, $pangkat.'/'.$bio1_detial->golongan, 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Jabatan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $bio1_detial->nmjabatan, 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Unit Kerja', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        if (isset($bio1_detial->nmues2)) {
            $pdf->MultiCell(0, 6, $bio1_detial->nmues2, 0, 1, 'L');
        }else{
            $pdf->MultiCell(0, 6, $bio1_detial->nmues1, 0, 1, 'L');
        }
        $pdf->MultiCell(0, 6, 'Dengan ini mengajukan permohonan ' . $row->jenis_cuti . ' selama ' . $row->jumlah_hari . ' hari pada tanggal ' . $row->wkt_cuti . ' Karena ' . $row->alasan, 0, 1, 'L');
        $pdf->MultiCell(0, 6, '        Demikian Surat permohonan ini dibuat untuk dapat dipertimbangkan sebagaimana mestinya.', 0, 1, 'L');
        $pdf->Ln(5);
        $pdf->Cell(117, 6, '', 0, 0, 'L');
        $pdf->Cell(0, 6, 'Dikeluarkan di Jakarta, ', 0, 1, 'L');
        $pdf->Cell(117, 6, '', 0, 0, 'L');
        if ($row->date_reaksi_dua != null) {
            $pdf->Cell(0, 6, 'Pada tanggal : ' . date('d M Y', strtotime($row->date_reaksi_dua)), 0, 1, 'L');
        } else {
            $pdf->Cell(0, 6, 'Pada tanggal : ', 0, 1, 'L');
        }
        $pdf->Ln(5);
        $pdf->Cell(85, 6, 'Mengetahui', 0, 0, 'C');
        $pdf->Cell(85, 6, 'Pemohon', 0, 1, 'C');
        $pdf->Cell(85, 30, '', 0, 0, 'C');
        $pdf->Cell(85, 30, '', 0, 1, 'C');
        switch ($status) {
            //case '1':
            case '3':
            case '4':
                $pdf->Cell(85, 6, $bio2->nama_cetak, 0, 0, 'C');
                break;
            default:
                $pdf->Cell(85, 6, '(Noor Sidharta)', 0, 0, 'C');
                break;
        }
        $pdf->Cell(85, 6, $bio1->nama_cetak, 0, 1, 'C');
        $pdf->Ln(5);
        switch ($status) {
            case '2':
                $pdf->Cell(0, 6, '', 0, 1, 'C');
                $pdf->Cell(0, 6, '', 0, 1, 'C');
                $pdf->Cell(0, 30, '', 0, 1, 'C');
                $pdf->Cell(0, 6, '', 0, 1, 'C');
                break;

            case '3':
                $pdf->Cell(0, 6, 'Menyetujui', 0, 1, 'C');
                $pdf->Cell(0, 6, 'Sekretaris jendral LPSK', 0, 1, 'C');
                $pdf->Cell(0, 30, '', 0, 1, 'C');
                $pdf->Cell(0, 6, '(Noor Sidharta)', 0, 1, 'C');
                break;

            default:
                $pdf->Cell(0, 6, 'Menyetujui', 0, 1, 'C');
                $pdf->Cell(0, 6, 'Kepala Biro Administrasi', 0, 1, 'C');
                $pdf->Cell(0, 30, '', 0, 1, 'C');
                $pdf->Cell(0, 6, '(Handari Restu Dewi)', 0, 1, 'C');
                break;
        }

        $pdf->Cell(0, 6, 'Tembusan : ', 0, 1, 'L');
        $pdf->Cell(0, 6, 'Sekretaris Jendral LPSK', 0, 1, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 6, 'Keterangan : ', 0, 1, 'L');
        $pdf->Cell(65, 6, 'Jumlah izin selama Tahun ' . $tahun_kemarin . ' : ', 0, 0, 'L');
        $pdf->Cell(30, 6, ' 0 ', 0, 0, 'L');
        $pdf->Cell(65, 6, 'Jumlah izin selama Tahun ' . $tahun_ini . ' : ', 0, 0, 'L');
        $pdf->Cell(30, 6, ' 0 ', 0, 1, 'L');
        $pdf->Cell(65, 6, 'Jumlah cuti selama Tahun ' . $tahun_kemarin . ' : ', 0, 0, 'L');
        $pdf->Cell(30, 6, ' ' . $jumlah_cuti_tahun_kemaren . ' dari 6 ', 0, 0, 'L');
        $pdf->Cell(65, 6, 'Jumlah cuti selama Tahun ' . $tahun_ini . ' : ', 0, 0, 'L');
        $pdf->Cell(30, 6, ' ' . $jumlah_cuti_tahun_ini . ' dari 12 ', 0, 1, 'L');

        if ($row->status != 'DIBATALKAN') {
            if ($row->date_diajukan != null) {
                $pdf->Image(base_url() . 'uploads/qrcode/CUTI-' . $id . '_' . $bio1->nip . '.png', 131, 153, 23, 0);
                //$pdf->Image(base_url() . 'assets/images/disetujui.png', 131, 153, 23, 0);
            }
            if ($status != '2') {
                // QRCODE approval atasan
                if ($row->date_reaksi_satu != null && $row->status != 'TOLAK_1' ) {
                    $pdf->Image(base_url() . 'uploads/qrcode/CUTI-' . $id . '_' . $bio2->nip . '.png', 46, 153, 23, 0);
                    //$pdf->Image(base_url() . 'assets/images/disetujui.png', 46, 153, 23, 0);
                }
                // QRCODE pejabat
                if ($row->date_reaksi_dua != null && $row->status != 'TOLAK_2') {
                    $pdf->Image(base_url() . 'uploads/qrcode/CUTI-' . $id . '_' . $bio3->nip . '.png', 93, 207, 23, 0);
                }
            } else {
                if ($row->date_reaksi_dua != null && $row->status != 'TOLAK_2') {
                    $pdf->Image(base_url() . 'uploads/qrcode/CUTI-' . $id . '_' . $bio3->nip . '.png', 46, 153, 23, 0);
                }
            }
        }

        // tampilkan dilayar
        $pdf->Output();
        // cetak output
        //$pdf->Output('uploads/berkas_cuti/CUTI-' . $id . '.pdf', 'F');
    }*/

    public function form_cuti($id, $status = '1')
    {
        $row = $this->Trans_cuti_model->get_by_id($id);

        $tahun_ini = date('Y');
        // $result1 = $this->Trans_cuti_model->hitung_cuti($row->nip, $tahun_ini);
        // $jumlah_cuti_tahun_ini = 0;
        // if ($result1) {
        //     $jumlah_cuti_tahun_ini = $result1->jumlah_hari;
        // }

        $tahun_kemarin = date('Y', strtotime('-1 year'));
        // $result2 = $this->Trans_cuti_model->hitung_cuti($row->nip, $tahun_kemarin);

        // $jumlah_cuti_tahun_kemaren = 0;
        // if ($result2) {
        //     $jumlah_cuti_tahun_kemaren = $result2->jumlah_hari;
        // }

        $sisa_cuti_tahun_ini = 12 - $this->Sisa_cuti_baru_model->get_by_nip_tahun($row->nip, $tahun_ini)->sisa;
        $sisa_cuti_tahun_kemarin = 6 - $this->Sisa_cuti_baru_model->get_by_nip_tahun($row->nip, $tahun_kemarin)->sisa;

        $bio1 = $this->Master_biodata_model->get_by_nip($row->nip);
        $bio1_detial = $this->Detail_pegawai_model->get_by_nip($row->nip);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->nip_atasan);
        $bio3 = $this->Master_biodata_model->get_by_nip($row->nip_pejabat);

        $pdf = new FPDF('p', 'mm', 'A4');
        $pdf->SetTitle('Formulir Pengajuan Cuti');
        $pdf->SetMargins(15, 15, 15);
        $pdf->AddPage();
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Image(base_url() . 'assets/images/logo.png', 20, 10, 23, 0);
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'LEMBAGA PERLINDUNGAN SAKSI DAN KORBAN', 0, 1, 'C');
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'SEKRETARIAT JENDRAL', 0, 1, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Jl. Raya Bogor KM. 24 No. 47-49', 0, 1, 'C');
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Kelurahan Susukan Kecamatan Ciracas Jakarta Timur', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(0, 6, '-------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'Surat Permohonan Cuti', 0, 1, 'C');
        switch ($status) {
            case '2':
                $pdf->Cell(0, 6, '(Eselon II dan TA)', 0, 1, 'C');
                break;

            case '3':
                $pdf->Cell(0, 6, '(Eselon III)', 0, 1, 'C');
                break;

            default:
                $pdf->Cell(0, 6, '(Eselon IV dan Staf)', 0, 1, 'C');
                break;
        }
        $pdf->Ln(10);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(5, 6, 'Yang bertanda tangan di bawah ini,', 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Nama', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        if (strlen($bio1->nama_cetak) > 0){
            $pdf->Cell(0, 6, $bio1->nama_cetak, 0, 1, 'L');
        }else{
            $pdf->Cell(0, 6, $bio1->nama, 0, 1, 'L');
        }
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'NIP/NRP', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $bio1->nip, 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Pangkat/Golongan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pangkat = $this->decide_pangkat($bio1_detial->golongan);
        $pdf->Cell(0, 6, $pangkat . '/' . $bio1_detial->golongan, 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Jabatan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $bio1_detial->nmjabatan, 0, 1, 'L');
        $pdf->Cell(15, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Unit Kerja', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        if (isset($bio1_detial->nmues2)) {
            $pdf->MultiCell(0, 6, $bio1_detial->nmues2, 0, 1, 'L');
        } else {
            $pdf->MultiCell(0, 6, $bio1_detial->nmues1, 0, 1, 'L');
        }
        $pdf->MultiCell(0, 6, 'Dengan ini mengajukan permohonan ' . $row->jenis_cuti . ' selama ' . $row->jumlah_hari . ' hari pada tanggal ' . $row->wkt_cuti . ' Karena ' . $row->alasan, 0, 1, 'L');
        $pdf->MultiCell(0, 6, '        Demikian Surat permohonan ini dibuat untuk dapat dipertimbangkan sebagaimana mestinya.', 0, 1, 'L');
        $pdf->Ln(5);
        $pdf->Cell(117, 6, '', 0, 0, 'L');
        $pdf->Cell(0, 6, 'Dikeluarkan di Jakarta, ', 0, 1, 'L');
        $pdf->Cell(117, 6, '', 0, 0, 'L');
        if ($row->date_reaksi_dua != null) {
            $pdf->Cell(0, 6, 'Pada tanggal : ' . date('d M Y', strtotime($row->date_reaksi_dua)), 0, 1, 'L');
        } else {
            $pdf->Cell(0, 6, 'Pada tanggal : ', 0, 1, 'L');
        }
        $pdf->Ln(5);
        $pdf->Cell(85, 6, 'Mengetahui', 0, 0, 'C');
        $pdf->Cell(85, 6, 'Pemohon', 0, 1, 'C');
        $pdf->Cell(85, 30, '', 0, 0, 'C');
        $pdf->Cell(85, 30, '', 0, 1, 'C');
        switch ($status) {
            //case '1':
            case '3':
            case '4':
                $pdf->Cell(85, 6, $bio2->nama_cetak, 0, 0, 'C');
                break;
            default:
                $pdf->Cell(85, 6, '(Noor Sidharta)', 0, 0, 'C');
                break;
        }
        $pdf->Cell(85, 6, $bio1->nama_cetak, 0, 1, 'C');
        $pdf->Ln(5);
        switch ($status) {
            case '2':
                $pdf->Cell(0, 6, '', 0, 1, 'C');
                $pdf->Cell(0, 6, '', 0, 1, 'C');
                $pdf->Cell(0, 30, '', 0, 1, 'C');
                $pdf->Cell(0, 6, '', 0, 1, 'C');
                break;

            case '3':
                $pdf->Cell(0, 6, 'Menyetujui', 0, 1, 'C');
                $pdf->Cell(0, 6, 'Sekretaris jendral LPSK', 0, 1, 'C');
                $pdf->Cell(0, 30, '', 0, 1, 'C');
                $pdf->Cell(0, 6, '(Noor Sidharta)', 0, 1, 'C');
                break;

            default:
                $pdf->Cell(0, 6, 'Menyetujui', 0, 1, 'C');
                $pdf->Cell(0, 6, 'Kepala Biro Administrasi', 0, 1, 'C');
                $pdf->Cell(0, 30, '', 0, 1, 'C');
                $pdf->Cell(0, 6, '(Handari Restu Dewi)', 0, 1, 'C');
                break;
        }

        $pdf->Cell(0, 6, 'Tembusan : ', 0, 1, 'L');
        $pdf->Cell(0, 6, 'Sekretaris Jendral LPSK', 0, 1, 'L');
        $pdf->Ln(5);
        $pdf->Cell(0, 6, 'Keterangan : ', 0, 1, 'L');
        $pdf->Cell(65, 6, 'Jumlah izin selama Tahun ' . $tahun_kemarin . ' : ', 0, 0, 'L');
        $pdf->Cell(30, 6, ' 0 ', 0, 0, 'L');
        $pdf->Cell(65, 6, 'Jumlah izin selama Tahun ' . $tahun_ini . ' : ', 0, 0, 'L');
        $pdf->Cell(30, 6, ' 0 ', 0, 1, 'L');
        $pdf->Cell(65, 6, 'Jumlah cuti selamat Tahun ' . $tahun_kemarin . ' : ', 0, 0, 'L');
        $pdf->Cell(30, 6, ' ' . $sisa_cuti_tahun_kemarin . ' dari 6 ', 0, 0, 'L');
        $pdf->Cell(65, 6, 'Jumlah cuti selamat Tahun ' . $tahun_ini . ' : ', 0, 0, 'L');
        $pdf->Cell(30, 6, ' ' . $sisa_cuti_tahun_ini . ' dari 12 ', 0, 1, 'L');

        if ($row->status != 'DIBATALKAN') {
            if ($row->date_diajukan != null) {
                $pdf->Image(base_url() . 'uploads/qrcode/CUTI-' . $id . '_' . $bio1->nip . '.png', 131, 153, 23, 0);
                //$pdf->Image(base_url() . 'assets/images/disetujui.png', 131, 153, 23, 0);
            }
            if ($status != '2') {
                // QRCODE approval atasan
                if ($row->date_reaksi_satu != null && $row->status != 'TOLAK_1') {
                    $pdf->Image(base_url() . 'uploads/qrcode/CUTI-' . $id . '_' . $bio2->nip . '.png', 46, 153, 23, 0);
                    //$pdf->Image(base_url() . 'assets/images/disetujui.png', 46, 153, 23, 0);
                }
                // QRCODE pejabat
                if ($row->date_reaksi_dua != null && $row->status != 'TOLAK_2') {
                    $pdf->Image(base_url() . 'uploads/qrcode/CUTI-' . $id . '_' . $bio3->nip . '.png', 93, 207, 23, 0);
                }
            } else {
                if ($row->date_reaksi_dua != null && $row->status != 'TOLAK_2') {
                    $pdf->Image(base_url() . 'uploads/qrcode/CUTI-' . $id . '_' . $bio3->nip . '.png', 46, 153, 23, 0);
                }
            }
        }

        // tampilkan dilayar
        $pdf->Output();
        // cetak output
        //$pdf->Output('uploads/berkas_cuti/CUTI-' . $id . '.pdf', 'F');
    }
    
    public function form_bmn($id_trans)
    {
        $row = $this->Trans_bmn_model->get_by_id($id_trans);
        $bio1 = $this->Master_biodata_model->get_by_nip($row->id_user);
        $bio1_detial = $this->Detail_pegawai_model->get_by_nip($row->id_user);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->id_atasan);
        $bio3 = $this->Master_biodata_model->get_by_nip($row->id_krt);

        $pdf = new FPDF('p', 'mm', 'A4');
        $pdf->SetTitle('Formulir Peminjaman BMN');
        $pdf->SetMargins(15, 15, 15);
        $pdf->AddPage();
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Image(base_url() . 'assets/images/logo.png', 20, 13, 23, 0);
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'LEMBAGA PERLINDUNGAN SAKSI DAN KORBAN', 0, 1, 'C');
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'SEKRETARIAT JENDRAL', 0, 1, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Jl. Raya Bogor KM. 24 No. 47-49', 0, 1, 'C');
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Kelurahan Susukan Kecamatan Ciracas Jakarta Timur', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(0, 6, '-------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'FORMULIR PENGJUAN PENGGUNAAN BMN', 0, 1, 'C');
        $pdf->Ln(10);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(45, 6, 'Nama Pemohon', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $bio1->nama_cetak, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Unit Kerja', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        if (isset($bio1_detial->nmues2)) {
            $pdf->Cell(0, 6, $bio1_detial->nmues2, 0, 1, 'L');
        }else{
            $pdf->Cell(0, 6, $bio1_detial->nmues1, 0, 1, 'L');
        }
        $pdf->Cell(45, 6, 'Keperluan Kegiatan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row->keperluan, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Tanggal Penggunaan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row->tgl_penggunaan, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Lokasi Kegiatan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row->lokasi, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Keterangan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row->keterangan, 0, 1, 'L');
        $pdf->Ln(25);
        if ($row->date_reaksi_dua != null) {
            $date = date("d - m - Y", strtotime($row->date_reaksi_dua));
            $pdf->Cell(155, 6, 'Jakarta, ' . $date, 0, 1, 'R');
        }
        $pdf->Cell(85, 6, 'Atasan Pemohon', 0, 0, 'C');
        $pdf->Cell(85, 6, 'Pemohon', 0, 1, 'C');
        $pdf->Cell(85, 30, '', 0, 0, 'C');
        $pdf->Cell(85, 30, '', 0, 1, 'C');
        $pdf->Cell(85, 6, $bio2->nama_cetak, 0, 0, 'C');
        $pdf->Cell(85, 6, $bio1->nama_cetak, 0, 1, 'C');
        $pdf->Ln(10);
        $pdf->Cell(0, 6, 'Mengetahui', 0, 1, 'C');
        $pdf->Cell(0, 6, 'Pejabat Berwenang', 0, 1, 'C');
        $pdf->Cell(0, 30, '', 0, 1, 'C');
        $pdf->Cell(0, 6, $bio3->nama_cetak, 0, 1, 'C');
        if ($row->status != 'DIBATALKAN') {
            // QRCODE yang mengajukan cuti
            if ($row->date_diajukan != null) {
                $pdf->Image(base_url() . 'uploads/qrcode/BMN-' . $id_trans . '_' . $bio1->nip . '.png', 130, 129, 23, 0);
                //$pdf->Image(base_url() . 'assets/images/disetujui.png', 130, 129, 23, 0);
            }
            // QRCODE approval atasan
            if ($row->date_reaksi_satu != null && $row->status != 'TOLAK_1') {
                $pdf->Image(base_url() . 'uploads/qrcode/BMN-' . $id_trans . '_' . $bio2->nip . '.png', 45, 129, 23, 0);
                //$pdf->Image(base_url() . 'assets/images/disetujui.png', 45, 129, 23, 0);
            }
            // QRCODE pejabat
            if ($row->date_reaksi_dua != null && $row->status != 'TOLAK_2') {
                $pdf->Image(base_url() . 'uploads/qrcode/BMN-' . $id_trans . '_' . $bio3->nip . '.png', 92, 192, 23, 0);
            }
        }
        // tampilkan dilayar
        $pdf->Output();
        // cetak output
        // $pdf->Output('uploads/berkas_bmn/BMN-' . $id_trans . '.pdf', 'F');
    }

    public function form_perjanjian_bmn($id_trans)
    {
        $perjanjian = $this->Perjanjian_bmn_model->get_by_id($id_trans);
        $row = $this->Trans_bmn_model->get_by_id($perjanjian->id_trans_bmn);
        $bio1 = $this->Master_biodata_model->get_by_nip($row->id_user);
        $bio1_detial = $this->Detail_pegawai_model->get_by_nip($row->id_user);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->id_krt);
        $bio2_detial = $this->Detail_pegawai_model->get_by_nip($row->id_krt);
        $bio3 = $this->Master_biodata_model->get_by_nip($perjanjian->id_pejabat);
        $bmn = $this->Bmn_model->get_by_id($row->id);

        $pdf = new FPDF('p', 'mm', 'LEGAL');
        $pdf->SetTitle('Formulir Perjanjian Peminjaman BMN');
        $pdf->SetMargins(16, 8, 16);
        $pdf->AddPage();
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Image(base_url() . 'assets/images/logo.png', 20, 6, 23, 0);
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'LEMBAGA PERLINDUNGAN SAKSI DAN KORBAN', 0, 1, 'C');
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'SEKRETARIAT JENDRAL', 0, 1, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Jl. Raya Bogor KM. 24 No. 47-49', 0, 1, 'C');
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Kelurahan Susukan Kecamatan Ciracas Jakarta Timur', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(0, 6, '-------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'BERITA ACARA PENGGUNAAN ASET', 0, 1, 'C');
        $pdf->Ln(2);
        $pdf->SetFont('Arial', '', 10);
        $hari = $perjanjian->date_reaksi != null ? date("l", strtotime($perjanjian->date_reaksi)) : "";
        $tgl = $perjanjian->date_reaksi != null ? date("d/m/Y", strtotime($perjanjian->date_reaksi)) : "";
        $pdf->Cell(45, 6, 'Pada hari ini, ' . $hari . ' tanggal ' . $tgl . ' kami yang bertanda tangan di bawah ini :', 0, 1, 'L');
        $pdf->Cell(10, 6, 'I.', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Nama', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $bio2->nama_cetak, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Nip', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $bio2->nip, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Jabatan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $bio2_detial->nmjabatan, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Unit Kerja', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        if (isset($bio2_detial->nmues2)) {
            $pdf->MultiCell(0, 6, $bio2_detial->nmues2, 0, 1, 'L');
        }else{
            $pdf->MultiCell(0, 6, $bio2_detial->nmues1, 0, 1, 'L');
        }
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Alamat', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $bio2_detial->alamat_tinggal, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'selanjutnya disebut PIHAK PERTAMA.', 0, 1, 'L');
        $pdf->Ln(2);
        $pdf->Cell(10, 6, 'II', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Nama', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $bio1->nama_cetak, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Jabatan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $bio1_detial->nmjabatan, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Alamat', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $bio1_detial->alamat_tinggal, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'selanjutnya disebut PIHAK KEDUA.', 0, 1, 'L');
        $pdf->Ln(4);

        $pdf->MultiCell(0, 6, 'PIHAK PERTAMA dan PIHAK KEDUA dalam Berita Acara ini selanjutnya disebut PARA PIHAK Dalam kedudukannya masing-masing. PARA PIHAK sepakat untuk melakukan penandatanganan Berita Acara Penggunaan Aset dengan ketentuan sebagai berikut : ', 0, 1, 'L');
        $pdf->Ln(4);

        $pdf->Cell(10, 6, '1.', 0, 0, 'L');
        $pdf->MultiCell(0, 6, 'PIHAK PERTAMA telah menyerahkan kepada PIHAK KEDUA aset BMN berupa ', 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Nama Barang', 0, 0, 'L');
        $pdf->Cell(10, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $row->nama_bmn, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Kode BMN', 0, 0, 'L');
        $pdf->Cell(10, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, 'BMN_LPSK-'.$row->id_bmn, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Jumlah', 0, 0, 'L');
        $pdf->Cell(10, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, '1', 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Keperluan', 0, 0, 'L');
        $pdf->Cell(10, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $row->keperluan, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Tanggal Penggunaan', 0, 0, 'L');
        $pdf->Cell(10, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $row->tgl_penggunaan, 0, 1, 'L');
        $pdf->Cell(10, 6, '', 0, 0, 'L');
        $pdf->Cell(45, 6, 'Catatan Kondisi BMN', 0, 0, 'L');
        $pdf->Cell(10, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, 'Baik', 0, 1, 'L');
        $pdf->Cell(10, 6, '2.', 0, 0, 'L');
        $pdf->MultiCell(0, 6, 'PIHAK KEDUA telah MENERIMA barang-barang tersebut pada butir (1) dengan baik.', 0, 1, 'L');
        $pdf->Cell(10, 6, '3.', 0, 0, 'L');
        $pdf->MultiCell(0, 6, 'PIHAK KEDUA dilarang memindahkan hak peminjaman aset tersebut kepada pihkal lain tanpa seizin Kuasa Pengguna Barang Lembaga Perlindungan Saksi dan Korban.', 0, 1, 'L');
        $pdf->Cell(10, 6, '4.', 0, 0, 'L');
        $pdf->MultiCell(0, 6, 'Jika terjadi kerusakan/kehilangan yang disebabkan karena kelalaian pengguna maka sepenuhnya menjadi Tanggung Jawab PIHAK KEDUA.', 0, 1, 'L');
        $pdf->Cell(10, 6, '5.', 0, 0, 'L');
        $pdf->MultiCell(0, 6, 'Aset harus dikembalikan kepada Lembaga Perlindungan Saksi dan Korban cq Bagian Umum setelah masa penggunaan berakhir.', 0, 1, 'L');
        $pdf->Ln(4);
        $pdf->MultiCell(0, 6, 'Demikian Berita Acara ini dibuat untuk dipergunakan sebagaimana mestinya.', 0, 1, 'L');
        $pdf->Ln(4);
        $pdf->Cell(85, 6, 'PIHAK KEDUA', 0, 0, 'C');
        $pdf->Cell(85, 6, 'PIHAK PERTAMA', 0, 1, 'C');
        $pdf->Cell(85, 25, '', 0, 0, 'C');
        $pdf->Cell(85, 25, '', 0, 1, 'C');
        $pdf->Cell(85, 6, $bio1->nama_cetak, 0, 0, 'C');
        $pdf->Cell(85, 6, 'Muhammad Ihsan, S.H.', 0, 1, 'C');
        $pdf->Ln(10);
        $pdf->Cell(0, 6, 'Mengetahui', 0, 1, 'C');
        $pdf->Cell(0, 6, 'Kepala Biro Administrasi', 0, 1, 'C');
        $pdf->Cell(0, 25, '', 0, 1, 'C');
        $pdf->Cell(0, 6, 'Dra. handari Restu Dewi, M.M.', 0, 1, 'C');
        // QRCODE yang mengajukan cuti
        //$pdf->Image(base_url().'uploads/qrcode/BMN-'.$row->id.'_'.$bio1->nip.'.png', 47, 243, 23, 0);
        // QRCODE approval atasan
        //$pdf->Image(base_url().'uploads/qrcode/BMN-'.$row->id.'_'.$bio2->nip.'.png', 130, 243, 23, 0);
        // QRCODE pejabat
        //if ($perjanjian->date_reaksi != null && $perjanjian->is_approved != '0') {
        //    $pdf->Image(base_url().'uploads/qrcode/PERJANJIAN-'.$perjanjian->id.'_'.$bio3->nip.'.png', 95, 303, 23, 0);
        //}
        // tampilkan dilayar
        $pdf->Output();
        // cetak output
        //$pdf->Output('uploads/berkas_bmn/PERJANJIAN-' . $id_trans . '.pdf', 'F');
    }

    public function form_ruang_rapat($id_trans)
    {
        $row = $this->Trans_ruang_rapat_model->get_by_id($id_trans);
        $row1 = $this->Ruang_rapat_model->get_by_id($row->id_ruang_rapat);
        $bio1 = $this->Master_biodata_model->get_by_nip($row->id_pegguna);
        $bio1_detial = $this->Detail_pegawai_model->get_by_nip($row->id_pegguna);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->id_atasan);
        $bio3 = $this->Master_biodata_model->get_by_nip($row->id_krt);

        $pdf = new FPDF('p', 'mm', 'A4');
        $pdf->SetTitle('Formulir Pengajuan Penggunaan Ruang Rapat');
        $pdf->SetMargins(15, 15, 15);
        $pdf->AddPage();
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Image(base_url() . 'assets/images/logo.png', 20, 13, 23, 0);
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'LEMBAGA PERLINDUNGAN SAKSI DAN KORBAN', 0, 1, 'C');
        $pdf->Cell(10, 6, '', 0, 0, 'C');
        $pdf->Cell(0, 6, 'SEKRETARIAT JENDRAL', 0, 1, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Jl. Raya Bogor KM. 24 No. 47-49', 0, 1, 'C');
        $pdf->Cell(10, 4, '', 0, 0, 'C');
        $pdf->Cell(0, 4, 'Kelurahan Susukan Kecamatan Ciracas Jakarta Timur', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(0, 6, '-------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(0, 6, 'FORMULIR PENGJUAN PENGGUNAAN RUANG RAPAT', 0, 1, 'C');
        $pdf->Ln(10);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(45, 6, 'Nama Pemohon', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->Cell(0, 6, $bio1->nama_cetak, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Unit Kerja', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        if (isset($bio1_detial->nmues2)) {
            $pdf->Cell(0, 6, $bio1_detial->nmues2, 0, 1, 'L');
        }else{
            $pdf->Cell(0, 6, $bio1_detial->nmues1, 0, 1, 'L');
        }
        $pdf->Cell(45, 6, 'Keperluan Kegiatan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row->kegiatan, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Tanggal Penggunaan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row->wkt_kegiatan, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Nama Ruangan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row1->ruangan, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Jumlah Peserta', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row->jml_peserta, 0, 1, 'L');
        $pdf->Cell(45, 6, 'Keterangan', 0, 0, 'L');
        $pdf->Cell(5, 6, ' : ', 0, 0, 'L');
        $pdf->MultiCell(0, 6, $row->keterangan_konsumsi, 0, 1, 'L');
        $pdf->Ln(25);
        if ($row->date_reaksi_dua != null) {
            $date = date("d - m - Y", strtotime($row->date_reaksi_dua));
            $pdf->Cell(155, 6, 'Jakarta, ' . $date, 0, 1, 'R');
        }
        $pdf->Cell(85, 6, 'Atasan Pemohon', 0, 0, 'C');
        $pdf->Cell(85, 6, 'Pemohon', 0, 1, 'C');
        $pdf->Cell(85, 30, '', 0, 0, 'C');
        $pdf->Cell(85, 30, '', 0, 1, 'C');
        $pdf->Cell(85, 6, $bio2->nama_cetak, 0, 0, 'C');
        $pdf->Cell(85, 6, $bio1->nama_cetak, 0, 1, 'C');
        $pdf->Ln(10);
        $pdf->Cell(0, 6, 'Mengetahui', 0, 1, 'C');
        $pdf->Cell(0, 6, 'Pejabat Berwenang', 0, 1, 'C');
        $pdf->Cell(0, 30, '', 0, 1, 'C');
        $pdf->Cell(0, 6, $bio3->nama_cetak, 0, 1, 'C');
        if ($row->status != 'DIBATALKAN') {
            // QRCODE yang mengajukan cuti
            if ($row->date_diajukan != null) {
                $pdf->Image(base_url() . 'uploads/qrcode/RR-' . $id_trans . '_' . $bio1->nip . '.png', 130, 139, 23, 0);
                //$pdf->Image(base_url() . 'assets/images/disetujui.png', 130, 139, 23, 0);
            }
            // QRCODE approval atasan
            if ($row->date_reaksi_satu != null && $row->status != 'TOLAK_1') {
                $pdf->Image(base_url() . 'uploads/qrcode/RR-' . $id_trans . '_' . $bio2->nip . '.png', 45, 139, 23, 0);
                //$pdf->Image(base_url() . 'assets/images/disetujui.png', 45, 139, 23, 0);
            }
            // QRCODE pejabat
            if ($row->date_reaksi_dua != null && $row->status != 'TOLAK_2') {
                $pdf->Image(base_url() . 'uploads/qrcode/RR-' . $id_trans . '_' . $bio3->nip . '.png', 92, 197, 23, 0);
            }
        }
        // tampilkan dilayar
        $pdf->Output();
        // cetak output
        // $pdf->Output('uploads/berka_rr/RR-' . $id_trans . '.pdf', 'F');
    }
}
