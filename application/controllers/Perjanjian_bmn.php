<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Perjanjian_bmn extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Perjanjian_bmn_model');
        $this->load->model('Trans_bmn_model');
        $this->load->model('Master_biodata_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'perjanjian_bmn/tbl_perjanjian_bmn_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        $nip = $this->session->userdata()['nip'];
        echo $this->Perjanjian_bmn_model->json($nip);
    }

    public function read($id)
    {
        $perjanjian = $this->Perjanjian_bmn_model->get_by_id($id);
        $row = $this->Trans_bmn_model->get_by_id($perjanjian->id_trans_bmn);
        $bio = $this->Master_biodata_model->get_by_nip($row->id_user);

        if ($perjanjian) {
            $data = array(
                'id' => $perjanjian->id,
                'id_trans_bmn' => $perjanjian->id_trans_bmn,
                'id_pejabat' => $perjanjian->id_pejabat,
                'is_approved' => $perjanjian->is_approved,
                // pemisah antara data perjanjian dan transaksi
                'id_user' => $row->id_user,
                'nama' => $bio->nama,
                'id_bmn' => $row->id_bmn,
                'nama_bmn' => $row->nama_bmn,
                'tgl_pinjam' => $row->tgl_pinjam,
                'tgl_kembali' => $row->tgl_kembali,
                'keperluan' => $row->keperluan,
                'lokasi' => $row->lokasi,
                'keterangan' => $row->keterangan,
                'tgl_penggunaan' => $row->tgl_penggunaan,
                'id_atasan' => $row->id_atasan,
                'id_krt' => $row->id_krt,
                'status' => $row->status,
            );
            $this->template->load('template', 'perjanjian_bmn/tbl_perjanjian_bmn_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('perjanjian_bmn'));
        }
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $data = array(
                'id_trans_bmn' => $this->input->post('id_trans_bmn', true),
                'id_pejabat' => $this->input->post('id_pejabat', true),
                'is_approved' => $this->input->post('is_approved', true),
            );

            $this->Perjanjian_bmn_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('perjanjian_bmn'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_trans_bmn', 'id trans bmn', 'trim|required');
        $this->form_validation->set_rules('id_pejabat', 'id pejabat', 'trim|required');
        $this->form_validation->set_rules('is_approved', 'is approved', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function create()
    {
        $data = array(
            'button' => 'Simpan',
            'action' => site_url('perjanjian_bmn/create_action'),
            'id' => set_value('id'),
            'id_trans_bmn' => set_value('id_trans_bmn'),
            'id_pejabat' => set_value('id_pejabat'),
            'is_approved' => set_value('is_approved'),
        );
        $this->template->load('template', 'perjanjian_bmn/tbl_perjanjian_bmn_form', $data);
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $data = array(
                'id_trans_bmn' => $this->input->post('id_trans_bmn', true),
                'id_pejabat' => $this->input->post('id_pejabat', true),
                'is_approved' => $this->input->post('is_approved', true),
            );

            $this->Perjanjian_bmn_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('perjanjian_bmn'));
        }
    }

    public function update($id)
    {
        $row = $this->Perjanjian_bmn_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('perjanjian_bmn/update_action'),
                'id' => set_value('id', $row->id),
                'id_trans_bmn' => set_value('id_trans_bmn', $row->id_trans_bmn),
                'id_pejabat' => set_value('id_pejabat', $row->id_pejabat),
                'is_approved' => set_value('is_approved', $row->is_approved),
            );
            $this->template->load('template', 'perjanjian_bmn/tbl_perjanjian_bmn_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('perjanjian_bmn'));
        }
    }

    public function delete($id)
    {
        $row = $this->Perjanjian_bmn_model->get_by_id($id);

        if ($row) {
            $this->Perjanjian_bmn_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('perjanjian_bmn'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('perjanjian_bmn'));
        }
    }

    public function buat_baru($id_trans_bmn)
    {
        $id_perjanjian = '';
        $row = $this->Perjanjian_bmn_model->get_by_id_trans($id_trans_bmn);
        if (empty($row)) {
            $data = array(
                'id_trans_bmn' => $id_trans_bmn,
                'id_pejabat' => '196404131991032001',
                'date_created' => date('Y-m-d H:i:s'),
                'date_ajukan' => date('Y-m-d H:i:s'),
            );

            $id_perjanjian = $this->Perjanjian_bmn_model->insert($data);
        } else {
            $id_perjanjian = $row->id;
        }
        redirect(site_url('perjanjian_bmn/read/' . $id_perjanjian));
    }

    public function up($id)
    {
        //$row = $this->Perjanjian_bmn_model->get_by_id($id);
        $nip = $this->session->userdata()['nip'];

        $data['is_approved'] = '1';
        $data['date_reaksi'] = date('Y-m-d H:i:s');
        $this->Perjanjian_bmn_model->update($id, $data);
        $this->generate_qr_code($id, $nip);
        redirect(base_url('perjanjian_bmn/read/' . $id));
    }

    public function generate_qr_code($id_cuti, $nip_actor)
    {
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE

        $config['cacheable'] = true; //boolean, the default is true
        $config['cachedir'] = './uploads/'; //string, the default is application/cache/
        $config['errorlog'] = './uploads/'; //string, the default is application/logs/
        $config['imagedir'] = './uploads/qrcode/'; //direktori penyimpanan qr code
        $config['quality'] = true; //boolean, the default is true
        $config['size'] = '1024'; //interger, the default is 1024
        $config['black'] = array(224, 255, 255); // array, default is array(255,255,255)
        $config['white'] = array(70, 130, 180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = 'PERJANJIAN-' . $id_cuti . '_' . $nip_actor . '.png'; //buat name dari qr code sesuai 

        $params['data'] = 'PERJANJIAN-' . $id_cuti . '_' . $nip_actor; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder 
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
    }

}

/* End of file Perjanjian_bmn.php */
/* Location: ./application/controllers/Perjanjian_bmn.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-17 16:33:38 */
/* http://harviacode.com */
