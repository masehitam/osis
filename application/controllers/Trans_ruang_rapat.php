<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Trans_ruang_rapat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Trans_ruang_rapat_model');
        $this->load->model('Ruang_rapat_model');
        $this->load->model('Master_biodata_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'trans_ruang_rapat/tbl_trans_ruang_rapat_list');
    }

    public function index_persetujuan()
    {
        $this->template->load('template', 'trans_ruang_rapat/tbl_trans_ruang_rapat_persetujuan_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        $nip = $this->session->userdata()['nip'];
        echo $this->Trans_ruang_rapat_model->json($nip);
    }

    public function json_persetujuan()
    {
        header('Content-Type: application/json');
        $nip = $this->session->userdata()['nip'];
        echo $this->Trans_ruang_rapat_model->json_persetujuan($nip);
    }

    public function read($id, $page = 'index')
    {
        $row = $this->Trans_ruang_rapat_model->get_by_id($id);
        $bio = $this->Master_biodata_model->get_by_nip($row->id_pegguna);
        $bio1 = $this->Master_biodata_model->get_by_nip($row->id_atasan);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->id_krt);
        $rr = $this->Ruang_rapat_model->get_by_id($row->id_ruang_rapat);

        if ($row) {
            $data = array(
                'id' => $row->id,
                'kegiatan' => $row->kegiatan,
                'id_ruang_rapat' => $row->id_ruang_rapat,
                'ruang_rapat' => $rr->ruangan,
                'id_pegguna' => $row->id_pegguna,
                'tgl_mulai' => $row->tgl_mulai,
                'tgl_akhir' => $row->tgl_akhir,
                'status' => $row->status,
                'wkt_kegiatan' => $row->wkt_kegiatan,
                'id_atasan' => $row->id_atasan,
                'nama_atasan' => $bio1->nama,
                'id_krt' => $row->id_krt,
                'nama_krt' => $bio2->nama,
                'konsumsi' => $row->konsumsi,
                'keterangan_konsumsi' => $row->keterangan_konsumsi,
                'alasan_pembatalan' => $row->alasan_pembatalan,
                'alasan_penolakan' => $row->alasan_penolakan,
                'jml_peserta' => $row->jml_peserta,
                'nama' => $bio->nama,
                'page' => $page,
            );
            $this->template->load('template', 'trans_ruang_rapat/tbl_trans_ruang_rapat_read', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function create_action()
    {
        //print_r($_POST);die();

        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $wkt_kegiatan = $this->input->post('wkt_kegiatan', true);
            $new_arr = array_map('trim', explode(' - ', $wkt_kegiatan));

            $konsumsi = $this->input->post('konsumsi', true);
            $konsumsi2 = "";
            if ($konsumsi) {
                $konsumsi2 = implode(",", $konsumsi);
            }

            $data = array(
                'kegiatan' => $this->input->post('kegiatan', true),
                'id_pegguna' => $this->session->userdata()['nip'],
                'id_ruang_rapat' => set_value('id_ruang_rapat'),
                'tgl_mulai' => strtotime($new_arr[0]),
                'tgl_akhir' => strtotime($new_arr[1]),
                'wkt_kegiatan' => $wkt_kegiatan,
                'id_atasan' => $this->input->post('id_atasan', true),
                'id_krt' => $this->input->post('id_krt', true),
                'konsumsi' => $konsumsi2,
                'keterangan_konsumsi' => $this->input->post('keterangan_konsumsi', true),
                'jml_peserta' => $this->input->post('jml_peserta', true),
            ); //print_r($data);die();

            $this->Trans_ruang_rapat_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Ditambahkan</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('kegiatan', 'kegiatan', 'trim|required');
        //$this->form_validation->set_rules('id_pegguna', 'id pegguna', 'trim|required');
        //$this->form_validation->set_rules('tgl_pesan', 'tgl pesan', 'trim|required');
        //$this->form_validation->set_rules('tgl_mulai', 'tgl mulai', 'trim|required');
        //$this->form_validation->set_rules('tgl_akhir', 'tgl akhir', 'trim|required');
        $this->form_validation->set_rules('wkt_kegiatan', 'tgl akhir', 'trim|required');
        //$this->form_validation->set_rules('status', 'id status', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function create()
    {
        $data = array(
            'button' => 'Simpan',
            'action' => site_url('trans_ruang_rapat/create_action'),
            'id' => set_value('id'),
            'kegiatan' => set_value('kegiatan'),
            'id_pegguna' => set_value('id_pegguna'),
            'ruang_rapat' => set_value('ruang_rapat'),
            'id_ruang_rapat' => set_value('id_ruang_rapat'),
            'tgl_mulai' => set_value('tgl_mulai'),
            'tgl_akhir' => set_value('tgl_akhir'),
            'status' => set_value('status'),
            'wkt_kegiatan' => set_value('wkt_kegiatan'),
            'id_atasan' => set_value('id_atasan'),
            'nama_atasan' => set_value('nama_atasan'),
            'id_krt' => set_value('id_krt'),
            'nama_krt' => set_value('nama_krt'),
            'konsumsi' => set_value('konsumsi'),
            'keterangan_konsumsi' => set_value('keterangan_konsumsi'),
            'jml_peserta' => set_value('jml_peserta'),
        );

        $data['id_krt'] = '198903162014021001';
        $data['nama_krt'] = 'Muhammad Ihsan';
        $this->template->load('template', 'trans_ruang_rapat/tbl_trans_ruang_rapat_form', $data);
    }

    public function update_action()
    {
        $id = $this->input->post('id', true);
        $row = $this->Trans_ruang_rapat_model->get_by_id($id);
        if ($row->status != 'DIBUAT') {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data yang sudah diajukan tidak dapat diperbarui</div>');
            redirect(site_url('trans_ruang_rapat'));
        }

        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $wkt_kegiatan = $this->input->post('wkt_kegiatan', true);
            $new_arr = array_map('trim', explode(' - ', $wkt_kegiatan));

            $konsumsi = $this->input->post('konsumsi', true);
            $konsumsi2 = "";
            if ($konsumsi) {
                $konsumsi2 = implode(",", $konsumsi);
            }

            $data = array(
                'kegiatan' => $this->input->post('kegiatan', true),
                'id_pegguna' => $this->session->userdata()['nip'],
                'id_ruang_rapat' => set_value('id_ruang_rapat'),
                'tgl_mulai' => strtotime($new_arr[0]),
                'tgl_akhir' => strtotime($new_arr[1]),
                'wkt_kegiatan' => $wkt_kegiatan,
                'id_atasan' => $this->input->post('id_atasan', true),
                'id_krt' => $this->input->post('id_krt', true),
                'konsumsi' => $konsumsi2,
                'keterangan_konsumsi' => $this->input->post('keterangan_konsumsi', true),
                'jml_peserta' => $this->input->post('jml_peserta', true),
            ); //print_r($data);die();

            $this->Trans_ruang_rapat_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Diperbarui</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function update($id)
    {
        $row = $this->Trans_ruang_rapat_model->get_by_id($id);
        $this->_rule_update($row);
        $bio1 = $this->Master_biodata_model->get_by_nip($row->id_atasan);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->id_krt);
        $rr = $this->Ruang_rapat_model->get_by_id($row->id_ruang_rapat);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('trans_ruang_rapat/update_action'),
                'id' => set_value('id', $row->id),
                'kegiatan' => set_value('kegiatan', $row->kegiatan),
                'id_pegguna' => set_value('id_pegguna', $row->id_pegguna),
                'ruang_rapat' => set_value('ruang_rapat', $rr->ruangan),
                'id_ruang_rapat' => set_value('id_ruang_rapat', $row->id_ruang_rapat),
                'tgl_mulai' => set_value('tgl_mulai', $row->tgl_mulai),
                'tgl_akhir' => set_value('tgl_akhir', $row->tgl_akhir),
                'status' => set_value('status', $row->status),
                'wkt_kegiatan' => set_value('wkt_kegiatan', $row->wkt_kegiatan),
                'id_atasan' => set_value('id_atasan', $row->id_atasan),
                'nama_atasan' => set_value('nama_atasan', $bio1->nama),
                'id_krt' => set_value('id_krt', $row->id_atasan),
                'nama_krt' => set_value('nama_krt', $bio2->nama),
                'jml_peserta' => set_value('jml_peserta', $row->jml_peserta),
                'konsumsi' => set_value('konsumsi', $row->konsumsi),
                'keterangan_konsumsi' => set_value('keterangan_konsumsi', $row->keterangan_konsumsi),
            );
            $this->template->load('template', 'trans_ruang_rapat/tbl_trans_ruang_rapat_form', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function _rule_update($row)
    {
        if ($row->status != "DIBUAT") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Detail Peminjaman yang Sudah Diajukan Tidak Dapat Diperbarui</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function delete($id)
    {
        $row = $this->Trans_ruang_rapat_model->get_by_id($id);
        $this->_rule_hapus($row);

        if ($row) {
            $this->Trans_ruang_rapat_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Dihapus</div>');
            redirect(site_url('trans_ruang_rapat'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function _rule_hapus($row)
    {
        if ($row->status == "DISETUJUI") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Status Peminjaman Sudah Disetujui Tidak Dapat Dihapus</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function pembatalan($id)
    {
        $row = $this->Trans_ruang_rapat_model->get_by_id($id);
        $this->_rule_pembatalan($row);
        $this->_rule_waktu($row);

        if ($row) {
            $data = array(
                'button' => 'Batalkan',
                'action' => site_url('trans_ruang_rapat/pembatalan_action'),
                'id' => set_value('id', $row->id),
                'alasan' => set_value('kegiatan'),
            );
            $this->template->load('template', 'trans_ruang_rapat/tbl_trans_ruang_rapat_batal', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function _rule_pembatalan($row)
    {
        if ($row->status == "DIBATALKAN") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Status Peminjaman Sudah Dibatalkan</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function _rule_waktu($row)
    {
        $date = new DateTime();
        if ($date->getTimestamp() >= $row->tgl_mulai) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Peminjaman tidak dapat dibatalkan apabila telah memasuki waktu peminjaman</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function pembatalan_action()
    {
        $this->_pembatalan_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $data = array(
                'status' => 'DIBATALKAN',
                'alasan_pembatalan' => $this->input->post('alasan', true),
            );

            $this->Trans_ruang_rapat_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Diperbarui</div>');
            redirect(site_url('trans_ruang_rapat'));
        }
    }

    public function _pembatalan_rules()
    {
        $this->form_validation->set_rules('alasan', 'kegiatan', 'trim|required');
    }

    public function up($id)
    {
        $row = $this->Trans_ruang_rapat_model->get_by_id($id);
        $nip = $this->session->userdata()['nip'];
        $isOccupied = $this->Trans_ruang_rapat_model->checkKetersediaan($id, $row->tgl_mulai, $row->tgl_akhir);
        //print_r($isOccupied);die();
        switch ($row->status) {
            case 'DIBUAT':
                if (!$isOccupied) {
                    $data['status'] = 'AJUKAN_1';
                    $data['date_diajukan'] = date('Y-m-d H:i:s');
                    $this->Trans_ruang_rapat_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-error" role="alert"> Gagal meminjam karena ruangana dipakai pada waktu tersebut</div>');
                }
                redirect(base_url('trans_ruang_rapat/read/' . $id));
                break;
            case 'AJUKAN_1':
                if (!$isOccupied) {
                    $data['status'] = 'AJUKAN_2';
                    $data['date_reaksi_satu'] = date('Y-m-d H:i:s');
                    $this->Trans_ruang_rapat_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-error" role="alert"> Gagal meminjam karena ruangana dipakai pada waktu tersebut</div>');
                }
                redirect(base_url('trans_ruang_rapat/read/' . $id . '/persetujuan'));
                break;
            case 'AJUKAN_2':
                if (!$isOccupied) {
                    $data['status'] = 'DISETUJUI';
                    $data['date_reaksi_dua'] = date('Y-m-d H:i:s');
                    $this->Trans_ruang_rapat_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-error" role="alert"> Gagal meminjam karena ruangana dipakai pada waktu tersebut</div>');
                }
                redirect(base_url('trans_ruang_rapat/read/' . $id . '/persetujuan'));
                break;

            default:
                echo 'gagal';
                die();
                break;
        }
    }

    public function generate_qr_code($id_cuti, $nip_actor)
    {
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE

        $config['cacheable'] = true; //boolean, the default is true
        $config['cachedir'] = './uploads/'; //string, the default is application/cache/
        $config['errorlog'] = './uploads/'; //string, the default is application/logs/
        $config['imagedir'] = './uploads/qrcode/'; //direktori penyimpanan qr code
        $config['quality'] = true; //boolean, the default is true
        $config['size'] = '1024'; //interger, the default is 1024
        $config['black'] = array(224, 255, 255); // array, default is array(255,255,255)
        $config['white'] = array(70, 130, 180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = 'RR-' . $id_cuti . '_' . $nip_actor . '.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = 'RR-' . $id_cuti . '_' . $nip_actor; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        //$this->mahasiswa_model->simpan_mahasiswa($nim,$nama,$prodi,$image_name); //simpan ke database
        //redirect('mahasiswa'); //redirect ke mahasiswa usai simpan data
    }

    public function down($id)
    {
        $row = $this->Trans_ruang_rapat_model->get_by_id($id);
        //PRINT_r($row);die();
        switch ($row->status) {
            case 'AJUKAN_1':
                $data['status'] = 'TOLAK_1';
                $data['date_reaksi_satu'] = date('Y-m-d H:i:s');
                $data['alasan_penolakan'] = $this->input->post('alasan_penolakan', true);
                $this->Trans_ruang_rapat_model->update($id, $data);
                redirect(base_url('trans_ruang_rapat/read/' . $id . '/persetujuan'));
                break;

            case 'AJUKAN_2':
                $data['status'] = 'TOLAK_2';
                $data['date_reaksi_dua'] = date('Y-m-d H:i:s');
                $data['alasan_penolakan'] = $this->input->post('alasan_penolakan', true);
                $this->Trans_ruang_rapat_model->update($id, $data);
                redirect(base_url('trans_ruang_rapat/read/' . $id . '/persetujuan'));
                break;

            default:
                echo 'gagal';
                die();
                break;
        }
    }
}

/* End of file Trans_ruang_rapat.php */
/* Location: ./application/controllers/Trans_ruang_rapat.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-10 15:09:35 */
/* http://harviacode.com */
