<?php

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->load->view('auth/login');
    }

    public function forgot_password()
    {
        $this->load->view('auth/forgot_password');
    }

    public function cheklogin()
    {
        $email = $this->input->post('email');
        //$password   = $this->input->post('password');
        $password = $this->input->post('password', true);
        $hashPass = password_hash($password, PASSWORD_DEFAULT);
        $test = password_verify($password, $hashPass);
        // query chek users
        $this->db->where('email', $email);
        //$this->db->where('password',  $test);
        $users = $this->db->get('tbl_user');
        if ($users->num_rows() > 0) {
            $user = $users->row_array();
            if (password_verify($password, $user['password'])) {
                // retrive user data to session
                $this->session->set_userdata($user);
                redirect('welcome');
            } else {
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('status_login', 'email atau password yang anda input salah');
            redirect('auth');
        }
    }

    public function forgot_password_action()
    {
        $email = $this->input->post('email');
        $this->db->where('email_kantor', $email);
        $this->db->or_where('email_pribadi', $email);
        $users = $this->db->get('master_biodata');
        if ($users->num_rows() > 0) {
            $user = $users->row_array();
            //print_r($user);die();
            $this->send_mail($user);
            $this->session->set_flashdata('status_login', 'alamat email tidak terdaftar, silahkan hubungi administrator');
            redirect('auth');
        } else {
            $this->session->set_flashdata('status_login', 'alamat email tidak terdaftar, silahkan hubungi administrator');
            redirect('auth');
        }
    }

    public function send_mail($user)
    {
        $from_email = "no-reply@lpsk.go.id";

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.lpsk.go.id',
            'smtp_port' => 465,
            'smtp_user' => 'simpelkan@lpsk.go.id',
            'smtp_pass' => 'Cijantung@47',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->from($from_email, "Sistem Informasi Pelayanan Kantor");
        $this->email->to($row->email);//https://www.guerrillamail.com/

        $this->email->subject('Permohonan Reset Password');
        $this->email->message('Untuk mereset password silahkan anda kunjungi <a href=' . base_url() . 'auth/update_password/' . $user['nip'] . '>Halaman ini</a>');
        //Send mail
        if ($this->email->send()) {
            $this->session->set_flashdata("message", "Email berhasil terkirim.");
        } else {
            $this->session->set_flashdata("message", "Email gagal dikirim.");
        }
    }

    public function update_password_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->update_password($this->input->post('nip', true));
        } else {
            $password = $this->input->post('password', true);
            $repassword = $this->input->post('repassword', true);
            $nip = $this->input->post('nip', true);
            if (strcmp($password, $repassword) == 0) {
                $hashPass = password_hash($password, PASSWORD_DEFAULT);
                $test = password_verify($password, $hashPass);
                // query chek users
                $this->db->where('nip', $nip);
                $this->db->update('tbl_user', array('password' => $hashPass));
                if ($this->db->affected_rows() > 0) {
                    $this->session->set_flashdata('status_login', 'Kata sandi sudah direset, Silahkan masuk kembali');
                    redirect('auth');
                } else {
                    $this->session->set_flashdata('status_login', 'Gagal mereset passowrd, Silahkan hubungi admin');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('status_login', 'password tidak sama');
                redirect('auth');
            }

        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('repassword', 'Konfirmasi Password', 'trim|required|matches[password]');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function update_password($nip)
    {
        $data['nip'] = $nip;
        $this->load->view('auth/update_password', $data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('status_login', 'Anda sudah berhasil keluar dari aplikasi');
        redirect('auth');
    }
}
