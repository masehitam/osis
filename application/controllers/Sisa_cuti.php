<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sisa_cuti extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Sisa_cuti_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'sisa_cuti/rf_sisa_cuti_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Sisa_cuti_model->json();
    }

    public function read($id)
    {
        $row = $this->Sisa_cuti_model->get_by_id($id);
        if ($row) {
            $data = array(
                'master_biodata_id' => $row->master_biodata_id,
                'bkn_id' => $row->bkn_id,
                'nip' => $row->nip,
                'nama_cetak' => $row->nama_cetak,
                'th2018' => $row->th2018,
                'th2019' => $row->th2019,
                'th2020' => $row->th2020,
                'th2021' => $row->th2021,
                'th2022' => $row->th2022,
                'th2023' => $row->th2023,
                'th2024' => $row->th2024,
                'th2025' => $row->th2025,
            );
            $this->template->load('template', 'sisa_cuti/rf_sisa_cuti_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sisa_cuti'));
        }
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'bkn_id' => $this->input->post('bkn_id', TRUE),
                'nip' => $this->input->post('nip', TRUE),
                'nama_cetak' => $this->input->post('nama_cetak', TRUE),
                'th2018' => $this->input->post('th2018', TRUE),
                'th2019' => $this->input->post('th2019', TRUE),
                'th2020' => $this->input->post('th2020', TRUE),
                'th2021' => $this->input->post('th2021', TRUE),
                'th2022' => $this->input->post('th2022', TRUE),
                'th2023' => $this->input->post('th2023', TRUE),
                'th2024' => $this->input->post('th2024', TRUE),
                'th2025' => $this->input->post('th2025', TRUE),
            );

            $this->Sisa_cuti_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('sisa_cuti'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('bkn_id', 'bkn id', 'trim|required');
        $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('nama_cetak', 'nama cetak', 'trim|required');
        $this->form_validation->set_rules('th2018', 'th2018', 'trim|required');
        $this->form_validation->set_rules('th2019', 'th2019', 'trim|required');
        $this->form_validation->set_rules('th2020', 'th2020', 'trim|required');
        $this->form_validation->set_rules('th2021', 'th2021', 'trim|required');
        $this->form_validation->set_rules('th2022', 'th2022', 'trim|required');
        $this->form_validation->set_rules('th2023', 'th2023', 'trim|required');
        $this->form_validation->set_rules('th2024', 'th2024', 'trim|required');
        $this->form_validation->set_rules('th2025', 'th2025', 'trim|required');

        $this->form_validation->set_rules('master_biodata_id', 'master_biodata_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function create()
    {
        $data = array(
            'button' => 'Simpan',
            'action' => site_url('sisa_cuti/create_action'),
            'master_biodata_id' => set_value('master_biodata_id'),
            'bkn_id' => set_value('bkn_id'),
            'nip' => set_value('nip'),
            'nama_cetak' => set_value('nama_cetak'),
            'th2018' => set_value('th2018'),
            'th2019' => set_value('th2019'),
            'th2020' => set_value('th2020'),
            'th2021' => set_value('th2021'),
            'th2022' => set_value('th2022'),
            'th2023' => set_value('th2023'),
            'th2024' => set_value('th2024'),
            'th2025' => set_value('th2025'),
        );
        $this->template->load('template', 'sisa_cuti/rf_sisa_cuti_form', $data);
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('master_biodata_id', TRUE));
        } else {
            $data = array(
                'bkn_id' => $this->input->post('bkn_id', TRUE),
                'nip' => $this->input->post('nip', TRUE),
                'nama_cetak' => $this->input->post('nama_cetak', TRUE),
                'th2018' => $this->input->post('th2018', TRUE),
                'th2019' => $this->input->post('th2019', TRUE),
                'th2020' => $this->input->post('th2020', TRUE),
                'th2021' => $this->input->post('th2021', TRUE),
                'th2022' => $this->input->post('th2022', TRUE),
                'th2023' => $this->input->post('th2023', TRUE),
                'th2024' => $this->input->post('th2024', TRUE),
                'th2025' => $this->input->post('th2025', TRUE),
            );

            $this->Sisa_cuti_model->update($this->input->post('master_biodata_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('sisa_cuti'));
        }
    }

    public function update($id)
    {
        $row = $this->Sisa_cuti_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('sisa_cuti/update_action'),
                'master_biodata_id' => set_value('master_biodata_id', $row->master_biodata_id),
                'bkn_id' => set_value('bkn_id', $row->bkn_id),
                'nip' => set_value('nip', $row->nip),
                'nama_cetak' => set_value('nama_cetak', $row->nama_cetak),
                'th2018' => set_value('th2018', $row->th2018),
                'th2019' => set_value('th2019', $row->th2019),
                'th2020' => set_value('th2020', $row->th2020),
                'th2021' => set_value('th2021', $row->th2021),
                'th2022' => set_value('th2022', $row->th2022),
                'th2023' => set_value('th2023', $row->th2023),
                'th2024' => set_value('th2024', $row->th2024),
                'th2025' => set_value('th2025', $row->th2025),
            );
            $this->template->load('template', 'sisa_cuti/rf_sisa_cuti_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sisa_cuti'));
        }
    }

    public function delete($id)
    {
        $row = $this->Sisa_cuti_model->get_by_id($id);

        if ($row) {
            $this->Sisa_cuti_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('sisa_cuti'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('sisa_cuti'));
        }
    }

}

/* End of file Sisa_cuti.php */
/* Location: ./application/controllers/Sisa_cuti.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-06 19:42:01 */
/* http://harviacode.com */