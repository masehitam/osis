<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Trans_bmn extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Trans_bmn_model');
        $this->load->model('Master_biodata_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'trans_bmn/tbl_trans_bmn_list');
    }

    public function index_persetujuan()
    {
        $this->template->load('template', 'trans_bmn/tbl_trans_bmn_persetujuan_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        $nip = $this->session->userdata()['nip'];
        echo $this->Trans_bmn_model->json($nip);
    }

    public function json_persetujuan()
    {
        header('Content-Type: application/json');
        $nip = $this->session->userdata()['nip'];
        echo $this->Trans_bmn_model->json_persetujuan($nip);
    }

    public function read($id, $page = 'index')
    {
        $row = $this->Trans_bmn_model->get_by_id($id);
        $bio = $this->Master_biodata_model->get_by_nip($row->id_user);
        $bio1 = $this->Master_biodata_model->get_by_nip($row->id_atasan);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->id_krt);

        if ($row) {
            $data = array(
                'id' => $row->id,
                'id_user' => $row->id_user,
                'nama' => $bio->nama,
                'id_bmn' => $row->id_bmn,
                'nama_bmn' => $row->nama_bmn,
                'tgl_pinjam' => $row->tgl_pinjam,
                'tgl_kembali' => $row->tgl_kembali,
                'keperluan' => $row->keperluan,
                'lokasi' => $row->lokasi,
                'keterangan' => $row->keterangan,
                'tgl_penggunaan' => $row->tgl_penggunaan,
                'id_atasan' => $row->id_atasan,
                'nama_atasan' => $bio1->nama,
                'alasan_pembatalan' => $row->alasan_pembatalan,
                'alasan_penolakan' => $row->alasan_penolakan,
                'id_krt' => $row->id_krt,
                'nama_krt' => $bio2->nama,
                'status' => $row->status,
                'page' => $page, // menentukan halaman back
            );

            //print_r($data);die();
            $this->template->load('template', 'trans_bmn/tbl_trans_bmn_read', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->create();
        } else {
            $tgl_penggunaan = $this->input->post('tgl_penggunaan', true);
            $tgl_penggunaan2 = explode(' - ', $tgl_penggunaan);

            $date1 = explode('/', $tgl_penggunaan2[0]);
            $date2 = explode('/', $tgl_penggunaan2[1]);

            $finalDate1 = $date1[2] . '-' . $date1[1] . '-' . $date1[0];
            $finalDate2 = $date2[2] . '-' . $date2[1] . '-' . $date2[0];

            $data = array(
                'id_user' => $this->session->userdata()['nip'],
                'nama_bmn' => $this->input->post('nama_bmn', true),
                'id_bmn' => $this->input->post('id_bmn', true),
                'tgl_pinjam' => strtotime($finalDate1),
                'tgl_kembali' => strtotime($finalDate2),
                'tgl_penggunaan' => $this->input->post('tgl_penggunaan', true),
                'keperluan' => $this->input->post('keperluan', true),
                'lokasi' => $this->input->post('lokasi', true),
                'keterangan' => $this->input->post('keterangan', true),
                'id_atasan' => $this->input->post('id_atasan', true),
                'id_krt' => $this->input->post('id_krt', true),
            );

            $this->Trans_bmn_model->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Ditambahkan</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function _rules()
    {
        //$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
        //$this->form_validation->set_rules('id_bmn', 'id bmn', 'trim|required');
        $this->form_validation->set_rules('nama_bmn', 'nama bmn', 'trim|required');
        //$this->form_validation->set_rules('tgl_pinjam', 'tgl pinjam', 'trim|required');
        //$this->form_validation->set_rules('tgl_kembali', 'tgl kembali', 'trim|required');
        $this->form_validation->set_rules('keperluan', 'keperluan', 'trim|required');
        $this->form_validation->set_rules('lokasi', 'lokasi', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
        $this->form_validation->set_rules('tgl_penggunaan', 'tgl penggunaan', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function create()
    {
        $data = array(
            'button' => 'Simpan',
            'action' => site_url('trans_bmn/create_action'),
            'id' => set_value('id'),
            'id_user' => set_value('id_user'),
            // 'id_bmn' => set_value('id_bmn'),
            'id_bmn' => set_value('id_bmn'),
            'nama_bmn' => set_value('nama_bmn'),
            'tgl_pinjam' => set_value('tgl_pinjam'),
            'tgl_kembali' => set_value('tgl_kembali'),
            'keperluan' => set_value('keperluan'),
            'lokasi' => set_value('lokasi'),
            'keterangan' => set_value('keterangan'),
            'tgl_penggunaan' => set_value('tgl_penggunaan'),
            'id_atasan' => set_value('id_atasan'),
            'nama_atasan' => set_value('nama_atasan'),
            'id_krt' => set_value('id_krt'),
            'nama_krt' => set_value('nama_krt'),
        );

        $data['id_krt'] = '198903162014021001';
        $data['nama_krt'] = 'Muhammad Ihsan';
        $this->template->load('template', 'trans_bmn/tbl_trans_bmn_form', $data);
    }

    public function update_action()
    {
        $id = $this->input->post('id', true);
        $row = $this->Trans_bmn_model->get_by_id($id);
        if ($row->status != 'DIBUAT') {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data yang sudah diajukan tidak dapat diperbarui</div>');
            redirect(site_url('trans_bmn'));
        }

        $this->_rules();
        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $tgl_penggunaan = $this->input->post('tgl_penggunaan', true);
            $tgl_penggunaan2 = explode(' - ', $tgl_penggunaan);

            $date1 = explode('/', $tgl_penggunaan2[0]);
            $date2 = explode('/', $tgl_penggunaan2[1]);

            $finalDate1 = $date1[2] . '-' . $date1[1] . '-' . $date1[0];
            $finalDate2 = $date2[2] . '-' . $date2[1] . '-' . $date2[0];

            $data = array(
                'id_user' => $this->session->userdata()['nip'],
                'nama_bmn' => $this->input->post('nama_bmn', true),
                'id_bmn' => $this->input->post('id_bmn', true),
                'tgl_pinjam' => strtotime($finalDate1),
                'tgl_kembali' => strtotime($finalDate2),
                'tgl_penggunaan' => $this->input->post('tgl_penggunaan', true),
                'keperluan' => $this->input->post('keperluan', true),
                'lokasi' => $this->input->post('lokasi', true),
                'keterangan' => $this->input->post('keterangan', true),
                'id_atasan' => $this->input->post('id_atasan', true),
                'id_krt' => $this->input->post('id_krt', true),
            );

            $this->Trans_bmn_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Diperbarui</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function update($id)
    {
        $row = $this->Trans_bmn_model->get_by_id($id);
        $this->_rule_update($row);

        $bio1 = $this->Master_biodata_model->get_by_nip($row->id_atasan);
        $bio2 = $this->Master_biodata_model->get_by_nip($row->id_krt);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('trans_bmn/update_action'),
                'id' => set_value('id', $row->id),
                'id_user' => set_value('id_user', $row->id_user),
                'id_bmn' => set_value('id_bmn', $row->id_bmn),
                'nama_bmn' => set_value('nama_bmn', $row->nama_bmn),
                'tgl_pinjam' => set_value('tgl_pinjam', $row->tgl_pinjam),
                'tgl_kembali' => set_value('tgl_kembali', $row->tgl_kembali),
                'keperluan' => set_value('keperluan', $row->keperluan),
                'lokasi' => set_value('lokasi', $row->lokasi),
                'keterangan' => set_value('keterangan', $row->keterangan),
                'tgl_penggunaan' => set_value('tgl_penggunaan', $row->tgl_penggunaan),
                'id_atasan' => set_value('id_atasan', $row->id_atasan),
                'nama_atasan' => set_value('nama_atasan', $bio1->nama),
                'id_krt' => set_value('id_krt', $row->id_krt),
                'nama_krt' => set_value('nama_krt', $bio2->nama),
            );
            $this->template->load('template', 'trans_bmn/tbl_trans_bmn_form', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data tidak ditemukan</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function _rule_update($row)
    {
        if ($row->status != "DIBUAT") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Detail Peminjaman yang Sudah Diajukan Tidak Dapat Diperbarui</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function delete($id)
    {
        $row = $this->Trans_bmn_model->get_by_id($id);
        $this->_rule_hapus($row);

        if ($row) {
            $this->Trans_bmn_model->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Dihapus</div>');
            redirect(site_url('trans_bmn'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function _rule_hapus($row)
    {
        if ($row->status == "DISETUJUI") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Status Peminjaman Sudah Disetujui Tidak Dapat Dihapus</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function pembatalan($id)
    {
        $row = $this->Trans_bmn_model->get_by_id($id);
        $this->_rule_pembatalan($row);
        $this->_rule_waktu($row);

        if ($row) {
            $data = array(
                'button' => 'Batalkan',
                'action' => site_url('trans_bmn/pembatalan_action'),
                'id' => set_value('id', $row->id),
                'alasan' => set_value('kegiatan'),
            );
            $this->template->load('template', 'trans_bmn/tbl_trans_bmn_batal', $data);
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Data Tidak Ditemukan</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function _rule_pembatalan($row)
    {
        if ($row->status == "DIBATALKAN") {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Status Peminjaman Sudah Dibatalkan</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function _rule_waktu($row)
    {
        $date = new DateTime();
        if ($date->getTimestamp() >= $row->tgl_pinjam) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Peminjaman tidak dapat dibatalkan apabila telah memasuki waktu peminjaman</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function pembatalan_action()
    {
        $this->_pembatalan_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $data = array(
                'status' => 'DIBATALKAN',
                'alasan_pembatalan' => $this->input->post('alasan', true),
            );

            $this->Trans_bmn_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert"> Data Berhasil Diperbarui</div>');
            redirect(site_url('trans_bmn'));
        }
    }

    public function _pembatalan_rules()
    {
        $this->form_validation->set_rules('alasan', 'kegiatan', 'trim|required');
    }

    public function up($id)
    {
        $row = $this->Trans_bmn_model->get_by_id($id);
        $nip = $this->session->userdata()['nip'];
        $isOccupied = $this->Trans_bmn_model->checkKetersediaan($id, $row->tgl_pinjam, $row->tgl_kembali);

        switch ($row->status) {
            case 'DIBUAT':
                if (!$isOccupied) {
                    $data['status'] = 'AJUKAN_1';
                    $data['date_diajukan'] = date('Y-m-d H:i:s');
                    $this->Trans_bmn_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-error" role="alert"> Gagal meminjam karena ruangana dipakai pada waktu tersebut</div>');
                }
                redirect(base_url('trans_bmn/read/' . $id));
                break;

            case 'AJUKAN_1':
                if (!$isOccupied) {
                    $data['status'] = 'AJUKAN_2';
                    $data['date_reaksi_satu'] = date('Y-m-d H:i:s');
                    $this->Trans_bmn_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-error" role="alert"> Gagal meminjam karena ruangana dipakai pada waktu tersebut</div>');
                }
                redirect(base_url('trans_bmn/read/' . $id . '/persetujuan'));
                break;

            case 'AJUKAN_2':
                if (!$isOccupied) {
                    $data['status'] = 'DISETUJUI';
                    $data['date_reaksi_dua'] = date('Y-m-d H:i:s');
                    $this->Trans_bmn_model->update($id, $data);
                    $this->generate_qr_code($id, $nip);
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-error" role="alert"> Gagal meminjam karena ruangana dipakai pada waktu tersebut</div>');
                }
                redirect(base_url('trans_bmn/read/' . $id . '/persetujuan'));
                break;
            case 'DISETUJUI':
                $data['status'] = 'DIKEMBALIKAN';
                $data['date_dikembalikan'] = date('Y-m-d H:i:s');
                $this->Trans_bmn_model->update($id, $data);

                redirect(base_url('perjanjian_bmn/'));
                break;

            default:
                echo 'gagal';
                die();
                break;
        }
    }

    public function generate_qr_code($id_cuti, $nip_actor)
    {
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE

        $config['cacheable'] = true; //boolean, the default is true
        $config['cachedir'] = './uploads/'; //string, the default is application/cache/
        $config['errorlog'] = './uploads/'; //string, the default is application/logs/
        $config['imagedir'] = './uploads/qrcode/'; //direktori penyimpanan qr code
        $config['quality'] = true; //boolean, the default is true
        $config['size'] = '1024'; //interger, the default is 1024
        $config['black'] = array(224, 255, 255); // array, default is array(255,255,255)
        $config['white'] = array(70, 130, 180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $image_name = 'BMN-' . $id_cuti . '_' . $nip_actor . '.png'; //buat name dari qr code sesuai dengan nim

        $params['data'] = 'BMN-' . $id_cuti . '_' . $nip_actor; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        //$this->mahasiswa_model->simpan_mahasiswa($nim,$nama,$prodi,$image_name); //simpan ke database
        //redirect('mahasiswa'); //redirect ke mahasiswa usai simpan data
    }

    public function down($id)
    {
        //print_r($_POST);die();
        $row = $this->Trans_bmn_model->get_by_id($id);
        switch ($row->status) {
            case 'AJUKAN_1':
                $data['status'] = 'TOLAK_1';
                $data['date_reaksi_satu'] = date('Y-m-d H:i:s');
                $data['alasan_penolakan'] = $this->input->post('alasan_penolakan', true);
                $this->Trans_bmn_model->update($id, $data);
                redirect(base_url('trans_bmn/read/' . $id . '/persetujuan'));
                break;

            case 'AJUKAN_2':
                $data['status'] = 'TOLAK_2';
                $data['date_reaksi_dua'] = date('Y-m-d H:i:s');
                $data['alasan_penolakan'] = $this->input->post('alasan_penolakan', true);
                $this->Trans_bmn_model->update($id, $data);
                redirect(base_url('trans_bmn/read/' . $id . '/persetujuan'));
                break;

            default:
                echo 'gagal';
                die();
                break;
        }
    }
}

/* End of file Trans_bmn.php */
/* Location: ./application/controllers/Trans_bmn.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-10 20:24:40 */
/* http://harviacode.com */
