<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dispo_ruang_rapat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Dispo_rurang_rapat_model');
        $this->load->model('Trans_ruang_rapat_model');
        $this->load->model('Ruang_rapat_model');
        $this->load->model('Master_biodata_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'dispo_ruang_rapat/tbl_dispo_rr_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        $nip = $this->session->userdata()['nip'];
        echo $this->Dispo_rurang_rapat_model->json($nip);
    }

    public function read($id)
    {
        $dispo_rr = $this->Dispo_rurang_rapat_model->get_by_id($id);
        $row = $this->Trans_ruang_rapat_model->get_by_id($dispo_rr->id_trans_rr);
        $bio = $this->Master_biodata_model->get_by_nip($row->id_pegguna);
        $bio_penerima = $this->Master_biodata_model->get_by_nip($dispo_rr->id_penerima);
        $rr = $this->Ruang_rapat_model->get_by_id($row->id_ruang_rapat);
        if ($row) {
            $data = array(
                'id' => $dispo_rr->id,
                'id_trans_rr' => $dispo_rr->id_trans_rr,
                'id_penerima' => $bio_penerima->nama,
                'komentar' => $dispo_rr->komentar,
                'bagian' => $dispo_rr->bagian,
                'date_created' => $dispo_rr->date_created,
                'id' => $row->id,
                'kegiatan' => $row->kegiatan,
                'id_ruang_rapat' => $row->id_ruang_rapat,
                'ruang_rapat' => $rr->ruangan,
                'id_pegguna' => $row->id_pegguna,
                'tgl_mulai' => $row->tgl_mulai,
                'tgl_akhir' => $row->tgl_akhir,
                'status' => $row->status,
                'wkt_kegiatan' => $row->wkt_kegiatan,
                'id_atasan' => $row->id_atasan,
                'id_krt' => $row->id_krt,
                'konsumsi' => $row->konsumsi,
                'keterangan_konsumsi' => $row->keterangan_konsumsi,
                'jml_peserta' => $row->jml_peserta,
                'nama' => $bio->nama,
            );
            $this->template->load('template', 'dispo_ruang_rapat/tbl_dispo_rr_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dispo_ruang_rapat'));
        }
    }

    public function create_action()
    {
        //print_r($_POST);die();
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $id = $this->input->post('id_trans_rr', true);
            $this->create($id);
        } else {
            $data = array(
                'id_trans_rr' => $this->input->post('id_trans_rr', true),
                'id_pengirim' => '198903162014021001',
                'id_penerima' => $this->input->post('id_penerima', true),
                'komentar' => $this->input->post('komentar', true),
                'date_created' => date('Y-m-d H:i:s'),
                'bagian' => "KONSUMSI",
            );

            $this->Dispo_rurang_rapat_model->insert($data);

            $data = array(
                'id_trans_rr' => $this->input->post('id_trans_rr', true),
                'id_pengirim' => '198903162014021001',
                'id_penerima' => $this->input->post('id_penerima2', true),
                'komentar' => $this->input->post('komentar', true),
                'date_created' => date('Y-m-d H:i:s'),
                'bagian' => "PERLENGKAPAN",
            );

            $this->Dispo_rurang_rapat_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('dispo_ruang_rapat'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_trans_rr', 'id trans rr', 'trim|required');
        $this->form_validation->set_rules('id_penerima', 'id penerima', 'trim|required');
        $this->form_validation->set_rules('id_penerima2', 'id penerima2', 'trim|required');
        $this->form_validation->set_rules('komentar', 'komentar', 'trim|required');
        //$this->form_validation->set_rules('date_created', 'date created', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function create($id_trans_rr = "")
    {
        $data = array(
            'button' => 'Simpan',
            'action' => site_url('dispo_ruang_rapat/create_action'),
            'id' => set_value('id'),
            'id_trans_rr' => set_value('id_trans_rr'),
            'id_penerima' => set_value('id_penerima'),
            'id_penerima2' => set_value('id_penerima2'),
            'nama_penerima' => set_value('nama_penerima'),
            'nama_penerima2' => set_value('nama_penerima2'),
            'komentar' => set_value('komentar'),
            'date_created' => set_value('date_created'),
        );
        $data['id_trans_rr'] = $id_trans_rr;
        $this->template->load('template', 'dispo_ruang_rapat/tbl_dispo_rr_form', $data);
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == false) {
            $this->update($this->input->post('id', true));
        } else {
            $data = array(
                'id_trans_rr' => $this->input->post('id_trans_rr', true),
                'id_penerima' => $this->input->post('id_penerima', true),
                'komentar' => $this->input->post('komentar', true),
            );

            $this->Dispo_rurang_rapat_model->update($this->input->post('id', true), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('dispo_ruang_rapat'));
        }
    }

    public function update($id)
    {
        $row = $this->Dispo_rurang_rapat_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('dispo_ruang_rapat/update_action'),
                'id' => set_value('id', $row->id),
                'id_trans_rr' => set_value('id_trans_rr', $row->id_trans_rr),
                'id_penerima' => set_value('id_penerima', $row->id_penerima),
                'komentar' => set_value('komentar', $row->komentar),
                'date_created' => set_value('date_created', $row->date_created),
            );
            $this->template->load('template', 'dispo_ruang_rapat/tbl_dispo_rr_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dispo_ruang_rapat'));
        }
    }

    public function delete($id)
    {
        $row = $this->Dispo_rurang_rapat_model->get_by_id($id);

        if ($row) {
            $this->Dispo_rurang_rapat_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('dispo_ruang_rapat'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dispo_ruang_rapat'));
        }
    }

}

/* End of file Dispo_ruang_rapat.php */
/* Location: ./application/controllers/Dispo_ruang_rapat.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-17 20:50:36 */
/* http://harviacode.com */
