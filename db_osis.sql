/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:8889
 Source Schema         : db_osis

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 22/05/2020 10:19:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for master_biodata
-- ----------------------------
DROP TABLE IF EXISTS `master_biodata`;
CREATE TABLE `master_biodata` (
  `master_biodata_id` int(5) NOT NULL AUTO_INCREMENT,
  `bkn_id` varchar(60) DEFAULT '0',
  `nip` varchar(18) DEFAULT '0',
  `nama_cetak` varchar(200) DEFAULT '',
  `nama` varchar(200) DEFAULT NULL,
  `jenis_jab_id` tinyint(1) DEFAULT NULL COMMENT 'lihat tabel referensi daf_jenis_jab',
  `struk_id` int(5) DEFAULT '0',
  `fung_id` int(2) DEFAULT '0' COMMENT 'jabatan fungsional tertentu',
  `unit_id_es1` int(10) DEFAULT '0',
  `unit_id_es2` int(10) DEFAULT '0',
  `unit_id_es3` int(10) DEFAULT '0',
  `unit_id_es4` int(10) DEFAULT '0',
  `unit_staf_id` varchar(20) DEFAULT NULL COMMENT 'jabatan fungsional umum',
  `statpeg_id` tinyint(2) DEFAULT '3',
  `aktif_id` tinyint(2) DEFAULT '0',
  `jenis_peg_id` tinyint(2) DEFAULT '0',
  `jenjang_id` tinyint(2) DEFAULT '0',
  `gol_id` tinyint(5) DEFAULT '10',
  `no_karpeg` varchar(9) DEFAULT NULL,
  `absensi_id` varchar(5) DEFAULT NULL,
  `jeniskel_id` int(11) DEFAULT NULL,
  `tmp_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama_id` tinyint(2) DEFAULT '1',
  `kawin_id` int(2) DEFAULT '1',
  `alamat_ktp` varchar(255) DEFAULT NULL,
  `kode_pos_ktp` varchar(5) DEFAULT NULL,
  `alamat_tinggal` varchar(255) DEFAULT NULL,
  `kode_pos_tinggal` varchar(5) DEFAULT NULL,
  `telp_rumah` varchar(18) DEFAULT NULL,
  `telp_cellular` varchar(18) DEFAULT NULL,
  `no_ext_kantor` varchar(11) DEFAULT NULL,
  `email_kantor` varchar(100) DEFAULT NULL,
  `email_pribadi` varchar(100) DEFAULT '@lpsk.go.id',
  `gol_darah` varchar(4) DEFAULT NULL,
  `no_bpjs` varchar(50) DEFAULT NULL,
  `ktp_id` varchar(20) DEFAULT NULL,
  `npwp` varchar(20) DEFAULT NULL,
  `bank_id` varchar(3) DEFAULT NULL,
  `cabang_bank` varchar(100) DEFAULT NULL,
  `no_rek` varchar(30) DEFAULT NULL,
  `atas_nama` varchar(150) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `dihapus` enum('ya','tidak') DEFAULT 'tidak',
  `ktp_fileup` text,
  `npwp_fileup` text,
  `kartukeluarga_fileup` text,
  `karpeg_fileup` text,
  `editdate` datetime DEFAULT NULL,
  `userlog` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`master_biodata_id`),
  UNIQUE KEY `nip` (`nip`)
) ENGINE=MyISAM AUTO_INCREMENT=256 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_biodata
-- ----------------------------
BEGIN;
INSERT INTO `master_biodata` VALUES (1, '0', '080197', 'Drs. Hasto Atmojo Suroyo, M.Krim', 'Hasto Atmojo Suroyo', 2, 1, 0, 0, 0, 0, 0, '1', 0, 1, 0, 9, 46, '', '08019', 1, 'Bandung', '1959-03-09', 1, 1, 'Jl. Swadaya No. 61 RT. 08 RW. 03 Kel. Lubang Buaya Kec. Cipayung - Jakarta ', '', 'Jl. Swadaya No. 61 RT. 08 RW. 03 Kel. Lubang Buaya Kec. Cipayung - Jakarta ', '', '', '0811154566', '(021) 29681', 'hs_atmojo@yahoo.com', 'hs_atmojo@yahoo.com', '', NULL, '3175100903590003', '592371686009000', '002', 'Ciracas', '0485153748', 'Hasto Atmojo Suroyo', '080197.jpg\r', '', 'tidak', NULL, NULL, NULL, NULL, '2019-10-07 09:16:36', 'Roery Ayu Mayangsari Riyadi');
INSERT INTO `master_biodata` VALUES (2, '0', '080414', 'Brigjen. Pol. (Purn) Dr . Achmadi, S.H., M.AP', 'Achmadi', 2, 2, 0, 2, 0, 0, 0, '2', 1, 1, 0, 0, 46, '', '08041', 1, 'Sragen', '1960-09-20', 1, 1, 'Perum. Shangril Indah Dua Jl. Sakti II No. 17 005/006 Petukangan Selatan, Pesanggrahan - Jakarta', '', 'Perum. Shangril Indah Dua Jl. Sakti II No. 17 005/006 Petukangan Selatan, Pesanggrahan - Jakarta', '', '', '081213165041', '(021) 29681', 'achmadi@lpsk.go.id', 'achmadi_map@yahoo.com', '', NULL, '3573052009600005', '086938065621000', '002', 'Ciracas', '0799671322', 'Achmadi', '080414.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (3, '0', '080416', 'Dr. Iur. Antonius Prijadi Soesilo Wibowo, S.H., M.H', 'Antonius Prijadi Soesilo Wibowo', 2, 2, 0, 2, 0, 0, 0, '2', 0, 0, 0, 0, 46, '', '08041', 1, 'Ponorogo', '1964-05-10', 3, 0, 'Jl. Pandanwangi III No. B. 15 RT. 004 RW. 012 Kel. Larangan Selatan Kec. Larangan -Tangerang', '', 'Jl. Pandanwangi III No. B. 15 RT. 004 RW. 012 Kel. Larangan Selatan Kec. Larangan -Tangerang', '', '7316550', '', '(021) 29681', 'antonius.prijadi@lpsk.go.id', '', '', NULL, '3671131005640004', '498933787416000', '002', 'Ciracas', '0800850527', 'Antonius Prijadi Soesilo Wibowo', '080416.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (4, '0', '080196', 'Edwin Partogi Pasaribu, S.H', 'Edwin Partogi Pasaribu', 2, 2, 0, 2, 0, 0, 0, '2', 0, 0, 0, 0, 46, '', '08019', 1, 'Tanjung Karang', '1971-03-20', 1, 0, 'Jl. Sepakat VII RT. 005 RW. 001 Kel. Cilangkap Kec. Cipayung-Jakarta Timur', '', 'Jl. Sepakat VII RT. 005 RW. 001 Kel. Cilangkap Kec. Cipayung-Jakarta Timur', '', '', '', '(021) 29681', 'edwinpartogi@lpsk.go.id', '', '', NULL, '3175102003710009', '490735297071000', '002', 'Ciracas', '0485316513', 'Edwin Partogi Pasaribu', '080196.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (5, '0', '080415', 'Dr. Livia Istania DF Iskandar. M.Sc', 'Livia Istania DF Iskandar', 2, 2, 0, 2, 0, 0, 0, '2', 0, 0, 0, 0, 46, '', '08041', 2, 'Surabaya', '1970-01-07', 1, 0, 'Jl. Bunga Kenanga No. 6 Kec. Cipete Selatan - Jakarta ', '', 'Jl. Bunga Kenanga No. 6 Kec. Cipete Selatan - Jakarta ', '', '7691302', '08119708600', '(021) 29681', 'livia.iskandar@lpsk.go.id', '', '', NULL, '3174034701700002', '', '002', 'Ciracas', '0799926611', 'Livia Istania DF Iskandar', '080415.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (6, '0', '080417', 'Dr. Maneger Nasution, M.A', 'Maneger Nasution', 2, 2, 0, 2, 0, 0, 0, '2', 0, 0, 0, 0, 46, '', '08041', 1, 'Sumatera Barat', '1968-02-02', 1, 0, 'Kp. Kebon No. 75 RT. 004 RW. 007 Kel. Cinangka Kec. Sawangan - Depok', '', 'Kp. Kebon No. 75 RT. 004 RW. 007 Kel. Cinangka Kec. Sawangan - Depok', '16516', '', '08128038760', '(021) 29681', '', 'managernasution@gmail.com', '', NULL, '3276030202680008', '453319873421000', '002', 'Ciracas', '0799671344', 'Maneger Nasution', '080417.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (7, '0', '080330', 'Susilaningtias, S.H', 'Susilaningtias', 2, 2, 0, 2, 0, 0, 0, '2', 0, 0, 0, 0, 46, '', '08033', 2, 'Surabaya', '1977-10-20', 1, 0, 'Perum. Karadenan No MT-02 RT. 008 RW. 018 Kel. Karadenan Kec. Cibinong - Bogor', '', 'Perum. Karadenan No MT-02 RT. 008 RW. 018 Kel. Karadenan Kec. Cibinong - Bogor', '', '', '08121048957', '(021) 29681', '', '', '', NULL, '3520101601770020', '075787911014000', '002', 'Ciracas', '0198054517', 'Susilaningtias', '080330.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (8, '0', '196409051990310004', 'Dr. Ir. Noor Sidharta, M.H., MBA', 'Noor Sidharta', 2, 3, 0, 6, 0, 0, 0, '6', 1, 0, 0, 0, 45, '', '08033', 1, 'Malang', '1964-09-05', 1, 2, 'Duren Sawit Baru C. 8 /30 RT 007 RW. 001 Kel. Duren Sawit Kec. Duren Sawit - Jakarta Timur', '', 'Duren Sawit Baru C. 8 /30 RT 007 RW. 001 Kel. Duren Sawit Kec. Duren Sawit - Jakarta Timur', '', '', '08158814388', '(021) 29681', 'sidhartanoor@lpsk.go.id', '', '', NULL, '3175070509640010', '473276731008000', '002', 'Ciracas', '0614187811', 'Noor Sidharta', '196409051990310004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (9, '0', '196404131991032001', 'Dra. Handari Restu Dewi, M.M', 'Handari Restu Dewi', 3, 6, 0, 6, 7, 0, 0, '7', 1, 1, 0, 0, 43, '', '08005', 2, 'Kulonprogo ', '1964-04-13', 1, 1, 'Gg. Madrasah No.14 RT. 001 RW. 013 Kel. Menteng Dalam Kec. Tebet ', '', 'Gg. Madrasah No.14 RT. 001 RW. 013 Kel. Menteng Dalam Kec. Tebet ', '', '', '', '(021) 29681', 'hrdewi@lpsk.go.id', '', '', NULL, '3174015304640007', '472527662061000', '002', 'Ciracas', '0011573071', 'Handari Restu Dewi', '196404131991032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (10, '0', '197203242002121002', 'Eko Sunarko,  S. Kom., M.M', 'Eko Sunarko', 4, 9, 0, 6, 7, 8, 0, '8', 1, 0, 0, 0, 41, '', '08026', 1, 'Jakarta', '1972-03-24', 1, 2, 'Pondok Setia No. 5 RT. 007 RW. 006 Kel. Pondok Pinang Kec. Kebayoran Lama - Jakarta Selatan', '12310', 'Pondok Setia No. 5 RT. 007 RW. 006 Kel. Pondok Pinang Kec. Kebayoran Lama - Jakarta Selatan', '12310', '', '08128023225', '(021) 29681', 'eko.sunarko@lpsk.go.id', '', '', NULL, '317405240372001', '472527373013000', '002', 'Ciracas', '0346900644', 'Eko Sunarko', '197203242002121002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (11, '0', '198409282010121003', 'Dian Herdiansah, S.IP', 'Dian Herdiansah', 5, 11, 0, 6, 7, 8, 9, '9', 1, 0, 0, 0, 33, '081754', '08007', 1, 'Sumedang ', '1984-09-28', 1, 2, 'Jl. Urun Sari No. 14 Y RT. 014 RW. 001 Kel. Uian Kayu Selatan Kec. Matraman', '13120', 'Jl. Urun Sari No. 14 Y RT. 014 RW. 001 Kel. Uian Kayu Selatan Kec. Matraman', '13120', '', '', '(021) 29681', 'dian.herdiansah@lpsk.go.id', '', '', NULL, '3175012809841003', '792653487446000', '002', 'Ciracas', '1984280999', 'Dian Herdiansah', '198409282010121003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (12, '0', '198911302014022001', 'Novita Prima Dewi, S.IP', 'Novita Prima Dewi', 5, 11, 0, 6, 7, 8, 11, '11', 1, 0, 0, 0, 32, '', '08022', 2, 'Kulonprogo', '1989-11-30', 1, 1, 'Pandowan Pendukuhan II RT. 05 RW. 03 Kel. Pandowan Kec. Galur - Kulon Progo ', '55661', 'Pandowan Pendukuhan II RT. 05 RW. 03 Kel. Pandowan Kec. Galur - Kulon Progo ', '55661', '', '0817465985', '(021) 29681', 'novita.prima@lpsk.go.id', '', '', NULL, '3401047011890001', '668498553544000', '002', 'Ciracas', '0331802077', 'Novita Prima Dewi', '198911302014022001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (13, '0', '198307202010121001', 'Singgih Wisnubroto, S.E', 'Singgih Wisnubroto', 5, 11, 0, 6, 7, 8, 10, '10', 1, 0, 0, 0, 41, '', '08004', 1, 'Jakarta', '1983-07-20', 1, 2, 'Komp. Sekneg Blok A.IV/8 RT. 002 RW. 003 Kel. Panunggangan Utara Kec. Pinang ', '15143', 'Komp. Sekneg Blok A.IV/8 RT. 002 RW. 003 Kel. Panunggangan Utara Kec. Pinang ', '15143', '', '082225000757', '(021) 29681', 'singgih.wisnubroto@lpsk.go.id', '', '', NULL, '3671112007830001', '676972888416000', '002', 'Ciracas', '0215440096', 'Singgih Wisnubroto', '198307202010121001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (14, '0', '198808162014022001', 'Roery Ayu Mayangsari Riyadi, S.H', 'Roery Ayu Mayangsari Riyadi', 5, 11, 0, 6, 7, 12, 14, '14', 1, 0, 0, 0, 32, '', '08022', 2, 'Bandung', '1988-08-16', 1, 2, 'Bumi Parahiyangan Kencana No. 4 RT. 005 RW. 014 Kel. Bandasari Kec. Cangkuang - Bandung', '40377', 'Bumi Parahiyangan Kencana No. 4 RT. 005 RW. 014 Kel. Bandasari Kec. Cangkuang - Bandung', '40377', '', '085318453656', '(021) 29681', 'roery.ayu@lpsk.go.id', '', '', NULL, '3273155608880002', '457442440422000', '002', 'Ciracas', '0331900255', 'Roery Ayu Mayangsari Riyadi', '198808162014022001.jpg\r', 'bb', 'tidak', NULL, NULL, NULL, NULL, '2019-10-07 04:34:53', 'Roery Ayu Mayangsari Riyadi');
INSERT INTO `master_biodata` VALUES (15, '0', '198903162014021001', 'Muhammad Ihsan, S.H', 'Muhammad Ihsan', 5, 11, 0, 6, 7, 12, 13, '13', 1, 0, 0, 0, 32, '', '08022', 1, 'Jakarta', '1989-03-16', 1, 1, 'Jl. Buaran III Blok ME/6 RT. 005 RW. 015 Kel. Duren Sawit Kec. Duren Sawit - JaKarta', '', 'Jl. Buaran III Blok ME/6 RT. 005 RW. 015 Kel. Duren Sawit Kec. Duren Sawit - JaKarta', '', '', '08568935202', '(021) 29681', 'm.ihsan@lpsk.go.id', '', '', NULL, '3175071603890004', '725394621008000', '002', 'Ciracas', '0331900108', 'Muhammad Ihsan', '198903162014021001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (16, '0', '196604011986031001', 'Abadi Yanto, S.H., M.H', 'Abadi Yanto', 4, 9, 0, 6, 7, 16, 0, '16', 1, 0, 0, 0, 41, '', '08034', 1, 'Aceh', '1966-04-01', 1, 2, 'Jl. Mandala V RT. 004 RW. 002 Kel. Cililitan Kec. Kramat Jati ', '', 'Jl. Mandala V RT. 004 RW. 002 Kel. Cililitan Kec. Kramat Jati ', '', '', '', '(021) 29681', 'abadi.yanto@lpsk.go.id', '', '', NULL, '3175040104660002', '', '002', 'Ciracas', '0326657081', 'Abadi Yanto', '196604011986031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (17, '0', '199009302014022003', 'Nunut Amalia, S.E', 'Nunut Amalia', 5, 11, 0, 6, 7, 16, 18, '18', 1, 0, 0, 0, 32, '', '08023', 2, 'Jakarta', '1990-09-30', 1, 2, 'Perum Taman Griya Permai, Blok 11 No. 10 Kel.Pujung Kec. Kotabaru', '', 'Perum Taman Griya Permai, Blok 11 No. 10 Kel.Pujung Kec. Kotabaru', '', '', '085780284878', '(021) 29681', 'nunut.amalia@lpsk.go.id', '', '', NULL, '3215257009900001', '666415633433000', '002', 'Ciracas', '0331798127', 'Nunut Amalia', '199009302014022003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (18, '0', '198701062010122001', 'Indryasari. S.IP', 'Indryasari', 5, 11, 0, 6, 7, 20, 21, '21', 1, 0, 0, 0, 34, '', '08008', 2, 'Jakarta', '1987-01-06', 1, 2, 'Jl. Naga Raya No. 35 RT. 007 RW. 003 Kel. Duren Sawit Kec. Duren Sawit - Jakarta', '13440', 'Jl. Naga Raya No. 35 RT. 007 RW. 003 Kel. Duren Sawit Kec. Duren Sawit - Jakarta', '13440', '', '082114678087', '(021) 29681', 'indriyasari@lpsk.go.id', '', '', NULL, '3175074601870002', '361954233008000', '002', 'Ciracas', '0215493093', 'Indryasari', '198701062010122001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (19, '0', '199004092014022003', 'Yuli Yuliah, S.H', 'Yuli Yuliah', 5, 11, 0, 6, 7, 20, 23, '23', 1, 0, 0, 0, 32, '', '08022', 2, 'Sukabumi ', '1990-04-09', 1, 2, 'Kp. Majlis RT. 001 RW. 019 Kel. Palabuhanratu Kec. Palabuhanratu-Sukabumi', '43364', 'Kp. Majlis RT. 001 RW. 019 Kel. Palabuhanratu Kec. Palabuhanratu-Sukabumi', '43364', '', '085723205842', '(021) 29681', 'yuli.yuliah@lpsk.go.id', '', '', NULL, '3202014904900011', '665952115405000', '002', 'Ciracas', '0331900017', 'Yuli Yuliah', '199004092014022003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (20, '0', '197309302001121001', 'Dr. Drama Panca Putra, S.Pi., M.Si', 'Drama Panca Putra', 3, 6, 0, 6, 24, 0, 0, '24', 1, 0, 0, 0, 43, '', '08035', 1, 'Bandar Lampung', '1973-09-30', 1, 2, 'Jl. Kemanggisan Raya No. C7 RT. 006 RW. 013 Kel. Palmerah Kec. Palmerah - JaKarta', '11480', 'Jl. Kemanggisan Raya No. C7 RT. 006 RW. 013 Kel. Palmerah Kec. Palmerah - JaKarta', '11480', '', '08129066507', '(021) 29681', 'drama.panca@lpsk.go.id', '', '', NULL, '3275103009730007', '485608673411000', '002', 'Ciracas', '0178068284   ', 'Drama Panca Putra', '197309302001121001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (21, '0', '197708152005012001', 'Eviyati, S.Pd., M.AP', 'Eviyati', 4, 9, 0, 6, 24, 25, 0, '25', 1, 0, 0, 0, 34, '', '08002', 2, 'Tegal', '1977-08-15', 1, 2, 'Jl. Pisok IX EB 22 / 1 BTR 5 RT. 009 RW. 011 Kel. Jurangmangu Timur Kec. Pondok Aren', '', 'Jl. Berlian IV No. 68 Perumnas Rawa Roko Mas RT. 011 RW.037 Kel. Bojong Rawa Lumbu Kec. Rawalumbu', '', '', '', '(021) 29681', 'eviyati@lpsk.go.id', '', '', NULL, '3275055508770030', '258645845432000', '002', 'Ciracas', '0067353732', 'Eviyati', '197708152005012001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (22, '0', '198805112014022002', 'Inggit Nursafitri, S.H., M.H', 'Inggit Nursafitri', 5, 11, 0, 6, 24, 25, 26, '26', 1, 0, 0, 0, 33, 'BB 000070', '08023', 2, 'Purwakarta ', '1988-05-11', 1, 2, 'Komp. Kostrad RT. 007 RW. 004 Kel. Kalibaru Kec. Cilodong - Depok', '', 'Komp. Kostrad RT. 007 RW. 004 Kel. Kalibaru Kec. Cilodong - Depok', '', '', '081221986957', '(021) 29681', 'inggit.nursafitri@lpsk.go.id', '', '', NULL, '3214065105880003', '894694629409000', '002', 'Ciracas', '0331796276', 'Inggit Nursafitri', '198805112014022002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (23, '0', '197007131996032001', 'Enteng Mundiati, S.H., M.M', 'Enteng Mundiati', 4, 9, 0, 6, 24, 28, 0, '28', 1, 0, 0, 0, 34, '010656', '08018', 2, 'Lubuk Jantan ', '1970-07-13', 1, 2, 'Kp. Klingkit No. 16 RT. 003 RW. 012 Kel. Rawa Buaya Kec. Cengkareng - Jakarta', '11740', 'Kp. Klingkit No. 16 RT. 003 RW. 012 Kel. Rawa Buaya Kec. Cengkareng - Jakarta', '11740', '', '081381115693', '(021) 29681', 'enteng.mundiati@lpsk.go.id', '', '', NULL, '3173015307700010', '480632553034000', '002', 'Ciracas', '0018079459', 'Enteng Mundiati', '197007131996032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (24, '0', '199102122014022001', 'Lia Gunawan, S.H', 'Lia Gunawan', 5, 11, 0, 6, 24, 28, 29, '29', 1, 0, 0, 0, 32, '', '08022', 2, 'Jambi', '1991-02-12', 1, 2, 'Griya Nisa 1 Tunas Karsa No. 46B RT. 003 RW. 008 Kel. Sukamaju Baru Kec. Tapos - Depok', '16455', 'Griya Nisa 1 Tunas Karsa No. 46B RT. 003 RW. 008 Kel. Sukamaju Baru Kec. Tapos - Depok', '16455', '', '081317863172', '(021) 29681', 'lia.gunawan@lpsk.go.id', '', '', NULL, '1571085202910021', '666330121331000', '002', 'Ciracas', '0331899862', 'Lia Gunawan', '199102122014022001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (25, '0', '198104292010122001', 'Dinar Rahmayani, S.H', 'Dinar Rahmayani', 5, 11, 0, 6, 24, 28, 30, '30', 1, 0, 0, 0, 33, '', '08001', 1, 'Cilacap ', '1981-04-29', 1, 2, 'Jl. Pisok IX EB 22 / 1 BTR 5 RT. 009 RW. 011 Kel. Jurangmangu Timur Kec. Pondok Aren', '', 'Jl. Pisok IX EB 22 / 1 BTR 5 RT. 009 RW. 011 Kel. Jurangmangu Timur Kec. Pondok Aren', '', '', '', '(021) 29681', 'dinar.rahmayani@lpsk.go.id', '', '', NULL, '3674036804810006', '', '002', 'Ciracas', '0215491620', 'Dinar Rahmayani', '198104292010122001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (26, '0', '197001021989031001', 'Sriyana, S.H., LL.M., DFM', 'Sriyana', 3, 6, 0, 6, 31, 0, 0, '31', 1, 0, 0, 0, 42, '', '08035', 1, 'Sukoharjo', '1970-01-02', 1, 2, 'Jalan Cipinang Muara (Hanafi) No. 25 RT. 011 RW. 03 Kel. Pondok Bambu Kec. Duren Sawit-Jakarta', '13430', 'Jalan Cipinang Muara (Hanafi) No. 25 RT. 011 RW. 03 Kel. Pondok Bambu Kec. Duren Sawit-Jakarta', '13430', '86610119', '0812894388971', '(021) 29681', 'sriyana@lpsk.go.id', '', '', NULL, '3175070201700014', '140900903008000', '002', 'Ciracas', '0721515807', 'Sriyana', '197001021989031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (27, '0', '69020416', 'Marjoko', 'Marjoko', 4, 9, 0, 6, 31, 32, 0, '32', 1, 0, 0, 0, 0, '', '08036', 1, 'Semarang', '1969-02-03', 1, 2, '', '', '', '', '', '', '(021) 29681', 'Marjoko@lpsk.go.id', '', '', NULL, '', '', '002', 'Ciracas', '0177460422', 'Marjoko', '69020416.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (28, '0', '198709202010122002', 'Kombes. Pol . Sandra Anggita, S.I.K., M.Si', 'Sandra Anggita', 5, 11, 0, 6, 31, 32, 33, '33', 1, 0, 0, 0, 32, '', '08009', 2, 'Bandung', '1987-09-20', 1, 2, 'Puyuh Barat II No. 22 - Tangerang Selatan', '', 'Puyuh Barat II No. 22 - Tangerang Selatan', '', '', '082117887608', '(021) 29681', 'sandra.anggita@lpsk.go.id', '', '', NULL, '3204106009870009', '582113379424000', '002', 'Ciracas', '0215508078', 'Sandra Anggita', '198709202010122002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (29, '0', '199101142014022002', 'Fatimah Nuryani, S.H., MH', 'Fatimah Nuryani', 5, 11, 0, 6, 31, 32, 34, '34', 1, 0, 0, 0, 32, 'BB 000070', '08022', 2, 'Bekasi', '1991-01-14', 1, 2, 'Jl. Kp. Pulo Rt. 04 RW. 05 Kel. Bojong Baru Kec. Bojong Gede Kab. Bogor', '17115', 'Jl. Kp. Pulo Rt. 04 RW. 05 Kel. Bojong Baru Kec. Bojong Gede Kab. Bogor', '17115', '', '081287256052', '(021) 29681', 'fatimah.nuryani@lpsk.go.id', '', '', NULL, '3275055401910009', '665322590432000', '002', 'Ciracas', '0331899590', 'Fatimah Nuryani', '199101142014022002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (30, '0', '196408081989032001', 'Dr. Latri Mumpuni, M.Si', 'Latri Mumpuni', 4, 9, 0, 6, 31, 35, 0, '35', 1, 0, 0, 0, 42, '', '08035', 2, 'Bandung', '1964-08-08', 1, 2, 'taman Kota J3/5 Bekasi Jaya Kel. Bekasi Jaya Kec. Bekasi Timur Bekasi', '17112', 'taman Kota J3/5 Bekasi Jaya Kel. Bekasi Jaya Kec. Bekasi Timur Bekasi', '17112', '', '0817804108', '(021) 29681', 'latri.mumpuni@lpsk.go.id', '', '', NULL, '3275014808640028', '', '002', 'Ciracas', '0190905602', 'Latri Mumpuni', '196408081989032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (31, '0', '198409282010121004', 'Achmad Soleh, S.IP', 'Achmad Soleh', 5, 11, 0, 6, 31, 35, 36, '36', 1, 0, 0, 0, 32, 'AA 000068', '08007', 1, 'Cirebon', '1984-09-28', 1, 2, 'Jl. Graha Mulya Cibinong NO. F5 RT. 002 RW. 003 Kel. Harapan Jaya Kec. Cibinong Kab. Bogor', '11480', 'Jl. Graha Mulya Cibinong NO. F5 RT. 002 RW. 003 Kel. Harapan Jaya Kec. Cibinong Kab. Bogor', '11480', '', '082123791037', '(021) 29681', 'achmad.soleh@lpsk.go.id', '', '', NULL, '3173072809840014', '793756149031000', '002', 'Ciracas', '0215488197', 'Achmad Soleh', '198409282010121004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (32, '0', '198209162006041005', 'Firdiansyah, S.Sos', 'Firdiansyah', 5, 11, 0, 6, 31, 35, 37, '37', 1, 0, 0, 0, 34, '', '08035', 1, 'Tangerang', '1982-09-16', 1, 2, 'Jl. Raya Pondok Aren Gg. Mushollah Assomad RT. 003 RW. 001 Pondok Aren Kec. Pondok Aren', '15424', 'Jl. Raya Pondok Aren Gg. Mushollah Assomad RT. 003 RW. 001 Pondok Aren Kec. Pondok Aren', '15424', '', '', '(021) 29681', 'firdiansyah@lpsk.go.id', '', '', NULL, '3674031609820015', '', '002', 'Ciracas', '0714831945', 'Firdiansyah', '198209162006041005.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (33, '0', '197301061996031001', 'Aris Fajari Teguh Nugroho, S.Sos., M.AP', 'Aris Fajari Teguh Nugroho', 4, 9, 0, 6, 0, 38, 0, '38', 1, 0, 0, 9, 34, 'G.348328', '08034', 1, 'Klaten', '1973-01-06', 1, 2, 'Puri Bintaro Hijau Blok. E2/6 RT. 01 RW. 012 Kel. Pondok Aren Kec. Pondok Aren-Tangerang Selatan', '15424', 'Puri Bintaro Hijau Blok. E2/6 RT. 01 RW. 012 Kel. Pondok Aren Kec. Pondok Aren-Tangerang Selatan', '15424', '', '081291159976', '(021) 29681', 'aris.fajari@lpsk.go.id', '', 'B', NULL, '3674030601730009', '670165950411000', '002', 'Ciracas', '0710963599', 'Aris Fajari Teguh Nugroho', '197301061996031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (34, '0', '080079', 'Abdanev Jopa C, S.H', 'Abdanev Jopa C.', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', 'abdanev.jopa@lpsk.go.id', '', '', NULL, '', '', '', '', '', '', '080079.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (35, '0', '080261', 'Basuki Haryono, S.H., M.H', 'Basuki Haryono', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', 'basuki.haryono@lpsk.go.id', '', '', NULL, '', '', '', '', '', '', '080261.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (36, '0', '080249', 'Muhammad Mardiansyah, S.IP', 'Muhammad Mardiansyah', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '080249.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (37, '0', '080006', 'Mulatingsih, S.H., M.H', 'Mulatingsih', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 2, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '080006.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (38, '0', '080193', 'Drs. Prasetyo Djafar, M.M', 'Prasetyo Djafar', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '080193.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (39, '0', '080077', 'Rully Novian, S.H, M.H', 'Rully Novian', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '080077.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (40, '0', '080166', 'Sukri Agama, S.H., M.Hum', 'Sukri Agama', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '080166.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (41, '0', '080062', 'Syahrial Martanto Wiryawan, S.H', 'Syahrial Martanto Wiryawan', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '080062.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (42, '0', '080040', 'Galih Prihanto Jati, S.E', 'Galih Prihanto Jati', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', '080040.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (43, '0', '080049', 'Armein Rizal B, Ak., MBA', 'Armein Rizal B.', 8, 15, 0, 6, 0, 0, 0, '', 4, 0, 0, 0, 0, '', '', 1, '', '0000-00-00', 0, 0, '', '', '', '', '', '', '', 'armein.rizal@lpsk.go.id', '', '', NULL, '', '', '', '', '', '', '080049.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (44, '0', '197707281997031001', 'Agus Janan Widyantoro', 'Agus Janan Widyantoro', 6, 99, 0, 6, 31, 35, 36, '133', 1, 0, 0, 0, 32, '', '08026', 1, 'Ponorogo', '1977-07-28', 1, 2, 'Gg. Sosial RT. 010 RW. 001 Kel. Pasar Minggu Kec. Pasar Minggu - Jakarta', '12520', 'Gg. Sosial RT. 010 RW. 001 Kel. Pasar Minggu Kec. Pasar Minggu - Jakarta', '12520', '', '', '(021) 29681', 'agus.janan@lpsk.go.id', '', '', NULL, '3174072807770002', '004444188025000', '002', 'Ciracas', '0018077269', 'Agus Janan Widyantoro', '197707281997031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (45, '0', '198912172015032001', 'Anisa Dewi Prajanti,  S.Kom', 'Anisa Dewi Prajanti', 6, 99, 0, 6, 7, 12, 14, '48', 1, 0, 0, 0, 32, '', '08025', 2, 'Sukoharjo', '1989-12-17', 1, 1, 'Dompilan RT. 003 RW. 001 Kel. Jombor Kec. Bendosari -Sukoharjo  ', '57521', 'Dompilan RT. 003 RW. 001 Kel. Jombor Kec. Bendosari -Sukoharjo  ', '57521', '0271590449', '08990266266', '(021) 29681', 'anisa.dewi@lpsk.go.id', '', '', NULL, '3311066712890004', '711688721532000', '002', 'Ciracas', '0373763682', 'Anisa Dewi Prajanti', '198912172015032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (46, '0', '198212132015032001', 'Anna Khoiru Nisya,  S.H', 'Anna Khoiru Nisya', 6, 99, 0, 6, 7, 20, 21, '42', 1, 0, 0, 0, 32, '', '08025', 2, 'Kotabumi', '1982-12-13', 1, 2, 'Perumahan Pandan Valley No. AC1/G RT. 002 RW. 009 Kel. Parakanjaya Kec. Kemang - Bogor', '16310', 'Perumahan Pandan Valley No. AC1/G RT. 002 RW. 009 Kel. Parakanjaya Kec. Kemang - Bogor', '16310', '', '081383063625', '(021) 29681', 'anna.khoiru@lpsk.go.id', '', '', NULL, '3201125312820001', '725181861408000', '002', 'Ciracas', '0373923842', 'Anna Khoiru Nisya', '198212132015032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (47, '0', '198109292010122001', 'Cita Mismiana,  A.Md', 'Cita Mismiana', 6, 99, 0, 6, 7, 16, 17, '114', 1, 0, 0, 0, 24, '055070', '08001', 2, 'Jakarta', '1981-09-29', 1, 2, 'Jl. H. Saikin No. 32 RT. 011 RW. 008 Kel. Pondok Pinang Kec. Kebayoran Lama-Jakarta', '12310', 'Jl. H. Saikin No. 32 RT. 011 RW. 008 Kel. Pondok Pinang Kec. Kebayoran Lama-Jakarta', '12310', '', '08111195081', '(021) 29681', 'cita.mismiana@lpsk.go.id', '', '', NULL, '3174056909810009', '692044290411000', '002', 'Ciracas', '0215424700', 'Cita Mismiana', '198109292010122001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (48, '0', '198801172010122004', 'Eni Nurtika,  A.Md', 'Eni Nurtika', 6, 99, 0, 6, 7, 8, 9, '111', 1, 0, 0, 0, 24, '', '08002', 2, 'Kebumen', '1988-01-17', 1, 1, 'Jl. Kemanggisan Raya RT. 001 RW. 003 Kel. Palmerah Kec. Palmerah - JaKarta', '', 'Jl. Kemanggisan Raya RT. 001 RW. 003 Kel. Palmerah Kec. Palmerah - JaKarta', '', '', '085921469242', '(021) 29681', 'eni.nurtika@lpsk.go.id', '', '', NULL, '3305065701880001', '472109818523000', '002', 'Ciracas', '0245459410', 'Eni Nurtika', '198801172010122004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (49, '0', '198101062015031001', 'Fakhrur Haqiqi,  S.H', 'Fakhrur Haqiqi', 6, 99, 0, 6, 7, 20, 23, '39', 1, 0, 0, 0, 32, '', '08025', 1, 'Palembang', '1981-01-06', 1, 2, 'Jl. Pegangsaan Dua No. 002 RW. 003 Kel. Pegangsaan Dua Kec. Kelapa Gading-Jakarta', '', 'Jl. Pegangsaan Dua No. 002 RW. 003 Kel. Pegangsaan Dua Kec. Kelapa Gading-Jakarta', '', '', '087888824916', '(021) 29681', 'fakhrur.haqiqi@lpsk.go.id', '', '', NULL, '3171060601810002', '245531264407000', '002', 'Ciracas', '0373765715', 'Fakhrur Haqiqi', '198101062015031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (50, '0', '196808301993031001', 'I Gusti Agung Adnyana', 'I Gusti Agung Adnyana', 6, 99, 0, 6, 31, 32, 34, '115', 1, 0, 0, 0, 32, '', '08026', 1, 'Jakarta', '1968-08-30', 1, 2, 'Kav. Taman duta Blok D 31/18 RT. 001 RW. 039 Kel. Bahagia Kec. Babelan-Bekasi', '', 'Kav. Taman duta Blok D 31/18 RT. 001 RW. 039 Kel. Bahagia Kec. Babelan-Bekasi', '', '', '082260609901', '(021) 29681', 'igusti.agung@lpsk.go.id', '', '', NULL, '3216023008680001', '280662396017000', '002', 'Ciracas', '0018087574', 'I Gusti Agung Adnyana', '196808301993031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (51, '0', '198207072015032001', 'Kartini Megawati,  S.Pd', 'Kartini Megawati', 6, 99, 0, 6, 7, 12, 13, '52', 1, 0, 0, 0, 32, '', '08025', 2, 'Jakarta', '1982-07-07', 2, 2, 'Jl. Mandor Hasan No. 2 RT. 07 RW. 01 Kel. Bambu Apus Kec. Ciayung - Jakarta', '', 'Jl. Mandor Hasan No. 2 RT. 07 RW. 01 Kel. Bambu Apus Kec. Ciayung - Jakarta', '', '', '08128427877', '(021) 29681', 'kartini.megawati@lpsk.go.id', '', '', NULL, '3175104707820007', '585267057009000', '002', 'Ciracas', '0373763933', 'Kartini Megawati', '198207072015032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (52, '0', '198606182015031001', 'Mahari Is Subangun,  S.Pd', 'Mahari Is Subangun', 6, 99, 0, 6, 7, 8, 11, '110', 1, 0, 0, 0, 32, '', '08025', 1, 'Cilacap', '1986-06-18', 1, 2, 'Jl. H. Mahjur No. 80 RT. 13 RW. 02 Kel. Lenteng Agung Kec. Jagakarsa-Jakarta', '12610', 'Jl. H. Mahjur No. 80 RT. 13 RW. 02 Kel. Lenteng Agung Kec. Jagakarsa-Jakarta', '12610', '', '081904258097', '(021) 29681', 'mahari.is@lpsk.go.id', '', '', NULL, '3404141806860003', '881071112541000', '002', 'Ciracas', '0373765782', 'Mahari Is Subangun', '198606182015031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (53, '0', '198701032015032001', 'Riani Anggraeni Soedirgo, S. Hum', 'Riani Anggraeni Soedirgo', 6, 99, 0, 6, 7, 8, 10, '109', 1, 0, 0, 0, 32, '', '08025', 2, 'Jakarta', '1987-01-03', 1, 2, 'Jl. Salatiga No. 2 RT. 003 RW. 004 Kel. Menteng Kec. Menteng - Jakarta Pusat', '10310', 'Jl. Salatiga No. 2 RT. 003 RW. 004 Kel. Menteng Kec. Menteng - Jakarta Pusat', '10310', '', '08561115788', '(021) 29681', 'riani.anggraeni@lpsk.go.id', '', '', NULL, '3171064301870001', '723560116071000', '002', 'Ciracas', '0373765839', 'Riani Anggraeni Soedirgo', '198701032015032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (54, '0', '198209242015031001', 'Setiawan, S.E', 'Setiawan', 6, 99, 0, 6, 7, 8, 10, '108', 1, 0, 0, 0, 32, '', '08025', 1, 'Sanggau', '1962-09-24', 1, 2, 'Sukoharjo Rt. 002 RW. 002 Kel. Sukoharjo Kec. Rembang-Rembang', '59219', 'Sukoharjo Rt. 002 RW. 002 Kel. Sukoharjo Kec. Rembang-Rembang', '59219', '', '081328197029', '(021) 29681', 'setiawan@lpsk.go.id', '', '', NULL, '3317102409820002', '352422380507000', '002', 'Ciracas', '0373764052', 'Setiawan', '198209242015031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (55, '0', '198810192015032001', 'Sri Wiji Astuti, S.E', 'Sri Wiji Astuti', 6, 99, 0, 6, 7, 8, 9, '112', 1, 0, 0, 0, 31, '', '08026', 2, 'Jakarta', '1988-10-19', 1, 2, 'Jl. Permata I No. 6 RT. 10 RW. 06 Kel. Kampung Melayu Kec. Jatinegara-Jakarta', '13320', 'Jl. Permata I No. 6 RT. 10 RW. 06 Kel. Kampung Melayu Kec. Jatinegara-Jakarta', '13320', '', '081282288175', '(021) 29681', 'sri.wiji@lpsk.go.id', '', '', NULL, '3175035910880004', '352573695002000', '002', 'Ciracas', '0312876803', 'Sri Wiji Astuti', '198810192015032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (56, '0', '198010092015032001', 'Therecia Lies Triana Sulanjari,  S.Sos', 'Therecia Lies Triana Sulanjari', 6, 99, 0, 6, 7, 12, 14, '49', 1, 0, 0, 0, 32, '', '08025', 2, 'Cilacap', '1980-10-09', 3, 2, 'Perumahan Medan Lestari Blok A2 No. A28 RT. 006 RW. 013 Kel. Medang Kec. Pagedangan - Tangerang', '', 'Perumahan Medan Lestari Blok A2 No. A28 RT. 006 RW. 013 Kel. Medang Kec. Pagedangan - Tangerang', '', '', '08121523601', '(021) 29681', 'therecia.lies@lpsk.go.id', '', '', NULL, '3402084910800004', '344156575543000', '002', 'Ciracas', '0373765646', 'Therecia Lies Triana Sulanjari', '198010092015032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (57, '0', '199011082015032001', 'Tiara Rachmawati,  S.Kep', 'Tiara Rachmawati', 6, 99, 0, 6, 7, 16, 17, '113', 1, 0, 0, 0, 32, '00026230', '08025', 2, 'Bandung', '1990-11-08', 1, 2, 'Jl. Batu Merah I No. 30 RT. 02 RW. 02 Kel. Pejaten Timur Kec. Pasar Minggu -  Jakarta', '12510', 'Jl. Batu Merah I No. 30 RT. 02 RW. 02 Kel. Pejaten Timur Kec. Pasar Minggu -  Jakarta', '12510', '', '0373764018', '(021) 29681', 'tiara.rachmawati@lpsk.go.id', '', '', NULL, '3217034811900008', '548681139421000', '002', 'Ciracas', '0373764018', 'Tiara Rachmawati', '199011082015032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (58, '0', '197812272005021001', 'Revando Burhan,  S.H', 'Revando Burhan', 6, 99, 0, 6, 31, 35, 37, '134', 1, 0, 0, 0, 34, '', '08035', 1, 'Jakarta', '1978-12-27', 1, 2, 'Petamburan VI RT. 007 RW. 006 Kel. Petamburan Kec. Tanah Abang-Jakarta Pusat', '', 'Petamburan VI RT. 007 RW. 006 Kel. Petamburan Kec. Tanah Abang-Jakarta Pusat', '', '', '081296325442', '(021) 29681', 'revando@lpsk.go.id', '', '', NULL, '3275062712780010', '774146872072000', '002', 'Ciracas', '0356524145', 'Revando Burhan', '197812272005021001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (59, '0', '199307232019032005', 'Murniasih, S.E', 'Murniasih', 6, 99, 0, 6, 7, 8, 10, '104', 2, 0, 0, 0, 31, '', '08036', 2, 'Jakarta', '1993-07-23', 1, 2, 'Jl. Languar Gg. Pedati 4 No. 108 RT.002 RW 002 Desa. Rawa Panjang Kec. Bojong Gede Bogor', '16320', 'Jl. Languar Gg. Pedati 4 No. 108 RT.002 RW 002 Desa. Rawa Panjang Kec. Bojong Gede Bogor', '16320', '', '082113417364', '(021) 29681', 'murniasih@lpsk.go.id', '', '', NULL, '3175106307930002', '442360996009000', '002', 'Ciracas', '0804596316', 'Murniasih', '199307232019032005.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (60, '0', '198908312019031001', 'Fuadiansyah, S.E', 'Fuadiansyah', 6, 99, 0, 6, 7, 8, 10, '105', 2, 0, 0, 0, 31, '', '08036', 1, 'Padang', '1989-08-31', 1, 1, 'Jl.Belibis Blok B No. 5 RT.01 RW. 04 Desa. Air Tawar Barat Kec. Padang Utara Pandang', '', 'Jl.Belibis Blok B No. 5 RT.01 RW. 04 Desa. Air Tawar Barat Kec. Padang Utara Pandang', '', '', '081283504688', '(021) 29681', 'fuadiansyah@lpsk.go.id', 'fuadiansyah@gmail.com', '', NULL, '1371043108890003', '554481911201000', '002', 'Ciracas', '0804596190', 'Fuadiansyah', '198908312019031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (61, '0', '199111102019032003', 'Tri Indah Lestari, S.E', 'Tri Indah Lestari', 6, 99, 0, 6, 7, 8, 10, '106', 2, 0, 0, 0, 31, '', '08036', 2, 'Jakarta', '1991-11-10', 1, 1, 'Jl. Flamboyan III Blok B No.125 RT.004 RW. 021 Desa. Jati Rahayu Kec. Pondok Melati Bekasi', '', 'Jl. Flamboyan III Blok B No.125 RT.004 RW. 021 Desa. Jati Rahayu Kec. Pondok Melati Bekasi', '', '', '085813665535', '(021) 29681', 'tri.indah@lpsk.go.id', '', '', NULL, '3275125011910001', '559216502432000', '002', 'Ciracas', '0804596372', 'Tri Indah Lestari', '199111102019032003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (62, '0', '199011232019031003', 'Bimo Setiajie, S.E', 'Bimo Setiajie', 6, 99, 0, 6, 7, 8, 10, '107', 2, 0, 0, 0, 31, '', '08036', 1, 'Jakarta', '1990-11-23', 1, 2, 'Jl. Bendera No. 250 No. 09 RW. 11 Desa. Cijantung Kec. Pasar Rebo DKI Jakarta', '', 'Jl. Bendera No. 250 No. 09 RW. 11 Desa. Cijantung Kec. Pasar Rebo DKI Jakarta', '', '', '087791288511', '(021) 29681', 'bimo.setiajie@lpsk.go.id', '', '', NULL, '3175052311900003', '685844045009000', '002', 'Ciracas', '0804160813', 'Bimo Setiajie', '199011232019031003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (63, '0', '198809092019032002', 'Linda Widya, S.Sos', 'Linda Widya', 6, 99, 0, 6, 7, 12, 14, '45', 2, 0, 0, 0, 31, '', '08036', 2, 'Pekanbaru', '1988-09-09', 1, 2, 'Jl. Taskurun No. 96 Rt. 4 RW. 2 Desa. Wonorejo Kec. Marpoyan Damai - Pekanbaru', '', 'Jl. Taskurun No. 96 Rt. 4 RW. 2 Desa. Wonorejo Kec. Marpoyan Damai - Pekanbaru', '', '', '085271112747', '(021) 29681', 'linda.widya@lpsk.go.id', '', '', NULL, '1471094909880043', '163170871216000', '002', 'Ciracas', '0804596258', 'Linda Widya', '198809092019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (64, '0', '198605192019032002', 'Rahma Sartika, S.E', 'Rahma Sartika', 6, 99, 0, 6, 7, 12, 14, '47', 2, 0, 0, 0, 31, '', '08036', 2, 'Bogor', '1986-05-19', 1, 2, 'Komp. BPPB Selakopi No. 26 RT 002 RW. 004 Desa. Pasirmulya Kec. Bogor Barat', '', 'Komp. BPPB Selakopi No. 26 RT 002 RW. 004 Desa. Pasirmulya Kec. Bogor Barat', '', '', '08561855105', '(021) 29681', 'rahma.sartika@lpsk.go.id', '', '', NULL, '3271045905860001', '446712663404000', '002', 'Ciracas', '0805008347', 'Rahma Sartika', '198605192019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (65, '0', '199403312019031002', 'Ali Gunung Siregar, S.E', 'Ali Gunung Siregar', 6, 99, 0, 6, 7, 12, 14, '46', 2, 0, 0, 0, 31, '', '08036', 1, 'Bogor', '1994-03-31', 1, 1, 'Kp.Cibeureum RT. 002 RW. 005 Desa. Cileungsi Kidul, Kec. Cileungsi Bogor', '', 'Kp.Cibeureum RT. 002 RW. 005 Desa. Cileungsi Kidul, Kec. Cileungsi Bogor', '', '', '08990736043', '(021) 29681', 'ali.gunung@lpsk.go.id', 'aligunungs@gmail.com', '', NULL, '3201073103940005', '808051387436000', '002', 'Ciracas', '0804160755', 'Ali Gunung Siregar', '199403312019031002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (66, '0', '198603292019031001', 'Muhamad Fahrudin, A.Md', 'Muhamad Fahrudin', 6, 99, 0, 6, 7, 12, 13, '51', 2, 0, 0, 0, 23, '', '08036', 1, 'Bogor', '1986-03-29', 1, 2, 'Jl. Ciherang Rawakalong No. 7 RT. 04 RW.08 Desa. Ciherang Kec. Dramaga - Bogor', '', 'Jl. Ciherang Rawakalong No. 7 RT. 04 RW.08 Desa. Ciherang Kec. Dramaga - Bogor', '', '', '08710266501', '(021) 29681', 'muhamad.fahrudin@lpsk.go.id', '', '', NULL, '3201300808140001', '368961116434000', '002', 'Ciracas', '0805008314', 'Muhamad Fahrudin', '198603292019031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (67, '0', '199004192019032003', 'Milati Nurul Haqi, A.Md', 'Milati Nurul Haqi', 6, 99, 0, 6, 7, 12, 13, '50', 2, 0, 0, 0, 23, '', '08037', 2, 'Cirebon', '1990-04-19', 1, 2, 'Perumahan Graha Aradea No. 16 RT 05 RW. 12 Desa. Ciherang Kec. Dramaga - Bogor', '', 'Perumahan Graha Aradea No. 16 RT 05 RW. 12 Desa. Ciherang Kec. Dramaga - Bogor', '', '', '089661321775', '(021) 29681', 'milati.nurul@lpsk.go.id', '', '', NULL, '320130230119004', '810875468434000', '002', 'Ciracas', '0804596281', 'Milati Nurul Haqi', '199004192019032003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (68, '0', '199607062019031001', 'Ery Kurnia, S.H', 'Ery Kurnia', 6, 99, 0, 6, 7, 20, 21, '41', 2, 0, 0, 0, 31, '', '08037', 1, 'Tangerang', '1996-07-06', 1, 1, 'Jl. Bratasena 8 No. 29 RT.005 RW.014 Desa. Pondok Benda Kec. Pamulang - Tangerang', '', 'Jl. Bratasena 8 No. 29 RT.005 RW.014 Desa. Pondok Benda Kec. Pamulang - Tangerang', '', '', '089601220653', '(021) 29681', 'ery.kurnia@lpsk.go.id', 'erykurnia711@gmail.com', '', NULL, '3674060607960005', '863986451453000', '002', 'Ciracas', '0804160857', 'Ery Kurnia', '199607062019031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (69, '0', '198407112019031002', 'Albar Aliyyus, S.H', 'Albar Aliyyus', 6, 99, 0, 6, 7, 20, 21, '40', 2, 0, 0, 0, 31, '', '08037', 1, 'Cirebon', '1984-07-11', 1, 2, 'Jl. Pasar Barat No. 351 Rt 003 RW. 001 Desa. Kuningan Kec. Kuningan', '', 'Jl. Pasar Barat No. 351 Rt 003 RW. 001 Desa. Kuningan Kec. Kuningan', '', '', '089515777096', '(021) 29681', 'albar.aliyyus@lpsk.go.id', 'albarsaudara@gmail.com', '', NULL, '3209212008140005', '791739279426000', '002', 'Ciracas', '0804160744', 'Albar Aliyyus', '198407112019031002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (70, '0', '199108232019032004', 'Soviana Nur Afifah', 'Soviana Nur Afifah', 6, 99, 0, 6, 7, 20, 22, '44', 2, 0, 0, 0, 31, '', '08037', 2, 'Banyumas', '1991-08-23', 1, 2, 'Jl. Raya Bulak RT. 24 RW.07 Desa. Bulak Kec. Jatibarang - Indramayu', '', 'Jl. Raya Bulak RT. 24 RW.07 Desa. Bulak Kec. Jatibarang - Indramayu', '', '', '087824897404', '(021) 29681', 'soviana.nur@lpsk.go.id', 'sovianaurss@gmail.com', '', NULL, '3217096308910002', '764982658421000', '002', 'Ciracas', '0805008381', 'Soviana Nur Afifah', '199108232019032004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (71, '0', '198609152019032002', 'Siti Nurliyah, S.H', 'Siti Nurliyah', 6, 99, 0, 6, 7, 20, 22, '43', 2, 0, 0, 0, 31, '', '08037', 2, 'Jakarta', '1986-09-15', 1, 1, 'Jl. Bahagia Indah No. 26 RT.26 RW.004 Desa. Medan Satria Kec. Bekasi', '', 'Jl. Bahagia Indah No. 26 RT.26 RW.004 Desa. Medan Satria Kec. Bekasi', '', '', '085773201222', '(021) 29681', 'siti.nurliyah@lpsk.go.id', 'lia_joya@yahoo.co.id', '', NULL, '3275065509860013', '895375681407000', '002', 'Ciracas', '0804596350', 'Siti Nurliyah', '198609152019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (72, '0', '199506212019032003', 'Mutia Ariesca Suwandi, S.H', 'Mutia Ariesca Suwandi', 6, 99, 0, 6, 24, 25, 26, '153', 2, 0, 0, 0, 31, '', '08037', 2, 'Magelang', '1995-06-21', 1, 1, 'Desa. Bandarjaya Timur Kec. Terbanggi Besar - Lampung Tengah', '', 'Desa. Bandarjaya Timur Kec. Terbanggi Besar - Lampung Tengah', '', '', '089510400581', '(021) 29681', 'mutia.ariesca@lpsk.go.id', 'ariescamutia@gmail.com', '', NULL, '1802076106950004', '900306077321000', '002', 'Ciracas', '0804596327', 'Mutia Ariesca Suwandi', '199506212019032003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (73, '0', '199304262019032002', 'Holy Apriliani, S.H', 'Holy Apriliani', 6, 99, 0, 6, 24, 25, 26, '151', 2, 0, 0, 0, 31, '', '08037', 2, 'Jakarta', '1993-04-26', 2, 1, 'JL. Mangga No 21 RT. 002 RW. 011 Desa. Jatikramat Kec. Jati Asih - Bekasi', '', 'JL. Mangga No 21 RT. 002 RW. 011 Desa. Jatikramat Kec. Jati Asih - Bekasi', '', '', '085888372307', '(021) 29681', 'holy.apriliani@lpsk.go.id', 'holyholy.apriliani@gmail.com', '', NULL, '3275096604930009', '756861738447000', '002', 'Ciracas', '0804596203', 'Holy Apriliani', '199304262019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (74, '0', '199404142019032007', 'Syafatania, S.Psi', 'Syafatania', 6, 99, 0, 6, 24, 25, 26, '152', 2, 0, 0, 0, 31, '', '08037', 2, 'Pamekasan', '1994-04-14', 1, 1, 'Perum. Bantuan Blok D No. 07 RT.02 RW.02 Desa. Bantuan Kec. Bantuan - Sumenep', '', 'Perum. Bantuan Blok D No. 07 RT.02 RW.02 Desa. Bantuan Kec. Bantuan - Sumenep', '', '', '081333918512', '(021) 29681', 'syafatania@lpsk.go.id', 'fany.syafatania@gmail.com', '', NULL, '2529265404940002', '866946395608000', '002', 'Ciracas', '0804596361', 'Syafatania', '199404142019032007.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (75, '0', '199512052019032003', 'Ananda Rasullia, S.Psi', 'Ananda Rasullia', 6, 99, 0, 6, 24, 25, 26, '154', 2, 0, 0, 0, 31, '', '08037', 2, 'Bogor', '1995-12-05', 1, 1, 'Jl. Sayaga Blok F3 No. 4 Rt 03 RW.06 Desa. Sukahati Kec. Cibinong - Bogor', '', 'Jl. Sayaga Blok F3 No. 4 Rt 03 RW.06 Desa. Sukahati Kec. Cibinong - Bogor', '', '', '082110851210', '(021) 29681', 'ananda.rasullia@lpsk.go.id', 'arasullia@gmail@gmail.com', '', NULL, '3201014512950004', '869881086403000', '002', 'Ciracas', '0804160777', 'Ananda Rasullia', '199512052019032003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (76, '0', '198807112019031005', 'Cahya Tutuk Irana Sakti, S.Psi', 'Cahya Tutuk Irana Sakti', 6, 99, 0, 6, 24, 25, 26, '149', 2, 0, 0, 0, 31, '', '08037', 1, 'Malang', '1988-07-11', 1, 2, 'Jl. Perum Awan Jingga Blok 09 RT 008 RW. 003 Desa. Baturetno Kec. Singosari - Malang', '', 'Jl. Perum Awan Jingga Blok 09 RT 008 RW. 003 Desa. Baturetno Kec. Singosari - Malang', '', '', '081232009720', '(021) 29681', 'cahya.tutuk@lpsk.go.id', 'seamellody7@gmail.com', '', NULL, '3507241107880001', '806759353657000', '002', 'Ciracas', '0804160824', 'Cahya Tutuk Irana Sakti', '198807112019031005.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (77, '0', '199002252019032002', 'Farah Shafira, S.Psi', 'Farah Shafira', 6, 99, 0, 6, 24, 25, 26, '150', 2, 0, 0, 0, 31, '', '08038', 2, 'Jakarta', '1990-02-25', 1, 1, 'Jl. Pondok Cabe Indah No. H/4 RT 001 RW. 006 Desa. Pondok Cabe Udik Kec. Pamulang - Tangerang Selatan', '', 'Jl. Pondok Cabe Indah No. H/4 RT 001 RW. 006 Desa. Pondok Cabe Udik Kec. Pamulang - Tangerang Selatan', '', '', '081222295056', '(021) 29681', 'farah.shafira@lpsk.go.id', 'farah.shafiraa@gmail.com', '', NULL, '3674066502900010', '544258858411000', '002', 'Ciracas', '0804596178', 'Farah Shafira', '199002252019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (78, '0', '198406152019032002', 'Sri Juniardhani Tjahjaningsih, S.Psi', 'Sri Juniardhani Tjahjaningsih.', 6, 99, 0, 6, 24, 25, 26, '147', 2, 0, 0, 0, 31, '', '08038', 2, 'Jakarta', '1984-06-15', 1, 2, 'Komp. Pamulang Permai 1 B8 RT. 002 RW. 015 Desa. Pamulang Barat Kec. Pamulang - Tangerang Selatan', '', 'Komp. Pamulang Permai 1 B8 RT. 002 RW. 015 Desa. Pamulang Barat Kec. Pamulang - Tangerang Selatan', '', '', '081315278394', '(021) 29681', 'sri.juniardhani@lpsk.go.id', 'juniardhani@gmail.com', '', NULL, '3674065506840004', '883407066411000', '002', 'Ciracas', '0804631546', 'Sri Juniardhani Tjahjaningsih.', '198406152019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (79, '0', '198608072019032002', 'Candrawati Puspitasari, S.Psi', 'Candrawati Puspitasari', 6, 99, 0, 6, 24, 25, 26, '148', 2, 0, 0, 0, 31, '', '08038', 2, 'Purworejo', '1986-08-07', 1, 2, 'Desa. Nanggewer RT. 002 RW. 008 Kec. Cibinong - Bogor', '', 'Desa. Nanggewer RT. 002 RW. 008 Kec. Cibinong - Bogor', '', '', '082134262634', '(021) 29681', 'candrawati.puspitasari@lpsk.go.id', 'nochshand@gmail.com', '', NULL, '3201014708860022', '343946174403000', '002', 'Ciracas', '0804160835', 'Candrawati Puspitasari', '198608072019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (80, '0', '199109102019032005', 'Kezia Henderisa Rumbiak, S.E', 'Kezia Henderisa Rumbiak', 6, 99, 0, 6, 24, 25, 27, '141', 2, 0, 0, 0, 31, '', '08038', 2, 'Fakfak', '1991-09-10', 2, 2, 'Jl. Silva Griya RT. 002 RW. 005 Desa. Vim Kec. Abepura - Jayapura', '', 'Jl. Silva Griya RT. 002 RW. 005 Desa. Vim Kec. Abepura - Jayapura', '', '', '082189677127', '(021) 29681', 'kezia.henderisa@lpsk.go.id', 'keziarumbiak@yahoo.com', '', NULL, '9171035009910005', '900212218952000', '002', 'Ciracas', '0804596225', 'Kezia Henderisa Rumbiak', '199109102019032005.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (81, '0', '198710072019031002', 'Yogi Suryamansyah, S.AP', 'Yogi Suryamansyah', 6, 99, 0, 6, 24, 25, 27, '138', 2, 0, 0, 0, 31, '', '08024', 1, 'Bandung', '1987-10-07', 1, 2, 'Jl. Taman Kopo Indah No. i-6 RT. 05 RW. 16 Desa. Margahayu Selatan Kec. Margahayu - Bandung', '', 'Jl. Taman Kopo Indah No. i-6 RT. 05 RW. 16 Desa. Margahayu Selatan Kec. Margahayu - Bandung', '', '', '08562158214', '(021) 29681', 'yogi.suryamansyah@lpsk.go.id', 'yogisuryamansyah07@gmail.com', '', NULL, '3204090710870003', '880920467445000', '002', 'Ciracas', '0804596407', 'Yogi Suryamansyah', '198710072019031002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (82, '0', '198609222019031001', 'Andreas Lucky Lukwira, S.Sos', 'Andreas Lucky Lukwira', 6, 99, 0, 6, 24, 25, 27, '136', 2, 0, 0, 0, 31, '', '08017', 1, 'Jakarta', '1986-09-22', 2, 2, 'Jl. Kramat Asih  No. 10 RT. 004 RW. 003', '', 'Jl. Kramat Asih  No. 10 RT. 004 RW. 003', '', '', '08127778786', '(021) 29681', 'andreas.lucky@lpsk.go.id', 'andraesluckylukwira@gmail.com', '', NULL, '317542209860013', '46315290005000', '002', 'Ciracas', '0805008278', 'Andreas Lucky Lukwira', '198609222019031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (83, '0', '199210012019031002', 'Hevy Ukasyah Bahreisy, S.Sos', 'Hevy Ukasyah Bahreisy', 6, 99, 0, 6, 24, 25, 27, '143', 2, 0, 0, 0, 31, '', '08038', 1, 'Jepara', '1992-10-01', 1, 2, 'Desa. Lebuawu RT. 18 RW. 04 Kec. Pencangaan - Jepara', '', 'Desa. Lebuawu RT. 18 RW. 04 Kec. Pencangaan - Jepara', '', '', '085641086133', '(021) 29681', 'hevy.ukasyah@lpsk.go.id', 'hevybahreisy@gmail.com', '', NULL, '3320020110920002', '746251982516000', '002', 'Ciracas', '0804631513', 'Hevy Ukasyah Bahreisy', '199210012019031002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (84, '0', '199408182019032006', 'Firdha Ratnasari Guntoro, S.K.Pm', 'Firdha Ratnasari Guntoro', 6, 99, 0, 6, 24, 25, 27, '145', 2, 0, 0, 0, 31, '', '08038', 2, 'Dili', '1994-08-18', 1, 1, 'Dk. Kademangah RT.002 RW.001 Desa. Singayudan Kec. Mirit - Kebumen', '', 'Dk. Kademangah RT.002 RW.001 Desa. Singayudan Kec. Mirit - Kebumen', '', '', '082240199914', '(021) 29681', 'firdha.ratnasari@lpsk.go.id', 'guntoro.firdha@gmail.com', '', NULL, '3305085808940002', '900063819523000', '002', 'Ciracas', '0804596189', 'Firdha Ratnasari Guntoro', '199408182019032006.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (85, '0', '199010042019031001', 'Muhammad Abdul Aziz, S.H', 'Muhammad Abdul Aziz', 6, 99, 0, 6, 24, 25, 27, '140', 2, 0, 0, 0, 31, '', '08038', 1, 'Wonogiri', '1990-10-04', 1, 1, 'Jl. Gadungan RT. 001 RW 003 Desa. Kismantoro Kec. Wonogiri', '', 'Jl. Gadungan RT. 001 RW 003 Desa. Kismantoro Kec. Wonogiri', '', '', '085641724840', '(021) 29681', 'muhammad.abdul@lpsk.go.id', 'bdulazizsh@gmail.com', '', NULL, '3312160410900003', '706099371532000', '002', 'Ciracas', '0804596305', 'Muhammad Abdul Aziz', '199010042019031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (86, '0', '199110192019031005', 'Indria Dwi Sukarno, S.I.Kom', 'Indria Dwi Sukarno', 6, 99, 0, 6, 24, 25, 27, '142', 2, 0, 0, 0, 31, '', '08038', 1, 'Jakarta', '1991-10-19', 1, 2, 'Jl. Jatayu IX Bekasi Regensi 1 Blok J4 Desa. Wanasari Kec. Cibitung - Bekasi', '', 'Jl. Jatayu IX Bekasi Regensi 1 Blok J4 Desa. Wanasari Kec. Cibitung - Bekasi', '', '', '081219345556', '(021) 29681', 'indria.dwi@lpsk.go.id', 'indriadsukarno@gmail.com', '', NULL, '3216071910910009', '810550962435000', '002', 'Ciracas', '0804596214', 'Indria Dwi Sukarno', '199110192019031005.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (87, '0', '198610222019031002', 'Ayub Oktaviano, S.I.Kom', 'Ayub Oktaviano', 6, 99, 0, 6, 24, 25, 27, '137', 2, 0, 0, 0, 31, '', '08039', 1, 'Bantul', '1986-10-22', 1, 2, 'Jl. Pabuaran RT.08 RW. 04 Desa. Pabuaran Kec. Cibinong - Bogor', '', 'Jl. Pabuaran RT.08 RW. 04 Desa. Pabuaran Kec. Cibinong - Bogor', '', '', '08179446298', '(021) 29681', 'ayub.oktaviano@lpsk.go.id', 'ayub.oktaviano@yahoo.com', '', NULL, '3402082210860002', '558510806543000', '002', 'Ciracas', '0805008290', 'Ayub Oktaviano', '198610222019031002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (88, '0', '199009192019032004', 'Septiana Sundari, S.I.Kom', 'Septiana Sundari', 6, 99, 0, 6, 24, 25, 27, '139', 2, 0, 0, 0, 31, '', '08039', 2, 'Wonogiri', '1990-09-19', 1, 2, 'Jl. Regalia No. 67 RT 006 RW. 006 Desa. Susukan Kec. Ciracas - Jakarta', '', 'Jl. Regalia No. 67 RT 006 RW. 006 Desa. Susukan Kec. Ciracas - Jakarta', '', '', '085810357595', '(021) 29681', 'septiana.sundari@lpsk.go.id', 'septianasundari@gmail.com', '', NULL, '3312195909900001', '869944801009000', '002', 'Ciracas', '0805008370', 'Septiana Sundari', '199009192019032004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (89, '0', '198606182019031002', 'Ardiyanto Wibowo, S.H', 'Ardiyanto Wibowo', 6, 99, 0, 6, 24, 25, 27, '135', 2, 0, 0, 0, 31, '', '08039', 1, 'Jayapura', '1986-06-18', 1, 1, 'Jl. Flamboyan NO 160 A RT 05 RW. 57 Desa. Sambilegi Baru/Maguwoharjo Kec. Depok - Jakarta', '', 'Jl. Flamboyan NO 160 A RT 05 RW. 57 Desa. Sambilegi Baru/Maguwoharjo Kec. Depok - Jakarta', '', '', '081227248271', '(021) 29681', 'ardiyanto.wibowo@lpsk.go.id', 'ardy.bungsu@gmail.com', '', NULL, '3404071806860002', '744625161542000', '002', 'Ciracas', '0805252738', 'Ardiyanto Wibowo', '198606182019031002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (90, '0', '199402202019032004', 'Maharani Indah Puspita, S.I.Kom', 'Maharani Indah Puspita', 6, 99, 0, 6, 24, 25, 27, '144', 2, 0, 0, 0, 31, '', '08039', 2, 'Jakarta', '1994-02-20', 1, 1, 'Jl. Rungkut Mapan Barat No. Af-01 RT.06 RW.08 Desa. Rungkut Tengah Kec. Gunung Anyar - Surabaya', '', 'Jl. Rungkut Mapan Barat No. Af-01 RT.06 RW.08 Desa. Rungkut Tengah Kec. Gunung Anyar - Surabaya', '', '', '087759239929', '(021) 29681', 'maharani.indah@lpsk.go.id', 'naharaniop202@gmail.com', '', NULL, '3214036002940001', '869926840615000', '002', 'Ciracas', '0804596269', 'Maharani Indah Puspita', '199402202019032004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (91, '0', '199504132019032003', 'I\'anah Marfu\'ah, S.I.Kom', 'I\'anah Marfu\'ah', 6, 99, 0, 6, 24, 25, 27, '146', 2, 0, 0, 0, 31, '', '08039', 2, 'Klaten', '1995-04-13', 1, 1, 'DK Karanganom Rt. 1 RW. 3 Desa. Karanganom, Kec. Karanganom - Klaten ', '', 'DK Karanganom Rt. 1 RW. 3 Desa. Karanganom, Kec. Karanganom - Klaten ', '', '', '085786967355', '(021) 29681', 'ianah.marfuah@lpsk.go.id', 'ianahmrf@gmail.com', '', NULL, '3310185304950003', '', '002', 'Ciracas', '0805252727', 'I\'anah Marfu\'ah', '199504132019032003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (92, '0', '199611172019032003', 'Vinda Noviasari, S.H', 'Vinda Noviasari', 6, 99, 0, 6, 24, 28, 30, '156', 2, 0, 0, 0, 31, '', '08039', 2, 'Timika', '1996-11-17', 1, 1, 'Jl. Yos Sudarso No 42 RT. 013 RT. 003 Desa. Koperapoka Kec. Mimika Baru - Timika Mimika', '', 'Jl. Yos Sudarso No 42 RT. 013 RT. 003 Desa. Koperapoka Kec. Mimika Baru - Timika Mimika', '', '', '082242342563', '(021) 29681', 'vinda.noviasari@lpsk.go.id', 'vinda.novi@gmail.com', '', NULL, '9109015711960004', '847710365953000', '002', 'Ciracas', '0804596383', 'Vinda Noviasari', '199611172019032003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (93, '0', '199007092019032005', 'Mir\'atul Latifah, S.H', 'Mir\'atul Latifah', 6, 99, 0, 6, 24, 28, 30, '155', 2, 0, 0, 0, 31, '', '08039', 2, 'Magetan', '1990-07-09', 1, 2, 'Jl. Komp. BKP Blok III A No. 9 RT 01 RW. 01 Desa. Margatani Kec. Kramatwatu - Serang', '', 'Jl. Komp. BKP Blok III A No. 9 RT 01 RW. 01 Desa. Margatani Kec. Kramatwatu - Serang', '', '', '085780473859', '(021) 29681', 'miratul.latifah@lpsk.go.id', 'miralatifa@gmail.com', '', NULL, '352004490790001', '666045075646000', '002', 'Ciracas', '0805023977', 'Mir\'atul Latifah', '199007092019032005.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (94, '0', '199405312019032002', 'Maylisa Eka Handani Putri, S.E', 'Maylisa Eka Handani Putri', 6, 99, 0, 6, 31, 32, 33, '128', 2, 0, 0, 0, 31, '', '08039', 2, 'Kediri', '1994-05-31', 1, 1, 'Jl.Sumber RT. 005 RW. 005 Desa. Ngadiluwih Kec. Ngadiluwih - Kediri', '', 'Jl.Sumber RT. 005 RW. 005 Desa. Ngadiluwih Kec. Ngadiluwih - Kediri', '', '', '085784888572', '(021) 29681', 'maylisa.eka@lpsk.go.id', 'maylisaekap@gmail.com', '', NULL, '3506047105940000', '818272106655000', '002', 'Ciracas', '0804596270', 'Maylisa Eka Handani Putri', '199405312019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (95, '0', '199010072019031001', 'Askar Eko Harpito, S.Kom', 'Askar Eko Harpito', 6, 99, 0, 6, 31, 32, 33, '122', 2, 0, 0, 0, 31, '', '08039', 1, 'Yogyakarta', '1990-10-07', 1, 1, 'Perumahaan Ngori Indah NO C3 RT 13 RW 68 Desa. Wedomartani Kec. Ngemplak - Sleman', '', 'Perumahaan Ngori Indah NO C3 RT 13 RW 68 Desa. Wedomartani Kec. Ngemplak - Sleman', '', '', '085726242020', '(021) 29681', 'askar.eko@lpsk.go.id', 'askareko@gmail.com', '', NULL, '3404110710900002', '842247348542000', '002', 'Ciracas', '0804160788', 'Askar Eko Harpito', '199010072019031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (96, '0', '199509272019032004', 'Aulia Aminda Dhianti, S.Sos', 'Aulia Aminda Dhianti', 6, 99, 0, 6, 31, 32, 33, '132', 2, 0, 0, 0, 31, '', '08039', 2, 'Jakarta', '1995-09-27', 1, 2, 'Jl. Tanjung IX No 14 RT. 10 RW. 12 Desa. Jatisampurna Ke. Jatisampurna - Bekasi', '', 'Jl. Tanjung IX No 14 RT. 10 RW. 12 Desa. Jatisampurna Ke. Jatisampurna - Bekasi', '', '', '083899799226', '(021) 29681', 'aulia.amida@lpsk.go.id', 'auliaasm@gmail.com', '', NULL, '3275106709950008', '829809037447000', '002', 'Ciracas', '0804160799', 'Aulia Aminda Dhianti', '199509272019032004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (97, '0', '199306282019032002', 'Lina Rosdiana, S.T', 'Lina Rosdiana', 6, 99, 0, 6, 31, 32, 33, '126', 2, 0, 0, 0, 31, '', '08040', 2, 'Jakarta', '1993-06-28', 1, 2, 'Jl. Kebon Nanas Utara No. 46 RT.13 RW.04 Desa. Cipinang Cempedak Kec. Jatinegara - DKI Jakarta', '', 'Jl. Kebon Nanas Utara No. 46 RT.13 RW.04 Desa. Cipinang Cempedak Kec. Jatinegara - DKI Jakarta', '', '', '0816819331', '(021) 29681', 'lina.rosdiana@lpsk.go.id', 'linarosdianasimamora@gmail.com', '', NULL, '3275036806930001', '75311957702000', '002', 'Ciracas', '0804596247', 'Lina Rosdiana', '199306282019032002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (98, '0', '199303132019032006', 'Yusnia Arianingsih, S.IP', 'Yusnia Arianingsih', 6, 99, 0, 6, 31, 32, 33, '124', 2, 0, 0, 0, 31, '', '08040', 2, 'Salatiga', '1993-03-13', 1, 1, 'Jl.Gunung Payung II Tangerang No 57 RT 007 RW 003 Desa Blotongan Kec. Sidorejo - Salatiga', '', 'Jl.Gunung Payung II Tangerang No 57 RT 007 RW 003 Desa Blotongan Kec. Sidorejo - Salatiga', '', '', '085641086133', '(021) 29681', 'yusnia.arianingsih@lpsk.go.id', 'yusnia.ya@gmail.com', '', NULL, '3373015303930002', '767578958505000', '002', 'Ciracas', '0804596418', 'Yusnia Arianingsih', '199303132019032006.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (99, '0', '199111242019032000', 'Lidya Susanti Siburian, S.H', 'Lidya Susanti Siburian', 6, 99, 0, 6, 31, 32, 33, '123', 2, 0, 0, 0, 31, '', '08040', 2, 'Medan', '1991-11-24', 3, 1, 'Jl. Tangguk Bongkar XI No. 22 Desa. Tegak Sari Mandala 1 Kec. Medan Denai - Medan ', '', 'Jl. Tangguk Bongkar XI No. 22 Desa. Tegak Sari Mandala 1 Kec. Medan Denai - Medan ', '', '', '085277722449', '(021) 29681', 'lidya.susanti@lpsk.go.id', '', '', NULL, '1271046411910007', '869880328122000', '002', 'Ciracas', '0804596236', 'Lidya Susanti Siburian', '199111242019032000.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (100, '0', '198812052019031002', 'Putra Wijaya, S.I.A', 'Putra Wijaya', 6, 99, 0, 6, 31, 32, 33, '119', 2, 0, 0, 0, 31, '', '08040', 1, 'Jakarta', '1988-12-05', 1, 1, 'Jl. Selayar No 16 RT 003 RW. 004 Desa. Pasar Rebo Kec. Pasar Rebo - Jakarta', '', 'Jl. Selayar No 16 RT 003 RW. 004 Desa. Pasar Rebo Kec. Pasar Rebo - Jakarta', '', '', '081511393306', '(021) 29681', 'putra.wijaya@lpsk.go.id', 'putrawijaya1288@gmail.com', '', NULL, '3175050512880004', '357481829009000', '002', 'Ciracas', '0804631535', 'Putra Wijaya', '198812052019031002.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (101, '0', '198312272019031001', 'Fakhrurrazi, S.IP', 'Fakhrurrazi', 6, 99, 0, 6, 31, 32, 33, '116', 2, 0, 0, 0, 31, '', '08040', 1, 'Jakarta', '1983-12-27', 1, 2, 'Jl. Pinang Ranti No. 33A RT.016 RW. 001 Desa. Pinang Ranti Kec. Makasar - Jakarta', '', 'Jl. Pinang Ranti No. 33A RT.016 RW. 001 Desa. Pinang Ranti Kec. Makasar - Jakarta', '', '', '0811940629', '(021) 29681', 'fakhrurrazi@lpsk.go.id', '', '', NULL, '3175082101160015', '571122068005000', '002', 'Ciracas', '0804596167', 'Fakhrurrazi', '198312272019031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (102, '0', '199507142019031003', 'Harry Nugraha Pratama, S.IP', 'Harry Nugraha Pratama', 6, 99, 0, 6, 31, 32, 33, '131', 2, 0, 0, 0, 31, '', '08040', 1, 'Jakarta', '1995-07-14', 1, 1, 'Jl. Harapan Indah Regency No. 6 RT 009 RW. 019 Desa. Pejuang Kec. Medan Satria - Bekasi', '', 'Jl. Harapan Indah Regency No. 6 RT 009 RW. 019 Desa. Pejuang Kec. Medan Satria - Bekasi', '', '', '089636805195', '(021) 29681', 'harry.nugraha@lpsk.go.id', 'harry.npratama31@gmail.com', '', NULL, '3275061407950017', '869886622427000', '002', 'Ciracas', '0804631502', 'Harry Nugraha Pratama', '199507142019031003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (103, '0', '198905062019031004', 'Ronaldi Maika Putra, S.IP', 'Ronaldi Maika Putra', 6, 99, 0, 6, 31, 32, 33, '121', 2, 0, 0, 0, 31, '', '08040', 1, 'Padang', '1989-05-06', 1, 1, 'Jl. Cupak Tangah No. 24 RT. 001 RW. 002 Desa. Cupak Tangah Kec. Pauh - Padang', '', 'Jl. Cupak Tangah No. 24 RT. 001 RW. 002 Desa. Cupak Tangah Kec. Pauh - Padang', '', '', '081807008574', '(021) 29681', 'ronaldi.maika@lpsk.go.id', 'ronald_maika@yahoo.com', '', NULL, '1371080606890004', '721328128416000', '002', 'Ciracas', '0804596338', 'Ronaldi Maika Putra', '198905062019031004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (104, '0', '199503292019032003', 'Nurfianingsih, S.IP', 'Nurfianingsih', 6, 99, 0, 6, 31, 32, 33, '130', 2, 0, 0, 0, 31, '', '08040', 2, 'Tulungagung', '1995-03-29', 1, 1, 'Desa. Pojok NO. 03 RT 05 RW. 01 Kec. Campurdata - Tulungagung', '', 'Desa. Pojok NO. 03 RT 05 RW. 01 Kec. Campurdata - Tulungagung', '', '', '081333918512', '(021) 29681', 'nurfianingsih@lpsk.go.id', 'nurfianingsih@gmail.com', '', NULL, '3504166303950003', '869981449629000', '002', 'Ciracas', '0804631524', 'Nurfianingsih', '199503292019032003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (105, '0', '199402162019031003', 'Hafidz Alam Islami, S.IP', 'Hafidz Alam Islami', 6, 99, 0, 6, 31, 32, 33, '127', 2, 0, 0, 0, 31, '', '08040', 1, 'Bogor', '1994-02-16', 1, 1, 'Jl. Kp. Gadog RT 003 RW. 003 Desa. Gadog Kec. Megamendung - Bogor', '', 'Jl. Kp. Gadog RT 003 RW. 003 Desa. Gadog Kec. Megamendung - Bogor', '', '', '085714906241', '(021) 29681', 'hafidz.alam@lpsk.go.id', 'hafidzalam@gmail.com', '', NULL, '3201271602930002', '869882399434000', '002', 'Ciracas', '0805008303', 'Hafidz Alam Islami', '199402162019031003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (106, '0', '199503142019032004', 'Rizki Amalia, S.H', 'Rizki Amalia', 6, 99, 0, 6, 31, 32, 33, '129', 2, 0, 0, 0, 31, '', '08040', 2, 'Sidoarjo', '1995-03-14', 1, 1, 'JL Gg. Masjid No. 23 RT 06 RW. 02 Desa. Kemangsen Kec. Balongbendo - Sidoarjo', '', 'JL Gg. Masjid No. 23 RT 06 RW. 02 Desa. Kemangsen Kec. Balongbendo - Sidoarjo', '', '', '081232461047', '(021) 29681', 'rizki.amalia@lpsk.go.id', 'rizki.amalia1446@gmail.com', '', NULL, '3515125403950001', '856915947603000', '002', 'Ciracas', '0805008358', 'Rizki Amalia', '199503142019032004.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (107, '0', '199306062019032003', 'Rachelia Eni Putri, S.E', 'Rachelia Eni Putri', 6, 99, 0, 6, 31, 32, 33, '125', 2, 0, 0, 0, 31, '', '08041', 2, 'Jakarta', '1993-06-06', 1, 1, 'Jl. Lewa No 5 RT 06 RW. 010 Desa. Pekayon Kec. Pasar Rebo - Jakarta Timur', '', 'Jl. Lewa No 5 RT 06 RW. 010 Desa. Pekayon Kec. Pasar Rebo - Jakarta Timur', '', '', '085710383304', '(021) 29681', 'rachelia.eni@lpsk.go.id', '', '', NULL, '3175054606930004', '733002364009000', '002', 'Ciracas', '0805008325', 'Rachelia Eni Putri', '199306062019032003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (108, '0', '198410122019032001', 'Wulan Dewi Satriyani, S.Sos', 'Wulan Dewi Satriyani', 6, 99, 0, 6, 31, 32, 33, '117', 2, 0, 0, 0, 31, '', '08041', 2, 'Bandung', '1984-10-12', 1, 2, 'Jl. Panca Wisma No. 12 RT 03 RW. 05 Desa. Ceger Kec. Cipayung - Jakarta Timur', '', 'Jl. Panca Wisma No. 12 RT 03 RW. 05 Desa. Ceger Kec. Cipayung - Jakarta Timur', '', '', '08121424651', '(021) 29681', 'wulan.dewi@lpsk.go.id', 'wulan.dewi.satriyani@gmail.com', '', NULL, '3273285210840002', '246142459429000', '002', 'Ciracas', '0804596394', 'Wulan Dewi Satriyani', '198410122019032001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (109, '0', '198903162019031001', 'Ali Imran, S.E', 'Ali Imran', 6, 99, 0, 6, 31, 32, 33, '120', 2, 0, 0, 0, 31, '', '08041', 1, 'Jakarta', '1989-03-16', 1, 2, 'Jl. Bambu Larangan No. 53 RT 007 RW. 02 Desa. Cilandak Timur Kec. Pasar Minggu - Jakarta', '', 'Jl. Bambu Larangan No. 53 RT 007 RW. 02 Desa. Cilandak Timur Kec. Pasar Minggu - Jakarta', '', '', '087887775989', '(021) 29681', 'ali.imran@lpsk.go.id', 'limran741@gmail.com', '', NULL, '3174041603890008', '667078117017000', '002', 'Ciracas', '0804160766', 'Ali Imran', '198903162019031001.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (110, '0', '198611192019031003', 'Dimas Susanto, S.E', 'Dimas Susanto', 6, 99, 0, 6, 31, 32, 33, '118', 2, 0, 0, 0, 31, '', '08041', 1, 'Jakarta', '1986-11-19', 1, 2, 'Jl. Mabes Hankam No. 1 RT. 001 RW. 005 desa. Setu Kec. Cipayung - Bogor', '', 'Jl. Mabes Hankam No. 1 RT. 001 RW. 005 desa. Setu Kec. Cipayung - Bogor', '', '', '082112392300', '(021) 29681', 'dimas.susanto@lpsk.go.id', 'susantodimasIII@yahoo.com', '', NULL, '3175101911860001', '241389212009000', '002', 'Ciracas', '0804160846', 'Dimas Susanto', '198611192019031003.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (111, '0', '080029', 'Abdul Aziz Muslih, S.H', 'Abdul Aziz Muslih', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08002', 1, 'Pekalonga', '1985-06-01', 1, 2, 'Kp. Kapitan No. 24 017 004 Klender Duren Sawit Jakarta Timur DKI Jakarta', '13470', 'Kp. Kapitan No. 24 017 004 Klender Duren Sawit Jakarta Timur DKI Jakarta', '13470', '', '085692621314', '(021) 29681', 'abdul.aziz@lpsk.go.id', 'aazis178@gmail.com', '', NULL, '3175070106851001', '548929223008000', '002', 'Ciracas', '0363442924', 'Abdul Aziz Muslih', '080029.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (112, '0', '080118', 'Ade Hendra Mulyana, S.E', 'Ade Hendra Mulyana', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08011', 1, 'Tanggeran', '1974-04-12', 1, 2, 'KP. Sepatan Wetan 002 002 Pondok Jaya Sepatan Tanggerang Banten', '15520', 'KP. Sepatan Wetan 002 002 Pondok Jaya Sepatan Tanggerang Banten', '15520', '', '082111796666', '(021) 29681', 'ade.hendra@lpsk.go.id', 'adehendra.391@gmail', '', NULL, '3603161204740006', '889472908418000', '002', 'Ciracas', '0297804626', 'Ade Hendra Mulyana', '080118.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (113, '0', '080342', 'Riska, A.Md Keb', 'Riska', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08034', 2, 'Lebak  ', '1994-02-03', 1, 2, 'KP. Kaduranggon 007 002 Banjarsari Cileles Tanggerang Banten', '42353', 'KP. Kaduranggon 007 002 Banjarsari Cileles Tanggerang Banten', '42353', '', '085710156784', '(021) 29681', '', 'khariska4500@gmial.com', '', NULL, '3602104302940003', '846238632009000', '002', 'Ciracas', '0706455708', 'Riska', '080342.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (114, '0', '080275', 'Yogi Nugraha, S.Kom', 'Yogi Nugraha', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08027', 1, 'Bekasi ', '1992-07-27', 1, 2, 'Jl. Delima Raya No. 805 013 012 Duren Jaya Bekasi Timur Kota Bekasi Jawa Barat', '17111', 'Jl. Delima Raya No. 805 013 012 Duren Jaya Bekasi Timur Kota Bekasi Jawa Barat', '17111', '', '081386857549', '(021) 29681', 'yogi.nugraha@lpsk.go.id', '', '', NULL, '3275012707920022', '766663439407000', '002', 'Ciracas', '0461480004', 'Yogi Nugraha', '080275.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (115, '0', '080325', 'Septia Prima Hardiyanti, S.E', 'Septia Prima Hardiyanti', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08032', 2, 'Oku Timur', '1992-09-30', 1, 1, 'Ulak Baru 002 000 Ulak Baru Cempaka Oku Timur Sumatera Selatan', '32184', 'Ulak Baru 002 000 Ulak Baru Cempaka Oku Timur Sumatera Selatan', '32184', '', '082282561789', '(021) 29681', '', 'septiptima@yahoo.com', '', NULL, '1608107009920002', '821004546009000', '002', 'Ciracas', '0573327412', 'Septia Prima Hardiyanti', '080325.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (116, '0', '080033', 'Aan Munarto, S.Kom', 'Aan Munarto. S.Kom', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08003', 1, 'Jakarta ', '1982-09-03', 1, 2, 'Jl. Cipinang Muara III 010 015 Cipinang Muara Jatinegara Jakarta Timur DKI Jakarta', '13420', 'Jl. Cipinang Muara III 010 015 Cipinang Muara Jatinegara Jakarta Timur DKI Jakarta', '13420', '', '081212161628', '(021) 29681', 'aan@lpsk.go.id', 'aan_82yahoo.com', '', NULL, '3175030309820011', '672294451002000', '002', 'Ciracas', '0295416557', 'Aan Munarto', '080033.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (117, '0', '080214', 'Agus Fitria Budi, S.Pd', 'Agus Fitria Budi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08021', 1, 'Tasikmala', '1980-08-17', 1, 2, 'Legenda Wisata Einstein R.11/27 004 026 Gunung Putri Wanaherang Bogor Jawa Barat', '16965', 'Legenda Wisata Einstein R.11/27 004 026 Gunung Putri Wanaherang Bogor Jawa Barat', '16965', '', '', '(021) 29681', 'agus.fitriana.@lpsk.go.id', '', '', NULL, '3201071708800005', '768182560436000', '002', 'Ciracas', '0331979678', 'Agus Fitria Budi', '080214.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (118, '0', '080145', 'Andreas Kriwanto', 'Andreas Kriwanto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08014', 1, 'Jakarta ', '1984-03-11', 3, 2, 'Pejanten Timur 016 006 Pasar Minggu Pejaten imur Jakarta Selatan DKI Jakarta', '', 'Pejanten Timur 016 006 Pasar Minggu Pejaten imur Jakarta Selatan DKI Jakarta', '', '', '081212660876', '(021) 29681', 'andreas.kiswanto@lpsk.go.id', 'kiswantoandreas3@gmail.com', '', NULL, '3174041103840002', '579768038017000', '002', 'Ciracas', '0295386665', 'Andreas Kriwanto', '080145.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (119, '0', '080241', 'Puti Mayang Sari, S.H', 'Puti Mayang Sari', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08024', 2, 'Jakarta ', '1988-03-07', 1, 1, 'Jl. Galur Sari VII 008 001 Matraman Utan Kayu Selatan Jakarta Timur DKI Jakarta', '', 'Jl. Galur Sari VII 008 001 Matraman Utan Kayu Selatan Jakarta Timur DKI Jakarta', '', '', '081398447779', '(021) 29681', '', 'putimayangsari88@gmail.com', '', NULL, '3175014703880002', '884366071001000', '002', 'Ciracas', '0355517627', 'Puti Mayang Sari', '080241.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (120, '0', '080341', 'Mufidathul Izhmy S, S.IP', 'Mufidathul Izhmy S', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08034', 2, 'Ujung Pandang', '1995-01-08', 1, 1, 'Jl. KER Selatan BLK H/403/10 004 020 Tamalanrea Tamalanrea Makassar Sulawesi Selatan', '', 'Jl. KER Selatan BLK H/403/10 004 020 Tamalanrea Tamalanrea Makassar Sulawesi Selatan', '', '', '085211075183', '(021) 29681', '', 'izhmymufidathul@gmail.com', '', NULL, '7371144801950003', '737114480195003', '002', 'Ciracas', '0706387724', 'Mufidathul Izhmy S', '080341.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (121, '0', '080326', 'Christina Puspa Dewi, A.Md', 'Christina Puspa Dewi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08032', 2, 'Jakarta ', '1986-03-29', 1, 2, 'Jl. Bambu No. 21 001 010 Larangan Kreo Tangerang Banten', '', 'Jl. Bambu No. 21 001 010 Larangan Kreo Tangerang Banten', '', '', '081293704903', '(021) 29681', '', 'christianpuspad3wi@gmail.com', '', NULL, '3671136903860006', '699910435416000', '002', 'Ciracas', '0566649701', 'Christina Puspa Dewi', '080326.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (122, '0', '080243', 'Clara Monica, S.os', 'Clara Monica', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08024', 2, 'Jakarta ', '1987-07-18', 1, 2, 'Komplek Sekneg Block A No.4 001 006 Pondok Kacang Barat Pondok Aren Tanggerang Selatan Jawa Barat', '', 'Komplek Sekneg Block A No.4 001 006 Pondok Kacang Barat Pondok Aren Tanggerang Selatan Jawa Barat', '', '', '087771378153', '(021) 29681', 'clara.monica@lpsk.go.id', '', '', NULL, '3674035807870010', '550027122411000', '002', 'Ciracas', '0341484540', 'Clara Monica', '080243.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (123, '0', '080069', 'Ni\'matul Hidayati, S.S', 'Ni\'matul Hidayati', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08006', 2, 'Purworejo', '1982-09-24', 1, 1, 'Ds. Rumping Jaya  002 001 Kese Grabag Purworejo Jawa Tengah', '', 'Ds. Rumping Jaya  002 001 Kese Grabag Purworejo Jawa Tengah', '', '', '', '(021) 29681', '', 'hidajati@gmail.com', '', NULL, '3306016409820003', '778797415531000', '002', 'Ciracas', '0297810641', 'Ni\'matul Hidayati', '080069.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (124, '0', '080209', 'Rizqi Aulia Rahman, S.E.Sy', 'Rizqi Aulia Rahman', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08020', 2, 'Tanggeran', '1990-06-30', 1, 2, 'Jl. Reni Jaya AI -2/17  003 018 Pamulang Pamulang Barat Tanggerang Selatan Banten', '', 'Jl. Reni Jaya AI -2/17  003 018 Pamulang Pamulang Barat Tanggerang Selatan Banten', '', '', '085691039976', '(021) 29681', '', 'suksesor45@gmail.com', '', NULL, '3674063006900003', '705584795411000', '002', 'Ciracas', '0333173894', 'Rizqi Aulia Rahman', '080209.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (125, '0', '080076', 'Rizki Aji Pamungkas, S.H', 'Rizki Aji Pamungkas ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08007', 1, 'Tegal', '1982-10-04', 1, 2, 'Perum Grand Sawangan Regency Blok D No. 14 002 003 Bedahan Sawangan Depok', '', 'Perum Grand Sawangan Regency Blok D No. 14 002 003 Bedahan Sawangan Depok', '', '', '081314802702', '(021) 29681', '', 'rizkiap04@rap@gmail.com', '', NULL, '3674040410820003', '', '002', 'Ciracas', '0297812093', 'Rizki Aji Pamungkas ', '080076.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (126, '0', '080263', 'Vendi Purnomo, S.I.Kom', 'Vendi Purnomo ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08026', 1, 'Blora', '1987-07-05', 1, 2, 'Perum Taman Kuantan Blok E.1 Jongke 011 025 Sendangadi Mlati Sleman', '', 'Perum Taman Kuantan Blok E.1 Jongke 011 025 Sendangadi Mlati Sleman', '', '', '081225438364', '(021) 29681', '', '', '', NULL, '3316090507870012', '', '002', 'Ciracas', '0370623364', 'Vendi Purnomo ', '080263.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (127, '0', '080171', 'Deby Darmawati, S.H', 'Deby Darmawati ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08017', 2, '', '1987-05-28', 1, 1, 'Jl. Proklamasi No. 89 004 003 Pegangsaan Menteng Jakarta Pusat', '', 'Jl. Proklamasi No. 89 004 003 Pegangsaan Menteng Jakarta Pusat', '', '', '', '(021) 29681', '', '', '', NULL, '7371063805870005', '', '002', 'Ciracas', '0355612272', 'Deby Darmawati ', '080171.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (128, '0', '080300', 'Zulkarnaen Bara, S.H', 'Zulkarnaen Bara ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08030', 1, 'Bima', '1983-11-27', 1, 2, 'Kp. Mangga 013 003 Tugu Selatan Koja Jakarta Utara', '', 'Kp. Mangga 013 003 Tugu Selatan Koja Jakarta Utara', '', '', '089693721522', '(021) 29681', '', 'izul.nau@gmail.com', '', NULL, '3172032711830003', '365806165045000', '002', 'Ciracas', '0505988246', 'Zulkarnaen Bara ', '080300.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (129, '0', '080015', 'Amalia Mahsunah SH', 'Amalia Mahsunah ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08001', 2, 'Blitar', '1986-12-23', 1, 2, 'Jl. P Sumatera 06 No. 114 007 014 Aren Jaya Bekasi Timur Bekasi', '', 'Jl. P Sumatera 06 No. 114 007 014 Aren Jaya Bekasi Timur Bekasi', '', '', '', '(021) 29681', 'amalia.mahsunah@lpsk.go.id', 'milla.mahsu86@gmail.com', '', NULL, '3275036312860021', '897920856407000', '002', 'Ciracas', '0211063472', 'Amalia Mahsunah ', '080015.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (130, '0', '080021', 'M Hasyim Muhsoni I, S.H.I', 'M Hasyim Muhsoni I', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08002', 1, 'Pekalongan', '1981-07-20', 1, 2, 'Pringlangu Gg. 5 No. 11 005 005 Pringlangu Pekalongan Barat Pekalongan', '', 'Pringlangu Gg. 5 No. 11 005 005 Pringlangu Pekalongan Barat Pekalongan', '', '', '081285206776', '(021) 29681', '', 'hasyimmuhsoni@gmail.com', '', NULL, '3375012107810006', '676972870502000', '002', 'Ciracas', '0333173328', 'M Hasyim Muhsoni I', '080021.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (131, '0', '080298', 'Tawang Amuhara, S.H', 'Tawang Amuhara ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08029', 1, 'Bogor', '1993-04-22', 1, 1, 'Komp. Bakosurtanal Blok C.5 006 011 Pabuaran Cibinong Bogor', '', 'Komp. Bakosurtanal Blok C.5 006 011 Pabuaran Cibinong Bogor', '', '', '082226328322', '(021) 29681', '', 'tawang.martha@gmail.com', '', NULL, '3201012204493000', '811716943403000', '002', 'Ciracas', '0507221301', 'Tawang Amuhara ', '080298.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (132, '0', '080009', 'Trisna Pasaribu , SE', 'Trisna Pasaribu ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08000', 2, 'Balikpapan', '1979-03-15', 2, 2, 'Jl. Kayu Manis Timur No. 22 015 001 Kayu Manis Matraman Jakarta Timur', '', 'Jl. Kayu Manis Timur No. 22 015 001 Kayu Manis Matraman Jakarta Timur', '', '', '08121842428', '(021) 29681', '', 'maretjuli@yahoo.co.id', '', NULL, '3175015503790003', '496027574003000', '002', 'Ciracas', '0236708877', 'Trisna Pasaribu ', '080009.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (133, '0', '080303', 'Siti Sukaesih A.Md', 'Siti Sukaesih ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08030', 2, 'Jakarta ', '1991-02-28', 1, 2, 'Lubang Buaya 009 010 Lubang Buaya Cipayung Jakarta Timur', '', 'Lubang Buaya 009 010 Lubang Buaya Cipayung Jakarta Timur', '', '', '081297646446', '(021) 29681', 'siti.sukaesih@lpsk.go.id', '', '', NULL, '3175106802910001', '540257656009000', '002', 'Ciracas', '0507838094', 'Siti Sukaesih ', '080303.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (134, '0', '080210', 'Susandhi Bin Sukatma, S.E', 'Susandhi Bin Sukatma ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08021', 1, 'Jakarta ', '1980-04-21', 1, 2, 'Jl. Sukatani Raya Blok A 8 / 19 001 007 Tegal Alur Kalideres Jakarta Barat', '', 'Jl. Sukatani Raya Blok A 8 / 19 001 007 Tegal Alur Kalideres Jakarta Barat', '', '', '081317120807', '(021) 29681', '', '', '', NULL, '3173062704800004', '690183298085000', '002', 'Ciracas', '0331999221', 'Susandhi Bin Sukatma ', '080210.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (135, '0', '080064', 'Acik Amaliyah, S.H', 'Acik Amaliyah ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08006', 2, 'Purworejo', '1983-01-06', 1, 2, 'Kp Rawa Bugel No. 91 002 003 Marga Mulya Bekasi Utara Bekasi', '', 'Kp Rawa Bugel No. 91 002 003 Marga Mulya Bekasi Utara Bekasi', '', '', '087884243244', '(021) 29681', 'acik.amaliyah@lpsk.go.id', 'acik.nice@gmail.com', '', NULL, '3306014601830003', '822195558531000', '002', 'Ciracas', '0297715088', 'Acik Amaliyah ', '080064.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (136, '0', '080012', 'Fany Ratih Prawiasari, A.Md.', 'Fany Ratih Prawiasari ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08001', 2, 'Jakarta ', '1988-02-22', 1, 2, 'Telaga Mas Blok K7 No. 1 001 013 Harapan Baru Bekasi Utara Bekasi', '', 'Telaga Mas Blok K7 No. 1 001 013 Harapan Baru Bekasi Utara Bekasi', '', '', '', '(021) 29681', '', 'fany.paneh@gmail.com', '', NULL, '3275036202880009', '341356947407000', '002', 'Ciracas', '0192074222', 'Fany Ratih Prawiasari ', '080012.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (137, '0', '080054', 'Suciasih Retno Kartika, A.Md.', 'Suciasih Retno Kartika ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08005', 2, 'Jakarta ', '1987-05-19', 1, 2, 'Jl. Dukuh V No. 10 008 003 Dukuh Kramat Jati Jakarta Timur', '', 'Jl. Dukuh V No. 10 008 003 Dukuh Kramat Jati Jakarta Timur', '', '', '', '(021) 29681', 'suciasih.nretno@lpsk.go.id', '', '', NULL, '3275035905870017', '670799212407000', '002', 'Ciracas', '0297286818', 'Suciasih Retno Kartika ', '080054.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (138, '0', '080063', 'Ndaru Utomo Kusumo, S.H.', 'Ndaru Utomo Kusumo ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08006', 1, 'Serang', '1984-02-26', 1, 2, 'Jl. P Sumatera 06 No. 114 007 014 Aren Jaya Bekasi Timur Kota Bekasi', '', 'Jl. P Sumatera 06 No. 114 007 014 Aren Jaya Bekasi Timur Kota Bekasi', '', '', '087871071003', '(021) 29681', '', 'ndaru.utomo02@gmail.com', '', NULL, '3672022602840003', '859843856407000', '002', 'Ciracas', '0295417709', 'Ndaru Utomo Kusumo ', '080063.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (139, '0', '080090', 'Adji Muhammad Dana Kusuma, S.H.', 'Adji Muhammad Dana Kusuma ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08009', 1, 'Jakarta', '1983-12-16', 1, 1, 'Jl. Cimahi No. 14 005 004 Menteng Menteng Jakarta Pusat', '', 'Jl. Cimahi No. 14 005 004 Menteng Menteng Jakarta Pusat', '', '', '085246772666', '(021) 29681', 'adji.m.dana@lpsk.go.id', 'adji.dana@rocketmail.com', '', NULL, '6408060612830009', '360434518728000', '002', 'Ciracas', '0295460222', 'Adji Muhammad Dana Kusuma ', '080090.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (140, '0', '080086', 'Cici Amirah, S.H.', 'Cici Amirah ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08008', 2, 'Ujung Pandang', '1986-05-30', 1, 1, 'Jl. Adhyaksa IV/D.111 005 005 Lebak Bulus Cilandak Jakarta Selatan', '', 'Jl. Adhyaksa IV/D.111 005 005 Lebak Bulus Cilandak Jakarta Selatan', '', '', '085242827434', '(021) 29681', '', 'cicimirah@gmail.com', '', NULL, '7371137005860007', '833541345016000', '002', 'Ciracas', '0243986685', 'Cici Amirah ', '080086.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (141, '0', '080238', 'Andi Rully Asgap, S.H.', 'Andi Rully Asgap ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08023', 1, 'Mare', '1988-12-10', 1, 2, 'Jl. Dukuh Timur No. 2 003 015 Samper Barat Cilincing Jakarta Utara', '', 'Jl. Dukuh Timur No. 2 003 015 Samper Barat Cilincing Jakarta Utara', '', '', '081380361789', '(021) 29681', 'andi.rully@lpsk.go.id', 'andirullyasgap@gmail.com', '', NULL, '3172041012860014', '096638960045000', '002', 'Ciracas', '0285132453', 'Andi Rully Asgap ', '080238.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (142, '0', '080027', 'Salahudin', 'Salahudin', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08002', 1, 'Jakarta', '1975-05-17', 1, 2, 'Prumpung Tengah No. 36 011 006 Cipinang Besar Utara Jatinegara Jakarta Timur', '', 'Prumpung Tengah No. 36 011 006 Cipinang Besar Utara Jatinegara Jakarta Timur', '', '', '081387364638', '(021) 29681', 'salahudin@lpsk.g.id', '', '', NULL, '3175031705750005', '300643541071000', '002', 'Ciracas', '0297977463', 'Salahudin', '080027.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (143, '0', '080305', 'Siti Mu\'minah, S.Pd', 'Siti Mu\'minah', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '#N/A', 1, 'Tangerang', '1991-08-18', 1, 1, 'KP. Cibelut 003 001 Cibogo Cisauk Tangerang', '', 'KP. Cibelut 003 001 Cibogo Cisauk Tangerang', '', '', '082297397113', '(021) 29681', '', 'sitimuminah16@gmail.com', '', NULL, '3603235808910005', '358090835451000', '002', 'Ciracas', '0507836799', 'Siti Mu\'minah', '080305.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (144, '0', '080008', 'Gunawan Artho Nugoho  S.Psi', 'Gunawan Artho Nugoho  ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08000', 1, 'Jakarta', '1984-12-16', 1, 2, 'Komp. Pomad Jl. Kalibata Tengah XII No. 24 016 006 Kalibata Pancoran Jakarta Selatan', '', 'Komp. Pomad Jl. Kalibata Tengah XII No. 24 016 006 Kalibata Pancoran Jakarta Selatan', '', '', '081212129246', '(021) 29681', '', 'qunlpskri@gmail.com', '', NULL, '3174081612840002', '778797423061000', '002', 'Ciracas', '0210284200', 'Gunawan Artho Nugoho  ', '080008.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (145, '0', '080340', 'Angga Sukma Pratama  S.H', 'Angga Sukma Pratama ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08034', 1, 'Bandung', '1987-12-19', 1, 1, 'Jl. Sekelola No. 107/152 C 002 005 Sekelola Coblong Bandung', '', 'Jl. Sekelola No. 107/152 C 002 005 Sekelola Coblong Bandung', '', '', '081220662410', '(021) 29681', 'angga.sukma@lpsk.go.id', 'pratamaanggasukma@gmail.com', '', NULL, '3273021912870001', '846238269009000', '002', 'Ciracas', '0706387702', 'Angga Sukma Pratama ', '080340.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (146, '0', '080018', 'Pascalis Risdiana FP S.E', 'Pascalis Risdiana FP ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08001', 1, 'Yogyaka', '1980-04-08', 1, 2, 'Jl.H Tholib Mazis No.16 004 007 Bintaro Pesanggrahan Jakarta Selatan', '', 'Jl.H Tholib Mazis No.16 004 007 Bintaro Pesanggrahan Jakarta Selatan', '', '', '081299006467', '(021) 29681', '', 'pascalisprasetya@gmail.com', '', NULL, '3174070804800004', '778797431012000', '002', 'Ciracas', '0464754212', 'Pascalis Risdiana FP ', '080018.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (147, '0', '080129', 'Tirta Kartika Rianto, S.H.', 'Tirta Kartika Rianto ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08012', 2, 'Jambi', '1992-09-19', 1, 2, 'Jl. Evakuasi Gg. Al Karomah 002 002 Karyamulya Kesambi Cirebon', '', 'Jl. Evakuasi Gg. Al Karomah 002 002 Karyamulya Kesambi Cirebon', '', '', '081222112923', '(021) 29681', '', 'tirtakartikarianto@yahoo.co.id', '', NULL, '3274055909920010', '751713280426000', '002', 'Ciracas', '0462964458', 'Tirta Kartika Rianto ', '080129.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (148, '0', '080157', 'Wiyatiningsih S.Sos', 'Wiyatiningsih ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08015', 2, 'Semarang', '1955-03-13', 1, 2, 'Jati Padang 001 009 Jati Padang Pasar Minggu Jakarta Selatan', '', 'Jati Padang 001 009 Jati Padang Pasar Minggu Jakarta Selatan', '', '', '081381097931', '(021) 29681', '', 'yatisukardi55@gmail.com', '', NULL, '3174045303550002', '472527712017000', '002', 'Ciracas', '0004959916', 'Wiyatiningsih ', '080157.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (149, '0', '080183', 'Mayselina Nuriza, S.Psi', 'Mayselina Nuriza ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08018', 2, 'Jakarta', '1990-05-22', 1, 2, 'Jl. DN Mahalona E II / 72 018 004 Bendungan Hilir Tanah Abang Jakarta Pusat', '', 'Jl. DN Mahalona E II / 72 018 004 Bendungan Hilir Tanah Abang Jakarta Pusat', '', '', '081296292655', '(021) 29681', 'mayselina.nuriza@lpsk.go.id', '', '', NULL, '3603236205900001', '812396489452000', '002', 'Ciracas', '0297725143', 'Mayselina Nuriza ', '080183.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (150, '0', '080070', 'M Tommy Permana, S.Sos', 'M Tommy Permana ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08007', 1, 'Jakarta', '1986-05-02', 1, 1, 'Jl. Balita 6 No. 1 KMP 003 004 Kunciran Indah Pinang Tangerang', '', 'Jl. Balita 6 No. 1 KMP 003 004 Kunciran Indah Pinang Tangerang', '', '', '081219577706', '(021) 29681', '', 'mtommypermana@gmail.com', '', NULL, '3671110205860001', '90790890031000', '002', 'Ciracas', '0465131036', 'M Tommy Permana ', '080070.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (151, '0', '080207', 'Pikri Zulpikar HQ, S.H.', 'Pikri Zulpikar HQ ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08020', 1, 'Bogor', '1989-07-09', 1, 2, 'Kp. Sawah 001 003 Lengkong Kulon Pagedangan Kab. Tangerang', '', 'Kp. Sawah 001 003 Lengkong Kulon Pagedangan Kab. Tangerang', '', '', '087878400917', '(021) 29681', '', 'dikarha@gmail.com', '', NULL, '3603220907890006', '724129259451000', '002', 'Ciracas', '0796930824', 'Pikri Zulpikar HQ ', '080207.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (152, '0', '080302', 'Hendri Aditya  S.E', 'Hendri Aditya ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08030', 1, 'Jakarta', '1982-09-14', 1, 2, 'Komp. RRI Cimanggis 002 001 Cisalak Sukmajaya Depok', '', 'Komp. RRI Cimanggis 002 001 Cisalak Sukmajaya Depok', '', '', '087777243678', '(021) 29681', '', 'hendryadhitya@gmail.com', '', NULL, '3671131409820005', '246628036416000', '002', 'Ciracas', '0466543016', 'Hendri Aditya ', '080302.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (153, '0', '080014', 'Sulung Gogor Samodro, S.Sos', 'Sulung Gogor Samodro ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08001', 1, 'Jakarta', '1985-03-09', 1, 2, 'Jl. Kebagusan IV No. 5 006 005 Kebagusan Pasar Minggu Jakarta Selatan', '', 'Jl. Kebagusan IV No. 5 006 005 Kebagusan Pasar Minggu Jakarta Selatan', '', '', '081318459345', '(021) 29681', '', 'sulunggogor@gmail.com', '', NULL, '3174040903850012', '778797399017000', '002', 'Ciracas', '0254478919', 'Sulung Gogor Samodro ', '080014.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (154, '0', '080047', 'Citra Wulandari, S.E', 'Citra Wulandari ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08004', 2, 'Jakarta', '1987-06-05', 1, 1, 'Jl. Komp. Sekneg Blok B. VI/20 004 003 Panunggangan Utara Pinang Tangerang', '', 'Jl. Komp. Sekneg Blok B. VI/20 004 003 Panunggangan Utara Pinang Tangerang', '', '', '081289026701', '(021) 29681', 'citra.wulandari@lpsk.go.id', '', '', NULL, '3671114506870010', '447242793416000', '002', 'Ciracas', '0295387273', 'Citra Wulandari ', '080047.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (155, '0', '080216', 'Rizki Sammaulidah, A.Md', 'Rizki Sammaulidah ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08021', 2, '', '1900-01-00', 0, 2, '     ', '', '     ', '', '', '', '(021) 29681', '', '', '', NULL, '', '', '002', 'Ciracas', '0331990002', 'Rizki Sammaulidah ', '080216.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (156, '0', '080339', 'Sofie Emilia Mustika, S.Psi', 'Sofie Emilia Mustika ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08033', 2, 'Mataram', '1991-07-22', 1, 2, 'Jl. Cilinaya Indah No. 56 007 193 Kekalik Jaya Sekarbela Mataram', '', 'Jl. Cilinaya Indah No. 56 007 193 Kekalik Jaya Sekarbela Mataram', '', '', '082297473676', '(021) 29681', '', 'sofieemiliamustika1991@gmail.com', '', NULL, '5271046207910001', '762207009911000', '002', 'Ciracas', '0706458630', 'Sofie Emilia Mustika ', '080339.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (157, '0', '080208', 'Yulisa Maharani,  S.H.,M.H.', 'Yulisa Maharani ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08020', 2, 'Makassar', '0000-00-00', 1, 1, 'Komp IDI Blok C2 003 006 Tello Baru Panakkukang Makassar', '', 'Komp IDI Blok C2 003 006 Tello Baru Panakkukang Makassar', '', '', '085242525571', '(021) 29681', '', 'maharaniyulisa@yahoo.co.id', '', NULL, '7371116307880008', '764259461009000', '002', 'Ciracas', '0331999935', 'Yulisa Maharani ', '080208.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (158, '0', '080067', 'Rianto Wicaksono, S.H.', 'Rianto Wicaksono ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08006', 1, 'Jakarta', '1984-04-25', 1, 2, 'Jl.Kramat Batu No.11 002 005 Gandaria Selatan Cilandak  Jakarta Selatan', '', 'Jl.Kramat Batu No.11 002 005 Gandaria Selatan Cilandak  Jakarta Selatan', '', '', '', '(021) 29681', '', '', '', NULL, '', '', '002', 'Ciracas', '0295386825', 'Rianto Wicaksono ', '080067.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (159, '0', '080240', 'Anisa Roda Diana S.H.', 'Anisa Roda Diana ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08024', 2, 'Jakarta', '1990-08-29', 1, 2, 'Jl. Dr. Rajiman No. 14 003 004 Kauman Pasar Kliwon Surakarta', '', 'Jl. Dr. Rajiman No. 14 003 004 Kauman Pasar Kliwon Surakarta', '', '', '082111982596', '(021) 29681', 'anisa.roshda@lpsk.go.id', 'anisaroshdiana@gmail.com', '', NULL, '3372036908900002', '712771518526000', '002', 'Ciracas', '0323902297', 'Anisa Roda Diana ', '080240.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (160, '0', '080016', 'Robi Saripudin, S.H', 'Robi Saripudin ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08001', 2, 'Ciamis', '1982-09-14', 1, 2, 'Permata Balaraja Blok A. 11 C/C 007 001 Saga Balaraja Tangerang', '', 'Permata Balaraja Blok A. 11 C/C 007 001 Saga Balaraja Tangerang', '', '', '081297955049', '(021) 29681', 'robi.saripudin@lpsk.co.id', '', '', NULL, '3279041409920004', '768229460442000', '002', 'Ciracas', '0254398136', 'Robi Saripudin ', '080016.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (161, '0', '080272', 'Ergard Putra Geerards, S.T', 'Ergard Putra Geerards', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08027', 1, 'Ambon', '1978-06-05', 1, 1, 'Perum Pinus Asri D-6 RT. 007 RW. 005 Kel. Lindah Wetan Kec. Lakar Santri', '', 'Perum Pinus Asri D-6 RT. 007 RW. 005 Kel. Lindah Wetan Kec. Lakar Santri', '', '', '08121922008', '(021) 29681', '', 'ergard.geerards@gmail.com', '', NULL, '3578180506780002', '085666683604000', '002', 'Ciracas', '0461480151', 'Ergard Putra Geerards', '080272.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (162, '0', '080130', 'Abdul Latief', 'Abdul Latief', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08013', 1, 'Bogor ', '1984-06-01', 1, 2, 'Kp. Bojong Gede RT 004 RW 013 Kel. Bojong Gede Kec. Bojong Gede, Bogor Jawa Barat', '', 'Kp. Bojong Gede RT 004 RW 013 Kel. Bojong Gede Kec. Bojong Gede, Bogor Jawa Barat', '', '', '0895347653384', '(021) 29681', '', '', '', NULL, '3201130106840010', '982580706403000', '002', 'Ciracas', '0295252519', 'Abdul Latief', '080130.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (163, '0', '080295', 'Adi Purwanto', 'Adi Purwanto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08029', 1, 'Yogyakarta ', '1977-04-09', 1, 2, 'Jl. Kampung Jembatan RT01 RW 0010 Cipinang Besar Selatan Jatinegara Jakarta DKI Jakarta', '', 'Jl. Kampung Jembatan RT01 RW 0010 Cipinang Besar Selatan Jatinegara Jakarta DKI Jakarta', '', '', '082299613306', '(021) 29681', '', '', '', NULL, '3175030904771001', '800813024002000', '002', 'Ciracas', '0461477579', 'Adi Purwanto', '080295.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (164, '0', '080281', 'Adi Rusyadi', 'Adi Rusyadi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Jakarta ', '1980-12-05', 1, 2, 'Jl. Puri Bukit Depok RT 003 RW 010 Sasak Panjang Tajur Halang Bogor Jawa Barat', '', 'Jl. Puri Bukit Depok RT 003 RW 010 Sasak Panjang Tajur Halang Bogor Jawa Barat', '', '', '081290043442', '(021) 29681', '', '', '', NULL, '3201370512800005', '574627360403000', '002', 'Ciracas', '0465143814', 'Adi Rusyadi', '080281.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (165, '0', '080131', 'Agus Sutisna', 'Agus Sutisna', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08013', 1, 'Jakarta ', '1982-08-11', 1, 2, 'Lontar I RT 007 RW 004 Tugu Utara Koja Jakarta UtaraDKI Jakarta', '', 'Lontar I RT 007 RW 004 Tugu Utara Koja Jakarta UtaraDKI Jakarta', '', '', '083898999567', '(021) 29681', '', '', '', NULL, '3172031108820009', '982580599045000', '002', 'Ciracas', '0295253680', 'Agus Sutisna', '080131.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (166, '0', '080146', 'Ahmat Safei', 'Ahmat Safei', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08014', 1, 'Jakarta ', '1980-09-09', 1, 2, 'Jl. Industri 3 Dalam RT 010 RW 014 Pademangan Barat Pademanagan Jakarta Utara DKI Jakarta', '', 'Jl. Industri 3 Dalam RT 010 RW 014 Pademangan Barat Pademanagan Jakarta Utara DKI Jakarta', '', '', '083116450156', '(021) 29681', '', '', '', NULL, '317205090980014', '982580755044000', '002', 'Ciracas', '0295359358', 'Ahmat Safei', '080146.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (167, '0', '080141', 'Alfian', 'Alfian', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08014', 1, 'Jakarta ', '1975-01-01', 1, 2, 'Jl. Menteng Wadas Urata RT 010 RW 011Pasar Manggis Setia Budi Jakarta Selatan DKI Jakarta', '', 'Jl. Menteng Wadas Urata RT 010 RW 011Pasar Manggis Setia Budi Jakarta Selatan DKI Jakarta', '', '', '082124848281', '(021) 29681', '', '', '', NULL, '3174020101750013', '982580722018000', '002', 'Ciracas', '0295183111', 'Alfian', '080141.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (168, '0', '080282', 'Apendi', 'Apendi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Sumedang ', '1963-06-11', 1, 2, 'Dusun Margalaksana RT 003 RW 001 Margalaksana Sumedang Selatan Sumedang Jawa Barat', '', 'Dusun Margalaksana RT 003 RW 001 Margalaksana Sumedang Selatan Sumedang Jawa Barat', '', '', '087883694160', '(021) 29681', '', '', '', NULL, '3211171106630004', '891003386412000', '002', 'Ciracas', '0462967164', 'Apendi', '080282.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (169, '0', '080203', 'Apriliyanto', 'Apriliyanto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08020', 1, 'Jakarta', '1983-04-21', 1, 2, 'H. Jusin RT 002 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', 'H. Jusin RT 002 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', '', '081510229121', '(021) 29681', '', '', '', NULL, '3175092104830005', '495149049009000', '002', 'Ciracas', '0332316064', 'Apriliyanto', '080203.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (170, '0', '080133', 'Arthur P Taihutu', 'Arthur P Taihutu', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08013', 1, 'Ambon ', '1976-06-16', 1, 2, 'Jl. Kamboja No. 12 RT 001 RW 007 Depok Pancoran Mas Depok Jawa Barat', '', 'Jl. Kamboja No. 12 RT 001 RW 007 Depok Pancoran Mas Depok Jawa Barat', '', '', '082119463117', '(021) 29681', '', '', '', NULL, '3276011606760020', '982580649412000', '002', 'Ciracas', '0295361481', 'Arthur P Taihutu', '080133.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (171, '0', '080296', 'Dian Ratna Sari', 'Dian Ratna Sari', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08029', 2, 'Jakarta ', '1983-09-26', 1, 2, 'Kebon Nanas Selatan RT 010 RW 002 Cipinang Cempedak Jatinegara Jakarta Timur DKI Jakarta', '', 'Kebon Nanas Selatan RT 010 RW 002 Cipinang Cempedak Jatinegara Jakarta Timur DKI Jakarta', '', '', '085714770139', '(021) 29681', '', '', '', NULL, '3175036609830007', '457767473002000', '002', 'Ciracas', '0461477295', 'Dian Ratna Sari', '080296.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (172, '0', '080201', 'Edi Susanto', 'Edi Susanto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08020', 1, 'Tanggerang', '1970-09-07', 1, 2, 'Jl. Suplier RT 002 RW 004 Cipayung Cipayung Jakarta Timur DKI Jakarta', '', 'Jl. Suplier RT 002 RW 004 Cipayung Cipayung Jakarta Timur DKI Jakarta', '', '', '085772711459', '(021) 29681', '', '', '', NULL, '3175100709700003', '694802406009000', '002', 'Ciracas', '0332315276', 'Edi Susanto', '080201.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (173, '0', '080280', 'Elwen Ebenhard T', 'Elwen Ebenhard T.', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Jakarta ', '1995-07-09', 2, 2, 'Kp. Bojong Jati RT 003 RW 016 Depok Pancoran Mas Depok Jawa Barat', '', 'Kp. Bojong Jati RT 003 RW 016 Depok Pancoran Mas Depok Jawa Barat', '', '', '082299860406', '(021) 29681', '', '', '', NULL, '3276010907950007', '765319538448000', '002', 'Ciracas', '0464151757', 'Elwen Ebenhard T.', '080280.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (174, '0', '080246', 'Fatty Hillah', 'Fatty Hillah', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08024', 1, 'Jakarta ', '1976-04-30', 1, 2, 'Kp. Kramat RT 001 RW 016 Cililitan Kramat Jati Jakata Timur DKI Jakarta', '', 'Kp. Kramat RT 001 RW 016 Cililitan Kramat Jati Jakata Timur DKI Jakarta', '', '', '087886951411', '(021) 29681', '', '', '', NULL, '3175043004760003', '897971115005000', '002', 'Ciracas', '0355617587', 'Fatty Hillah', '080246.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (175, '0', '080188', 'Hendri Sutopo', 'Hendri Sutopo', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08018', 1, 'Magetan ', '1980-09-29', 1, 2, 'Tanah Kusir 2 RT 010 RW 009 Kebayoran Lama Selatan Kebayoran Lama Jakarta Selatan DKI Jakarta', '', 'Tanah Kusir 2 RT 010 RW 009 Kebayoran Lama Selatan Kebayoran Lama Jakarta Selatan DKI Jakarta', '', '', '081806723486', '(021) 29681', '', '', '', NULL, '3174052909800013', '573170172013000', '002', 'Ciracas', '0466543016', 'Hendri Sutopo', '080188.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (176, '0', '080143', 'Heri Sanjaya', 'Heri Sanjaya', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08014', 1, 'Jakarta ', '1979-02-15', 1, 2, 'Blok Duku RT 002 RW 010 Cibubur Ciracas Jakarta Timur DKI Jakarta', '', 'Blok Duku RT 002 RW 010 Cibubur Ciracas Jakarta Timur DKI Jakarta', '', '', '081288989079', '(021) 29681', '', '', '', NULL, '3175091502780007', '778391466009000', '002', 'Ciracas', '0295390955', 'Heri Sanjaya', '080143.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (177, '0', '080124', 'Idup Abdul Rohman', 'Idup Abdul Rohman', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08012', 1, 'Bogor ', '1984-02-01', 1, 2, 'Ciomas I RT 001 RW 002 Ciomas Rahayu Ciomas Bogor DKI Jakarta', '', 'Ciomas I RT 001 RW 002 Ciomas Rahayu Ciomas Bogor DKI Jakarta', '', '', '081314110733', '(021) 29681', '', '', '', NULL, '3201290102840004', '982580730434000', '002', 'Ciracas', '0295365022', 'Idup Abdul Rohman', '080124.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (178, '0', '080125', 'Iwan', 'Iwan', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08012', 1, 'Jakarta ', '1972-12-24', 1, 2, 'Kp. Bojong Indah RT 011 RW 003 Bojong Indah Parung Bogor Jawa Barat', '', 'Kp. Bojong Indah RT 011 RW 003 Bojong Indah Parung Bogor Jawa Barat', '', '', '087874136019', '(021) 29681', '', '', '', NULL, '3201102412720003', '982580672028000', '002', 'Ciracas', '0295179557', 'Iwan', '080125.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (179, '0', '080247', 'M Hamzah ', 'M Hamzah ', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08024', 1, 'Jakarta ', '1966-05-05', 1, 2, 'Pengadegan Timur RT 010 RW 001 Pengadegan Pancoran Jakarta Selatan DKI Jakarta', '', 'Pengadegan Timur RT 010 RW 001 Pengadegan Pancoran Jakarta Selatan DKI Jakarta', '', '', '081780342135', '(021) 29681', '', '', '', NULL, '3174080505660001', '768685224061000', '002', 'Ciracas', '0355614654', 'M Hamzah ', '080247.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (180, '0', '080294', 'Nurhansyah', 'Nurhansyah', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08029', 1, 'Jakarta ', '1980-06-27', 1, 2, 'Jl. H. Jusin RT 002 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', 'Jl. H. Jusin RT 002 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', '', '089696641066', '(021) 29681', '', '', '', NULL, '3175092706800006', '590511622009000', '002', 'Ciracas', '0464130599', 'Nurhansyah', '080294.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (181, '0', '080189', 'Ovi Suhatman', 'Ovi Suhatman', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08018', 1, 'Jakarta ', '1977-10-25', 1, 2, 'Petojo Selatan III RT 010 RW 005 Petojo Utara Gambir Jakarta DKI Jakarta', '', 'Petojo Selatan III RT 010 RW 005 Petojo Utara Gambir Jakarta DKI Jakarta', '', '', '083890407357', '(021) 29681', '', '', '', NULL, '3171012510770006', '690026729029000', '002', 'Ciracas', '0330733985', 'Ovi Suhatman', '080189.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (182, '0', '080138', 'Ratmono', 'Ratmono', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08013', 1, 'Jakarta ', '1978-05-17', 1, 2, 'Petojo Selatan IV RT011 RW 005 Petojo Utara Gambir Jakarta Pusat DKI Jakarta', '', 'Petojo Selatan IV RT011 RW 005 Petojo Utara Gambir Jakarta Pusat DKI Jakarta', '', '', '081287817174', '(021) 29681', '', '', '', NULL, '3171011705780002', '982580631029000', '002', 'Ciracas', '0295363057', 'Ratmono', '080138.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (183, '0', '080135', 'Rianto', 'Rianto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08013', 1, 'Jakarta ', '1971-07-31', 1, 2, 'Jl. Palbatu I No. 21 RT 007 RW004 Menteng Dalam Tebet Jakarta Selatan DKI Jakarta', '', 'Jl. Palbatu I No. 21 RT 007 RW004 Menteng Dalam Tebet Jakarta Selatan DKI Jakarta', '', '', '085288346488', '(021) 29681', '', '', '', NULL, '3174013107710012', '982580698015000', '002', 'Ciracas', '0295386825', 'Rianto', '080135.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (184, '0', '080134', 'Rio Gatot Sulistiawan', 'Rio Gatot Sulistiawan', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08013', 1, 'Jakarta ', '1979-10-13', 1, 2, 'Jl. Asrama Polri Kemayoran RT009 RW 009 Gunung Sahari Selatan Kemayoran Jakarta Pusat DKI Jakarta', '', 'Jl. Asrama Polri Kemayoran RT009 RW 009 Gunung Sahari Selatan Kemayoran Jakarta Pusat DKI Jakarta', '', '', '085930335369', '(021) 29681', '', '', '', NULL, '3171031310790003', '982580573027000', '002', 'Ciracas', '0299014091', 'Rio Gatot Sulistiawan', '080134.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (185, '0', '080137', 'Sukardi', 'Sukardi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08013', 1, 'Jakarta ', '1973-01-03', 1, 2, 'Jl. Mawar 60 RW 10 RW006 Tugu Utara Koja Jakarta Utara DKI Jakarta', '', 'Jl. Mawar 60 RW 10 RW006 Tugu Utara Koja Jakarta Utara DKI Jakarta', '', '', '085711082932', '(021) 29681', '', '', '', NULL, '3172030301730013', '982580714045000', '002', 'Ciracas', '0716446996', 'Sukardi', '080137.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (186, '0', '080202', 'Sulisman', 'Sulisman', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08020', 1, 'Cilacap ', '1974-05-25', 1, 2, 'Jl. Cempaka RT 003 RW 003 Tritih LorJeruk Legi Cilacap Cilacap', '', 'Jl. Cempaka RT 003 RW 003 Tritih LorJeruk Legi Cilacap Cilacap', '', '', '081385380139', '(021) 29681', '', '', '', NULL, '3301082505740006', '800449530009000', '002', 'Ciracas', '0214066877', 'Sulisman', '080202.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (187, '0', '080128', 'Syamsul Anwar', 'Syamsul Anwar', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08012', 1, 'Jakarta ', '1969-02-07', 1, 2, 'Kp. Pulo RT004 RW 005 Bojong Baru Bojong Gede Bogor Jawa Barat', '', 'Kp. Pulo RT004 RW 005 Bojong Baru Bojong Gede Bogor Jawa Barat', '', '', '0895804107679', '(021) 29681', '', '', '', NULL, '3201130702690002', '982580623403000', '002', 'Ciracas', '0295372710', 'Syamsul Anwar', '080128.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (188, '0', '080293', 'Totom Sukanda', 'Totom Sukanda', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08029', 1, 'Cianjur ', '1983-03-12', 1, 2, 'Kp. Temanggungan RT003 RW 003 Taman sari Rumpin Bogor Jawa Barat', '', 'Kp. Temanggungan RT003 RW 003 Taman sari Rumpin Bogor Jawa Barat', '', '', '085217602865', '(021) 29681', '', '', '', NULL, '3201181203830011', '765702527434000', '002', 'Ciracas', '0461477829', 'Totom Sukanda', '080293.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (189, '0', '080190', 'Tony Nurhadi', 'Tony Nurhadi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08019', 1, 'Surabaya ', '1975-06-16', 1, 2, 'H. Nadih Sidamukti No. 23 RT 003 RW004 Sukamaju Cilodong Depok Jawa Barat', '', 'H. Nadih Sidamukti No. 23 RT 003 RW004 Sukamaju Cilodong Depok Jawa Barat', '', '', '081291272791', '(021) 29681', '', '', '', NULL, '3276051606750014', '775837313017000', '002', 'Ciracas', '0329720222', 'Tony Nurhadi', '080190.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (190, '0', '080115', 'Abdul Rafiq', 'Abdul Rafiq', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08011', 1, 'Palembang ', '1978-12-27', 1, 2, 'Kampung Kelapa RT 004 RW 011 Cikokol Tangerang Tangerang Banten', '', 'Kampung Kelapa RT 004 RW 011 Cikokol Tangerang Tangerang Banten', '', '', '0878877904786', '(021) 29681', '', '', '', NULL, '3671012712780007', '982580771416000', '002', 'Ciracas', '0429747229', 'Abdul Rafiq', '080115.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (191, '0', '080162', 'Amir Fatah', 'Amir Fatah', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08016', 1, 'Brebes ', '1986-01-03', 1, 2, 'Dk Mingguhan RT 013 RW 001 Winduaji Paguyangan Brebes Jawa Tengah', '', 'Dk Mingguhan RT 013 RW 001 Winduaji Paguyangan Brebes Jawa Tengah', '', '', '', '(021) 29681', '', '', '', NULL, '3329040301860006', '80084330200900', '002', 'Ciracas', '0297809737', 'Amir Fatah', '080162.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (192, '0', '080116', 'Amir Fauzi', 'Amir Fauzi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08011', 1, 'Cilacap ', '1969-08-17', 1, 2, 'Jl Warung Dukuh Rt 003 RW 005 Pabuaran Cibinong Bogor Jawa Barat', '', 'Jl Warung Dukuh Rt 003 RW 005 Pabuaran Cibinong Bogor Jawa Barat', '', '', '', '(021) 29681', '', '', '', NULL, '3201011708690033', '986100667403000', '002', 'Ciracas', '0465128738', 'Amir Fauzi', '080116.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (193, '0', '080032', 'Andi Cahyadi', 'Andi Cahyadi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08003', 1, 'Jakarta ', '1984-08-14', 1, 2, 'Jl Madrasah RT 003 RW 003 Pondok Benda Pamulang Tangerang Banten', '', 'Jl Madrasah RT 003 RW 003 Pondok Benda Pamulang Tangerang Banten', '', '', '', '(021) 29681', '', '', '', NULL, '3674061408840004', '769295163453000', '002', 'Ciracas', '0295182913', 'Andi Cahyadi', '080032.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (194, '0', '080111', 'Anen', 'Anen', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08011', 1, 'Bogor ', '1954-07-15', 1, 2, 'Jl Kukupu RT 003 RW 008 Cibadak Tanah Sareal BogorJawa Barat', '', 'Jl Kukupu RT 003 RW 008 Cibadak Tanah Sareal BogorJawa Barat', '', '', '', '(021) 29681', '', '', '', NULL, '3174041507540001', '982580797017000', '002', 'Ciracas', '0295371716', 'Anen', '080111.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (195, '0', '080284', 'Danny A. Sulaksana', 'Danny A. Sulaksana', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Purwakarta ', '1969-08-29', 1, 2, 'Jl Pagelamang RT 008 RW 003 Lubang Buaya Cipayung Jakarta Timur DKI Jakarta', '', 'Jl Pagelamang RT 008 RW 003 Lubang Buaya Cipayung Jakarta Timur DKI Jakarta', '', '', '', '(021) 29681', '', '', '', NULL, '3175102908690001', '811320845009000', '002', 'Ciracas', '0464163229', 'Danny A. Sulaksana', '080284.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (196, '0', '080232', 'Dwi Jatmiko', 'Dwi Jatmiko', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08023', 1, 'Jakarta ', '1984-01-04', 1, 2, 'Jl.Ir. H. Juanda Gg Karyawan RT 002 RW 001 Ciputat Ciputat Tangerang Banten', '', 'Jl.Ir. H. Juanda Gg Karyawan RT 002 RW 001 Ciputat Ciputat Tangerang Banten', '', '', '', '(021) 29681', '', '', '', NULL, '3674060101840020', '797083391411000', '002', 'Ciracas', '0237467704', 'Dwi Jatmiko', '080232.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (197, '0', '080120', 'Heru Pranoto', 'Heru Pranoto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08012', 1, 'Jakarta ', '1966-01-31', 1, 2, 'Jl. Lagoa TRS Gg III D/I RT 005 RW 003 Lagoa Koja Jakarta Utara DKI Jakarta', '', 'Jl. Lagoa TRS Gg III D/I RT 005 RW 003 Lagoa Koja Jakarta Utara DKI Jakarta', '', '', '085771090746', '(021) 29681', '', '', '', NULL, '3172033101660001', '', '002', 'Ciracas', '0297711979', 'Heru Pranoto', '080120.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (198, '0', '080220', 'Haerudin', 'Haerudin', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08022', 1, 'Jakarta', '1967-06-06', 1, 2, 'Jl. Maryadinata011015Tg. PriokTg. PriokJakrta UtaraDKI Jakarta', '', 'Jl. Maryadinata011015Tg. PriokTg. PriokJakrta UtaraDKI Jakarta', '', '', '081311125589', '(021) 29681', '', '', '', NULL, '3172020606670003', '576754543042000', '002', 'Ciracas', '0332316393', 'Haerudin', '080220.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (199, '0', '080265', 'Karyono', 'Karyono', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08026', 2, 'Purbalingga ', '1977-11-10', 1, 2, 'Jl.Kebon Kopi RT 001 RW 005 Pondok Betung Pindok Aren Tangerang Banten', '', 'Jl.Kebon Kopi RT 001 RW 005 Pondok Betung Pindok Aren Tangerang Banten', '', '', '085781387476', '(021) 29681', '', '', '', NULL, '3674031011770025', '984471748411000', '002', 'Ciracas', '0429743451', 'Karyono', '080265.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (200, '0', '080112', 'Manan Bin Mipan', 'Manan Bin Mipan', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08011', 1, 'Depok ', '1954-10-08', 1, 2, 'Jl. Moch-Hasyim RT 002 RW 005 Rangkapan Jaya Pancoran Mas Depok Jawa Barat', '', 'Jl. Moch-Hasyim RT 002 RW 005 Rangkapan Jaya Pancoran Mas Depok Jawa Barat', '', '', '087775373856', '(021) 29681', '', '', '', NULL, '3276010810540008', '982580813412000', '002', 'Ciracas', '0295385913', 'Manan Bin Mipan', '080112.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (201, '0', '080221', 'Mochamad Ichsan', 'Mochamad Ichsan', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08022', 1, 'Jakarta ', '1970-01-28', 1, 2, 'Jl. Kobber Kecil 00 6008 Rawa Bunga Jatinegara Jakarta Timur DKI Jakarta', '', 'Jl. Kobber Kecil 00 6008 Rawa Bunga Jatinegara Jakarta Timur DKI Jakarta', '', '', '081288143766', '(021) 29681', '', '', '', NULL, '3175032801700001', '694954349002000', '002', 'Ciracas', '0332454866', 'Mochamad Ichsan', '080221.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (202, '0', '080194', 'Muhamad Muharoji', 'Muhamad Muharoji', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08019', 1, 'Jakarta ', '1968-04-04', 1, 2, 'Jl. Perum Pesona Kahuripan RT 001 RW 018 Cikahuripan Klapanunggal Bogor Jawa Barat', '', 'Jl. Perum Pesona Kahuripan RT 001 RW 018 Cikahuripan Klapanunggal Bogor Jawa Barat', '', '', '081284227438', '(021) 29681', '', '', '', NULL, '3276050404680007', '664383734412000', '002', 'Ciracas', '0331260532', 'Muhamad Muharoji', '080194.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (203, '0', '080283', 'Rahmat Hidayat', 'Rahmat Hidayat', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Jakarta ', '1981-07-09', 1, 2, 'Jl. Perum Pesona Kahuripan RT 015 RW 008 Mustika Sari Mustika Jaya Bekasi Jawa Barat', '', 'Jl. Perum Pesona Kahuripan RT 015 RW 008 Mustika Sari Mustika Jaya Bekasi Jawa Barat', '', '', '087881500998', '(021) 29681', '', '', '', NULL, '3275110907810003', '877334201432000', '002', 'Ciracas', '0464159428', 'Rahmat Hidayat', '080283.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (204, '0', '080271', 'Rio Afrizal', 'Rio Afrizal', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08027', 1, 'Ulak Baru ', '1989-04-18', 1, 2, 'Jl. Rawa Maya Tengah RT002 RW 002 Beji Beji Depok Jawa Barat', '', 'Jl. Rawa Maya Tengah RT002 RW 002 Beji Beji Depok Jawa Barat', '', '', '082372451849', '(021) 29681', '', '', '', NULL, '1608101804890001', '801961814302000', '002', 'Ciracas', '0462966240', 'Rio Afrizal', '080271.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (205, '0', '080219', 'Rusdianto', 'Rusdianto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08021', 1, 'Jakarta ', '1976-02-08', 1, 2, 'Jl. Pesing Poglar RT 007 RW 005 Kedang Kopi Angke Cengkareng Jakarta DKI Jakarta', '', 'Jl. Pesing Poglar RT 007 RW 005 Kedang Kopi Angke Cengkareng Jakarta DKI Jakarta', '', '', '08159947887', '(021) 29681', '', '', '', NULL, '3173010802750006', '765528690034000', '002', 'Ciracas', '0328622487', 'Rusdianto', '080219.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (206, '0', '080113', 'Sopian', 'Sopian', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08011', 1, 'Palembang ', '1970-03-23', 1, 2, 'Jl. Artayasa Blok Tengki RT001 RW 010 Meruyung Limo Depok Jawa Barat', '', 'Jl. Artayasa Blok Tengki RT001 RW 010 Meruyung Limo Depok Jawa Barat', '', '', '081382170943', '(021) 29681', '', '', '', NULL, '3276042303700001', '982580805412000', '002', 'Ciracas', '0295389270', 'Sopian', '080113.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (207, '0', '080248', 'Tarsim Masruri', 'Tarsim Masruri', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08024', 1, 'Banyumas ', '1969-05-04', 1, 2, 'Jl. Lombok RT 006 RW 007 Pondok Cabe Ilir Pamulang Tangerang Banten', '', 'Jl. Lombok RT 006 RW 007 Pondok Cabe Ilir Pamulang Tangerang Banten', '', '', '085711076753', '(021) 29681', '', '', '', NULL, '3674060511960005', '762965085453000', '002', 'Ciracas', '0355709262', 'Tarsim Masruri', '080248.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (208, '0', '080119', 'Tita Jatnika', 'Tita Jatnika', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08011', 1, 'Bandung ', '1968-08-11', 1, 2, 'Jl Anyer RT003 RW 009 Menteng Menteng Jakarta Pusat DKI Jakarta', '', 'Jl Anyer RT003 RW 009 Menteng Menteng Jakarta Pusat DKI Jakarta', '', '', '081282488323', '(021) 29681', '', '', '', NULL, '3175091106680009', '982580789008000', '002', 'Ciracas', '0295182822', 'Tita Jatnika', '080119.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (209, '0', '080195', 'Waryanto', 'Waryanto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08019', 1, 'Cilacap ', '1985-03-28', 1, 2, 'Jl. Dusun 03 RT 004 RW 006 Kalimukti Pabedilan Cirebon Jawa Barat', '', 'Jl. Dusun 03 RT 004 RW 006 Kalimukti Pabedilan Cirebon Jawa Barat', '', '', '081398936570', '(021) 29681', '', '', '', NULL, '3209042803850004', '762636231426000', '002', 'Ciracas', '0447458532', 'Waryanto', '080195.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (210, '0', '080114', 'Yanuar Ariwahyudi', 'Yanuar Ariwahyudi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08011', 1, 'Jakarta ', '1976-01-16', 1, 2, 'Komp. SEKNEG Blok B. IV/8 RT 005 RW 003 Panunggangan Utara Pinang Tangerang Banten', '', 'Komp. SEKNEG Blok B. IV/8 RT 005 RW 003 Panunggangan Utara Pinang Tangerang Banten', '', '', '081214052305', '(021) 29681', '', '', '', NULL, '3671111601760001', '780454765403000', '002', 'Ciracas', '0295359030', 'Yanuar Ariwahyudi', '080114.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (211, '0', '080333', 'Arya Alai Permana', 'Arya Alai Permana', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08033', 1, 'Bengkulu ', '1985-02-02', 1, 2, 'Gang Golf Ling RT 005 RW 008 Ciriung Cibinong Bogor Jawa Barat', '', 'Gang Golf Ling RT 005 RW 008 Ciriung Cibinong Bogor Jawa Barat', '', '', '081297011616', '(021) 29681', '', '', '', NULL, '1601140202850008', '821082161009000', '002', 'Ciracas', '0569126798', 'Arya Alai Permana', '080333.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (212, '0', '080336', 'Uus Agustian', 'Uus Agustian', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08033', 1, 'Sukabumi ', '1972-08-16', 1, 2, 'Cikaret Hilir RT 004 RW 002 Sasagaran Kebon Pedes Sukabumi Jawa Barat', '', 'Cikaret Hilir RT 004 RW 002 Sasagaran Kebon Pedes Sukabumi Jawa Barat', '', '', '085862011344', '(021) 29681', '', '', '', NULL, '3202341608720006', '808630735405000', '002', 'Ciracas', '0599772167', 'Uus Agustian', '080336.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (213, '0', '080344', 'Achmad Zelani', 'Achmad Zelani', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08034', 1, 'Brebes ', '1973-04-05', 1, 2, 'Kelapa Dua Wetan RT 008 RW 008 Kelapa Dua Wetan CiracasJakarta Timur DKI Jakarta', '', 'Kelapa Dua Wetan RT 008 RW 008 Kelapa Dua Wetan CiracasJakarta Timur DKI Jakarta', '', '', '081211248562', '(021) 29681', '', '', '', NULL, '3175090504730002', '845185487009000', '002', 'Ciracas', '0706387713', 'Achmad Zelani', '080344.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (214, '0', '080345', 'Andi Rosyid', 'Andi Rosyid', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08034', 1, 'Jakarta ', '1975-03-30', 1, 2, 'Jl. Raya Patriot RT 001 RW 018 Jakasampurna Bekasi Utara Bekasi Jawa Barat', '', 'Jl. Raya Patriot RT 001 RW 018 Jakasampurna Bekasi Utara Bekasi Jawa Barat', '', '', '087880658550', '(021) 29681', '', '', '', NULL, '3275024309790015', '845589233427000', '002', 'Ciracas', '0706387699', 'Andi Rosyid', '080345.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (215, '0', '080361', 'Purnomo', 'Purnomo', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08036', 1, 'Tegal ', '1989-06-01', 1, 2, 'Jl. Bangka IX A RT 008 RW 010 Pela Mampang Mampang Prapatan Jakarta DKI Jakarta', '', 'Jl. Bangka IX A RT 008 RW 010 Pela Mampang Mampang Prapatan Jakarta DKI Jakarta', '', '', '081281724054', '(021) 29681', '', '', '', NULL, '3328061906890005', '767632722014000', '002', 'Ciracas', '0230116423', 'Purnomo', '080361.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (216, '0', '080290', 'Agung Tri Yuli Hantoro', 'Agung Tri Yuli Hantoro', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08029', 1, 'Pati ', '1985-07-07', 1, 2, 'Dusun Pasinggahan RT 002 RW 002 Sugih rejo Gabus Kabupaten Pati Jawa Tengah', '', 'Dusun Pasinggahan RT 002 RW 002 Sugih rejo Gabus Kabupaten Pati Jawa Tengah', '', '', '081912523899', '(021) 29681', '', '', '', NULL, '3318110707850004', '769517673507000', '002', 'Ciracas', '0461484779', 'Agung Tri Yuli Hantoro', '080290.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (217, '0', '080095', 'Amin Yamin', 'Amin Yamin', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08009', 1, 'Cianjur ', '1985-06-02', 1, 2, 'Jl. Rawamangun RT 010 RW 002 Rawasari Cempaka Putih Jakarta DKI Jakarta', '', 'Jl. Rawamangun RT 010 RW 002 Rawasari Cempaka Putih Jakarta DKI Jakarta', '', '', '087889905480', '(021) 29681', '', '', '', NULL, '3203101507870011', '812348886024000', '002', 'Ciracas', '0297911989', 'Amin Yamin', '080095.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (218, '0', '080276', 'Ari Sanjaya', 'Ari Sanjaya', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08027', 1, 'Oku Timur  ', '1993-10-17', 1, 2, 'Ulak Baru RT 003 RW 003 Ulak Baru Cempaka Oku Timur Sumatera Selatan', '', 'Ulak Baru RT 003 RW 003 Ulak Baru Cempaka Oku Timur Sumatera Selatan', '', '', '081211557537', '(021) 29681', '', '', '', NULL, '1808101710930002', '767455215302000', '002', 'Ciracas', '0461478222', 'Ari Sanjaya', '080276.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (219, '0', '080285', 'Arif Hidayat', 'Arif Hidayat', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Jakarta ', '1989-12-12', 1, 2, 'Jl. SMA PGRI RT 007 RW 002 Cipayung Cipayung Jakarta Timur DKI Jakarta', '', 'Jl. SMA PGRI RT 007 RW 002 Cipayung Cipayung Jakarta Timur DKI Jakarta', '', '', '081316534593', '(021) 29681', '', '', '', NULL, '3175101212890002', '768735441009000', '002', 'Ciracas', '', 'Arif Hidayat', '080285.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (220, '0', '080105', 'Aripin', 'Aripin', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08010', 1, 'Jakarta ', '1976-04-24', 1, 2, 'Kp. Bojonggede RT 004 RW 013 Bojonggede Bojong Gede Bogor Jawa Barat', '', 'Kp. Bojonggede RT 004 RW 013 Bojonggede Bojong Gede Bogor Jawa Barat', '', '', '081284169298', '(021) 29681', '', '', '', NULL, '3201132404760001', '780454765403000', '002', 'Ciracas', '0295355321', 'Aripin', '080105.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (221, '0', '080102', 'Della Isaura Karauwan', 'Della Isaura Karauwan', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08010', 2, 'Depok ', '1988-12-12', 1, 2, 'Pasir Putih RT 006 RW002 Pasir Putih Sawangan Kota Depok Jawa Barat', '', 'Pasir Putih RT 006 RW002 Pasir Putih Sawangan Kota Depok Jawa Barat', '', '', '085218087148', '(021) 29681', '', '', '', NULL, '3276035212880011', '982580904412000', '002', 'Ciracas', '0285597985', 'Della Isaura Karauwan', '080102.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (222, '0', '080291', 'Didi Sumadi', 'Didi Sumadi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08029', 1, 'Jakarta ', '1982-08-22', 1, 2, 'Jl. Swadaya IV RT 005 RW 006 Rawa Bunga Jatinegara Jakarta Timur DKI Jakarta', '', 'Jl. Swadaya IV RT 005 RW 006 Rawa Bunga Jatinegara Jakarta Timur DKI Jakarta', '', '', '085694538302', '(021) 29681', '', '', '', NULL, '3175032208820011', '800586547002000', '002', 'Ciracas', '0461484532', 'Didi Sumadi', '080291.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (223, '0', '080278', 'Dulloh Rafsanjani', 'Dulloh Rafsanjani', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08027', 1, 'Kuningan ', '1998-04-10', 1, 2, 'KP. Rawa Tengah RT 007 RW 004 Galur Johar Baru Jakarta Pusat DKI Jakarta', '', 'KP. Rawa Tengah RT 007 RW 004 Galur Johar Baru Jakarta Pusat DKI Jakarta', '', '', '082118981290', '(021) 29681', '', '', '', NULL, '3171081004980003', '763909074024000', '002', 'Ciracas', '0461484258', 'Dulloh Rafsanjani', '080278.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (224, '0', '080109', 'Dwi Purwo Seputro', 'Dwi Purwo Seputro', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08010', 1, 'Purworejo ', '1976-01-03', 1, 2, 'KP. Pisangan RT 003 RW 003 Penggilingan Cakung Jakarta Timur DKI Jakarta', '', 'KP. Pisangan RT 003 RW 003 Penggilingan Cakung Jakarta Timur DKI Jakarta', '', '', '081315502688', '(021) 29681', '', '', '', NULL, '3175070301760004', '982580987004000', '002', 'Ciracas', '0291390494', 'Dwi Purwo Seputro', '080109.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (225, '0', '080175', 'Febby Yurike Y Paluppa', 'Febby Yurike Y Paluppa', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08017', 2, 'Depok ', '1984-01-06', 1, 2, 'Gg. H. Encat RT 003 RW 013 Beji Beji Depok Jawa Barat', '', 'Gg. H. Encat RT 003 RW 013 Beji Beji Depok Jawa Barat', '', '', '081253426262', '(021) 29681', '', '', '', NULL, '3276014601840011', '986100618412000', '002', 'Ciracas', '0295389225', 'Febby Yurike Y Paluppa', '080175.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (227, '0', '080098', 'Neneng Ratna Ningsih', 'Neneng Ratna Ningsih', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08009', 2, 'Sukabumi ', '1957-10-13', 1, 2, 'Perum Jati Agung I Rt 002 RW 010 Jati Bening Baru Pondok Gede Bekasi Jawa Barat', '', 'Perum Jati Agung I Rt 002 RW 010 Jati Bening Baru Pondok Gede Bekasi Jawa Barat', '', '', '081293899934', '(021) 29681', '', '', '', NULL, '3275085310570007', '982580961432000', '002', 'Ciracas', '0295354815', 'Neneng Ratna Ningsih', '080098.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (228, '0', '080107', 'Pujiati', 'Pujiati', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08010', 2, 'Jakarta ', '1973-04-03', 1, 2, 'Jl. Kober Kecil No. 2 RT 006 RW 008 Rawa Bunga Jatinegara Jakarta Timur DKI Jakarta', '', 'Jl. Kober Kecil No. 2 RT 006 RW 008 Rawa Bunga Jatinegara Jakarta Timur DKI Jakarta', '', '', '081288235551', '(021) 29681', '', '', '', NULL, '3175034304730005', '982580896002000', '002', 'Ciracas', '0295247826', 'Pujiati', '080107.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (229, '0', '080100', 'Ridwan Ahmad', 'Ridwan Ahmad', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08010', 1, 'Bogor ', '1989-09-28', 1, 2, 'Pasar Rebo No. 19 RT 005 RT 010 Bojong Pondok Terong Cipayung Depok Jawa Barat', '', 'Pasar Rebo No. 19 RT 005 RT 010 Bojong Pondok Terong Cipayung Depok Jawa Barat', '', '', '085215568741', '(021) 29681', '', '', '', NULL, '3276072809860001', '982580995412000', '002', 'Ciracas', '0295362292', 'Ridwan Ahmad', '080100.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (230, '0', '080104', 'Salamah', 'Salamah', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08010', 1, 'Magelang ', '1977-08-09', 1, 2, 'Padurenan RT 003 RW 005 Pabuaran Cibinong Bogor Jawa Barat', '', 'Padurenan RT 003 RW 005 Pabuaran Cibinong Bogor Jawa Barat', '', '', '085311649659', '(021) 29681', '', '', '', NULL, '3171034908770007', '982580912027000', '002', 'Ciracas', '0295405374', 'Salamah', '080104.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (231, '0', '080140', 'Sudirta', 'Sudirta', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08014', 1, 'Jakarta ', '1960-08-28', 1, 2, 'Jl. Tanah Abang IV DLM No. 60 RT 04 RW 003 Petojo Selatan Gambir Jakarta Pusat Jakarta', '', 'Jl. Tanah Abang IV DLM No. 60 RT 04 RW 003 Petojo Selatan Gambir Jakarta Pusat Jakarta', '', '', '0895359124727', '(021) 29681', '', '', '', NULL, '3171012808600004', '982580615028000', '002', 'Ciracas', '0330018445', 'Sudirta', '080140.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (232, '0', '080286', 'Syahrul Nurmawansyah', 'Syahrul Nurmawansyah', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Jakarta ', '1982-06-24', 1, 2, 'Jl. H. Jusin RT 006 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', 'Jl. H. Jusin RT 006 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', '', '0812939482222', '(021) 29681', '', '', '', NULL, '3175092406820007', '640098117009000', '002', 'Ciracas', '0461484145', 'Syahrul Nurmawansyah', '080286.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (233, '0', '080287', 'Tedi Wirawan Putra', 'Tedi Wirawan Putra', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Cianjur ', '1995-12-02', 1, 2, 'Jl. H. Jusin RT 004 RW 001 Susukan Ciracas Jakarta Timu rDKI Jakarta', '', 'Jl. H. Jusin RT 004 RW 001 Susukan Ciracas Jakarta Timu rDKI Jakarta', '', '', '081311484922', '(021) 29681', '', '', '', NULL, '3175092012950007', '733104129009000', '002', 'Ciracas', '0461482965', 'Tedi Wirawan Putra', '080287.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (234, '0', '080099', 'Titin Supriatin', 'Titin Supriatin', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08009', 1, 'Bandung ', '1987-08-07', 1, 2, 'KP. Bojong Rangkas RT 001 RW 005 Bojong Rangkas Ciampea Bogor Jawa Barat', '', 'KP. Bojong Rangkas RT 001 RW 005 Bojong Rangkas Ciampea Bogor Jawa Barat', '', '', '087887531771', '(021) 29681', '', '', '', NULL, '3201154708870001', '982580946434000', '002', 'Ciracas', '0297911934', 'Titin Supriatin', '080099.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (235, '0', '080288', 'Wahyu Supriatna', 'Wahyu Supriatna', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08028', 1, 'Subang', '1996-09-25', 1, 2, 'Jl. H. Jusin RT 004 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', 'Jl. H. Jusin RT 004 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', '', '', '(021) 29681', '', '', '', NULL, '', '', '002', 'Ciracas', '0461481439', 'Wahyu Supriatna', '080288.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (236, '0', '080103', 'Wildan', 'Wildan', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08010', 1, 'Semarang ', '1988-12-01', 1, 2, 'Jl. Semangka Raya No. 426 RT 004 RW 006 Kranji Bekasi barat Bekasi Jawa Barat', '', 'Jl. Semangka Raya No. 426 RT 004 RW 006 Kranji Bekasi barat Bekasi Jawa Barat', '', '', '081293444982', '(021) 29681', '', '', '', NULL, '3275020112880011', '982580953407000', '002', 'Ciracas', '0297726033', 'Wildan', '080103.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (237, '0', '080306', 'Ahmad Fauzi', 'Ahmad Fauzi', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08030', 1, 'Tangerang ', '1997-10-07', 1, 2, 'Jl.Lombok I Pondok Cabe RT 006 RW 007 Pondok Cabe Ilir Pamulang Tanggerang Banten', '', 'Jl.Lombok I Pondok Cabe RT 006 RW 007 Pondok Cabe Ilir Pamulang Tanggerang Banten', '', '', '081382232158', '(021) 29681', '', '', '', NULL, '3674060710970001', '813279535453000', '002', 'Ciracas', '0506026707', 'Ahmad Fauzi', '080306.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (238, '0', '080308', 'Muhammad Hudzaifah', 'Muhammad Hudzaifah', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08030', 1, 'Bekasi ', '1998-09-11', 1, 2, 'Jl. Tengah RT 003 RW 003 Mustika Sari Mustika Jaya Bekasi Jawa Barat', '', 'Jl. Tengah RT 003 RW 003 Mustika Sari Mustika Jaya Bekasi Jawa Barat', '', '', '08128949369', '(021) 29681', '', '', '', NULL, '3275051109980015', '812070621432000', '002', 'Ciracas', '0506840707', 'Muhammad Hudzaifah', '080308.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (239, '0', '080309', 'Ujang Sutenda', 'Ujang Sutenda', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08030', 1, 'Sukabumi ', '1981-07-07', 1, 2, 'Kp. Cihaur RT 003 RW 003 Cihaur Simpenan Sukabumi Jawa Barat', '', 'Kp. Cihaur RT 003 RW 003 Cihaur Simpenan Sukabumi Jawa Barat', '', '', '085883693040', '(021) 29681', '', '', '', NULL, '3202020707810002', '811053073405000', '002', 'Ciracas', '0507828122', 'Ujang Sutenda', '080309.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (240, '0', '080310', 'Aditya Susilo', 'Aditya Susilo', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08031', 1, 'Jakarta ', '1991-10-08', 1, 2, 'Jl. H Jusin Rt 002 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', 'Jl. H Jusin Rt 002 RW 001 Susukan Ciracas Jakarta Timur DKI Jakarta', '', '', '087777997678', '(021) 29681', '', '', '', NULL, '3175090810910003', '748928538009000', '002', 'Ciracas', '0507829045', 'Aditya Susilo', '080310.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (241, '0', '080329', 'Hendra Wijaya', 'Hendra Wijaya', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08032', 1, 'oku Timur  ', '1988-06-12', 1, 2, 'Ulak Baru RT 003 RW 003 Ulak Baru Cempaka Palembang Sumatera Selatan', '', 'Ulak Baru RT 003 RW 003 Ulak Baru Cempaka Palembang Sumatera Selatan', '', '', '081210678403', '(021) 29681', '', '', '', NULL, '1608041206880002', '821338266009000', '002', 'Ciracas', '0573323858', 'Hendra Wijaya', '080329.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (242, '0', '080332', 'Saepul Ridwan', 'Saepul Ridwan', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08033', 1, 'Garut', '1989-10-04', 1, 2, 'Gg. Damai RT 005 RW 003 Mekas Sari Tambun Selatan Bekasi Jawa Barat', '', 'Gg. Damai RT 005 RW 003 Mekas Sari Tambun Selatan Bekasi Jawa Barat', '', '', '085695623933', '(021) 29681', '', '', '', NULL, '3216060410890020', '820976215009000', '002', 'Ciracas', '0569133625', 'Saepul Ridwan', '080332.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (243, '0', '080334', 'Asri Hidayat', 'Asri Hidayat', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08033', 1, 'Purwakarta', '1980-08-13', 1, 2, 'Jl. Sukajaga RT 008 RW 003 Ci Han jawar Bojong Purwakarta Jawa Barat', '', 'Jl. Sukajaga RT 008 RW 003 Ci Han jawar Bojong Purwakarta Jawa Barat', '', '', '087876127178', '(021) 29681', '', '', '', NULL, '3214111308800001', '821337771009000', '002', 'Ciracas', '0573346048', 'Asri Hidayat', '080334.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (245, '0', '080331', 'Yusrina', 'Yusrina', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08033', 2, 'Jakarta', '1995-01-07', 1, 2, 'Jl. Cendana 5 RT 014 RW 004 Suradita Cisauk Tangerang Banten', '', 'Jl. Cendana 5 RT 014 RW 004 Suradita Cisauk Tangerang Banten', '', '', '083874216143', '(021) 29681', '', '', '', NULL, '3603234701950001', '804004000452000', '002', 'Ciracas', '0573335693', 'Yusrina', '080331.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (246, '0', '080328', 'Supriyanto', 'Supriyanto', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08032', 1, 'Temanggung ', '1988-03-20', 1, 2, 'Jl. Gempol RT 006 RW 002 Ceger Cipayung Jakarta Timur DKI Jakarta', '', 'Jl. Gempol RT 006 RW 002 Ceger Cipayung Jakarta Timur DKI Jakarta', '', '', '083806898829', '(021) 29681', '', '', '', NULL, '3175102003881001', '985221860009000', '002', 'Ciracas', '0573337667', 'Supriyanto', '080328.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
INSERT INTO `master_biodata` VALUES (247, '0', '080127', 'Priyatin', 'Priyatin', 8, 99, 0, 6, 7, 12, 13, '', 4, 0, 0, 0, 0, '', '08012', 2, 'Jakarta', '1970-03-07', 1, 2, 'Jl. Harsono RT 006 RW 004 Ragunan Pasar Minggu Jakarta Selatan DKI Jakarta', '', 'Jl. Harsono RT 006 RW 004 Ragunan Pasar Minggu Jakarta Selatan DKI Jakarta', '', '', '089513579879', '(021) 29681', '', '', '', NULL, '3174040703700009', '475855136017000', '002', 'Ciracas', '0299013337', 'Priyatin', '080127.jpg\r', NULL, 'tidak', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);
COMMIT;

-- ----------------------------
-- Table structure for rf_sisa_cuti
-- ----------------------------
DROP TABLE IF EXISTS `rf_sisa_cuti`;
CREATE TABLE `rf_sisa_cuti` (
  `master_biodata_id` int(5) NOT NULL AUTO_INCREMENT,
  `bkn_id` varchar(60) DEFAULT '0',
  `nip` varchar(18) DEFAULT '0',
  `nama_cetak` varchar(200) DEFAULT '',
  `th2018` int(255) DEFAULT '14',
  `th2019` int(255) DEFAULT '14',
  `th2020` int(255) DEFAULT '14',
  `th2021` int(255) DEFAULT '14',
  `th2022` int(255) DEFAULT '14',
  `th2023` int(255) DEFAULT '14',
  `th2024` int(255) DEFAULT '14',
  `th2025` int(255) DEFAULT '14',
  PRIMARY KEY (`master_biodata_id`),
  UNIQUE KEY `nip` (`nip`)
) ENGINE=MyISAM AUTO_INCREMENT=256 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rf_sisa_cuti
-- ----------------------------
BEGIN;
INSERT INTO `rf_sisa_cuti` VALUES (1, '0', '080197', 'Drs. Hasto Atmojo Suroyo, M.Krim', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (2, '0', '080414', 'Brigjen. Pol. (Purn) Dr . Achmadi, S.H., M.AP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (3, '0', '080416', 'Dr. Iur. Antonius Prijadi Soesilo Wibowo, S.H., M.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (4, '0', '080196', 'Edwin Partogi Pasaribu, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (5, '0', '080415', 'Dr. Livia Istania DF Iskandar. M.Sc', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (6, '0', '080417', 'Dr. Maneger Nasution, M.A', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (7, '0', '080330', 'Susilaningtias, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (8, '0', '196409051990310004', 'Dr. Ir. Noor Sidharta, M.H., MBA', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (9, '0', '196404131991032001', 'Dra. Handari Restu Dewi, M.M', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (10, '0', '197203242002121002', 'Eko Sunarko,  S. Kom., M.M', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (11, '0', '198409282010121003', 'Dian Herdiansah, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (12, '0', '198911302014022001', 'Novita Prima Dewi, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (13, '0', '198307202010121001', 'Singgih Wisnubroto, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (14, '0', '198808162014022001', 'Roery Ayu Mayangsari Riyadi, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (15, '0', '198903162014021001', 'Muhammad Ihsan, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (16, '0', '196604011986031001', 'Abadi Yanto, S.H., M.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (17, '0', '199009302014022003', 'Nunut Amalia, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (18, '0', '198701062010122001', 'Indryasari. S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (19, '0', '199004092014022003', 'Yuli Yuliah, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (20, '0', '197309302001121001', 'Dr. Drama Panca Putra, S.Pi., M.Si', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (21, '0', '197708152005012001', 'Eviyati, S.Pd., M.AP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (22, '0', '198805112014022002', 'Inggit Nursafitri, S.H., M.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (23, '0', '197007131996032001', 'Enteng Mundiati, S.H., M.M', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (24, '0', '199102122014022001', 'Lia Gunawan, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (25, '0', '198104292010122001', 'Dinar Rahmayani, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (26, '0', '197001021989031001', 'Sriyana, S.H., LL.M., DFM', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (27, '0', '69020416', 'Marjoko', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (28, '0', '198709202010122002', 'Kombes. Pol . Sandra Anggita, S.I.K., M.Si', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (29, '0', '199101142014022002', 'Fatimah Nuryani, S.H., MH', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (30, '0', '196408081989032001', 'Dr. Latri Mumpuni, M.Si', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (31, '0', '198409282010121004', 'Achmad Soleh, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (32, '0', '198209162006041005', 'Firdiansyah, S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (33, '0', '197301061996031001', 'Aris Fajari Teguh Nugroho, S.Sos., M.AP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (34, '0', '080079', 'Abdanev Jopa C, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (35, '0', '080261', 'Basuki Haryono, S.H., M.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (36, '0', '080249', 'Muhammad Mardiansyah, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (37, '0', '080006', 'Mulatingsih, S.H., M.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (38, '0', '080193', 'Drs. Prasetyo Djafar, M.M', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (39, '0', '080077', 'Rully Novian, S.H, M.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (40, '0', '080166', 'Sukri Agama, S.H., M.Hum', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (41, '0', '080062', 'Syahrial Martanto Wiryawan, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (42, '0', '080040', 'Galih Prihanto Jati, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (43, '0', '080049', 'Armein Rizal B, Ak., MBA', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (44, '0', '197707281997031001', 'Agus Janan Widyantoro', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (45, '0', '198912172015032001', 'Anisa Dewi Prajanti,  S.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (46, '0', '198212132015032001', 'Anna Khoiru Nisya,  S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (47, '0', '198109292010122001', 'Cita Mismiana,  A.Md', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (48, '0', '198801172010122004', 'Eni Nurtika,  A.Md', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (49, '0', '198101062015031001', 'Fakhrur Haqiqi,  S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (50, '0', '196808301993031001', 'I Gusti Agung Adnyana', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (51, '0', '198207072015032001', 'Kartini Megawati,  S.Pd', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (52, '0', '198606182015031001', 'Mahari Is Subangun,  S.Pd', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (53, '0', '198701032015032001', 'Riani Anggraeni Soedirgo, S. Hum', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (54, '0', '198209242015031001', 'Setiawan, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (55, '0', '198810192015032001', 'Sri Wiji Astuti, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (56, '0', '198010092015032001', 'Therecia Lies Triana Sulanjari,  S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (57, '0', '199011082015032001', 'Tiara Rachmawati,  S.Kep', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (58, '0', '197812272005021001', 'Revando Burhan,  S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (59, '0', '199307232019032005', 'Murniasih, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (60, '0', '198908312019031001', 'Fuadiansyah, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (61, '0', '199111102019032003', 'Tri Indah Lestari, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (62, '0', '199011232019031003', 'Bimo Setiajie, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (63, '0', '198809092019032002', 'Linda Widya, S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (64, '0', '198605192019032002', 'Rahma Sartika, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (65, '0', '199403312019031002', 'Ali Gunung Siregar, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (66, '0', '198603292019031001', 'Muhamad Fahrudin, A.Md', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (67, '0', '199004192019032003', 'Milati Nurul Haqi, A.Md', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (68, '0', '199607062019031001', 'Ery Kurnia, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (69, '0', '198407112019031002', 'Albar Aliyyus, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (70, '0', '199108232019032004', 'Soviana Nur Afifah', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (71, '0', '198609152019032002', 'Siti Nurliyah, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (72, '0', '199506212019032003', 'Mutia Ariesca Suwandi, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (73, '0', '199304262019032002', 'Holy Apriliani, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (74, '0', '199404142019032007', 'Syafatania, S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (75, '0', '199512052019032003', 'Ananda Rasullia, S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (76, '0', '198807112019031005', 'Cahya Tutuk Irana Sakti, S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (77, '0', '199002252019032002', 'Farah Shafira, S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (78, '0', '198406152019032002', 'Sri Juniardhani Tjahjaningsih, S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (79, '0', '198608072019032002', 'Candrawati Puspitasari, S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (80, '0', '199109102019032005', 'Kezia Henderisa Rumbiak, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (81, '0', '198710072019031002', 'Yogi Suryamansyah, S.AP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (82, '0', '198609222019031001', 'Andreas Lucky Lukwira, S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (83, '0', '199210012019031002', 'Hevy Ukasyah Bahreisy, S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (84, '0', '199408182019032006', 'Firdha Ratnasari Guntoro, S.K.Pm', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (85, '0', '199010042019031001', 'Muhammad Abdul Aziz, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (86, '0', '199110192019031005', 'Indria Dwi Sukarno, S.I.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (87, '0', '198610222019031002', 'Ayub Oktaviano, S.I.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (88, '0', '199009192019032004', 'Septiana Sundari, S.I.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (89, '0', '198606182019031002', 'Ardiyanto Wibowo, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (90, '0', '199402202019032004', 'Maharani Indah Puspita, S.I.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (91, '0', '199504132019032003', 'I\'anah Marfu\'ah, S.I.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (92, '0', '199611172019032003', 'Vinda Noviasari, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (93, '0', '199007092019032005', 'Mir\'atul Latifah, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (94, '0', '199405312019032002', 'Maylisa Eka Handani Putri, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (95, '0', '199010072019031001', 'Askar Eko Harpito, S.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (96, '0', '199509272019032004', 'Aulia Aminda Dhianti, S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (97, '0', '199306282019032002', 'Lina Rosdiana, S.T', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (98, '0', '199303132019032006', 'Yusnia Arianingsih, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (99, '0', '199111242019032000', 'Lidya Susanti Siburian, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (100, '0', '198812052019031002', 'Putra Wijaya, S.I.A', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (101, '0', '198312272019031001', 'Fakhrurrazi, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (102, '0', '199507142019031003', 'Harry Nugraha Pratama, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (103, '0', '198905062019031004', 'Ronaldi Maika Putra, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (104, '0', '199503292019032003', 'Nurfianingsih, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (105, '0', '199402162019031003', 'Hafidz Alam Islami, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (106, '0', '199503142019032004', 'Rizki Amalia, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (107, '0', '199306062019032003', 'Rachelia Eni Putri, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (108, '0', '198410122019032001', 'Wulan Dewi Satriyani, S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (109, '0', '198903162019031001', 'Ali Imran, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (110, '0', '198611192019031003', 'Dimas Susanto, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (111, '0', '080029', 'Abdul Aziz Muslih, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (112, '0', '080118', 'Ade Hendra Mulyana, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (113, '0', '080342', 'Riska, A.Md Keb', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (114, '0', '080275', 'Yogi Nugraha, S.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (115, '0', '080325', 'Septia Prima Hardiyanti, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (116, '0', '080033', 'Aan Munarto, S.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (117, '0', '080214', 'Agus Fitria Budi, S.Pd', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (118, '0', '080145', 'Andreas Kriwanto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (119, '0', '080241', 'Puti Mayang Sari, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (120, '0', '080341', 'Mufidathul Izhmy S, S.IP', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (121, '0', '080326', 'Christina Puspa Dewi, A.Md', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (122, '0', '080243', 'Clara Monica, S.os', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (123, '0', '080069', 'Ni\'matul Hidayati, S.S', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (124, '0', '080209', 'Rizqi Aulia Rahman, S.E.Sy', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (125, '0', '080076', 'Rizki Aji Pamungkas, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (126, '0', '080263', 'Vendi Purnomo, S.I.Kom', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (127, '0', '080171', 'Deby Darmawati, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (128, '0', '080300', 'Zulkarnaen Bara, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (129, '0', '080015', 'Amalia Mahsunah SH', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (130, '0', '080021', 'M Hasyim Muhsoni I, S.H.I', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (131, '0', '080298', 'Tawang Amuhara, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (132, '0', '080009', 'Trisna Pasaribu , SE', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (133, '0', '080303', 'Siti Sukaesih A.Md', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (134, '0', '080210', 'Susandhi Bin Sukatma, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (135, '0', '080064', 'Acik Amaliyah, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (136, '0', '080012', 'Fany Ratih Prawiasari, A.Md.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (137, '0', '080054', 'Suciasih Retno Kartika, A.Md.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (138, '0', '080063', 'Ndaru Utomo Kusumo, S.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (139, '0', '080090', 'Adji Muhammad Dana Kusuma, S.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (140, '0', '080086', 'Cici Amirah, S.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (141, '0', '080238', 'Andi Rully Asgap, S.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (142, '0', '080027', 'Salahudin', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (143, '0', '080305', 'Siti Mu\'minah, S.Pd', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (144, '0', '080008', 'Gunawan Artho Nugoho  S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (145, '0', '080340', 'Angga Sukma Pratama  S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (146, '0', '080018', 'Pascalis Risdiana FP S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (147, '0', '080129', 'Tirta Kartika Rianto, S.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (148, '0', '080157', 'Wiyatiningsih S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (149, '0', '080183', 'Mayselina Nuriza, S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (150, '0', '080070', 'M Tommy Permana, S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (151, '0', '080207', 'Pikri Zulpikar HQ, S.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (152, '0', '080302', 'Hendri Aditya  S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (153, '0', '080014', 'Sulung Gogor Samodro, S.Sos', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (154, '0', '080047', 'Citra Wulandari, S.E', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (155, '0', '080216', 'Rizki Sammaulidah, A.Md', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (156, '0', '080339', 'Sofie Emilia Mustika, S.Psi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (157, '0', '080208', 'Yulisa Maharani,  S.H.,M.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (158, '0', '080067', 'Rianto Wicaksono, S.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (159, '0', '080240', 'Anisa Roda Diana S.H.', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (160, '0', '080016', 'Robi Saripudin, S.H', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (161, '0', '080272', 'Ergard Putra Geerards, S.T', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (162, '0', '080130', 'Abdul Latief', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (163, '0', '080295', 'Adi Purwanto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (164, '0', '080281', 'Adi Rusyadi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (165, '0', '080131', 'Agus Sutisna', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (166, '0', '080146', 'Ahmat Safei', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (167, '0', '080141', 'Alfian', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (168, '0', '080282', 'Apendi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (169, '0', '080203', 'Apriliyanto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (170, '0', '080133', 'Arthur P Taihutu', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (171, '0', '080296', 'Dian Ratna Sari', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (172, '0', '080201', 'Edi Susanto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (173, '0', '080280', 'Elwen Ebenhard T', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (174, '0', '080246', 'Fatty Hillah', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (175, '0', '080188', 'Hendri Sutopo', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (176, '0', '080143', 'Heri Sanjaya', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (177, '0', '080124', 'Idup Abdul Rohman', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (178, '0', '080125', 'Iwan', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (179, '0', '080247', 'M Hamzah ', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (180, '0', '080294', 'Nurhansyah', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (181, '0', '080189', 'Ovi Suhatman', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (182, '0', '080138', 'Ratmono', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (183, '0', '080135', 'Rianto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (184, '0', '080134', 'Rio Gatot Sulistiawan', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (185, '0', '080137', 'Sukardi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (186, '0', '080202', 'Sulisman', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (187, '0', '080128', 'Syamsul Anwar', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (188, '0', '080293', 'Totom Sukanda', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (189, '0', '080190', 'Tony Nurhadi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (190, '0', '080115', 'Abdul Rafiq', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (191, '0', '080162', 'Amir Fatah', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (192, '0', '080116', 'Amir Fauzi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (193, '0', '080032', 'Andi Cahyadi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (194, '0', '080111', 'Anen', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (195, '0', '080284', 'Danny A. Sulaksana', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (196, '0', '080232', 'Dwi Jatmiko', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (197, '0', '080120', 'Heru Pranoto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (198, '0', '080220', 'Haerudin', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (199, '0', '080265', 'Karyono', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (200, '0', '080112', 'Manan Bin Mipan', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (201, '0', '080221', 'Mochamad Ichsan', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (202, '0', '080194', 'Muhamad Muharoji', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (203, '0', '080283', 'Rahmat Hidayat', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (204, '0', '080271', 'Rio Afrizal', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (205, '0', '080219', 'Rusdianto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (206, '0', '080113', 'Sopian', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (207, '0', '080248', 'Tarsim Masruri', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (208, '0', '080119', 'Tita Jatnika', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (209, '0', '080195', 'Waryanto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (210, '0', '080114', 'Yanuar Ariwahyudi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (211, '0', '080333', 'Arya Alai Permana', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (212, '0', '080336', 'Uus Agustian', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (213, '0', '080344', 'Achmad Zelani', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (214, '0', '080345', 'Andi Rosyid', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (215, '0', '080361', 'Purnomo', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (216, '0', '080290', 'Agung Tri Yuli Hantoro', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (217, '0', '080095', 'Amin Yamin', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (218, '0', '080276', 'Ari Sanjaya', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (219, '0', '080285', 'Arif Hidayat', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (220, '0', '080105', 'Aripin', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (221, '0', '080102', 'Della Isaura Karauwan', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (222, '0', '080291', 'Didi Sumadi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (223, '0', '080278', 'Dulloh Rafsanjani', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (224, '0', '080109', 'Dwi Purwo Seputro', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (225, '0', '080175', 'Febby Yurike Y Paluppa', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (227, '0', '080098', 'Neneng Ratna Ningsih', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (228, '0', '080107', 'Pujiati', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (229, '0', '080100', 'Ridwan Ahmad', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (230, '0', '080104', 'Salamah', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (231, '0', '080140', 'Sudirta', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (232, '0', '080286', 'Syahrul Nurmawansyah', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (233, '0', '080287', 'Tedi Wirawan Putra', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (234, '0', '080099', 'Titin Supriatin', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (235, '0', '080288', 'Wahyu Supriatna', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (236, '0', '080103', 'Wildan', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (237, '0', '080306', 'Ahmad Fauzi', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (238, '0', '080308', 'Muhammad Hudzaifah', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (239, '0', '080309', 'Ujang Sutenda', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (240, '0', '080310', 'Aditya Susilo', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (241, '0', '080329', 'Hendra Wijaya', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (242, '0', '080332', 'Saepul Ridwan', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (243, '0', '080334', 'Asri Hidayat', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (245, '0', '080331', 'Yusrina', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (246, '0', '080328', 'Supriyanto', 14, 14, 14, 14, 14, 14, 14, 14);
INSERT INTO `rf_sisa_cuti` VALUES (247, '0', '080127', 'Priyatin', 14, 14, 14, 14, 14, 14, 14, 14);
COMMIT;

-- ----------------------------
-- Table structure for tbl_bmn
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bmn`;
CREATE TABLE `tbl_bmn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8 NOT NULL,
  `kode` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tahun` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `stok` int(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_bmn
-- ----------------------------
BEGIN;
INSERT INTO `tbl_bmn` VALUES (6, 'Testing BMN 1', '0001', '2013', 1);
INSERT INTO `tbl_bmn` VALUES (7, 'Testing BMN 2', '0002', '2015', 1);
INSERT INTO `tbl_bmn` VALUES (8, 'Testing BMN 3', '0003', '2012', 1);
COMMIT;

-- ----------------------------
-- Table structure for tbl_cuti
-- ----------------------------
DROP TABLE IF EXISTS `tbl_cuti`;
CREATE TABLE `tbl_cuti` (
  `jenis_cuti` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`jenis_cuti`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_cuti
-- ----------------------------
BEGIN;
INSERT INTO `tbl_cuti` VALUES ('Cuti Bersalin');
INSERT INTO `tbl_cuti` VALUES ('Cuti Besar');
INSERT INTO `tbl_cuti` VALUES ('Cuti Diluar Tanggungan Negara');
INSERT INTO `tbl_cuti` VALUES ('Cuti Karena Alasan Penting');
INSERT INTO `tbl_cuti` VALUES ('Cuti Sakit');
INSERT INTO `tbl_cuti` VALUES ('Cuti Tahunan');
COMMIT;

-- ----------------------------
-- Table structure for tbl_dispo_rr
-- ----------------------------
DROP TABLE IF EXISTS `tbl_dispo_rr`;
CREATE TABLE `tbl_dispo_rr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_trans_rr` int(11) NOT NULL,
  `id_pengirim` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `id_penerima` varchar(255) COLLATE utf8_bin NOT NULL,
  `komentar` text COLLATE utf8_bin NOT NULL,
  `date_created` timestamp NOT NULL,
  `bagian` varchar(255) COLLATE utf8_bin DEFAULT 'KONSUMSI',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for tbl_hak_akses
-- ----------------------------
DROP TABLE IF EXISTS `tbl_hak_akses`;
CREATE TABLE `tbl_hak_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_hak_akses
-- ----------------------------
BEGIN;
INSERT INTO `tbl_hak_akses` VALUES (15, 1, 1);
INSERT INTO `tbl_hak_akses` VALUES (19, 1, 3);
INSERT INTO `tbl_hak_akses` VALUES (24, 1, 9);
INSERT INTO `tbl_hak_akses` VALUES (30, 1, 2);
INSERT INTO `tbl_hak_akses` VALUES (31, 1, 10);
INSERT INTO `tbl_hak_akses` VALUES (32, 1, 11);
INSERT INTO `tbl_hak_akses` VALUES (33, 1, 12);
INSERT INTO `tbl_hak_akses` VALUES (34, 1, 13);
INSERT INTO `tbl_hak_akses` VALUES (37, 1, 16);
INSERT INTO `tbl_hak_akses` VALUES (38, 1, 17);
INSERT INTO `tbl_hak_akses` VALUES (39, 1, 18);
INSERT INTO `tbl_hak_akses` VALUES (41, 3, 11);
INSERT INTO `tbl_hak_akses` VALUES (42, 3, 18);
INSERT INTO `tbl_hak_akses` VALUES (43, 3, 19);
INSERT INTO `tbl_hak_akses` VALUES (44, 3, 20);
INSERT INTO `tbl_hak_akses` VALUES (45, 3, 16);
INSERT INTO `tbl_hak_akses` VALUES (46, 3, 13);
INSERT INTO `tbl_hak_akses` VALUES (47, 3, 17);
INSERT INTO `tbl_hak_akses` VALUES (48, 3, 21);
INSERT INTO `tbl_hak_akses` VALUES (49, 3, 14);
INSERT INTO `tbl_hak_akses` VALUES (50, 3, 23);
INSERT INTO `tbl_hak_akses` VALUES (51, 3, 24);
INSERT INTO `tbl_hak_akses` VALUES (52, 3, 22);
INSERT INTO `tbl_hak_akses` VALUES (53, 1, 25);
INSERT INTO `tbl_hak_akses` VALUES (54, 3, 25);
INSERT INTO `tbl_hak_akses` VALUES (55, 1, 26);
INSERT INTO `tbl_hak_akses` VALUES (57, 3, 26);
INSERT INTO `tbl_hak_akses` VALUES (58, 1, 20);
INSERT INTO `tbl_hak_akses` VALUES (59, 4, 25);
INSERT INTO `tbl_hak_akses` VALUES (64, 4, 27);
INSERT INTO `tbl_hak_akses` VALUES (65, 4, 14);
INSERT INTO `tbl_hak_akses` VALUES (66, 1, 28);
INSERT INTO `tbl_hak_akses` VALUES (67, 4, 28);
INSERT INTO `tbl_hak_akses` VALUES (71, 2, 3);
INSERT INTO `tbl_hak_akses` VALUES (73, 2, 2);
INSERT INTO `tbl_hak_akses` VALUES (74, 2, 10);
INSERT INTO `tbl_hak_akses` VALUES (75, 2, 11);
INSERT INTO `tbl_hak_akses` VALUES (76, 2, 12);
INSERT INTO `tbl_hak_akses` VALUES (77, 2, 13);
INSERT INTO `tbl_hak_akses` VALUES (78, 2, 16);
INSERT INTO `tbl_hak_akses` VALUES (79, 2, 17);
INSERT INTO `tbl_hak_akses` VALUES (80, 2, 18);
INSERT INTO `tbl_hak_akses` VALUES (81, 2, 28);
INSERT INTO `tbl_hak_akses` VALUES (82, 2, 20);
INSERT INTO `tbl_hak_akses` VALUES (83, 2, 26);
INSERT INTO `tbl_hak_akses` VALUES (84, 2, 25);
INSERT INTO `tbl_hak_akses` VALUES (85, 1, 27);
INSERT INTO `tbl_hak_akses` VALUES (86, 1, 28);
INSERT INTO `tbl_hak_akses` VALUES (87, 3, 29);
INSERT INTO `tbl_hak_akses` VALUES (88, 3, 30);
INSERT INTO `tbl_hak_akses` VALUES (89, 4, 26);
COMMIT;

-- ----------------------------
-- Table structure for tbl_menu
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_main_menu` int(11) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL COMMENT 'y=yes,n=no',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
BEGIN;
INSERT INTO `tbl_menu` VALUES (1, 'KELOLA MENU', 'kelolamenu', 'fa fa-server', 0, 'y');
INSERT INTO `tbl_menu` VALUES (2, 'KELOLA PENGGUNA', 'user', 'fa fa-user-o', 0, 'y');
INSERT INTO `tbl_menu` VALUES (3, 'level PENGGUNA', 'userlevel', 'fa fa-users', 0, 'y');
INSERT INTO `tbl_menu` VALUES (9, 'Contoh Form', 'welcome/form', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (10, 'Kelola BMN', 'bmn', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (11, 'Riwayat Pribadi', 'trans_bmn', 'fa fa-id-card', 20, 'y');
INSERT INTO `tbl_menu` VALUES (12, 'Kelola Ruang Rapat', 'ruang_rapat', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (13, 'Riwayat Pribadi', 'trans_ruang_rapat', 'fa fa-id-card', 16, 'y');
INSERT INTO `tbl_menu` VALUES (14, 'Riwayat Pribadi', 'trans_cuti', 'fa fa-id-card', 22, 'y');
INSERT INTO `tbl_menu` VALUES (16, 'Ruang Rapat', '#', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (17, 'Formulir', 'trans_ruang_rapat/create', 'fa fa-id-card', 16, 'y');
INSERT INTO `tbl_menu` VALUES (18, 'Formulir', 'trans_bmn/create', 'fa fa-id-card', 20, 'y');
INSERT INTO `tbl_menu` VALUES (19, 'Approval', 'trans_bmn/index_persetujuan', 'fa fa-id-card', 20, 'y');
INSERT INTO `tbl_menu` VALUES (20, 'Barang Milik Negara', '#', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (21, 'Approval', 'trans_ruang_rapat/index_persetujuan', 'fa fa-id-card', 16, 'y');
INSERT INTO `tbl_menu` VALUES (22, 'Cuti', '#', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (23, 'Formulir', 'trans_cuti/create', 'fa fa-id-card', 22, 'y');
INSERT INTO `tbl_menu` VALUES (24, 'Approval', 'trans_cuti/index_persetujuan', 'fa fa-id-card', 22, 'y');
INSERT INTO `tbl_menu` VALUES (25, 'Dashboard', 'welcome', 'fa fa-id-card', 0, 'n');
INSERT INTO `tbl_menu` VALUES (26, 'Cetak', 'cetak', 'fa fa-id-card', 0, 'n');
INSERT INTO `tbl_menu` VALUES (27, 'Validasi', 'trans_cuti/index_validasi', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (28, 'SISA CUTI', 'sisa_cuti', 'fa fa-id-card', 0, 'y');
INSERT INTO `tbl_menu` VALUES (29, 'Perjanjian', 'perjanjian_bmn', 'fa fa-id-card', 20, 'y');
INSERT INTO `tbl_menu` VALUES (30, 'Disposisi', 'dispo_ruang_rapat', 'fa fa-id-card', 16, 'y');
COMMIT;

-- ----------------------------
-- Table structure for tbl_perjanjian_bmn
-- ----------------------------
DROP TABLE IF EXISTS `tbl_perjanjian_bmn`;
CREATE TABLE `tbl_perjanjian_bmn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_trans_bmn` int(11) DEFAULT NULL,
  `id_pejabat` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `is_approved` tinyint(1) DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `date_ajukan` timestamp NULL DEFAULT NULL,
  `date_reaksi` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for tbl_ruang_rapat
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ruang_rapat`;
CREATE TABLE `tbl_ruang_rapat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruangan` varchar(255) CHARACTER SET utf8 NOT NULL,
  `id_pengelola` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_ruang_rapat
-- ----------------------------
BEGIN;
INSERT INTO `tbl_ruang_rapat` VALUES (1, 'Ruang Satu', 'test');
INSERT INTO `tbl_ruang_rapat` VALUES (2, 'Ruang Dua', 'test');
INSERT INTO `tbl_ruang_rapat` VALUES (3, 'Ruang Tiga', 'test');
COMMIT;

-- ----------------------------
-- Table structure for tbl_setting
-- ----------------------------
DROP TABLE IF EXISTS `tbl_setting`;
CREATE TABLE `tbl_setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `nama_setting` varchar(50) NOT NULL,
  `value` varchar(40) NOT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_setting
-- ----------------------------
BEGIN;
INSERT INTO `tbl_setting` VALUES (1, 'Tampil Menu', 'ya');
COMMIT;

-- ----------------------------
-- Table structure for tbl_status_bmn
-- ----------------------------
DROP TABLE IF EXISTS `tbl_status_bmn`;
CREATE TABLE `tbl_status_bmn` (
  `id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_status_bmn
-- ----------------------------
BEGIN;
INSERT INTO `tbl_status_bmn` VALUES ('AJUKAN_1');
INSERT INTO `tbl_status_bmn` VALUES ('AJUKAN_2');
INSERT INTO `tbl_status_bmn` VALUES ('DIBATALKAN');
INSERT INTO `tbl_status_bmn` VALUES ('DIBUAT');
INSERT INTO `tbl_status_bmn` VALUES ('DISETUJUI');
INSERT INTO `tbl_status_bmn` VALUES ('GAGAL_VALIDASI');
INSERT INTO `tbl_status_bmn` VALUES ('PERLU_VALIDASI');
INSERT INTO `tbl_status_bmn` VALUES ('TOLAK_1');
INSERT INTO `tbl_status_bmn` VALUES ('TOLAK_2');
COMMIT;

-- ----------------------------
-- Table structure for tbl_trans_bmn
-- ----------------------------
DROP TABLE IF EXISTS `tbl_trans_bmn`;
CREATE TABLE `tbl_trans_bmn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `id_bmn` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nama_bmn` text NOT NULL,
  `tgl_pinjam` varchar(50) DEFAULT NULL,
  `tgl_kembali` varchar(50) DEFAULT NULL,
  `keperluan` text,
  `lokasi` text,
  `keterangan` text,
  `tgl_penggunaan` varchar(255) DEFAULT NULL,
  `id_atasan` varchar(255) DEFAULT NULL,
  `id_krt` varchar(255) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_diajukan` timestamp NULL DEFAULT NULL,
  `date_reaksi_satu` timestamp NULL DEFAULT NULL,
  `date_reaksi_dua` timestamp NULL DEFAULT NULL,
  `status` varchar(50) DEFAULT 'DIBUAT',
  `alasan_pembatalan` text,
  PRIMARY KEY (`id`),
  KEY `FK_bmn_status` (`status`),
  CONSTRAINT `FK_bmn_status` FOREIGN KEY (`status`) REFERENCES `tbl_status_bmn` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_trans_cuti
-- ----------------------------
DROP TABLE IF EXISTS `tbl_trans_cuti`;
CREATE TABLE `tbl_trans_cuti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `jenis_cuti` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `tgl_mulai` varchar(255) NOT NULL,
  `tgl_selesai` varchar(255) NOT NULL,
  `alamat` text CHARACTER SET utf8 NOT NULL,
  `nip_atasan` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `nip_pejabat` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `alasan` text CHARACTER SET utf8 NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'DIBUAT',
  `wkt_cuti` varchar(255) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_diajukan` timestamp NULL DEFAULT NULL,
  `date_reaksi_satu` timestamp NULL DEFAULT NULL,
  `date_validasi` timestamp NULL DEFAULT NULL,
  `date_reaksi_dua` timestamp NULL DEFAULT NULL,
  `jumlah_hari` int(255) NOT NULL,
  `alasan_pembatalan` text,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cuti_status` (`status`),
  KEY `FK_tbl_trans_cuti_tbl_cuti` (`jenis_cuti`),
  CONSTRAINT `FK_cuti_status` FOREIGN KEY (`status`) REFERENCES `tbl_status_bmn` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_trans_cuti_tbl_cuti` FOREIGN KEY (`jenis_cuti`) REFERENCES `tbl_cuti` (`jenis_cuti`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_trans_ruang_rapat
-- ----------------------------
DROP TABLE IF EXISTS `tbl_trans_ruang_rapat`;
CREATE TABLE `tbl_trans_ruang_rapat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kegiatan` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `id_ruang_rapat` int(11) DEFAULT NULL,
  `id_pegguna` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Pemesan',
  `tgl_mulai` varchar(50) NOT NULL DEFAULT '0',
  `tgl_akhir` varchar(50) NOT NULL DEFAULT '',
  `wkt_kegiatan` varchar(255) NOT NULL,
  `id_atasan` varchar(255) NOT NULL,
  `id_krt` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_diajukan` timestamp NULL DEFAULT NULL,
  `date_reaksi_satu` timestamp NULL DEFAULT NULL,
  `date_reaksi_dua` timestamp NULL DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'DIBUAT',
  `konsumsi` varchar(255) DEFAULT NULL,
  `keterangan_konsumsi` varchar(255) DEFAULT NULL,
  `jml_peserta` int(255) DEFAULT '0',
  `alasan_pembatalan` text,
  PRIMARY KEY (`id`),
  KEY `FK_rrapat_status` (`status`),
  CONSTRAINT `FK_rrapat_status` FOREIGN KEY (`status`) REFERENCES `tbl_status_bmn` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id_users` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq',
  `images` varchar(255) NOT NULL DEFAULT 'atomix_user31.png',
  `id_user_level` int(11) NOT NULL DEFAULT '3',
  `is_aktif` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=742 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user` VALUES (1, '0000', 'Nuris Akbar M.Kom', 'nuris.akbar@gmail.com', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 1, 'y');
INSERT INTO `tbl_user` VALUES (3, '0001', 'Muhammad hafidz Muzaki', 'hafid@gmail.com', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', '7.png', 2, 'y');
INSERT INTO `tbl_user` VALUES (494, '80033', 'Aan Munarto. S.Kom', 'as.kom', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (495, '196604011986031001', 'Abadi Yanto', 'ayanto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (496, '80079', 'Abdanev Jopa C.', 'ajopa', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (497, '80029', 'Abdul Aziz Muslih', 'amuslih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (498, '80130', 'Abdul Latief', 'alatief', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (499, '80115', 'Abdul Rafiq', 'arafiq', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (500, '198409282010121004', 'Achmad Soleh', 'asoleh', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (501, '80344', 'Achmad Zelani', 'azelani', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (502, '80414', 'Achmadi', 'aachmadi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (503, '80064', 'Acik Amaliyah ', 'aamaliyah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (504, '80118', 'Ade Hendra Mulyana', 'amulyana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (505, '80295', 'Adi Purwanto', 'apurwanto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (506, '80281', 'Adi Rusyadi', 'arusyadi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (507, '80310', 'Aditya Susilo', 'asusilo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (508, '80090', 'Adji Muhammad Dana Kusuma ', 'akusuma', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (509, '80290', 'Agung Tri Yuli Hantoro', 'ahantoro', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (510, '80214', 'Agus Fitria Budi', 'abudi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (511, '197707281997031001', 'Agus Janan Widyantoro', 'awidyantoro', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (512, '80131', 'Agus Sutisna', 'asutisna', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (513, '80306', 'Ahmad Fauzi', 'afauzi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (514, '80146', 'Ahmat Safei', 'asafei', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (515, '198407112019031002', 'Albar Aliyyus', 'aaliyyus', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (516, '80141', 'Alfian', 'aalfian', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (517, '199403312019031002', 'Ali Gunung Siregar', 'asiregar', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (518, '198903162019031001', 'Ali Imran', 'aimran', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (519, '80015', 'Amalia Mahsunah ', 'amahsunah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (520, '80095', 'Amin Yamin', 'ayamin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (521, '80162', 'Amir Fatah', 'afatah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (522, '80116', 'Amir Fauzi', 'afauzi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (523, '199512052019032003', 'Ananda Rasullia', 'arasullia', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (524, '80032', 'Andi Cahyadi', 'acahyadi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (525, '80345', 'Andi Rosyid', 'arosyid', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (526, '80238', 'Andi Rully Asgap ', 'aasgap', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (527, '80145', 'Andreas Kriwanto', 'akriwanto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (528, '198609222019031001', 'Andreas Lucky Lukwira', 'alukwira', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (529, '80111', 'Anen', 'aanen', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (530, '80340', 'Angga Sukma Pratama ', 'apratama', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (531, '198912172015032001', 'Anisa Dewi Prajanti', 'aprajanti', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (532, '80240', 'Anisa Roda Diana ', 'adiana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (533, '198212132015032001', 'Anna Khoiru Nisya', 'anisya', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (534, '80416', 'Antonius Prijadi Soesilo Wibowo', 'awibowo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (535, '80282', 'Apendi', 'aapendi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (536, '80203', 'Apriliyanto', 'aapriliyanto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (537, '198606182019031002', 'Ardiyanto Wibowo', 'awibowo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (538, '80276', 'Ari Sanjaya', 'asanjaya', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (539, '80285', 'Arif Hidayat', 'ahidayat', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (540, '80105', 'Aripin', 'aaripin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (541, '197301061996031001', 'Aris Fajari Teguh Nugroho', 'anugroho', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (542, '80049', 'Armein Rizal B.', 'arizal', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (543, '80133', 'Arthur P Taihutu', 'ataihutu', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (544, '80333', 'Arya Alai Permana', 'apermana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (545, '199010072019031001', 'Askar Eko Harpito', 'aharpito', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (546, '80334', 'Asri Hidayat', 'ahidayat', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (547, '199509272019032004', 'Aulia Aminda Dhianti', 'adhianti', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (548, '198610222019031002', 'Ayub Oktaviano', 'aoktaviano', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (549, '80261', 'Basuki Haryono', 'bharyono', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (550, '199011232019031003', 'Bimo Setiajie', 'bsetiajie', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (551, '198807112019031005', 'Cahya Tutuk Irana Sakti', 'csakti', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (552, '198608072019032002', 'Candrawati Puspitasari', 'cpuspitasari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (553, '80326', 'Christina Puspa Dewi', 'cdewi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (554, '80086', 'Cici Amirah ', 'camirah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (555, '198109292010122001', 'Cita Mismiana', 'cmismiana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (556, '80047', 'Citra Wulandari ', 'cwulandari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (557, '80243', 'Clara Monica', 'cmonica', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (558, '80284', 'Danny A. Sulaksana', 'dsulaksana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (559, '80171', 'Deby Darmawati ', 'ddarmawati', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (560, '80102', 'Della Isaura Karauwan', 'dkarauwan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (561, '198409282010121003', 'Dian Herdiansah', 'dherdiansah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (562, '80296', 'Dian Ratna Sari', 'dsari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (563, '80291', 'Didi Sumadi', 'dsumadi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (564, '198611192019031003', 'Dimas Susanto', 'dsusanto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (565, '198104292010122001', 'Dinar Rahmayani', 'drahmayani', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (566, '197309302001121001', 'Drama Panca Putra', 'dputra', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (567, '80278', 'Dulloh Rafsanjani', 'drafsanjani', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (568, '80232', 'Dwi Jatmiko', 'djatmiko', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (569, '80109', 'Dwi Purwo Seputro', 'dseputro', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (570, '80201', 'Edi Susanto', 'esusanto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (571, '80196', 'Edwin Partogi Pasaribu', 'epasaribu', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (572, '197203242002121002', 'Eko Sunarko', 'esunarko', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (573, '80280', 'Elwen Ebenhard T.', 'eebenhard', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (574, '198801172010122004', 'Eni Nurtika', 'enurtika', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (575, '197007131996032001', 'Enteng Mundiati', 'emundiati', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (576, '80272', 'Ergard Putra Geerards', 'egeerards', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (577, '199607062019031001', 'Ery Kurnia', 'ekurnia', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (578, '197708152005012001', 'Eviyati', 'eeviyati', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (579, '198101062015031001', 'Fakhrur Haqiqi', 'fhaqiqi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (580, '198312272019031001', 'Fakhrurrazi', 'ffakhrurrazi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (581, '80012', 'Fany Ratih Prawiasari ', 'fprawiasari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (582, '199002252019032002', 'Farah Shafira', 'fshafira', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (583, '199101142014022002', 'Fatimah Nuryani', 'fnuryani', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (584, '80246', 'Fatty Hillah', 'fhillah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (585, '80175', 'Febby Yurike Y Paluppa', 'fpaluppa', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (586, '199408182019032006', 'Firdha Ratnasari Guntoro', 'fguntoro', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (587, '198209162006041005', 'Firdiansyah', 'ffirdiansyah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (588, '198908312019031001', 'Fuadiansyah', 'ffuadiansyah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (589, '80040', 'Galih Prihanto Jati', 'gjati', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (590, '80008', 'Gunawan Artho Nugoho  ', 'gnugroho', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (591, '80220', 'Haerudin', 'hhaerudin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (592, '199402162019031003', 'Hafidz Alam Islami', 'hislami', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (593, '196404131991032001', 'Handari Restu Dewi', 'hdewi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (594, '199507142019031003', 'Harry Nugraha Pratama', 'hpratama', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (595, '80197', 'Hasto Atmojo Suroyo', 'hsuroyo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (596, '80329', 'Hendra Wijaya', 'hwijaya', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (597, '80302', 'Hendri Aditya ', 'haditya', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (598, '80188', 'Hendri Sutopo', 'hsutopo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (599, '80143', 'Heri Sanjaya', 'hsanjaya', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (600, '80120', 'Heru Pranoto', 'hpranoto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (601, '199210012019031002', 'Hevy Ukasyah Bahreisy', 'hbahreisy', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (602, '199304262019032002', 'Holy Apriliani', 'hapriliani', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (603, '196808301993031001', 'I Gusti Agung Adnyana', 'iadnyana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (604, '199504132019032003', 'I\'anah Marfu\'ah', 'imarfu\'ah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (605, '80124', 'Idup Abdul Rohman', 'irohman', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (606, '199110192019031005', 'Indria Dwi Sukarno', 'isukarno', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (607, '198701062010122001', 'Indryasari', 'iindryasari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (608, '198805112014022002', 'Inggit Nursafitri', 'inursafitri', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (609, '80125', 'Iwan', 'iiwan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (610, '198207072015032001', 'Kartini Megawati', 'kmegawati', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (611, '80265', 'Karyono', 'kkaryono', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (612, '199109102019032005', 'Kezia Henderisa Rumbiak', 'krumbiak', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (613, '196408081989032001', 'Latri Mumpuni', 'lmumpuni', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (614, '199102122014022001', 'Lia Gunawan', 'lgunawan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (615, '1.99111e17', 'Lidya Susanti Siburian', 'lsiburian', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (616, '199306282019032002', 'Lina Rosdiana', 'lrosdiana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (617, '198809092019032002', 'Linda Widya', 'lwidya', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (618, '80415', 'Livia Istania DF Iskandar', 'liskandar', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (619, '80247', 'M Hamzah ', 'mhamzah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (620, '80021', 'M Hasyim Muhsoni I', 'mhmuhsoni', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (621, '80070', 'M Tommy Permana ', 'mpermana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (622, '199402202019032004', 'Maharani Indah Puspita', 'mpuspita', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (623, '198606182015031001', 'Mahari Is Subangun', 'msubangun', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (624, '80112', 'Manan Bin Mipan', 'mmipan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (625, '80417', 'Maneger Nasution', 'mnasution', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (626, '69020416', 'Marjoko', 'mmarjoko', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (627, '199405312019032002', 'Maylisa Eka Handani Putri', 'mputri', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (628, '80183', 'Mayselina Nuriza ', 'mnuriza', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (629, '199004192019032003', 'Milati Nurul Haqi', 'mhaqi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (630, '199007092019032005', 'Mir\'atul Latifah', 'mlatifah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (631, '80221', 'Mochamad Ichsan', 'michsan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (632, '80341', 'Mufidathul Izhmy S', 'mizhmy', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (633, '198603292019031001', 'Muhamad Fahrudin', 'mfahrudin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (634, '80194', 'Muhamad Muharoji', 'mmuharoji', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (635, '199010042019031001', 'Muhammad Abdul Aziz', 'maziz', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (636, '80308', 'Muhammad Hudzaifah', 'mhudzaifah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (637, '198903162014021001', 'Muhammad Ihsan', 'mihsan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (638, '80249', 'Muhammad Mardiansyah', 'mmardiansyah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (639, '80006', 'Mulatingsih', 'mmulatingsih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (640, '199307232019032005', 'Murniasih', 'mmurniasih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (641, '199506212019032003', 'Mutia Ariesca Suwandi', 'msuwandi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (642, '80063', 'Ndaru Utomo Kusumo ', 'nkusumo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (643, '80098', 'Neneng Ratna Ningsih', 'nningsih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (644, '80069', 'Ni\'matul Hidayati', 'nhidayati', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (645, '196409051990310004', 'Noor Sidharta', 'nsidharta', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (646, '198911302014022001', 'Novita Prima Dewi', 'ndewi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (647, '199009302014022003', 'Nunut Amalia', 'namalia', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (648, '199503292019032003', 'Nurfianingsih', 'nnurfianingsih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (649, '80294', 'Nurhansyah', 'nnurhansyah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (650, '80189', 'Ovi Suhatman', 'osuhatman', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (651, '80018', 'Pascalis Risdiana FP ', 'prisdiana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (652, '80207', 'Pikri Zulpikar HQ ', 'pzulpikar', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (653, '80193', 'Prasetyo Djafar', 'pdjafar', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (654, '80127', 'Priyatin', 'ppriyatin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (655, '80107', 'Pujiati', 'ppujiati', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (656, '80361', 'Purnomo', 'ppurnomo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (657, '80241', 'Puti Mayang Sari', 'psari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (658, '198812052019031002', 'Putra Wijaya', 'pwijaya', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (659, '199306062019032003', 'Rachelia Eni Putri', 'rputri', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (660, '198605192019032002', 'Rahma Sartika', 'rsartika', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (661, '80283', 'Rahmat Hidayat', 'rhidayat', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (662, '80138', 'Ratmono', 'rratmono', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (663, '197812272005021001', 'Revando Burhan', 'rburhan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (664, '198701032015032001', 'Riani Anggraeni Soedirgo', 'rsoedirgo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (665, '80135', 'Rianto', 'rrianto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (666, '80067', 'Rianto Wicaksono ', 'rwicaksono', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (667, '80100', 'Ridwan Ahmad', 'rahmad', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (668, '80271', 'Rio Afrizal', 'rafrizal', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (669, '80134', 'Rio Gatot Sulistiawan', 'rsulistiawan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (670, '80342', 'Riska', 'rriska', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (671, '80076', 'Rizki Aji Pamungkas ', 'rpamungkas', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (672, '199503142019032004', 'Rizki Amalia', 'ramalia', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (673, '80216', 'Rizki Sammaulidah ', 'rsammaulidah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (674, '80209', 'Rizqi Aulia Rahman', 'rrahman', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (675, '80016', 'Robi Saripudin ', 'rsaripudin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (676, '198808162014022001', 'Roery Ayu Mayangsari Riyadi', 'rriyadi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (677, '198905062019031004', 'Ronaldi Maika Putra', 'rputra', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (678, '80077', 'Rully Novian', 'rnovian', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (679, '80219', 'Rusdianto', 'rrusdianto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (680, '80332', 'Saepul Ridwan', 'sridwan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (681, '80027', 'Salahudin', 'ssalahudin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (682, '80104', 'Salamah', 'ssalamah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (683, '198709202010122002', 'Sandra Anggita', 'sanggita', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (684, '80325', 'Septia Prima Hardiyanti', 'shardiyanti', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (685, '199009192019032004', 'Septiana Sundari', 'ssundari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (686, '198209242015031001', 'Setiawan', 'ssetiawan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (687, '198307202010121001', 'Singgih Wisnubroto', 'swisnubroto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (688, '80305', 'Siti Mu\'minah', 'smuminah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (689, '198609152019032002', 'Siti Nurliyah', 'snurliyah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (690, '80303', 'Siti Sukaesih ', 'ssukaesih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (691, '80339', 'Sofie Emilia Mustika ', 'smustika', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (692, '80113', 'Sopian', 'ssopian', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (693, '199108232019032004', 'Soviana Nur Afifah', 'safifah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (694, '198406152019032002', 'Sri Juniardhani Tjahjaningsih.', 'stjahjaningsih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (695, '198810192015032001', 'Sri Wiji Astuti', 'sastuti', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (696, '197001021989031001', 'Sriyana', 'ssriyana', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (697, '80054', 'Suciasih Retno Kartika ', 'skartika', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (698, '80140', 'Sudirta', 'ssudirta', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (699, '80137', 'Sukardi', 'ssukardi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (700, '80166', 'Sukri Agama', 'sagama', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (701, '80202', 'Sulisman', 'ssulisman', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (702, '80014', 'Sulung Gogor Samodro ', 'ssamodro', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (703, '80328', 'Supriyanto', 'ssupriyanto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (704, '80210', 'Susandhi Bin Sukatma ', 'ssukatma', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (705, '80330', 'Susilaningtias', 'ssusilaningtias', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (706, '199404142019032007', 'Syafatania', 'ssyafatania', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (707, '80062', 'Syahrial Martanto Wiryawan', 'swiryawan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (708, '80286', 'Syahrul Nurmawansyah', 'snurmawansyah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (709, '80128', 'Syamsul Anwar', 'sanwar', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (710, '80248', 'Tarsim Masruri', 'tmasruri', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (711, '80298', 'Tawang Amuhara ', 'tamuhara', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (712, '80287', 'Tedi Wirawan Putra', 'tputra', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (713, '198010092015032001', 'Therecia Lies Triana Sulanjari', 'tsulanjari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (714, '199011082015032001', 'Tiara Rachmawati', 'trachmawati', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (715, '80129', 'Tirta Kartika Rianto ', 'tkartika', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (716, '80119', 'Tita Jatnika', 'tjatnika', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (717, '80099', 'Titin Supriatin', 'tsupriatin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (718, '80190', 'Tony Nurhadi', 'tnurhadi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (719, '80293', 'Totom Sukanda', 'tsukanda', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (720, '199111102019032003', 'Tri Indah Lestari', 'tlestari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (721, '80009', 'Trisna Pasaribu ', 'tpasaribu', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (722, '80309', 'Ujang Sutenda', 'usutenda', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (723, '80336', 'Uus Agustian', 'uagustian', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (724, '80263', 'Vendi Purnomo ', 'vpurnomo', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (725, '199611172019032003', 'Vinda Noviasari', 'vnoviasari', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (726, '80288', 'Wahyu Supriatna', 'wsupriatna', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (727, '80195', 'Waryanto', 'wwaryanto', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (728, '80103', 'Wildan', 'wwildan', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (729, '80157', 'Wiyatiningsih ', 'wiyatinigsih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (730, '198410122019032001', 'Wulan Dewi Satriyani', 'wsatriyani', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (731, '80114', 'Yanuar Ariwahyudi', 'yariwahyudi', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (732, '80275', 'Yogi Nugraha', 'ynugraha', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (733, '198710072019031002', 'Yogi Suryamansyah', 'ysuryamansyah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (734, '199004092014022003', 'Yuli Yuliah', 'yyuliah', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (735, '80208', 'Yulisa Maharani ', 'ymaharani', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (736, '199303132019032006', 'Yusnia Arianingsih', 'yarianingsih', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (737, '80331', 'Yusrina', 'yyusrina', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (738, '80300', 'Zulkarnaen Bara ', 'zbara', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (739, '080344', 'Achmad Zelani', 'azelani', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 3, 'y');
INSERT INTO `tbl_user` VALUES (740, '0002', 'Kepegawaian', 'kepegawaian', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 4, 'y');
INSERT INTO `tbl_user` VALUES (741, '0003', 'Admin', 'admin', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', 'atomix_user31.png', 2, 'y');
COMMIT;

-- ----------------------------
-- Table structure for tbl_user_level
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_level`;
CREATE TABLE `tbl_user_level` (
  `id_user_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(30) NOT NULL,
  PRIMARY KEY (`id_user_level`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user_level
-- ----------------------------
BEGIN;
INSERT INTO `tbl_user_level` VALUES (1, 'Super Admin');
INSERT INTO `tbl_user_level` VALUES (2, 'Admin');
INSERT INTO `tbl_user_level` VALUES (3, 'Pengguna');
INSERT INTO `tbl_user_level` VALUES (4, 'Verifikator Cuti');
INSERT INTO `tbl_user_level` VALUES (5, 'Pejabat Pembina Kepegawaian');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
